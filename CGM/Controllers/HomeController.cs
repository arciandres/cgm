﻿using CGM.Helpers.Attributes;
using CGM.Models.CGMModels;
using Microsoft.AspNet.Identity;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CGM.Controllers.API;
using Newtonsoft.Json;

namespace CGM.Controllers
{
    public class HomeController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        [CustomAuthorize]
        public ActionResult Index()
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
             
                var boundaries = db.Boundary.ToList();
                ViewBag.ActiveBoundaries= boundaries.Where(b =>b.is_active == true).Count();
                ViewBag.NonActiveBoundaries = boundaries.Where(b =>b.is_active == false).Count();

                var dateCompare = DateTime.Now.AddDays(-2);

                ViewBag.AlarmsFired = db.AlarmFires.Where(a => a.firedOn.CompareTo(dateCompare) > 0).Count();
                ViewBag.Meters = db.Meter.Where(m => !m.is_filed).Count();

                ViewBag.Program = db.Meter.Where(m => !m.is_filed).Count();



                #region Fronteras reportadas al mercado
                DateTime date = DateTime.Now.Date.AddDays(-1);

                var boundariesReadyDaysBefore1= db.Report_Inform.Where(r => r.assignedReading.Value // The measurement is accepted
                                                                        && r.Report.operation_date.Value.CompareTo(date) == 0) // On the day before
                                                                        .Select(r => r.boundary_id).Distinct().ToList().Count;

                date = DateTime.Now.Date.AddDays(-2);

                var boundariesReadyDaysBefore2 = db.Report_Inform.Where(r => r.assignedReading.Value // The measurement is accepted
                                                                        && r.Report.operation_date.Value.CompareTo(date) == 0) // On the day before
                                                                        .Select(r => r.boundary_id).Distinct().ToList().Count;

                ViewBag.boundariesReadyDaysBefore1 = boundariesReadyDaysBefore1;
                ViewBag.boundariesReadyDaysBefore2 = boundariesReadyDaysBefore2;
                #endregion

                #region Proceso de adquisiciones hoy (No se usó)
                // Calcula el número de adquisiciones que están programadas para realizarse el día de hoy, sobre las que ya se realizaron.
                // Es decir, si se programó dos instrucciones, y se ha cumplido 1, está al 50%.

                // Se calcula de la siguiente manera:
                // El 100% está conformado por: todas las fronteras que tienen la hora de ejecución en el día de hoy y no se han completado, más
                //                            + las que fueron completadas en el día de hoy.

                //(new DateTime(1970, 1, 1, 1, 0, 0)).AddMilliseconds(vr.Score.Value * 1000).Date.CompareTo(DateTime.Now.Date) == 0 // Date of today

                DateTime today = DateTime.Now.Date;
                DateTime daysago = DateTime.Now.Date.AddDays(-5);
                DateTime yesterday = DateTime.Now.Date.AddDays(-1);
                //First filter: 
                // Explaination of query: 
                // The time of next execute is in the value 'Score', but it's on javascript milliseconds.
                // Usually the conversion is the following: (new DateTime(1970, 1, 1, 1, 0, 0)).AddMilliseconds(vr.Score.Value * 1000)
                // but linq does not take 'add' methods. So we have to bring some reports_informs to be able to query this properly
                // However, we cannot bring all of them, since they're too many. So we apply some filters first, and then query what we need.

                //var instructionsActiveForToday = db.VReport.Where(vr => vr.dateProgrammed.CompareTo(daysago) >= 0 // Los reportes para ejecución en el rango de hoy, 5 días atrás.
                //                                                    && !(vr.StateName == "Succeeded" || vr.StateName == "Deleted") // Some extra filters to make the query lighter
                //                                                    && vr.Score.HasValue)
                //                                                    .ToList();
                //instructionsActiveForToday = instructionsActiveForToday.Where(vr => (new DateTime(1970, 1, 1, 1, 0, 0)).AddMilliseconds(vr.Score.Value * 1000).Date.CompareTo(today) == 0 ).ToList();

                //// Already successfully executed today
                //var instructionsCompletedForToday = db.VReport.Where(vr => vr.StateName == "Succeeded" && vr.end_time.Value.CompareTo(yesterday) == 0).ToList();

                //ViewBag.instructionsForToday = instructionsCompletedForToday.Count + instructionsActiveForToday.Count;
                //ViewBag.instructionsActiveForToday = instructionsActiveForToday.Count;



                #endregion

                List<Object> dataPie = StatsController.SimplifiedSummaryPie(DateTime.Now.Date.AddDays(-1).ToShortDateString(), null, 1, true);
                ViewBag.data1 = JsonConvert.SerializeObject(dataPie);
                dataPie = StatsController.SimplifiedSummaryPie(DateTime.Now.Date.AddDays(-2).ToShortDateString(), null, 1, true);
                ViewBag.data2 = JsonConvert.SerializeObject(dataPie);

            }

            ViewBag.Title = "Home Page";
            return View();
        }
    }
}
