﻿using CGM.Helpers;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace CGM.Controllers.CGM
{
    public class ErrorHandlerController : Controller
    {
        public ActionResult Error500()
        {
            ViewBag.ErrorMessage = DataAndDB.MensajeView;
            DataAndDB.MensajeView = "";
            //ViewBag.ErrorMessage = DataAndDB.MensajeView;
            return View();
        }

        public ActionResult Error404()
        {
            ViewBag.ErrorMessage = DataAndDB.MensajeView;
            DataAndDB.MensajeView = "";
            //ViewBag.ErrorMessage = DataAndDB.MensajeView;
            return View();
        }

        public static void LoggerDB(Exception ex)
        {
            ExceptionLog exLog = getExLog(ex);
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                try
                {
                    db.ExceptionLog.Add(exLog);
                    db.SaveChanges();
                }
                catch (Exception ex2)
                {
                    LoggerFile(ex2, true);
                }
            }
        }

        public static void LoggerFile(Exception ex, bool mail)
        {
            ExceptionLog exLog = getExLog(ex);
            string enter = Environment.NewLine + Environment.NewLine ;
            string fileContent =
                "Fecha y hora: " + exLog.logDate + enter +
                "URL: " + exLog.url + enter +
                "Mensaje de error: " + exLog.msg + enter +
                "Tipo de Excepción: " + exLog.type + enter +
                "Origen: " + exLog.source + enter +
                "Inner Message: " + exLog.innerExcMsg+ enter;

            string fileAddress = HostingEnvironment.ApplicationPhysicalPath+"/Files/Errors/" + exLog.logDate.Value.ToString().Replace('/', '-').Replace(':','-')+ ","+ exLog.logDate.Value.Second + ".txt";
            FileManagement.WriteTXT(fileContent, fileAddress);

            if (mail)
            {
                CGM_DB_Entities db = new CGM_DB_Entities();
                List<Config_CM> conf = db.Config_CM.ToList();
                if (db.Mail_Item.Any(m => m.active == true))
                {
                    MailManagement.EnviarCorreoAhora(
                         conf[4].text, //Dirección de Correo CMOR
                         StringCipher.Decrypt(conf[5].text, "nardideluca"), //Password de correo CMOR
                         "Error de página web",
                         fileContent + Environment.NewLine,
                         new List<string>() { conf[9].text }, //Correo de desarrollador
                         new List<string> { fileAddress },
                         false  //Se especifica que es un correo en formato HTML 
                         );
                }
            }
        }

        private static ExceptionLog getExLog(Exception ex) //Extrae y da formato a los datos de la excepción
        {
            ExceptionLog exLog = new ExceptionLog()
            {
                id = Guid.NewGuid(),
                logDate = DateTime.Now,
                msg = ex.Message,
                type = ex.GetType().Name,
                source = ex.StackTrace,
                
            };
            try
            {
                exLog.url = System.Web.HttpContext.Current.Request.Url.ToString();
            }
            catch (Exception ex1)
            {
            }

            var inner = ex.InnerException;
            exLog.innerExcMsg = "";
            while (inner != null)
            {
                exLog.innerExcMsg += inner.Message!= null ? "--   InnerMsg :" + inner.Message : "";
                inner = inner.InnerException;
            }
            return exLog;
        }

        public static void GeneralSaveError(Exception ex)
        {
            LoggerFile(ex, true); //1 = Enviar por correo a desarrollador
            LoggerDB(ex);
        }

    }
}
