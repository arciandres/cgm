﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Helpers;
using System.IO;
using System.Globalization;
using NLog;
using Newtonsoft.Json;
using CGM.Models.MyModels;
using Newtonsoft.Json.Linq;

namespace CGM.Controllers.CGM
{
    public class CurvaTipsController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private CGM_DB_Entities db = new CGM_DB_Entities();
        NumberFormatInfo provider = new NumberFormatInfo { NumberDecimalSeparator = "." };
        private static List<VMeasure> vMeasures;

        // GET: CurvaTips
        // Se hizo una modificación para mostrar las medidas de alumbrado público en este espacio
        public async Task<ActionResult> Index()
        {
            var curvaTip = db.CurvaTip.Include(c => c.Acquisition_Item)
                                      .Include(c => c.Acquisition_Item.Quantity)
                                      .Include(c => c.Acquisition_Item.Tag_Line).Where(c => c.scope != "alumbrado");
            
            ViewBag.ControllerName = "CurvaTips";

            return View(await curvaTip.ToListAsync());
        }

        public async Task<ActionResult> IndexAP()
        {
            var curvaTip = db.CurvaTip.Include(c => c.Acquisition_Item)
                                      .Include(c => c.Acquisition_Item.Quantity)
                                      .Include(c => c.Acquisition_Item.Tag_Line).Where(c => c.scope == "alumbrado");
            
            return View(await curvaTip.ToListAsync());
        }

        // GET: CurvaTips/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName = "CurvaTips";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurvaTip curvaTip = await db.CurvaTip.Include(c=> c.Groups)
                                                 .Include(c=> c.Groups.Select(g => g.Boundary))
                                                 .Where(c => c.id == id).FirstOrDefaultAsync();


            ViewBag.max = curvaTip.Groups.Select(g => g.Measures.Select(m => m.measure)).SelectMany(x => x).ToList().Max();
            ViewBag.min = curvaTip.Groups.Select(g => g.Measures.Select(m => m.measure)).SelectMany(x => x).ToList().Min();


            if (curvaTip == null)
            {
                return HttpNotFound();
            }
            return View(curvaTip);
        }

        // GET: CurvaTips/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName = "CurvaTips";

            var acq_items = db.Acquisition_Item.Select(a => new { a.Id, name = a.Quantity.name +" - "+a.Tag_Line.name, a.Quantity.name_short, name_tag = a.Tag_Line.name }).ToList();
            ViewBag.acq_items = JsonConvert.SerializeObject(acq_items.ToDictionary(x => x.Id, x => x.name_short + " - " + x.name_tag));
            ViewBag.acquisition_item_id = new SelectList(acq_items, "Id", "name");

            var tags = db.DataSourceItem.Where(d => d.DataSource.Name == "Etiquetas de curva típica")
                         .Select(a => new { a.Id, a.Value }).ToList();

            ViewBag.tags = new SelectList(tags, "Id", "Value");

            return View();
        }

        // POST: CurvaTips/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(int acquisition_item_id, string monthyear, string name, int?[] weekdays, string daysspec, int? tags)
        {

            // [Bind(Include = "id,name,month,year,acquisition_item_id,scope,dateCreated")]

            // This variable is stored as a string
            var scope = JsonConvert.SerializeObject(new { weekdays, tags });

            var my = monthyear.Split('/'); // [month, year]

            List<CurvaTipMeasureGroup> curvaTipMeasureGroups = vMeasuresToCurvaTipsGroups(vMeasures);

            CurvaTip curvaTip = new CurvaTip()
            {
                fecha1 = new DateTime(int.Parse(my[1]), int.Parse(my[0]), 1) ,
                acquisition_item_id = acquisition_item_id,
                dateCreated = DateTime.Now,
                name = name,
                scope = scope,
                Groups = curvaTipMeasureGroups
            };

            ViewBag.ControllerName = "CurvaTips";
            if (ModelState.IsValid)
            {
                db.CurvaTip.Add(curvaTip);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var acq_items = db.Acquisition_Item.Select(a => new { a.Id, a.Quantity.name, a.Quantity.name_short }).ToList();
            ViewBag.acquisition_item_id = new SelectList(acq_items, "Id", "name");
            return View(curvaTip);
        }

        public ActionResult LoadFile(bool is_alumbrado)
        {
            ViewBag.Result = TempData["result"];
            ViewBag.navbar1 = "Reportes";
            ViewBag.navbar2 = "Carga de medidas";
            //Se inicializa el objeto de reporte para tener a mano datos requeridos
            var boundaryQuery = db.Boundary;

            // Si es alumbrado, sólo se consideran esas fronteras, sino todas.
            List<Boundary> BoundaryList = is_alumbrado ? boundaryQuery.Where(b => b.Boundary_Source.source.Trim().Equals("Alumbrado Público")).ToList()
                                                       : boundaryQuery.ToList();

            if (BoundaryList.Count == 0)
            {
                ViewBag.Result = new KeyValuePair<string, string>("warning", "No se encontró ninguna fronter de alumbrado público en el sistema. Las puede agregar en la sección de Mercado/Fronteras.");
                //return Content("Este es un mensaje de error!");
                return PartialView("_Tabla_vMedidas");
            }

            vMeasures = new List<VMeasure>();

            string[] values = new string[] { };
            int ind = 0; // iterator variable defined outside. In case of error, I can get the line number

            DateTime fecha = DateTime.Now.Date;
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //string rta = FileManagement.LoadFileValidation(file, "CR");
                    string rta = "OK";
                    if (rta == "OK")
                    {
                        string path = Path.Combine(Server.MapPath("~/Files/Reports/CurvaTipica"),
                                      Path.GetFileName(file.FileName));
                        file.SaveAs(path);
                        rta = FileManagement.InsideFileValidatation(path);

                        if (rta != "OK")
                        {
                            ViewBag.Result = new KeyValuePair<string, string>("warning", rta);
                            //return Content("Este es un mensaje de error!");
                            return PartialView("_Tabla_vMedidas");
                        }

                        List<string> lines = FileManagement.readFileLines(path).ToList();
                        for (ind = 0; ind < lines.Count; ind++)
                        {
                            values = lines[ind].Split('\t');
                            if (values[1] == "R") continue; // Sólo se aceptan medidas principales (P)

                            Boundary boundary = BoundaryList.Where(b => b.code.ToLower() == values[0].Trim().ToLower()).First();
                            if (boundary == null)
                            {
                                rta = "Carga interrumpida. La frontera con código " + values[0] + " no esta incluida en el listado de fronteras.";
                                if (is_alumbrado) rta += Environment.NewLine + "(Fronteras de Alumbrado público).";

                                ViewBag.Result = new KeyValuePair<string, string>("warning", rta);
                                return PartialView("_Tabla_vMedidas");
                            }

                            bool is_backup = values[1] == "R" ? true : false;

                            for (int j = 0; j < 24; j++)
                            {
                                vMeasures.Add(new VMeasure()
                                {
                                    measure = Convert.ToDouble(values[j + 3].Replace(',', '.'), provider), // new NumberFormatInfo { NumberDecimalSeparator = "," }  
                                    hour = fecha.AddHours(j + 1),
                                    code = boundary.code,
                                    name = boundary.name,
                                });
                            }
                        }

                        if (rta != "OK")
                        {
                            //ViewBag.Result = TempData["result"];
                            ViewBag.Result = new KeyValuePair<string, string>("warning", rta);

                            //ViewBag.Error = rta;
                            return PartialView("_Tabla_vMedidas");
                        }

                        ViewBag.min = vMeasures.Min(m => m.measure);
                        ViewBag.max = vMeasures.Max(m => m.measure);

                        return PartialView("_Tabla_vMedidas", vMeasures);

                    }
                }
            }
            catch (InvalidOperationException invEx)
            {
                string msg = "No se encontró un elemento en la selección en base de datos. Probablemente la frontera no se haya creado, o tiene un código diferente. " + Environment.NewLine
                    + "Elemento en línea: " + ind + 1 + Environment.NewLine
                    + "Primer elemento: " + values[ind];
                ;
                logger.Error(invEx, msg);
                ViewBag.Result = new KeyValuePair<string, string>("warning", msg);

                //ViewBag.Error = msg;
                return PartialView("_Tabla_vMedidas");
            }
            catch (NullReferenceException ex)
            {
                string msg = "No se encontró un elemento en la selección en base de datos. Probablemente la frontera no se haya creado, o tiene un código diferente. " + Environment.NewLine
                + "Elemento en línea: " + ind + 1 + Environment.NewLine
                + "Primer elemento: " + values[0];
                ;
                logger.Error(ex, msg);
                ViewBag.Result = new KeyValuePair<string, string>("warning", msg);

                return PartialView("_Tabla_vMedidas");
            }

            catch (Exception ex)
            {
                ViewBag.Result = new KeyValuePair<string, string>("warning", "Se presentó la siguiente excepción en la carga del archivo: " + ex.Message);

                logger.Error(ex, ViewBag.Result);

                return PartialView("_Tabla_vMedidas");
            }
            return View();
        }

        // GET: CurvaTips/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.ControllerName = "CurvaTips";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurvaTip curvaTip = await db.CurvaTip.FindAsync(id);
            if (curvaTip == null)
            {
                return HttpNotFound();
            }

            ViewBag.acquisition_item_id = new SelectList(db.Acquisition_Item, "Id", "Id", curvaTip.acquisition_item_id);
            return View(curvaTip);
        }

        // POST: CurvaTips/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,name,month,year,acquisition_item_id,scope,dateCreated")] CurvaTip curvaTip)
        {
            ViewBag.ControllerName = "CurvaTips";
            if (ModelState.IsValid)
            {

                db.Entry(curvaTip).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.acquisition_item_id = new SelectList(db.Acquisition_Item, "Id", "Id", curvaTip.acquisition_item_id);
            return View(curvaTip);
        }

        // GET: CurvaTips/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurvaTip curvaTip = await db.CurvaTip.FindAsync(id);
            if (curvaTip == null)
            {
                return HttpNotFound();
            }
            return View(curvaTip);
        }

        // POST: CurvaTips/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CurvaTip curvaTip = await db.CurvaTip.FindAsync(id);
            db.CurvaTip.Remove(curvaTip);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Parses the vMeasure structure into the CurvaTipGroups structure.
        /// </summary>
        /// <param name="vMeasures"></param>
        /// <returns></returns>
        public List<CurvaTipMeasureGroup> vMeasuresToCurvaTipsGroups(List<VMeasure> vMeasures)
        {
            List<CurvaTipMeasureGroup> groups = new List<CurvaTipMeasureGroup>();
            List<Boundary> boundaries = db.Boundary.ToList();

            foreach (var vmgroup in vMeasures.GroupBy(vm => vm.code))
            {
                CurvaTipMeasureGroup group = new CurvaTipMeasureGroup()
                {
                    boundary_id = boundaries.Where(b => b.code == vmgroup.Key).Select(b => b.id).FirstOrDefault(),
                    Measures = new List<CurvaTipMeasure>()
                };

                foreach (var measure in vmgroup)
                {
                    group.Measures.Add(new CurvaTipMeasure()
                    {
                        measure = measure.measure,
                        datetime = measure.hour
                    });
                }
                groups.Add(group);
            }

            return groups;
        }


        /// <summary>
        /// Sets a determined Curva Tipica into the default one, so it can be used in further comparisons.
        /// There can be a default CurvaTip per Acquisition Item, which is any combination of Quantity and TagLine.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangeDefaultCurvaTip(int id)
        {
            string message;
            string result;
            try
            {
                var curvaTipNewDefault = db.CurvaTip.Find(id);

                // Get all the CurvaTips set as default with the corresponding Acq_item, and sets them to false
                var curvaTipsCurrentDefault = db.CurvaTip.Where(a => a.isDefault && a.acquisition_item_id == curvaTipNewDefault.acquisition_item_id).ToList(); // Se supone debería solamente una curva como máximo
                curvaTipsCurrentDefault.ForEach(c => c.isDefault = false);

                // Assign the new default CurvaTip
                curvaTipNewDefault.isDefault = true;
                db.SaveChanges();
                result = "true";
                message = "Instrucción realizada con éxito";
            }
            catch (Exception ex)
            {
                result = "error";
                message = "Error mientras se realizaba la instrucción. Mensaje: " + ex.Message;
            }

            return Json(new { result, message });
        }

        public ActionResult LoadFileAlumbrado()
        {
            ViewBag.Result = TempData["result"];

            ViewBag.ControllerName = "CurvaTips";

            var acq_items = db.Acquisition_Item.Select(a => new { a.Id, name = a.Quantity.name + " - " + a.Tag_Line.name, a.Quantity.name_short, name_tag = a.Tag_Line.name }).ToList();
            ViewBag.acq_items = JsonConvert.SerializeObject(acq_items.ToDictionary(x => x.Id, x => x.name_short + " - " + x.name_tag));
            ViewBag.acquisition_item_id = new SelectList(acq_items, "Id", "name");

            var tags = db.DataSourceItem.Where(d => d.DataSource.Name == "Etiquetas de curva típica")
                         .Select(a => new { a.Id, a.Value }).ToList();

            ViewBag.tags = new SelectList(tags, "Id", "Value");

            return View();
        }

        /// <summary>
        /// Uploads the measurements of Alumbrado público, and inserts them into all days of the month
        /// </summary>
        /// <param name="acquisition_item_id"></param>
        /// <param name="monthyear"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadAlumbrado(int acquisition_item_id, string fechaIni, string fechaFin)
        {
            DateTime fechaI = (fechaIni != "") ? DateTime.Parse(fechaIni) : new DateTime();
            DateTime fechaF = (fechaFin != "") ? DateTime.Parse(fechaFin) : new DateTime();

            List<CurvaTipMeasureGroup> curvaTipMeasureGroups = vMeasuresToCurvaTipsGroups(vMeasures);

            CurvaTip curvaTip = new CurvaTip()
            {
                fecha1 = fechaI,
                fecha2 = fechaF,
                acquisition_item_id = acquisition_item_id,
                dateCreated = DateTime.Now,
                Groups = curvaTipMeasureGroups,
                scope = "alumbrado"
            };

            ViewBag.ControllerName = "CurvaTips";
            if (ModelState.IsValid)
            {
                db.CurvaTip.Add(curvaTip);
                await db.SaveChangesAsync();

                List<Report_Inform> ris = new List<Report_Inform>();

                var totalDays = fechaI.Subtract(fechaF).Days;

                int days = fechaI.Subtract(fechaF).Days;
                DateTime datebase = fechaI;
                // Insert the measurements of the CurvaTip everyday day of the month.
                for (int i = 0; i < days; i++)
                {
                    setUnassignedPreviousMeasures(datebase.AddDays(i), curvaTip);
                    insertAlumbradoReport(datebase.AddDays(i), curvaTip);
                }
                TempData["result"] = new KeyValuePair<string, string>("true", "Los datos se han cargado exitosamente. Se realizó copia de las medidas en cada día del mes, que puede verificar en la sección de Medidas/Registro, o Reporte.");
                return RedirectToAction("IndexAP");
            }

            var acq_items = db.Acquisition_Item.Select(a => new { a.Id, a.Quantity.name, a.Quantity.name_short }).ToList();
            ViewBag.acquisition_item_id = new SelectList(acq_items, "Id", "name");
            return View(curvaTip);
        }
        #region Insert Measurements into Report_informs

        /// <summary>
        /// Inserts a report for all the boundaries contained on the curvaTip
        /// Each CurvaTip has only one kind of quantity, therefore, the groups are different than in Reports.
        /// Each group corresponds to a set of boundaries, and it has to be adapted accordingly.
        /// </summary>
        /// <param name="day"></param>
        /// <param name="curvaTip"></param>
        /// <returns></returns>
        public Report insertAlumbradoReport(DateTime datebase, CurvaTip curvaTip)
        {
            List<Measure> measures = new List<Measure>();
            List<Measure_Group> measure_groups = new List<Measure_Group>();
            List<Report_Inform> ris = new List<Report_Inform>();

            int count = 0;
            int boundary_id;
            try
            {
                foreach (var group in curvaTip.Groups) // In the context of CurvaTips, this is also the boundary.
                {
                    count = 0;
                    measures = new List<Measure>();
                    if (group.Measures.Count != 24)
                    {
                        throw new CGMResultException("La curva incluida no contiene 24 medidas. Este es un requerimiento.");
                    }
                    foreach (var m in group.Measures.OrderBy(m => m.datetime)) // It is asummed that there is 24 measurements
                    {
                        count++; // La primera hora será 1, y la última 24, que corresponde al día siguiente.
                        measures.Add(new Measure(){ measure1 = m.measure, measure_date = datebase});
                    }

                    ris.Add(new Report_Inform()
                    {
                        boundary_id = group.boundary_id,
                        is_backup = false,
                        meter_id = null,
                        assignedReading = true,
                        end_time = DateTime.Now,
                        substatus_id = 500,
                        time_elapsed = new TimeSpan(0),

                        // The list of groups will only have one element, because there is only one quantity
                        Measure_Groups = new List<Measure_Group>() { new Measure_Group() { acquisition_item_id = curvaTip.acquisition_item_id, Measures = measures } }
                    });
                }
                Report report = new Report()
                {
                    market_id = 1, 
                    is_active = false,
                    is_auto = false,
                    destination_id = 1,
                    operation_date = datebase,
                    acquisition_id = null, // No se requierte, pues la adquisición se hace directamente
                    status_id = 5,
                    program_id = null, // No se requiere, pues la información se carga sin necesidad de los parámetros del programa
                    Report_Informs = ris
                };
                DataAndDB.insertReports(report);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            // No se debería llegar hasta aquí.
            return null;
        }

        /// <summary>
        /// Set the previous measures to not assigned in order to assing the new ones.
        /// </summary>
        /// <param name="day"></param>
        /// <param name="curvaTip"></param>
        public void setUnassignedPreviousMeasures(DateTime datebase, CurvaTip curvaTip)
        {
            try
            {
                int[] curvaBoundaries = curvaTip.Groups.Select(c => c.boundary_id).ToArray();
                List<Report_Inform> ris_assigned = db.Report_Inform.Where(ri => curvaBoundaries.Contains(ri.boundary_id.Value) // Que sean las mismas fronteras
                                                                         && ri.Report.operation_date.Value.CompareTo(datebase) == 0).ToList(); // Que el día de operación sea el mismo

                if (ris_assigned.Count > 0)
                {
                    foreach (var ri in ris_assigned)
                    {
                        ri.assignedReading = false;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

