﻿using CGM.Areas.Telemetry.Controllers;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CGM.Controllers.CGM
{
    public class ReportBoundaryController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: ReportBoundary
        public ActionResult Index()
        {
            List<Boundary> datos = new List<Boundary>();
            using (var bd = new CGM_DB_Entities())
            {
                datos = bd.Boundary.ToList();
            }
            TempData["boundary"] = datos;

            return View();
        }  
        
        public ActionResult ViewByDate(int tipoReporte, string opcFecha, string fechaIni, string fechaFin, string annoIni, string annoFin, string fronteras,int report_type )
        {
            var fechaI = (fechaIni != "") ? DateTime.Parse(fechaIni) : new DateTime();
            var fechaF = (fechaFin != "") ? DateTime.Parse(fechaFin) : new DateTime();
            var annoI = int.Parse(annoIni);
            var annoF = int.Parse(annoFin);

            if(opcFecha == "rm")
            {
                fechaF = fechaF.AddMonths(1).AddDays(-1);
            }
            else if (opcFecha == "ra")
            {
                fechaI = DateTime.Parse("1/1/" + annoI);
                fechaF = DateTime.Parse("31/12/" + annoF);
            }

            List<VMeasure> vMeasureList;
            using (CGM_DB_Entities db = new CGM_DB_Entities()) 
            {
                int[] boundaries = fronteras.Split('/').Select(int.Parse).ToArray(); //Lista de ids de fronteras, normalmente se incluye la totalidad
                vMeasureList = db.VMeasure.AsNoTracking().Where(v => v.assignedReading.Value
                                                               && boundaries.Contains(v.id) //Que estén en la lista
                                                               && v.report_type_id == report_type //Que corresponda al tipo de reporte
                                                               && v.is_active //Que estén activas
                                                               && (v.operation_date >= fechaI && v.operation_date <= fechaF) //Que corresponda a la fecha de operación
                                                               ).ToList();
            }

            //&& v.unit == "kWh" //Que sea energía activa. En otros casos también incluir reactiva ("kVARh")

            var respuesta = new PartialViewResult();

            switch (tipoReporte)
            {
                case 1:  
                    respuesta = PartialView("_ReportHIM", vMeasureList); 
                    break;
                case 2:
                    respuesta = PartialView("_ReportDCGM", vMeasureList);
                    break;
                case 3:
                    respuesta = PartialView("_ReportCM", vMeasureList);
                    break;
                case 4:
                    respuesta = PartialView("_ReportDCER", vMeasureList);
                    break;
                case 5:
                    respuesta = PartialView("_ReportCIH", vMeasureList);
                    break;
                case 6:
                    respuesta = PartialView("_ReportCIV", vMeasureList);
                    break;
            }

            return respuesta;

        }

    } 
} 