﻿using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using CGM.Helpers;
using Newtonsoft.Json.Linq;

namespace CGM.Controllers.CGM
{
    public class MailController : Controller
    {

        private CGM_DB_Entities db = new CGM_DB_Entities();


        // GET: Mail
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public string DetailsToMailMultiple(int[] ids)
        {
            List<AlarmFire> alarmFires = db.AlarmFires.Where(a => ids.Contains(a.id)).ToList();
            if (alarmFires != null)
            {
                foreach (var alarmf in alarmFires)
                {
                    alarmf.CriticSummary = JsonConvert.DeserializeObject<CriticSummary>(alarmf.details);
                }

                string pageContent = RenderPartialToString("_DetailsMail", alarmFires);

                List<Config_CM> conf = db.Config_CM.ToList();

                // Why did I do that!!! It's awful!
                List<string> mails_receivers = new List<string>();
                foreach (var item in alarmFires)
                    foreach (var user in item.Alarm.Users)
                        mails_receivers.Add(user.Email);

                ;

                basicMailTemplate(pageContent, "[Alarma] Valor en adquisición excedido", mails_receivers.Distinct().ToList());

                return pageContent;
            }
            return null;
        }

        /// <summary>
        /// Used to send email notifications when the 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="severity"></param>
        /// <returns></returns>
        public string GeneralAlarmReportInform(int id, int? severity)
        {
            AlarmFire alarmFire = db.AlarmFires.Include(a => a.Alarm).Include(a => a.Report_Inform).Where(a => a.id == id).FirstOrDefault();

            // Este es un diccionario de los parámetros de para enviar el correo, y se debe validar.
            
            if (alarmFire != null)
            {
                string pageContent = RenderPartialToString("_GeneralAlarmReportInform", alarmFire);
                basicMailTemplate(pageContent, "[Alarma] " + alarmFire.Alarm.name, alarmFire.Alarm.Users.Select(u =>  u.Email).ToList());

            }
            return null;
        }

        public string GeneralAlarm(int id)
        {
            AlarmFire alarmFire = db.AlarmFires.Include(a => a.Alarm).Include(a => a.Report_Inform).Where(a => a.id == id).FirstOrDefault();

            Dictionary<string, string> dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(alarmFire.details);

            ViewBag.dict = dict;
            // Este es un diccionario de los parámetros de para enviar el correo, y se debe validar.
            string pageContent = "";
            if (alarmFire != null)
            {
                pageContent = RenderPartialToString("_GeneralAlarm", alarmFire);
                basicMailTemplate(pageContent, "[Notificación] " + alarmFire.Alarm.name, alarmFire.Alarm.Users.Select(u => u.Email).ToList());

            }
            return pageContent;
        }

        // ========================================================
        // ------------------------ UTILS -------------------------
        // ========================================================


        public string RenderPartialToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        //public List<string> getEmailsInParameters(Dictionary<string,string> mailParams)
        //{
        //    List<string> emails = new List<string>();

        //    if (mailParams.ContainsKey("email_users"))

        //    return emails;
        //}

        public void basicMailTemplate(string pageContent, string subject, List<string> mailreceivers)
        {
            List<Config_CM> conf = db.Config_CM.ToList();
            Helpers.MailManagement.EnviarCorreoAhora
                                       (
                                         conf.Where(c => c.element.Contains("CorreoCGM")).FirstOrDefault().text,
                                         Helpers.StringCipher.Decrypt(conf.Where(c => c.element == "PasswordCorreoCGM").FirstOrDefault().text, "nardidelucabothworkatsapienzauniversitadiroma"),
                                         subject,
                                         pageContent,
                                         mailreceivers,
                                         null,
                                         true
                                       );
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}