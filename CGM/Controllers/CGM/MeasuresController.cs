﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data;
using CGM.Models.CGMModels;
using CGM.Areas.Telemetry.Controllers;
using CGM.Models.MyModels;
using System.Web;
using NLog;
using System.IO;
using CGM.Helpers;
using CGM.Areas.Telemetry.Models;
using CGM.Helpers.Attributes;

namespace CGM.Controllers.CGM
{
    public class MeasuresController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static PDAMeter meter_data = new PDAMeter();
        private static List<Quantity> quantitiesDB = new List<Quantity>();
        private static List<Boundary> boundariesDB = new List<Boundary>();
        private static List<Tag_Line> tagLineDB = new List<Tag_Line>();
        private static AnalysisData analysisData = new AnalysisData();
        private static List<string> noData = new List<string>();
        private static List<Boundary> noDataBoundaries = new List<Boundary>();
        private static List<Quantity> noDataQuantities = new List<Quantity>();
        private static bool multi_boundary = false;

        public ActionResult Index()
        {
            boundariesDB = db.Boundary.ToList(); 
            quantitiesDB = db.Quantity.Include(q => q.Magnitude).Where(q => q.name == "Energía Reactiva Positiva" || q.name == "Energía Activa (Positiva)").ToList();
            tagLineDB = db.Tag_Line.Take(5).ToList();

            ViewBag.quantity_id = new SelectList(quantitiesDB , "id", "name", 1); // 1 = Energía activa
            ViewBag.tag_line_id = new SelectList(tagLineDB , "id", "name", 4); // 4 = Total
            TempData["boundary"] = boundariesDB;
            return View();
        }             

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcFecha"></param>
        /// <param name="fechaIni"></param>
        /// <param name="fechaFin"></param>
        /// <param name="annoIni"></param>
        /// <param name="annoFin"></param>
        /// <param name="boundariesSelected"> List of boundaries IDs, separated by comma</param>
        /// <param name="quantity_id"></param>
        /// <param name="tag_line_id"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetByDate(string opcFecha, string fechaIni, string fechaFin, string annoIni, string annoFin, string boundariesSelected, string quantity_id, int tag_line_id)
        {
            Tuple<string, string> userMessage = new Tuple<string, string>("",""); 
            var fechaI = (fechaIni != "") ? DateTime.Parse(fechaIni) : new DateTime();
            var fechaF = (fechaFin != "") ? DateTime.Parse(fechaFin) : new DateTime();
            var annoI = int.Parse(annoIni);
            var annoF = int.Parse(annoFin);
            if (opcFecha == "rm")
            {
                fechaF = fechaF.AddMonths(1).AddDays(-1);
            }
            else if (opcFecha == "ra")
            {
                fechaI = DateTime.Parse("1/1/" + annoI);
                fechaF = DateTime.Parse("31/12/" + annoF);
            }

            CGM_DB_Entities db = new CGM_DB_Entities();
            List<VMeasure> vMeasureList;
            List<SimpleBoundary> simpleBoundaries = new List<SimpleBoundary>();


            int[] boundariesIds = Array.ConvertAll(boundariesSelected.Split(','), int.Parse); //Lista de ids de fronteras, normalmente se incluye la totalidad
            int[] quantity_ids = Array.ConvertAll(quantity_id.Split(','), int.Parse);

            multi_boundary = false;

            if (boundariesIds.Count() > 1)
                multi_boundary = true;

            var vMeasureListQuery = db.VMeasure.AsNoTracking().Where(v => v.assignedReading.Value
                                                            && boundariesIds.Contains(v.id) //Que estén en la lista
                                                            && v.report_type_id == 1 //Que corresponda al tipo de reporte
                                                            && v.is_active //Que estén activas
                                                            && (v.operation_date >= fechaI && v.operation_date <= fechaF) //Que corresponda a la fecha de operación
                                                            && quantity_ids.Contains(v.quantity_id)
                                                            && v.tag_line_id == tag_line_id
                                                            );
            noData = new List<string>();
            int cont = 0;

            // If there is only one Boundary ---> Single Boundary mode
            if (!multi_boundary)
            {
                noDataQuantities = new List<Quantity>();
                // There is only one boundary, so the only distinction is the backup measure.
                // Selects only the main measures (P)
                // The comparison between Principal and Respaldo can be done in the multiple boundary category
                vMeasureList = vMeasureListQuery.Where(m => m.is_backup.Value == false)
                                                .OrderBy(m => m.operation_date).ToList();

                Boundary boundaryDB = db.Boundary.Find(boundariesIds.First());

                List<SimpleQuantity> quantities = new List<SimpleQuantity>();
                foreach (var q in quantity_ids)
                {
                    Quantity quantityDB = quantitiesDB.Find(qu => qu.id==q);

                    var measuresPerQuantity = vMeasureList.Where(v => v.quantity_id == q).ToList();

                    // Se verifica que la consulta haya traído resultados
                    if (measuresPerQuantity.Count == 0)
                    {
                        noDataQuantities.Add(quantityDB);
                        noData.Add(quantityDB.name);
                        continue;
                    }
                    
                    SimpleQuantity quantity = new SimpleQuantity()
                    {
                        name = quantityDB.name,
                        shortname = quantityDB.name_short,
                        unit = quantityDB.Magnitude.unit_symbol,
                        measures = SimpleMeasure.vMeasuresToSMeasures(measuresPerQuantity)
                    };

                    // Form groups by day. Group by day, and accumulate the measures
                    quantity.measures2 = quantity.measures.GroupBy(m => m.time.Date)
                                                          .Select(m => new SimpleMeasure() { time = m.Key, measure = m.Sum(me => me.measure)})
                                                          .ToList();
                    
                    quantities.Add(quantity);
                    cont++;

                }

                if (quantities.Count == 0)
                {
                    userMessage = new Tuple<string, string>("error", "No se encontró registros de la consulta realizada.");
                    return Json(new { result = userMessage.Item1, message = userMessage.Item2 }, JsonRequestBehavior.AllowGet);
                }

                SimpleBoundary boundary= new SimpleBoundary()
                {
                    name = boundaryDB.name,
                    code = boundaryDB.code,
                    Quantities = quantities
                };
                
                userMessage = GetUserMessage(noData);
                simpleBoundaries.Add(boundary);

            }
            else // Multiple boundaries are included in this section.
            {
                // The charts are similar to the ones used in the single boundary, 
                // but no multiple quantities are used in this case. Just comparison between boundaries

                noDataBoundaries = new List<Boundary>();
                vMeasureList = vMeasureListQuery.Where(m => m.is_backup.Value == false)
                                                .OrderBy(m => m.operation_date).ToList();

                List<Boundary> boundariesDBSelect = boundariesDB.Where(b => boundariesIds.Contains(b.id)).ToList();
                Quantity quantityDB = quantitiesDB.Where(q => q.id == quantity_ids.First()).FirstOrDefault();

                List<SimpleBoundary> boundaries = new List<SimpleBoundary>();

                foreach (var boundaryDB in boundariesDBSelect)
                {

                    var measuresPerBoundary = vMeasureList.Where(v => v.id == boundaryDB.id).ToList();

                    // Se verifica que la consulta haya traído resultados
                    if (measuresPerBoundary.Count == 0)
                    {
                        noDataBoundaries.Add(boundaryDB);
                        noData.Add(boundaryDB.code + " - "+ boundaryDB.name);
                        continue;
                    }

                    SimpleBoundary boundary = new SimpleBoundary()
                    {
                        name = boundaryDB.name,
                        code = boundaryDB.code,
                        measures = SimpleMeasure.vMeasuresToSMeasures(measuresPerBoundary)
                    };

                    // Form groups by day. Group by day, and accumulate the measures
                    boundary.measures2 = boundary.measures.GroupBy(m => m.time.Date)
                                                          .Select(m => new SimpleMeasure() { time = m.Key, measure = m.Sum(me => me.measure) })
                                                          .ToList();

                    simpleBoundaries.Add(boundary);
                    cont++;

                }

                if (simpleBoundaries.Count == 0)
                {
                    userMessage = new Tuple<string, string>("error", "No se encontró registros de la consulta realizada.");
                    return Json(new { result = userMessage.Item1, message = userMessage.Item2 }, JsonRequestBehavior.AllowGet);
                }

                userMessage = GetUserMessage(noData);
            }

            analysisData = new AnalysisData()
            {
                boundaries = simpleBoundaries,
                multi_boundary = multi_boundary,
                //grouping_level = g
            };

            var jsonResult = Json(new { result = userMessage.Item1, data = simpleBoundaries, message = userMessage.Item2 }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        private Tuple<string,string> GetUserMessage(List<String> noData) {
            // Si no hay elementos, significa que todas las consultas devolvieron resultados
            if (noData.Count == 0) {
                return new Tuple<string,string>("true", "Ningún");
            }
            else  // Sí hubo elementos de la consulta con errores.
            {
                string message = "No se encontró registros en los siguientes elementos: ";
                for (int i = 0; i < noData.Count; i++)
                {
                    message += "("+ (i+1) +") "+ noData[i] + Environment.NewLine;
                }
                return new Tuple<string, string>("warning", message);
            }
        }

        [HttpGet]
        public ActionResult GetNoDataElements()
        {
            ViewBag.multi_boundary = multi_boundary;
            ViewBag.noDataBoundaries = noDataBoundaries;
            ViewBag.noDataQuantities = noDataQuantities;
            return PartialView("_noData", noData);
        }

        [HttpGet]
        public ActionResult GetMeasureStats()
        {

            return null;
        }

        /// <summary>
        /// Takes the input file generated by the PDA assistant, decrypt it and loads the information to the database
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadFilePDA()
        {
            ViewBag.Result = TempData["result"];
            return View();
        }

        //[HttpPost]
        [CustomAuthorize(Roles = "Desarrollador,Administrator")]
        public ActionResult PDAFilePreview()
        {
            string[] values = new string[] { };
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file_input = Request.Files[fileName];
                    string path = Path.Combine(Server.MapPath("~/Files/PDA/"),
                                      Path.GetFileName(file_input.FileName));

                    //if (file_input.ContentType)
                    //{

                    //}

                    file_input.SaveAs(path); // Save the file locally
                    string file_encrypted = "";

                    using (StreamReader file = new StreamReader(path, System.Text.Encoding.Default)) // read the contents of the file
                    {
                        file_encrypted = file.ReadToEnd();
                        file_encrypted = string.Join("", file_encrypted.Split('\0')); // To remove Unicode
                    }
                    string file_decrypted = "";
                    // Decrypt file
                    try
                    {
                        file_decrypted = StringCipher.Decrypt(file_encrypted, "TheassassinationofJuliusCaesarwasaconspiracyofseveralRomansenators");
                    }
                    catch (Exception ex)
                    {
                        throw new CGMResultException("Error extrayendo la información del archivo. Posiblemente no esté encriptado, o la encripación usada no corresponde con la del sistema.");
                    }
                    // Deserialize object into PDAMeter object
                    var json_data = JsonConvert.DeserializeObject(file_decrypted);
                    meter_data = JsonConvert.DeserializeObject<PDAMeter>(file_decrypted);
                    Meter meter = db.Meter.Include(m => m.Boundary).Where(m => m.serial.Trim().Equals(meter_data.serial)).FirstOrDefault();
                    ViewBag.meter = meter;

                    return PartialView("_Tabla_Medidas", meter_data);
                }
            }
            catch (CGMResultException ex) {
                logger.Error(ex, ex.Message);
                ViewBag.Error = ex.Message;
            }
            catch (InvalidOperationException invEx)
            {
                string msg = "";
                logger.Error(invEx, msg);
                ViewBag.Error = invEx;
            }
            catch (Exception ex)
            {
                string msg = "Error cargando el archivo de PDA";
                logger.Error(ex, msg);
                ViewBag.Error = ex;

            }
            // Si llega hasta aquí es porque hubo algún error.
            return PartialView("_Tabla_Medidas");


        }

        public ActionResult UploadPDAMeasures()
        {
            // load data to Database
            TempData["result"] = PDAMeter.LoadPDAToDatabase(meter_data);
            return RedirectToAction("LoadFilePDA");
        }

        //==============================================================
        //------------------------- Informs ----------------------------
        //==============================================================

        public ActionResult Informs()
        {
            boundariesDB = db.Boundary.ToList();
            quantitiesDB = db.Quantity.Include(q => q.Magnitude).OrderBy(q => q.id).Skip(1).Take(22).ToList();
            tagLineDB = db.Tag_Line.Take(5).ToList();

            ViewBag.quantity_id = new SelectList(quantitiesDB, "id", "name", 1); // 1 = Energía activa
            ViewBag.tag_line_id = new SelectList(tagLineDB, "id", "name", 4); // 4 = Total
            TempData["boundary"] = boundariesDB;
            return View();
        }

        public ActionResult ViewByDate(string opcFecha, string fechaIni, string fechaFin, string annoIni, string annoFin, string boundariesSelected, string quantity_id, int tag_line_id, int tipoReporte)
        {
            var fechaI = (fechaIni != "") ? DateTime.Parse(fechaIni) : new DateTime();
            var fechaF = (fechaFin != "") ? DateTime.Parse(fechaFin) : new DateTime();
            var annoI = int.Parse(annoIni);
            var annoF = int.Parse(annoFin);

            if (opcFecha == "rm")
            {
                fechaF = fechaF.AddMonths(1).AddDays(-1);
            }
            else if (opcFecha == "ra")
            {
                fechaI = DateTime.Parse("1/1/" + annoI);
                fechaF = DateTime.Parse("31/12/" + annoF);
            }

            List<VMeasure> vMeasureList;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                int[] boundariesIds = Array.ConvertAll(boundariesSelected.Split(','), int.Parse); //Lista de ids de fronteras, normalmente se incluye la totalidad
                vMeasureList = db.VMeasure.AsNoTracking().Where(v => v.assignedReading.Value
                                                               && boundariesIds.Contains(v.id) //Que estén en la lista
                                                               && v.is_active //Que estén activas
                                                               && (v.operation_date >= fechaI && v.operation_date <= fechaF) //Que corresponda a la fecha de operación
                                                               ).ToList();
            }

            //&& v.unit == "kWh" //Que sea energía activa. En otros casos también incluir reactiva ("kVARh")

            var respuesta = new PartialViewResult();

            switch (tipoReporte)
            {
                case 1:
                    respuesta = PartialView("Informs/_ReportHIM", vMeasureList);
                    break;
                case 2:
                    respuesta = PartialView("Informs/_ReportDCGM", vMeasureList);
                    break;
                case 3:
                    respuesta = PartialView("Informs/_ReportCM", vMeasureList);
                    break;
                case 4:
                    respuesta = PartialView("Informs/_ReportDCER", vMeasureList);
                    break;
                case 5:
                    respuesta = PartialView("Informs/_ReportCIH", vMeasureList);
                    break;
                case 6:
                    respuesta = PartialView("Informs/_ReportCIV", vMeasureList);
                    break;
            }

            return respuesta;

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
 
}
 