﻿using CGM.Areas.Telemetry.Models;
using CGM.Helpers;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CGM.Controllers.CGM
{
    public class CriticController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Critic
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Se realiza la crítica de las medidas de manera aislada y asegurándose de que cada medida corresponde a la frontera y hora determinada.
        /// Previamente se ordenaba de modo que coincida con la hora, pero esto no dba garantía de la correspondencia.
        /// </summary>
        /// <returns></returns>
        public static CriticSummary GetCriticInReport(int[] boundariesids, DateTime opdate, int acq_item_id, int curvaTipId, string criticParameters, List<VMeasure> vMeasuresOriginal)
        {
            // El procedimiento es el siguiente: Se adquiere las medidas originales (realizadas por adquisición) y las de las curva típica. Luego se comparan
            // y se sacan algunas conclusiones estadísticas basadas en los parámetros de entrada, como los porcentajes umbrales.

            // Options in this object:
            // name: compareTo, value: 0 (Valor máximo), 1 (Curva Típica)
            // name: excessValuePick, value: 0 (Valor fijo) , 1 (Porcentaje (%))
            // name: excessValue, value: num

            Dictionary<string, string> criticParams = OtherMethods.htmlJsonFormToJobjectList(JsonConvert.DeserializeObject<List<JObject>>(criticParameters));

            CriticSummary criticSummary = new CriticSummary();
            List<CriticBoundary> criticBoundaries = new List<CriticBoundary>();

            criticSummary.parameters = criticParams;

            List<VMeasure> measuresOriginal;
            if (vMeasuresOriginal == null) // Es posible que se dé las medidas originales como parámetros de entrada. Si no se las tiene, se las adquiere.
            {   // Get the quantity in the database
                int quantity_id;
                using (CGM_DB_Entities db = new CGM_DB_Entities())
                    quantity_id = db.Acquisition_Item.Find(acq_item_id).quantity_id.Value;

                measuresOriginal = MarketReport.getMeasuresReport(boundariesids, opdate, quantity_id);
            }
            else
                measuresOriginal = vMeasuresOriginal;

            // Se pensó hacer CruvaTip y valor máximo juntos, pero los códigos eran muy independientes, y se terminó poniendo muchos condicionales (if). 
            // Por concepto de mejor lectura, se hacen dos bloques grandes. Sin embargo, esto implica que se repita mucho código.

            #region CurvaTip
            if (criticParams["compareTo"] == "1")
            {
                List<VCurvaTipMeasure> listCurvaTips;
                
                // Se trae las medidas en conjunto, y no individualmente por frontera, ya que pertenecen a la misma CurvaTip

                using (CGM_DB_Entities db2 = new CGM_DB_Entities())
                {
                    listCurvaTips = db2.VCurvaTipMeasures.AsNoTracking().Where(c => c.id == curvaTipId && boundariesids.Contains(c.id_boundary)).ToList();
                }

                var vCurvaMeasuresByBoundary = listCurvaTips.OrderBy(v => v.datetime).GroupBy(v => v.code).ToList();
                List<string> curvaTipsBoundariesCodes = vCurvaMeasuresByBoundary.Select(v => v.Key).ToList(); // To check if the current boundary has a CurvaTip

                List<VCurvaTipMeasure> curvaTipMeasures = new List<VCurvaTipMeasure>();

                foreach (var boundary in measuresOriginal.GroupBy(m => new Tuple<string, string, bool?>(m.name, m.code, m.is_backup)).OrderBy(g => g.Key.Item1))
                {
                    List<CriticMeasure> criticMeasures = new List<CriticMeasure>();
                    CriticBoundary cb = new CriticBoundary();
                    cb.curvaTip_id = curvaTipId;
                    if (curvaTipsBoundariesCodes.Contains(boundary.Key.Item2)) // If the value of the boundary is on the list (has CurvaTip values)
                    {
                        // Select by boundary code
                        curvaTipMeasures = vCurvaMeasuresByBoundary.Where(c => c.Key == boundary.Key.Item2).FirstOrDefault().ToList();

                        foreach (var ctmeas in curvaTipMeasures) // Match the operation date to compare with the other measures. CurvaTip has a static datetime by default
                        {
                            var ctdt = ctmeas.datetime;
                            var date = opdate;
                            if (ctdt.Hour == 0) // La hora 00:00 (media noche) corresponde al siguiente día
                                date = date.AddDays(1);

                            ctmeas.datetime = new DateTime(date.Year, date.Month, date.Day, ctdt.Hour, ctdt.Minute, ctdt.Second);
                        }

                        foreach (var measure in boundary) // No se distingue si es backup o no
                        {
                            CriticMeasure cm = new CriticMeasure()
                            {
                                datetime = measure.hour,
                                original = measure.measure,
                                critic = curvaTipMeasures.Where(c => c.datetime.CompareTo(measure.hour) == 0).FirstOrDefault().measure
                            };

                            // Evaluate if the measure violates the range
                            EvaluateCritic(ref cm, criticParams);
                            
                            criticMeasures.Add(cm);
                        }

                        // if any of the measure violates the range, add the violation flag
                        if (criticMeasures.Where(c => c.violates != null).Where(c => c.violates.Value).Any())
                            cb.violates = true;
                        else
                            cb.violates = false;

                        cb.has_critic_source = true;
                    }
                    else // If the boundary has no CurvaTip values, simply adds the measures plain
                    {
                        foreach (var measure in boundary)
                        {
                            CriticMeasure cm = new CriticMeasure()
                            {
                                datetime = measure.hour,
                                original = measure.measure,
                            };
                            criticMeasures.Add(cm);
                        }
                        cb.has_critic_source = false;
                    }
                    
                    cb.criticMeasures = criticMeasures;
                    cb.name = boundary.Key.Item1;
                    cb.code = boundary.Key.Item2;
                    cb.is_backup = boundary.Key.Item3.Value;
                    cb.acq_item_id = acq_item_id; // Not sure if the best method, but all the measures have the same quantity_id.

                    criticBoundaries.Add(cb);
                }
            }

            #endregion

            #region Valor máximo
            if (criticParams["compareTo"] == "0")
            {
                Boundary boundaryDb;
                List<Boundary> BoundariesDB = new List<Boundary>();
                using (CGM_DB_Entities db2 = new CGM_DB_Entities())
                {
                    BoundariesDB = db2.Boundary.Where(b => boundariesids.Contains(b.id)).ToList();
                }

                foreach (var boundary in measuresOriginal.GroupBy(m => new Tuple<string, string, bool?>(m.name, m.code, m.is_backup)).OrderBy(g => g.Key.Item1))
                {
                    List<CriticMeasure> criticMeasures = new List<CriticMeasure>();
                    CriticBoundary cb = new CriticBoundary();

                    boundaryDb = BoundariesDB.Where(b => b.code == boundary.Key.Item2).FirstOrDefault();

                    double? max_value_compare = null;

                    if (acq_item_id == 1) // Energía activa, Total
                        max_value_compare = boundaryDb.max_active;
                    
                    if (acq_item_id == 43) // Energía reactiva (Positiva), Total
                        max_value_compare = boundaryDb.max_reactive;

                    if (max_value_compare != null) // If the value of the boundary is on the list (has CurvaTip values)
                    {
                        // Select by boundary code
                        foreach (var measure in boundary) // No se distingue si es backup o no
                        {
                            CriticMeasure cm = new CriticMeasure()
                            {
                                datetime = measure.hour,
                                original = measure.measure,
                                critic = max_value_compare.Value
                            };

                            // Evaluate if the measure violates the range
                            EvaluateCritic(ref cm, criticParams);

                            criticMeasures.Add(cm);
                        }


                        // if any of the measure violates the range, add the violation flag
                        if (criticMeasures.Where(c => c.violates != null).Where(c => c.violates.Value).Any())
                            cb.violates = true;
                        else
                            cb.violates = false;

                        cb.has_critic_source = true;
                    }
                    else // If the boundary has no critic values, simply adds the measures plain
                    {
                        foreach (var measure in boundary)
                        {
                            CriticMeasure cm = new CriticMeasure()
                            {
                                datetime = measure.hour,
                                original = measure.measure,
                            };
                            criticMeasures.Add(cm);
                        }
                        cb.has_critic_source = false;
                    }
                    
                    cb.criticMeasures = criticMeasures;
                    cb.name = boundary.Key.Item1;
                    cb.code = boundary.Key.Item2;
                    cb.is_backup = boundary.Key.Item3.Value;
                    cb.acq_item_id = acq_item_id; // Not sure if the best method, but all the measures have the same acq_item_id.

                    criticBoundaries.Add(cb);
                }
            }
            #endregion

            criticSummary.boundaries = criticBoundaries;
            if (criticBoundaries.Where(c => c.violates != null).Where(c => c.violates.Value).Any()) // if any of the measure violates the range, add the flag
                criticSummary.violates = true;

            return criticSummary;
        }

        public static void EvaluateCritic(ref CriticMeasure cm, Dictionary<string,string> criticParams) {
            cm.difference = cm.original - cm.critic;
            cm.percentage = (cm.difference / cm.critic) * 100;

            if (criticParams["excessValue"] != "")
            {
                float excessValue = float.Parse(criticParams["excessValue"]);
                switch (criticParams["excessValuePick"])
                {
                    case "0": // Valor fijo
                        if (cm.difference > excessValue)
                            cm.violates = true;
                        else cm.violates = false; break;

                    case "1": // Porcentaje
                        if (cm.percentage > excessValue)
                            cm.violates = true;
                        else cm.violates = false; break;
                }
            }
        }

        public static Dictionary<string, List<double[]>> ChartData(List<CriticBoundary> boundaries)
        {
            Dictionary<string, List<double[]>> chartData = new Dictionary<string, List<double[]>>();

            foreach (var boundary in boundaries)
            {
                if (boundary.criticMeasures != null)
                {
                    chartData[boundary.code + "_" + (boundary.is_backup ? "R" : "P")] =
                        boundary.criticMeasures.Select(m => new double[] { OtherMethods.toJavaScriptDate(m.datetime),
                                                                           Math.Round(m.original,2),
                                                                           Math.Round(m.critic,2) }).ToList();
                }
            }

            return chartData;
        }

        [HttpPost]
        public ActionResult SetDefaultParametersConfig(string criticParameters) {
            logger.Info("Se cambia la configuración por defecto de los parámetros de crítica.");
            try
            {
                Config_CM config = db.Config_CM.Where(c => c.element.Trim().Equals("defaultParametersVisualConfig")).FirstOrDefault();
                config.text = criticParameters;
                db.SaveChanges();

                return Json("OK");
            }
            catch (Exception ex)
            {
                string msg = "Error cambiando la configuración por defecto de los parámetros de crítica.";
                logger.Error(ex, msg);
                return Json("Error");
            }
        }

    }
}