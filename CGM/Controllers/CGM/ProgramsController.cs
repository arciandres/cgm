﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Helpers.BackgroundManager;
using CGM.Helpers;
using Hangfire;
using System.IO;
using System.Text;
using NLog;

namespace CGM.Controllers.CGM
{
    public class ProgramsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Programs
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="Programs";
            ViewBag.Result = TempData["result"];
            var program = db.Program.Include(p => p.Filtro);
            return View(await program.ToListAsync());
        }

        // GET: Programs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="Programs";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Program program = await db.Program.Include(p => p.Destination).FirstOrDefaultAsync(p => p.id == id);
            if (program == null)
            {
                return HttpNotFound();
            }
            return View(program);
        }

        // GET: Programs/Create
        public ActionResult Create()
        {
            ViewBag.returnUrl = Request.Url.AbsolutePath;
            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName="Programs";
            ViewBag.IdFiltro = new SelectList(db.Filtro, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,name,destination_id,execution_time,multiple,IdFiltro,frequency,acquisition_id, daysBack")] Program program, string opday, bool? chkNow, string returnUrl = "")
        {
            //bool auto = true;
            // Este método se subdivide en regiones para gestionarse de manera ordenada.
            #region validation of parameters in form
            program.filed = false;
            if ((program.execution_time == null && program.multiple == false)
                || (program.frequency == null && program.multiple == true && program.destination_id == 2)
                || (program.daysBack == null && program.multiple == true && program.destination_id == 2)
                || program.IdFiltro == 0 
                || program.acquisition_id == 0)
            {
                TempData["result"] = new KeyValuePair<string, string>("warning", "Algunos campos del formulario no están diligenciados.");
                return RedirectToAction("Create");
            }
            #endregion

            #region Validation of program viability

            // Some programs are restricted on frontend, but just in case, I won't take the risk
            if (program.destination_id == 2 && !program.multiple)
            {
                TempData["result"] = new KeyValuePair<string, string>("warning", "Este programa no se puede crear. Limitarse a las opciones del formulario.");
                return RedirectToAction("Create");
            }

            #endregion

            #region Validation in DB
            if (db.Program.Any(p => p.multiple && !p.filed && p.IdFiltro == program.IdFiltro))
            {
                TempData["result"] = new KeyValuePair<string, string>("warning","Ya existe un programa (múltiple) activo con el mismo filtro.");
                return RedirectToAction("Index");
            }
            #endregion

            if (chkNow != null)
            {
                program.execution_time = DateTime.Now;
            }

            ViewBag.ControllerName="Programs";
            if (ModelState.IsValid)
            {
                db.Program.Add(program);
                await db.SaveChangesAsync();
                if (program.multiple)
                {
                    // Se realiza la programación de una instrucción múltiple
                    // Debido al modo continuo que se introduce en la parte de adquisiciones, no es necesario especificar una hora y fecha para
                    // registrar el programa múltiple en el servidor de segundo plano.

                    // En cambio, este parámetro sí se debe especificar en el caso de un programa para reporte al mercado múltiple, pues la hora es fundamental.

                    // Por defecto, el id del trabajo recurrente es el mismo id del programa, que sería único.

                    if (program.destination_id == 1) // Adquisición de medidas
                    {
                        // Por defecto, las medidas se registrarían nuevamente, con el nuevo día de operación, justo al empezar el día (a las 00:00).
                        program.frequency = new TimeSpan(0, 0, 0);

                        // Se suma un millón para aislarlos de los informes 
                        await db.SaveChangesAsync();
                        RecurringJob.AddOrUpdate((program.id).ToString(),() => OperationManager.RegInforms_AcqMulti(program.id), 
                                                                             OtherMethods.TimespanToCron(program.frequency.Value) //Conversión del timespan a Cron
                                                                             , null , "4" ); //Prioridad alta por defecto.

                    }

                    else if (program.destination_id == 2 ) // Reporte al mercado
                    {
                        RecurringJob.AddOrUpdate((program.id).ToString(), () => OperationManager.RegInforms_ReportMulti(program.id),
                                                                            OtherMethods.TimespanToCron(program.frequency.Value) //Conversión del timespan a Cron
                                                                            , null, "4"); //Prioridad alta por defecto.
                        TempData["result"] = "Creación de instrucción exitosa.";
                        //logger.Info("Creación de instrucción exitosa. Mensaje: " + scheduleResult.Value);
                        return RedirectToAction("Index", new { controller = "Monitor", area = "Telemetry" });
                    }

                }

                Tuple<string,string,Report> reportResult = OperationManager.registerReport(program.id, opday, true); //Es auto???

                if (reportResult.Item1 == "false") //If no reports were added
                {
                    logger.Info("Error. No se creó la instrucción. Mensaje: " + reportResult.Item2);
                    TempData["result"] = new KeyValuePair<string, string>("false", reportResult.Item2);
                    db.Program.Remove(program); // Se elimina el programa
                    //program.filed = true; // El programa creado se archiva
                    db.SaveChanges();
                }
                else if (reportResult.Item1 == "warning")
                {
                    KeyValuePair<string, string> scheduleResult = OperationManager.scheduleInforms(reportResult.Item3);
                    logger.Info("Advertencia. Se crea la instrucción partialmente. Mensaje: " + scheduleResult.Value + Environment.NewLine + reportResult.Item2);
                    TempData["result"] = new KeyValuePair<string, string>("warning", scheduleResult.Value + Environment.NewLine + reportResult.Item2);
                }
                else
                {
                    KeyValuePair<string, string> scheduleResult = OperationManager.scheduleInforms(reportResult.Item3);
                    TempData["result"] = scheduleResult;
                    logger.Info("Creación de instrucción exitosa. Mensaje: " + scheduleResult.Value);
                    return RedirectToAction("Index",new { controller = "Monitor" , area = "Telemetry"});

                }

                return RedirectToAction("Index");
            }

            ViewBag.IdFiltro = new SelectList(db.Filtro, "Id", "Name", program.IdFiltro);
            return View(program);
        }

        /// <summary>
        /// Se llama a este método desde la ventana de Monitor. Se crea una instrucción rápida de adquisición de una única adquisición
        /// con las fronteras seleccionadas para el día de ayer, para energía activa. Normalmente es la instrucción más requerida, y por
        /// esta razón se crea un método especializado.
        /// </summary>
        /// <param name="radioMarket"></param>
        /// <param name="selectList"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateQuickProgram(int radioMarket, int[] selectList)
        {
            if (selectList == null)
            {
                TempData["result"] = new KeyValuePair<string,string>("warning","Favor seleccionar una o varias fronteras.");
                return RedirectToAction("Index", new { controller = "Monitor", area = "Telemetry" });
            }

            var boundaries = db.Boundary.Where(b => selectList.Contains(b.id)).ToList();
            Filtro quickFilter = new Filtro()
            {
                destination_id = 1,
                market_id = radioMarket, // 0:Generación, 1:Comercialización
                isFiled = true,
                Boundaries = boundaries,
                dateCreated = DateTime.Now,
                defaultFilter = 2, // 2=Filtro creado automáticamente
                Name = FiltrosController.getSuggestedName("Filtro de ", radioMarket ," - instrucción rápida")
            };

            var quickProgramAcq = (int)db.Config_CM.Where(c => c.element.Equals("quickProgramAcq")).FirstOrDefault().value.Value; // Value of acquisition group for quick program

            Program quickProgram = new Program()
            {
                acquisition_id = quickProgramAcq,// 1:Energía activa y reactiva
                destination_id = 1, // 1:Fronteras
                multiple = false, // Única adquisición
                filed = false,
                Filtro = quickFilter,
                name = "Programa rápido",
                execution_time = DateTime.Now,
            };

            // Se añade el programa a la base de datos
            logger.Info("Se crea una instrucción de adquisición rápida, con las siguientes fronteras: " + String.Join(", ", boundaries.Select(b => b.code).ToArray()));
            db.Program.Add(quickProgram);
            db.SaveChanges();

            #region Return result

            // Se repite el código de arriba -_-
            Tuple<string, string, Report> reportResult = OperationManager.registerReport(quickProgram.id, DateTime.Now.AddDays(-1).Date.ToShortDateString(), false); // This can be considered manual

            if (reportResult.Item1 == "false") //If no reports were added
            {
                logger.Info("Error. No se creó la instrucción. Mensaje: " + reportResult.Item2);
                TempData["result"] = new KeyValuePair<string, string>("false", reportResult.Item2);
                quickProgram.filed = true; // El programa creado se archiva
                db.SaveChanges();
            }
            else if (reportResult.Item1 == "warning")
            {
                KeyValuePair<string, string> scheduleResult = OperationManager.scheduleInforms(reportResult.Item3);
                logger.Info("Advertencia. Se crea la instrucción partialmente. Mensaje: " + scheduleResult.Value + Environment.NewLine + reportResult.Item2);
                TempData["result"] = new KeyValuePair<string, string>("warning", scheduleResult.Value + Environment.NewLine + reportResult.Item2);
            }
            else
            {
                KeyValuePair<string, string> scheduleResult = OperationManager.scheduleInforms(reportResult.Item3);
                TempData["result"] = scheduleResult;
                logger.Info("Creación de instrucción exitosa. Mensaje: " + scheduleResult.Value);
                return RedirectToAction("Index", new { controller = "Monitor", area = "Telemetry" });

            }
            #endregion
            return RedirectToAction("Index", new { controller = "Monitor", Area = "Telemetry" });
        }


        // GET: Programs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.ControllerName="Programs";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Program program = await db.Program.FindAsync(id);
            if (program == null)
            {
                return HttpNotFound();
            }        
            
            ViewBag.IdFiltro = new SelectList(db.Filtro, "Id", "Name", program.IdFiltro);
            return View(program);
        }

        // POST: Programs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,Name,Destination,ExecutionTime,IdFiltro")] Program program)
        {
            ViewBag.ControllerName="Programs";
            if (ModelState.IsValid)
            {
                db.Entry(program).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdFiltro = new SelectList(db.Filtro, "Id", "Name", program.IdFiltro);
            return View(program);
        }

        // GET: Programs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName="Programs";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Program program = await db.Program.Include(p=> p.Destination)
                                              .Include(p => p.Filtro)
                                              .Where(p => p.id == id).FirstOrDefaultAsync();
            if (program == null)
            {
                return HttpNotFound();
            }
            return View(program);
        }

        // POST: Programs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="Programs";
            Program program = await db.Program.FindAsync(id);
            program.filed = true;
            db.Entry(program).State = EntityState.Modified;

            if (program.multiple) { RecurringJob.RemoveIfExists((program.id).ToString()); } 

            await db.SaveChangesAsync();
            TempData["result"] = new KeyValuePair<string, string>("success", "Programa archivado exitosamente.");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

