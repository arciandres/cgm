﻿using CGM.Models.CGMModels;
using Microsoft.AspNet.Identity;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CGM.Controllers
{
    public class LogUserController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //public ActionResult Index(string returnUrl)
        //{
        //    return RedirectToAction("Login", returnUrl);
        //}
        public ActionResult Index(string returnUrl)
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
               return RedirectToAction("Index", "Home");
            }
            ViewBag.Title = "Iniciar sesión";
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        public ActionResult NotAuthorized()
        {
            return View();
        }

        public ActionResult Register()
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                ViewBag.Title = "Registro de usuario";
                ViewBag.RoleId = new SelectList(db.AspNetRoles.Where(r => r.Name !="Desarrollador").ToList(), "Id", "Name"); // 4 = Total
                //ViewBag.Title = ""
                return View();
            }
        }

        [HttpGet]
        public void SaveLog(string ip, string lat, string lon)
        {
            bool LogUserLogin;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                try
                {
                    LogUserLogin = db.Config_CM.Where(c => c.element.Trim().Equals("LogUserLogin")).FirstOrDefault().boolean.Value;
                    if (LogUserLogin)
                    {
                        LogUser lg = new LogUser();
                        string id = User.Identity.GetUserId();
                        lg.id_user = new Guid(id);

                        string rol = "";

                        foreach (var item in db.AspNetRoles)
                        {
                            if (User.IsInRole(item.Name))
                            {
                                rol += item.Name;
                            }
                        }

                        lg.user_rol = rol;
                        lg.IP = ip;
                        lg.lat = lat;
                        lg.lon = lon;
                        lg.usr_name = User.Identity.GetUserName();
                        lg.f_ini = DateTime.Now;
                        lg.f_end = DateTime.Now; ;

                        db.Log.Add(lg);
                        db.SaveChanges();
                        logger.Info("Se registró la autenticación de un usuario.");
                    }
                }
                catch (Exception ex)
                {
                    logger.Info("Surgió un error no esperado en la autenticación de usuario.");
                    throw ex;
                }
            }
        }

    }
}
