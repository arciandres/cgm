﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using Newtonsoft.Json;
namespace CGM.Controllers.General
{
    public class ParamFormsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static int? paramForm_id;
        // GET: ParamForms
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName = "ParamForms";
            return View(await db.ParamForm.ToListAsync());
        }

        // GET: ParamForms/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName = "ParamForms";
            return View();
        }

        // POST: ParamForms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,name")] ParamForm paramForm)
        {
            ViewBag.ControllerName = "ParamForms";
            if (ModelState.IsValid)
            {
                db.ParamForm.Add(paramForm);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(paramForm);
        }

        // GET: ParamForms/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.ControllerName = "ParamForms";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParamForm paramForm = await db.ParamForm.FindAsync(id);

            // Get all the elements associated with the form

            List<ParamGroup> paramGroups = db.ParamGroups.Include(g => g.Questions)
                                                         .Include(g => g.Questions.Select(q => q.Type))
                                                         .Include(g => g.Questions.Select(q => q.AnswerOptions))
                                                         //.Include(g => g.Questions.Select(q => q.AnswerOptions))
                                                         .Where(g => g.paramForm_id == id).ToList();
            ViewBag.groups = paramGroups;            

            paramForm_id = id;
            if (paramForm == null)
            {
                return HttpNotFound();
            }

            return View(paramForm);
        }

        // POST: ParamForms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,name")] ParamForm paramForm)
        {
            ViewBag.ControllerName = "ParamForms";
            if (ModelState.IsValid)
            {

                db.Entry(paramForm).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(paramForm);
        }

        // GET: ParamForms/Delete/5
        public async Task<ActionResult> DeleteForm(int? id)
        {
            ViewBag.ControllerName = "ParamForms";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParamForm paramForm = await db.ParamForm.FindAsync(id);
            if (paramForm == null)
            {
                return HttpNotFound();
            }
            return View(paramForm);
        }

        // POST: ParamForms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteFormConfirmed(int id)
        {
            ViewBag.ControllerName = "ParamForms";
            ParamForm paramForm = await db.ParamForm.FindAsync(id);
            db.ParamForm.Remove(paramForm);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteElement(int? id, string typeOfElement)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.id = id;

            return View();
        }

        #region //-------------------------- Group -------------------------

        public ActionResult Group_Create(int? id)
        {
            bool edit = id != null ? true : false;
            ViewBag.edit = edit;
            if (edit)
            {
                ParamGroup paramGroup = db.ParamGroups.Find(id);
                return PartialView("~/Views/ParamForms/_Groups.cshtml", paramGroup);
            }
            else
            {
                return PartialView("~/Views/ParamForms/_Groups.cshtml", null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Group_Create(int? id, string name)
        {
            bool edit = id != null ? true : false;
            if (edit)
            {
                ParamGroup paramGroup = await db.ParamGroups.FindAsync(id);
                paramGroup.name = name;
                
                db.Entry(paramGroup).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", new { id = paramForm_id });

            }
            else
            {
                ParamGroup paramGroup = new ParamGroup()
                {
                    paramForm_id = paramForm_id.Value,
                    name = name,
                    index = db.ParamGroups.Count() + 1
                };

                db.ParamGroups.Add(paramGroup);
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", new { id = paramForm_id });
            }
        }

        // POST: ParamForms/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete_group(int id)
        {
            ParamGroup paramGroup = await db.ParamGroups.FindAsync(id);
            db.ParamGroups.Remove(paramGroup);
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = paramForm_id });
        }

        #endregion

        #region //-------------------------- Questions -------------------------

        public ActionResult Question_Create(int? id, int group_id)
        {
            bool edit = id != null ? true : false;
            ViewBag.edit = edit;
            ViewBag.type = new SelectList(db.ParamTypes.Where(p => p.level == 1),"id","type"); // Level 1 is for Questions. Level 0 for AnswerOptions.
            ViewBag.groups = new SelectList(db.ParamGroups,"id","name"); // The group id will be selected afterwards from the list
            ViewBag.group_id = group_id;
            if (edit)
            {
                ParamQuestion paramQuestion = db.ParamQuestions.Find(id);
                return PartialView("~/Views/ParamForms/_Questions.cshtml", paramQuestion);
            }
            else
            {
                return PartialView("~/Views/ParamForms/_Questions.cshtml", null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Question_Create(int? id, int groups, string name, string alias, int type)
        {
            bool edit = id != null ? true : false;
            if (edit)
            {
                ParamQuestion paramQuestion = await db.ParamQuestions.FindAsync(id);
                paramQuestion.group_id = groups;
                paramQuestion.name = name;
                paramQuestion.type_id = type;
                paramQuestion.alias = alias;

                db.Entry(paramQuestion).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return RedirectToAction("Edit", new { id = paramForm_id });
            }
            else
            {
                ParamQuestion paramQuestion = new ParamQuestion()
                {
                    group_id = groups,
                    name = name,
                    type_id = type,
                    alias = alias,
                    index = db.ParamQuestions.Where(q => q.group_id == groups).Count()+1
                };

                db.ParamQuestions.Add(paramQuestion);
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", new { id = paramForm_id });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete_question(int id)
        {
            ParamQuestion paramQuestion = await db.ParamQuestions.FindAsync(id);
            db.ParamQuestions.Remove(paramQuestion);
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = paramForm_id });
        }

        #endregion

        #region //-------------------------- AnswerOptions -------------------------

        public ActionResult AnswerOption_Create(int? id, int question_id)
        {
            bool edit = id != null ? true : false;
            ViewBag.edit = edit;
            ViewBag.questionid = question_id;
            ViewBag.type = new SelectList(db.ParamTypes.Where(p => p.level == 0), "id", "type"); // Level 1 is for Questions. Level 0 for AnswerOptions.
            // This object is build to validate the available answer type. If the question is radio, the answer has to be radio too
            ViewBag.questionid_type = JsonConvert.SerializeObject(db.ParamQuestions.Select(q => new { q.id, q.Type.type , q.type_id}));

            ViewBag.questions = new SelectList(db.ParamQuestions, "id", "name");

            if (edit)
            {
                ParamAnswerOption paramAnswerOption = db.ParamAnswerOptions.Find(id);
                return PartialView("~/Views/ParamForms/_AnswerOptions.cshtml", paramAnswerOption);
            }
            else
            {
                return PartialView("~/Views/ParamForms/_AnswerOptions.cshtml", null);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AnswerOption_Create(int? id, int questions, string name, string alias, string value, int type)
        {
            bool edit = id != null ? true : false;
            if (edit)
            {
                ParamAnswerOption paramAnswerOption = await db.ParamAnswerOptions.FindAsync(id);
                paramAnswerOption.paramQuestion_id = questions;
                paramAnswerOption.name = name;
                paramAnswerOption.type_id = type;
                paramAnswerOption.value = value;
                paramAnswerOption.alias = alias;
                              
                db.Entry(paramAnswerOption).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", new { id = paramForm_id });

            }
            else
            {
                ParamAnswerOption paramAnswerOption = new ParamAnswerOption()
                {
                    paramQuestion_id = questions,
                    name = name,
                    type_id = type,
                    value = value,
                    alias = alias,
                    index = db.ParamAnswerOptions.Where(a => a.paramQuestion_id == questions).Count()+1
                };

                db.ParamAnswerOptions.Add(paramAnswerOption);
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", new { id = paramForm_id });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete_answerOption(int id)
        {
            ParamAnswerOption paramAnswerOption = await db.ParamAnswerOptions.FindAsync(id);
            db.ParamAnswerOptions.Remove(paramAnswerOption);
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = paramForm_id });
        }

        #endregion

        [HttpPost]
        public async Task<ActionResult> UpdateOrder(string tableName, string[] id_order_list, int? parent_id)
        {
            // assign the corresponding index to the element
            if (tableName == "answerOption")
            {
                var itemsToModify = db.ParamAnswerOptions.Where(a => a.paramQuestion_id == parent_id).ToList();
                for (int i = 0; i < id_order_list.Count(); i++)
                    itemsToModify.Find(a => a.id == int.Parse(id_order_list[i])).index = i+1;
            }

            if (tableName == "question")
            {
                var itemsToModify = db.ParamQuestions.Where(a => a.group_id == parent_id).ToList();
                for (int i = 0; i < id_order_list.Count(); i++)
                    itemsToModify.Find(a => a.id == int.Parse(id_order_list[i])).index = i+1;
            }

            if (tableName == "group")
            {
                var itemsToModify = db.ParamGroups.ToList();
                for (int i = 0; i < id_order_list.Count(); i++)
                    itemsToModify.Find(a => a.id == int.Parse(id_order_list[i])).index = i+1;
            }
            
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = paramForm_id }); // Irrelevant, since it's AJAX
        }

        public async Task<ActionResult> Conditionals(int objClass, int objId)
        {
            // These parameters in Viewbag will be hidden in the form, since they are useful to register the information later, but not to modify

            ViewBag.Class= objClass;
            ViewBag.id = objId;

            var objClass0 = await db.ParamAnswerOptions.OrderBy(a => a.paramQuestion_id).ToListAsync();
            ViewBag.objsToChoose = new SelectList(objClass0, "id", "name");

            // Get the answerOptions with type radio button. Useful to handle the input value in the view
            int[] isRadioOption = db.ParamAnswerOptions.Where(a => a.type_id == 4).Select(a => a.id).ToArray(); // 4: Radio Button
            ViewBag.isRadioOption = JsonConvert.SerializeObject(isRadioOption);

            // DropDownList de los operadores
            string[] operators = Enum.GetNames(typeof(OperatorConditional));
            ViewBag.operators = new SelectList(operators.Select((r,index) => new SelectListItem {Text=r, Value=index.ToString()}),"Value","Text");

            return PartialView("~/Views/ParamForms/_Conditionals.cshtml", null);
        }

        /// <summary>
        /// Saves a conditional clause to the selected item. (JSON)
        /// </summary>
        /// <param name="id">Id of the selected (conditioned) item</param>
        /// <param name="Class">0:AnswerOption; 1:Question</param>
        /// <param name="objId">Id of the object that puts the condition</param>
        /// <param name="operatorId">equalsTo; notEqualsTo; higherThan; lowerThan </param>
        /// <param name="value">Valor del objeto (vacío para RadioButton, pues este ya tiene valor)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Conditionals(int id, int Class, int objId, int operatorId, string value)
        {
            dynamic dynConditionedObj = null;

            if (Class == (int)ParamLevel.AnswerOption) 
                dynConditionedObj = db.ParamAnswerOptions.Find(id);

            if (Class == (int)ParamLevel.Question)
                dynConditionedObj = db.ParamQuestions.Find(id);

            // Get previous conditionals on the object

            List<ParamConditional> conditionals = new List<ParamConditional>();
            if (dynConditionedObj.conditionals != null && dynConditionedObj.conditionals != "")
                conditionals = JsonConvert.DeserializeObject<List<ParamConditional>>(dynConditionedObj.conditionals);

            // Conditionals are saved in JSON format
            ParamAnswerOption answerOptionCondition = db.ParamAnswerOptions.Find(objId);
            conditionals.Add(new ParamConditional()
            {
                Id = objId,
                operatorId = operatorId,
                value = value,
                name = answerOptionCondition.name,
                type = answerOptionCondition.type_id
            });

            string conditionalsJSON = JsonConvert.SerializeObject(conditionals);
            dynConditionedObj.conditionals = conditionalsJSON;
            await db.SaveChangesAsync();
            
            return RedirectToAction("Edit", new { id = paramForm_id });
        }

        /// <summary>
        /// Returns the name of the parameter (title) of the form, considering the radio button options.
        /// </summary>
        /// <returns></returns>
        public static List<Tuple<string,string>> getQuestionTitles() 
        {
            List<Tuple<string, string>> questionTitles = new List<Tuple<string, string>>();
            using (CGM_DB_Entities db1 = new CGM_DB_Entities())
            {

                var answerOptions = db1.ParamAnswerOptions.Include(a => a.Question).ToList();
                foreach (var item in answerOptions)
                {
                    if (item.type_id == 4) // radio buttons. El campo tiene el nombre de la Question a la que pertenece.
                        questionTitles.Add(new Tuple<string, string>(item.Question.alias, item.Question.name));
                    else
                        questionTitles.Add(new Tuple<string, string>(item.alias, item.name));
                }
            }
            questionTitles = questionTitles.Distinct().ToList();
            return questionTitles;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}


