﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;

namespace CGM.Controllers.General
{
    public class RolesController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Roles
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="Roles";
            return View(await db.AspNetRoles.ToListAsync());
        }

        // GET: Roles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            ViewBag.ControllerName="Roles";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetRoles aspNetRoles = await db.AspNetRoles.FindAsync(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // GET: Roles/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName="Roles";
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name")] AspNetRoles aspNetRoles)
        {
            aspNetRoles.Id = Guid.NewGuid().ToString();
            ViewBag.ControllerName="Roles";
            if (ModelState.IsValid)
            {
                db.AspNetRoles.Add(aspNetRoles);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(aspNetRoles);
        }

        // GET: Roles/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            ViewBag.ControllerName="Roles";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetRoles aspNetRoles = await db.AspNetRoles.FindAsync(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }        
            
            return View(aspNetRoles);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] AspNetRoles aspNetRoles)
        {
            ViewBag.ControllerName="Roles";
            if (ModelState.IsValid)
            {

                db.Entry(aspNetRoles).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(aspNetRoles);
        }

        // GET: Roles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            ViewBag.ControllerName="Roles";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetRoles aspNetRoles = await db.AspNetRoles.FindAsync(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ViewBag.ControllerName="Roles";
            AspNetRoles aspNetRoles = await db.AspNetRoles.FindAsync(id);
            db.AspNetRoles.Remove(aspNetRoles);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

