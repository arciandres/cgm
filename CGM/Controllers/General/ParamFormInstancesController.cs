﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using Newtonsoft.Json;
using CGM.Helpers;
using NLog;

namespace CGM.Controllers.General
{
    public class ParamFormInstancesController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static int? idInstance;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        // GET: ParamFormInstances
        public async Task<ActionResult> Index()
        {
            var paramFormInstances = db.ParamFormInstances.Include(p => p.Form);
            return View(await paramFormInstances.ToListAsync());
        }

        // GET: ParamFormInstances/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParamFormInstance paramFormInstance = await db.ParamFormInstances.FindAsync(id);
            if (paramFormInstance == null)
            {
                return HttpNotFound();
            }
            return View(paramFormInstance);
        }

        // GET: ParamFormInstances/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName="ParamFormInstances";
            ViewBag.paramForm_id = new SelectList(db.ParamForm, "id", "name");
            return View();
        }

        // POST: ParamFormInstances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,paramForm_id")] ParamFormInstance paramFormInstance)
        {
            ViewBag.ControllerName="ParamFormInstances";
            if (ModelState.IsValid)
            {
                db.ParamFormInstances.Add(paramFormInstance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.paramForm_id = new SelectList(db.ParamForm, "id", "name", paramFormInstance.paramForm_id);
            return View(paramFormInstance);
        }

        // GET: ParamFormInstances/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.ControllerName="ParamFormInstances";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(id);
        }
        /// <summary>
        /// Includes the complete form fields. Useful to be called from other parts, like Meter configuration
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditFormPartial(int? id) 
        {
            ViewBag.ControllerName = "ParamFormInstances";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            idInstance = id;
            ParamFormInstance paramFormInstance = await db.ParamFormInstances.FindAsync(id);
            if (paramFormInstance == null)
            {
                return HttpNotFound();
            }

            List<ParamGroup> paramGroups = db.ParamGroups.Where(g => g.paramForm_id == paramFormInstance.paramForm_id)
                                                         .Include(g => g.Questions)
                                                         .Include(g => g.Questions.Select(q => q.AnswerOptions)).ToList();
            ViewBag.Result = TempData["result"];
            ViewBag.VAnswers = db.VParams.AsNoTracking().Where(vp => vp.id == id).ToList();

            // Get conditionals from Questions and AnswerOptions
            List<ParamConditionalsFull> conditionsSet = ParamConditionalsFull.GetParamConditionalFullSet();

            ViewBag.conditionsSet = JsonConvert.SerializeObject(conditionsSet);

            return PartialView("~/Views/ParamFormInstances/_EditFormInstance.cshtml", paramGroups);
        }

        /// <summary>
        /// Parses the data from the form, and saves it it the corresponding fields
        /// </summary>
        /// <param name="formdata"></param>
        /// <param name="__RequestVerificationToken"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Edit(string formdata, string __RequestVerificationToken)
        {
            // IdInstance is a global variable that tells the id of the current form.

            var data = JsonConvert.DeserializeObject<List<Newtonsoft.Json.Linq.JObject>>(formdata);

            #region For event registration
                // Get the lists to register in the Event
                // Original answers without modification
                db.Configuration.ProxyCreationEnabled = false; //Para que los elementos no se queden adjuntos al contexto. Solamente copias
                List<ParamAnswer> answersOriginal = db.ParamAnswers.AsNoTracking().Where(a => a.formInstance_id == idInstance).ToList();
                List<ParamAnswer> answersChanges = new List<ParamAnswer>();
                List<ParamAnswerOption> answerOptions = db.ParamAnswerOptions.ToList();
                db.Configuration.ProxyCreationEnabled = true;
            #endregion 

            List<ParamAnswer> answers = db.ParamAnswers.AsNoTracking().Where(a => a.formInstance_id == idInstance).ToList();
            try
            {
                foreach (var pair in data)
                {
                    if (pair["value"].ToString() == "") // If there's no value, keep going with the next one.
                        continue;

                    string[] pairName = pair["name"].ToString().Split(',');

                    // El caso del radio es especial, puesto que en el atributo 'name' no se pone el id de la opción 
                    // de respuesta, sino el id de la pregunta. Entonces se debe filtrar y darle un tratamiento especial
                    // manualmente
                    if (pairName.Count() == 2) // ["1008","radio"]
                    {
                        int question_id = int.Parse(pairName[0]);

                        // Get all the options related to that questions. Useful to delete the previous answer.
                        List<ParamAnswerOption> options = db.ParamAnswerOptions.AsNoTracking().Where(a => a.paramQuestion_id == question_id).ToList();
                        int[] optionsIds = options.Select(a => a.id).ToArray();

                        //Traer la opción de respuesta que corresponde a la respuesta elegida
                        string optionValue = pair["value"].ToString();
                        ParamAnswerOption answerOptionSelected = options.Where(a => a.value == optionValue).FirstOrDefault();

                        // This conditional is put only in case, but in general it should enter every time.

                        if (answerOptionSelected.type_id == 4) // if it's radio button, delete the previous answer
                        {
                            // There should be only only every time, since it's a radio button (single choice)

                            ParamAnswer prevAnswer = answers.Where(a => optionsIds.Contains(a.answerOption_id)).FirstOrDefault();
                            if (prevAnswer != null)
                            {
                                if (prevAnswer.value == answerOptionSelected.value) // Si el valor anterior el igual al elegido, que no haya cambios.
                                    continue;
                                //db.Entry(prevAnswer).State = EntityState.Deleted;
                                //db.SaveChanges();
                                db.ParamAnswers.Attach(prevAnswer);
                                db.ParamAnswers.Remove(prevAnswer);
                                db.SaveChanges();
                            }
                            ParamAnswer answer = new ParamAnswer() // se puede hacer algo para ahorrar este trozo de código, pero así es más entendible
                            {
                                answerOption_id = answerOptionSelected.id,
                                formInstance_id = idInstance.Value,
                                value = optionValue
                            };
                            db.ParamAnswers.Add(answer);

                            answersChanges.Add(answer); // Headache to parse later, since it's radio, but whatever. It's possible
                            await db.SaveChangesAsync();

                        }
                    }
                    else // ex: ["28"], donde 28 es el id de la opción de respuesta directamente
                    {
                        int answerOption_id = int.Parse(pairName[0]); // name is the id of the AnswerOption
                        ParamAnswerOption answerOption = db.ParamAnswerOptions.Find(answerOption_id);
                        // Get the first answer with those ids
                        ParamAnswer answer = answers.Where(a => a.answerOption_id == answerOption_id).FirstOrDefault();

                        // If such answer does not exist, create one
                        if (answer == null)
                        {
                            ParamAnswer answerAdd = new ParamAnswer()
                            {
                                answerOption_id = answerOption.id,
                                formInstance_id = idInstance.Value,
                                value = pair["value"].ToString()
                            };
                            db.ParamAnswers.Add(answerAdd);
                            answersChanges.Add(answerAdd);
                            await db.SaveChangesAsync();


                        }
                        else // Otherwise, modify the information
                        {
                            if (answer.value != pair["value"].ToString()) // Si las respuestas son diferentes, modificar registro
                            {
                                answer.value = pair["value"].ToString(); // All of them are strings
                                db.Entry(answer).State = EntityState.Modified;
                                answersChanges.Add(answer);

                                // While saving the changes, for security, I save all the current meter configuration, so changes can be easily trackable
                                // The changes might be new elements that did not exist before. (Note to avoid confusion)

                                await db.SaveChangesAsync();

                            }
                        }
                    }
                }
                await db.SaveChangesAsync();

                // get the elements from a detached version to be able to log in the event
                List<ParamAnswer> answersChangesDetached = new List<ParamAnswer>();
                db.Configuration.ProxyCreationEnabled = false; //Para que los elementos no se queden adjuntos al contexto. Solamente copias

                foreach (var answerChange in answersChanges)
                {
                    // The answerOption information is added later, to avoid conflicts with the original on the database
                    answersChangesDetached.Add(db.ParamAnswers.Include(a => a.AnswerOption).FirstOrDefault(a => a.id == answerChange.id));
                    //answerChange.AnswerOption = answerOptions.Where(a => a.id == answerChange.answerOption_id).FirstOrDefault();
                    
                }
                
                logger.Info("Se modificó los parámetros del formulario No. " + idInstance + ". Para ver los cambios, dirigirse a la ventana de Eventos.");
                DataAndDB.insertEvents(new { original = answersOriginal, cambios = answersChangesDetached }, 2, idInstance.ToString(), "ParamFormInstance"); //2 = Modificación, en Event_Category
                TempData["result"] = new KeyValuePair<string, string>("true", "Cambios guardados exitosamente");
            }
            catch (Exception)
            {
                TempData["result"] = new KeyValuePair<string, string>("warning","Error en el registro de la información. Contáctese con los administradores.");
            }
            //return RedirectToAction("Edit", new { id = idInstance });
            return Json("Proceso completado.");

        }
        
        // GET: ParamFormInstances/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName="ParamFormInstances";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParamFormInstance paramFormInstance = await db.ParamFormInstances.FindAsync(id);
            if (paramFormInstance == null)
            {
                return HttpNotFound();
            }
            return View(paramFormInstance);
        }

        // POST: ParamFormInstances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="ParamFormInstances";
            ParamFormInstance paramFormInstance = await db.ParamFormInstances.FindAsync(id);
            db.ParamFormInstances.Remove(paramFormInstance);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

