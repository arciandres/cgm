﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;

namespace CGM.Controllers.General
{
    public class UsersController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Users
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="Users";
            var usersQuery = db.AspNetUsers.Where(u => u.isActive);
            List<AspNetUsers> users;
            if (User.IsInRole("Desarrollador"))
            {
                users = usersQuery.ToList();
            }
            else
            {
                users = usersQuery.Where(u => !u.AspNetRoles.Select(r => r.Name).Contains("Desarrollador")).ToList();
            }

            return View(users);
        }

        // GET: Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            ViewBag.ControllerName="Users";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName="Users";
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] AspNetUsers aspNetUsers)
        {
            ViewBag.ControllerName="Users";
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUsers);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(aspNetUsers);
        }

        // GET: Users/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            ViewBag.ControllerName="Users";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }        
            
            return View(aspNetUsers);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,Name,LastName,isActive")] AspNetUsers aspNetUsers)
        {
            ViewBag.ControllerName="Users";
            if (ModelState.IsValid)
            {

                db.Entry(aspNetUsers).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(aspNetUsers);
        }

        // GET: Users/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            ViewBag.ControllerName="Users";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ViewBag.ControllerName="Users";
            AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);

            // The user is not eliminated, but simply put as Inactive, since there might be events attached to it.

            aspNetUsers.isActive = false;

            // Se añade un token aleatorio al correo electrónico en caso de que se quiera volver a usarlo para otro usuario.
            aspNetUsers.Email = aspNetUsers.Email + "_" + Guid.NewGuid();
            //db.AspNetUsers.Remove(aspNetUsers);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

