﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;

namespace CGM.Controllers.API
{
    public class FiltrosController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/Filtros
        public IQueryable<Filtro> GetFiltro()
        {
            return db.Filtro;
        }

        // GET: api/Filtros/5
        [ResponseType(typeof(Filtro))]
        public async Task<IHttpActionResult> GetFiltro(int id)
        {
            Filtro filtro = await db.Filtro.FindAsync(id);
            if (filtro == null)
            {
                return NotFound();
            }

            return Ok(filtro);
        }

        /// <summary>
        /// Get filters depending on their destination. 
        /// Disclaimer: Mainly works for Boundaries, because Meter filters were deprecated.
        /// </summary>
        /// <param name="destination"></param>
        /// <returns></returns>
        public async Task<List<Filtro>> GetFiltroDestination(int destination)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (destination == 2)
            {
                destination = 1;  //Se debe traer también la lista de fronteras
            }
            List<Filtro> filtros = await db.Filtro.Where(fil => fil.destination_id == destination&&!fil.isFiled).ToListAsync();

            return filtros;
        }

        // POST: api/Filtros
        [ResponseType(typeof(Filtro))]
        public async Task<IHttpActionResult> PostFiltro(Filtro filtro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Filtro.Add(filtro);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = filtro.Id }, filtro);
        }

        // DELETE: api/Filtros/5
        [ResponseType(typeof(Filtro))]
        public async Task<IHttpActionResult> DeleteFiltro(int id)
        {
            Filtro filtro = await db.Filtro.FindAsync(id);
            if (filtro == null)
            {
                return NotFound();
            }

            db.Filtro.Remove(filtro);
            await db.SaveChangesAsync();

            return Ok(filtro);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FiltroExists(int id)
        {
            return db.Filtro.Count(e => e.Id == id) > 0;
        }
    }
}