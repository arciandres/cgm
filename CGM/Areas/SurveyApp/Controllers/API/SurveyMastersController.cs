﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class SurveyMastersController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/SurveyMasters
        public IQueryable<SurveyMaster> GetSurveyMasters()
        {
            return db.SurveyMasters;
        }

        // GET: api/SurveyMasters/5
        [ResponseType(typeof(SurveyMaster))]
        public async Task<IHttpActionResult> GetSurveyMaster(int id)
        {
            SurveyMaster surveyMaster = await db.SurveyMasters.FindAsync(id);
            if (surveyMaster == null)
            {
                return NotFound();
            }

            return Ok(surveyMaster);
        }

        // PUT: api/SurveyMasters/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSurveyMaster(int id, SurveyMaster surveyMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != surveyMaster.Id)
            {
                return BadRequest();
            }

            db.Entry(surveyMaster).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SurveyMasterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SurveyMasters
        [ResponseType(typeof(SurveyMaster))]
        public async Task<IHttpActionResult> PostSurveyMaster(SurveyMaster surveyMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var q = await db.SurveyMasters.Where(m => m.Id_Artefact == surveyMaster.Id_Artefact &&
                      m.DateCreate == surveyMaster.DateCreate && 
                      m.DateFinish==surveyMaster.DateFinish &&
                      m.Id_SurveyTemplate==surveyMaster.Id_SurveyTemplate).FirstOrDefaultAsync();

            if (q != null)
            {
                return CreatedAtRoute("DefaultApi", new { id = q.Id }, q);
            }

            db.SurveyMasters.Add(surveyMaster);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = surveyMaster.Id }, surveyMaster);
        }

        // DELETE: api/SurveyMasters/5
        [ResponseType(typeof(SurveyMaster))]
        public async Task<IHttpActionResult> DeleteSurveyMaster(int id)
        {
            SurveyMaster surveyMaster = await db.SurveyMasters.FindAsync(id);
            if (surveyMaster == null)
            {
                return NotFound();
            }

            db.SurveyMasters.Remove(surveyMaster);
            await db.SaveChangesAsync();

            return Ok(surveyMaster);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SurveyMasterExists(int id)
        {
            return db.SurveyMasters.Count(e => e.Id == id) > 0;
        }
    }
}