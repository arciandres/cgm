﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class DeviceUsersController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

       
        // GET: api/DeviceUsers/5
        [ResponseType(typeof(ResponseDeviceUser))]
        public async Task<IHttpActionResult> GetDeviceUser(string id)
        {
            String id_user = User.Identity.GetUserId();
            int id_r = 0;
            if (User.IsInRole("Creator") || User.IsInRole("Administrator"))
            {
                var q = await db.DevicesUser.Where(m => m.DeviceId == id && m.Id_Creator == id_user).FirstOrDefaultAsync();
                if (q == null)
                {

                    DeviceUser du = new DeviceUser()
                    {
                        Id = 0,
                        DeviceId = id,
                        Id_Creator = User.Identity.GetUserId()
                    };
                    db.DevicesUser.Add(du);
                    await db.SaveChangesAsync();
                    id_r = du.Id;
                }
                else
                {
                    id_r = q.Id;
                }
            }
            
            
            List<String> roles = new List<string>();
            var mq = db.AspNetUsers.Where(m => m.Id == id_user).Select(m => m.AspNetRoles);
            foreach (var item in mq)
            {                
                foreach (var rol in item)
                {
                    String mrol= await db.AspNetRoles.Where(m => m.Id == rol.Id).Select(m => m.Name).FirstOrDefaultAsync();
                    roles.Add(mrol);
                }
            }
            ResponseDeviceUser responseDeviceUser = new ResponseDeviceUser();
            responseDeviceUser.Id_DeviceUser = id_r;
            responseDeviceUser.User_Roles = roles;
            return Ok(responseDeviceUser);
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DeviceUserExists(int id)
        {
            return db.DevicesUser.Count(e => e.Id == id) > 0;
        }
    }
}