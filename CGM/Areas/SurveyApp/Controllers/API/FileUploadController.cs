﻿
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
namespace CGM.Areas.SurveyApp.Controllers.API
{
    public class FileUploadController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]

        [HttpPost]
        public KeyValuePair<bool, string> UploadFile()
        {
            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];
                    int id = int.Parse(HttpContext.Current.Request.Form.Get("Id"));//Artefact
                    int Id_SurveyTemplate = int.Parse(HttpContext.Current.Request.Form.Get("Id_SurveyTemplate"));
                    int Id_Survey = int.Parse(HttpContext.Current.Request.Form.Get("Id_Survey"));
                    int Id_Question = int.Parse(HttpContext.Current.Request.Form.Get("Id_Question"));
                    string DateSurvey = HttpContext.Current.Request.Form.Get("DateSurvey");

                    string relative_path_artefact = Path.DirectorySeparatorChar+"UploadedFiles"+Path.DirectorySeparatorChar+"SurveyPictures"+Path.DirectorySeparatorChar+"Artefact_"+id;
                    string path_artefact= HttpContext.Current.Server.MapPath("~"+relative_path_artefact);                    
                    string path_surveytemplate = path_artefact+ Path.DirectorySeparatorChar + "SurveyTemplate_"+Id_SurveyTemplate;
                    string path_survey = path_surveytemplate+ Path.DirectorySeparatorChar + "Survey_" + Id_Survey + "_" + id + "_" + DateSurvey;
                    string path_question = path_survey+ Path.DirectorySeparatorChar + "Question_" + Id_Question+Path.DirectorySeparatorChar;

                    string relative_question = relative_path_artefact + 
                        Path.DirectorySeparatorChar + "SurveyTemplate_" + Id_SurveyTemplate +
                        Path.DirectorySeparatorChar + "Survey_" + Id_Survey + "_" + id + "_" + DateSurvey + 
                        Path.DirectorySeparatorChar + "Question_" + Id_Question + Path.DirectorySeparatorChar;


                    if (!Directory.Exists(path_artefact))
                    {
                        Directory.CreateDirectory(path_artefact);
                    }

                    if (!Directory.Exists(path_surveytemplate))
                    {
                        Directory.CreateDirectory(path_surveytemplate);
                    }

                    if (!Directory.Exists(path_survey))
                    {
                        Directory.CreateDirectory(path_survey);
                    }

                    if (!Directory.Exists(path_question))
                    {
                        Directory.CreateDirectory(path_question);
                    }


                    if (httpPostedFile != null)
                    {
                        // Validate the uploaded image(optional)
                        string filename = httpPostedFile.FileName;
                        // Get the complete file path
                        var fileSavePath = Path.Combine(path_question, filename);

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);
                        QuestionImage qi = new QuestionImage() {
                            Id=0,
                            Id_Question=Id_Question,
                            Path= Path.Combine(relative_question,filename),
                            DateCreate=DateSurvey,
                            Id_mobileArtefact=id,
                            Id_Survey=Id_Survey
                        };
                        db.QuestionImages.Add(qi);
                        db.SaveChanges();
                        

                        return new KeyValuePair<bool, string>(true, "File uploaded successfully.");
                    }

                    return new KeyValuePair<bool, string>(true, "Could not get the uploaded file.");
                }

                return new KeyValuePair<bool, string>(true, "No file found to upload.");
            }
            catch (Exception ex)
            {
                return new KeyValuePair<bool, string>(false, "An error occurred while uploading the file. Error Message: " + ex.Message);
            }
        }
    }
}
