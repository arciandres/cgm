﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class SurveyResponsesController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/SurveyResponses
        public IQueryable<SurveyResponse> GetSurveyResponse()
        {
            return db.SurveyResponse;
        }

        // GET: api/SurveyResponses/5
        [ResponseType(typeof(SurveyResponse))]
        public async Task<IHttpActionResult> GetSurveyResponse(int id)
        {
            SurveyResponse surveyResponse = await db.SurveyResponse.FindAsync(id);
            if (surveyResponse == null)
            {
                return NotFound();
            }

            return Ok(surveyResponse);
        }

        // PUT: api/SurveyResponses/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSurveyResponse(int id, SurveyResponse surveyResponse)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != surveyResponse.Id)
            {
                return BadRequest();
            }

            db.Entry(surveyResponse).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SurveyResponseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SurveyResponses
        [ResponseType(typeof(SurveyResponse))]
        public async Task<IHttpActionResult> PostSurveyResponse(List<SurveyResponse> surveyResponse)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SurveyResponse.AddRange(surveyResponse);
            await db.SaveChangesAsync();
            

            return CreatedAtRoute("DefaultApi", new { id = 201 }, surveyResponse);
        }

        // DELETE: api/SurveyResponses/5
        [ResponseType(typeof(SurveyResponse))]
        public async Task<IHttpActionResult> DeleteSurveyResponse(int id)
        {
            SurveyResponse surveyResponse = await db.SurveyResponse.FindAsync(id);
            if (surveyResponse == null)
            {
                return NotFound();
            }

            db.SurveyResponse.Remove(surveyResponse);
            await db.SaveChangesAsync();

            return Ok(surveyResponse);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SurveyResponseExists(int id)
        {
            return db.SurveyResponse.Count(e => e.Id == id) > 0;
        }
    }
}