﻿using Microsoft.AspNet.Identity;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Areas.SurveyApp.Models.MyModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class MapInteresPlaceController : Controller
    {

        private CGM_DB_Entities db = new CGM_DB_Entities();
        // GET: Map
        public ActionResult Index()
        {

            return View();

        }

        public ActionResult Test()
        {
            return View();
        }


        public async Task<ActionResult> InfoWindow(int id)
        {
            InteresPlace ip = await db.InteresPlaces.FindAsync(id);

            return PartialView("InfoWindow", ip);


        }

        public async Task<ActionResult> Dialog(int id)
        {
            List<InteresPlaceImage> interesPlaceImages = await db.InteresPlaceImages.Where(m => m.Id_InteresPlace == id).ToListAsync();
            return PartialView("Dialog",interesPlaceImages);
        }

        public async Task<ActionResult> getInteresPlaceCoords()
        {
            var q = await db.InteresPlaces.Select(m => new { id = m.Id, latitude = m.Latitude, longitude = m.Longitude }).ToListAsync();
            return Json(q, JsonRequestBehavior.AllowGet);

        }
    }

}