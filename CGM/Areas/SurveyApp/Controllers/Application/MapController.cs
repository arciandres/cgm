﻿using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class MapController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        // GET: Map
        public async Task<ActionResult> Index(int id)
        {
            var surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            string iduser = User.Identity.GetUserId();
            if (surveyTemplate!=null && (surveyTemplate.Id_UserCreator==iduser || User.IsInRole("Administrator")))
            {
                return View(id);
            }
            return View();
            
        }        

        public ActionResult InfoWindow(int id)
        {
            InfoWindowModel infoWindowModel = new InfoWindowModel();            
          
            var q=db.SurveyMasters.Where(m => m.Id == id).FirstOrDefault();
            infoWindowModel.Id = q.Id;
            infoWindowModel.Id_Artefact = q.Id_Artefact;
            infoWindowModel.Latitude = q.Latitude;
            infoWindowModel.Longitude = q.Longitude;
            infoWindowModel.Altitude = q.Altitude;
            infoWindowModel.DateCreate = q.DateCreate;
            infoWindowModel.DateFinish = q.DateFinish;
            var st = db.SurveyTemplates.Where(m => m.Id == q.Id_SurveyTemplate).Select(m =>new {m.Name, m.url_Image }).FirstOrDefault();
            infoWindowModel.SurveyTemplateName = st.Name;
            infoWindowModel.SurveyTemplateImage = st.url_Image;
            infoWindowModel.Id_SurveyTemplate = id;

            return PartialView("InfoWindow", infoWindowModel);
        }
        
        public async Task<ActionResult> getSurveyCoords(int id)
        {
            var surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            string iduser = User.Identity.GetUserId();
            if (surveyTemplate != null && (surveyTemplate.Id_UserCreator == iduser|| User.IsInRole("Administrator")))
            {
                var q = db.SurveyMasters.Where(m => m.Id_SurveyTemplate == id).Select(m => new { id = m.Id, latitude = m.Latitude, longitude = m.Longitude });
                return Json(q.ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}