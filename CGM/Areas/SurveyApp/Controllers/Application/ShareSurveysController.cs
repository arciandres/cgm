﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class ShareSurveysController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        

        // GET: ShareSurveys/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(id);
            if (shareSurvey == null)
            {
                return HttpNotFound();
            }
            return View(shareSurvey);
        }        

        [AllowAnonymous]
        // GET:AuthoShare
        public ActionResult AutoShare(int id)
        {
            var q = db.ConfigurationSurveys.OrderByDescending(m => m.EndDate).FirstOrDefault();
            if (!q.Is_Open)
            {
                return HttpNotFound();
            }
            ViewBag.Id_SurveyTemplate = new SelectList(db.SurveyTemplates.Where(m => m.Id == id), "Id", "Name");
            return View();
        }
        // POST: ShareSurveys/AuthoShare
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AutoShare([Bind(Include = "Id_SurveyTemplate,ContactName")] ShareSurvey shareSurvey)
        {
            var q = db.ConfigurationSurveys.OrderByDescending(m => m.EndDate).FirstOrDefault();
            if (!q.Is_Open)
            {
                return HttpNotFound();
            }
            var lst_contacts = db.ShareSurveys.Select(m => m.ContactName);
            if (lst_contacts.Contains(shareSurvey.ContactName))
            {
                ViewBag.Error = "El contacto "+shareSurvey.ContactName+" ya existe o es nulo";
                return View(shareSurvey);
            }

            if (ModelState.IsValid)
            {
                shareSurvey.Id = Guid.NewGuid();
                db.ShareSurveys.Add(shareSurvey);
                await db.SaveChangesAsync();
                return RedirectToAction("ConductSurvey", "SurveyManager",new { id = shareSurvey.Id });
            }
            ViewBag.Id_SurveyTemplate = new SelectList(db.SurveyTemplates.Where(m => m.Id == shareSurvey.Id_SurveyTemplate), "Id", "Name", shareSurvey.Id_SurveyTemplate);
            return View(shareSurvey);
        }
        // GET: ShareSurveys/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(id);
            if (shareSurvey == null)
            {
                return HttpNotFound();
            }            
            return View(shareSurvey);
        }

        // POST: ShareSurveys/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Id_SurveyTemplate,ContactName")] ShareSurvey shareSurvey)
        {
            if (ModelState.IsValid)
            {
                ShareSurvey eshareSurvey = await db.ShareSurveys.FindAsync(shareSurvey.Id);
                eshareSurvey.ContactName = shareSurvey.ContactName;
                db.Entry(eshareSurvey).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("ConductSurvey", "SurveyManager", new { id = shareSurvey.Id });
            }            
            return View(shareSurvey);
        }

        // GET: ShareSurveys/Delete/5        
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(id);
            if (shareSurvey == null)
            {
                return HttpNotFound();
            }
            return View(shareSurvey);
        }

        // POST: ShareSurveys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(id);            
            //Delete Files
            foreach (var item in db.ResponseFile.Where(m=>m.Id_ShareQuestion==shareSurvey.Id))
            {
                String path = Server.MapPath("~" + item.Url);
                System.IO.File.Delete(path);
            }
            
            //Delete Response File from db            
            db.ResponseFile.RemoveRange(db.ResponseFile.Where(m => m.Id_ShareQuestion == shareSurvey.Id));
            //Delete SurveyResponse
            db.SurveyResponse.RemoveRange(db.SurveyResponse.Where(m => m.Id_Survey == shareSurvey.Id_Survey));
            //Delete SurveyMaster
            db.SurveyMasters.Remove(db.SurveyMasters.Find(shareSurvey.Id_Survey));
            //Delete ShareSurvey
            db.ShareSurveys.Remove(shareSurvey);
            await db.SaveChangesAsync();
            return RedirectToAction("SurveyManagerList", "SurveyManager");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
