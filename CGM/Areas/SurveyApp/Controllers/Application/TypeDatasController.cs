﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [Authorize(Roles = "Administrator")]
    public class TypeDatasController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: TypeDatas
        public async Task<ActionResult> Index()
        {
            return View(await db.TypeData.ToListAsync());
        }

        // GET: TypeDatas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeData typeData = await db.TypeData.FindAsync(id);
            if (typeData == null)
            {
                return HttpNotFound();
            }
            return View(typeData);
        }

        // GET: TypeDatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeDatas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] TypeData typeData)
        {
            if (ModelState.IsValid)
            {
                db.TypeData.Add(typeData);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(typeData);
        }

        // GET: TypeDatas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeData typeData = await db.TypeData.FindAsync(id);
            if (typeData == null)
            {
                return HttpNotFound();
            }
            return View(typeData);
        }

        // POST: TypeDatas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] TypeData typeData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeData).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(typeData);
        }

        // GET: TypeDatas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeData typeData = await db.TypeData.FindAsync(id);
            if (typeData == null)
            {
                return HttpNotFound();
            }
            return View(typeData);
        }

        // POST: TypeDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TypeData typeData = await db.TypeData.FindAsync(id);
            db.TypeData.Remove(typeData);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
