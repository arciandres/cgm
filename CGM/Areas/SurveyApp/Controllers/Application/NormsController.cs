﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    public class NormsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Norms
        public async Task<ActionResult> Index()
        {
            return View(await db.Norms.ToListAsync());
        }

        // GET: Norms/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Norm norm = await db.Norms.FindAsync(id);
            if (norm == null)
            {
                return HttpNotFound();
            }
            return View(norm);
        }

        // GET: Norms/Create
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Norms/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,Url")] Norm norm)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = (HttpPostedFileBase)Request.Files[0];
                if (file == null)
                {
                    return View(norm);
                }
                string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") +'_'+ file.FileName;
                file.SaveAs(Server.MapPath("~/UploadedFiles/Norm/" + adjunto));
                norm.Url = "/UploadedFiles/Norm/" + adjunto;
                db.Norms.Add(norm);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(norm);
        }

        // GET: Norms/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Norm norm = await db.Norms.FindAsync(id);
            if (norm == null)
            {
                return HttpNotFound();
            }
            return View(norm);
        }

        // POST: Norms/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,Url")] Norm norm)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = (HttpPostedFileBase)Request.Files[0];
                var normdb = await db.Norms.Where(m => m.Id == norm.Id).Select(m => new { m.Id, m.Url }).FirstAsync();
                norm.Url = normdb.Url;
                if (file != null)
                {
                    System.IO.File.Delete(Server.MapPath("~" + normdb.Url));
                    string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + file.FileName;
                    file.SaveAs(Server.MapPath("~/UploadedFiles/Norm/" + adjunto));
                    norm.Url = "/UploadedFiles/Norm/" + adjunto;
                }

                db.Entry(norm).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(norm);
        }

        // GET: Norms/Delete/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Norm norm = await db.Norms.FindAsync(id);
            if (norm == null)
            {
                return HttpNotFound();
            }
            return View(norm);
        }

        // POST: Norms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Norm norm = await db.Norms.FindAsync(id);
            System.IO.File.Delete(Server.MapPath("~" + norm.Url));
            db.Norms.Remove(norm);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
