﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Areas.SurveyApp;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class ConfigurationSurveysController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: ConfigurationSurveys
        public async Task<ActionResult> Index()
        {
            return View(await db.ConfigurationSurveys.ToListAsync());
        }

        // GET: ConfigurationSurveys/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConfigurationSurvey configurationSurvey = await db.ConfigurationSurveys.FindAsync(id);
            if (configurationSurvey == null)
            {
                return HttpNotFound();
            }
            return View(configurationSurvey);
        }

        // GET: ConfigurationSurveys/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ConfigurationSurveys/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,StartDate,EndDate,Is_Open")] ConfigurationSurvey configurationSurvey)
        {
            if (ModelState.IsValid)
            {
                configurationSurvey.StartDate = DateTime.Now;
                configurationSurvey.EndDate = configurationSurvey.StartDate;
                db.ConfigurationSurveys.Add(configurationSurvey);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(configurationSurvey);
        }

        // GET: ConfigurationSurveys/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConfigurationSurvey configurationSurvey = await db.ConfigurationSurveys.FindAsync(id);
            if (configurationSurvey == null)
            {
                return HttpNotFound();
            }
            return View(configurationSurvey);
        }

        // POST: ConfigurationSurveys/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,StartDate,EndDate,Is_Open")] ConfigurationSurvey configurationSurvey)
        {
            if (ModelState.IsValid)
            {
                configurationSurvey.EndDate = DateTime.Now;
                db.Entry(configurationSurvey).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(configurationSurvey);
        }

        // GET: ConfigurationSurveys/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConfigurationSurvey configurationSurvey = await db.ConfigurationSurveys.FindAsync(id);
            if (configurationSurvey == null)
            {
                return HttpNotFound();
            }
            return View(configurationSurvey);
        }

        // POST: ConfigurationSurveys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ConfigurationSurvey configurationSurvey = await db.ConfigurationSurveys.FindAsync(id);
            db.ConfigurationSurveys.Remove(configurationSurvey);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
