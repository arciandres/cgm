﻿using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Areas.SurveyApp.Models.MyModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.IO;
using System.IO.Compression;
using CGM.Areas.SurveyApp.Models.Binding;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{

    public class SurveyManagerController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Clone(Guid Id) {
            DateTime datenow = DateTime.Now;
            String datestr = datenow.ToString("yyyyMMddHHmmss");
            ShareSurvey ss = db.ShareSurveys.Find(Id);
            SurveyMaster sm = db.SurveyMasters.Find(ss.Id_Survey);

            SurveyMaster nsm = new SurveyMaster() {
                Id=0,
                Altitude=0,
                Latitude=0,
                Longitude=0,
                DateCreate=datenow.ToShortDateString()+" "+ datenow.ToShortTimeString(),
                DateFinish=datenow.ToShortDateString()+" "+datenow.ToShortTimeString(),
                Id_SurveyTemplate=1,
                Is_active=1,
                Id_MobileArtefact=0,
            };
            db.SurveyMasters.Add(nsm);
            db.SaveChanges();

            ShareSurvey nss = new ShareSurvey()
            {
                Id = Guid.NewGuid(),
                ContactName = ss.ContactName,
                Id_SurveyTemplate = 1,
                Is_Accessed = true,
                Is_Finished = false,
                Id_Survey = nsm.Id,
            };
            db.ShareSurveys.Add(nss);
            db.SaveChanges();

            List<SurveyResponse> lstsr = new List<SurveyResponse>();
            foreach (var item in db.SurveyResponse.Where(m=>m.Id_Survey==sm.Id))
            {
                SurveyResponse sr = new SurveyResponse()
                {
                    Id=0,
                    Id_Survey=nsm.Id,
                    Id_Artefact=0,
                    Id_Quesion=item.Id_Quesion,
                    ID_QuestionNext=item.ID_QuestionNext,
                    ID_QuestionLast=item.ID_QuestionLast,
                    ID_Response=item.ID_Response,
                    Value=item.Value,
                    AditionalResponse=item.AditionalResponse,
                    ValueAditionalResponse=item.ValueAditionalResponse,
                    Observation=item.Observation

                };
                lstsr.Add(sr);
            }
            db.SurveyResponse.AddRange(lstsr);
            db.SaveChanges();

            List<ResponseFile> lstrf = new List<ResponseFile>();
            foreach (var item in db.ResponseFile.Where(m => m.Id_ShareQuestion == Id))
            {
                try
                {
                    string namefilecp = item.Name;
                    namefilecp = datestr + "-" + item.Name;
                    System.IO.File.Copy(Server.MapPath("~" + item.Url), Server.MapPath("~/UploadedFiles/Responses/" + namefilecp));
                    ResponseFile mrf = new ResponseFile()
                    {
                        Id=0,
                        Id_Question=item.Id_Question,
                        Id_Response=item.Id_Response,
                        Id_ShareQuestion=nss.Id,
                        Name=namefilecp,
                        Url= "/UploadedFiles/Responses/" + namefilecp,
                        CreateDate=datenow,
                        UpdateDate=datenow,
                    };
                    lstrf.Add(mrf);
                }
                catch (Exception)
                {

                }

            }

            db.ResponseFile.AddRange(lstrf);
            db.SaveChanges();
            return RedirectToAction("SurveyManagerList", "SurveyManager",new {id=ss.ContactName.Split(' ')[0]});
        }


        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult gratitude()
        {
            return PartialView();
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> DowloadSuport(Guid Id)
        {

            var archive = Server.MapPath("~/cv.zip");
            var temp = Server.MapPath("~/Temp");

            // clear any existing archive
            if (System.IO.File.Exists(archive))
            {
                System.IO.File.Delete(archive);
            }
            // empty the temp folder
            Directory.EnumerateFiles(temp).ToList().ForEach(f => System.IO.File.Delete(f));

            // copy the selected files to the temp folder
            //files.ForEach(f => System.IO.File.Copy(f, Path.Combine(temp, Path.GetFileName(f))));
            foreach (var f in db.ResponseFile.Where(m => m.Id_ShareQuestion == Id))
            {
                try
                {
                    System.IO.File.Copy(Server.MapPath("~/" + f.Url), Path.Combine(temp, f.Name));
                }
                catch (Exception)
                {
                    
                }
                
            }
            
            // create a new archive                              
            await Task.Run(() => ZipFile.CreateFromDirectory(temp, archive));

            return File(archive, "application/zip", "cv.zip");
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Details(Guid Id)
        {
            var q0 = db.ConfigurationSurveys.OrderByDescending(m => m.EndDate).FirstOrDefault();
            if (!q0.Is_Open && !User.Identity.IsAuthenticated)
            {
                return HttpNotFound();
            }

            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(Id);
            if (shareSurvey == null)
            {
                return HttpNotFound();
            }
            if (!User.Identity.IsAuthenticated && shareSurvey.Is_Finished)
            {
                return HttpNotFound();
            }
            var surveymaster = await db.SurveyMasters.FindAsync(shareSurvey.Id_Survey);
            int id = surveymaster.Id;

            int id_surveytemplate = db.SurveyMasters.Where(m => m.Id == id).Select(m => m.Id_SurveyTemplate).FirstOrDefault();
            ViewBag.Id_Template = id_surveytemplate;

            var question = await db.Questions.Where(m => m.Id_SurveyTemplate == id_surveytemplate).
                OrderBy(m => m.OrderQuestion).
                Select(q => new QuestionMaster()
                {
                    Id = q.Id,
                    EnumLabel = q.EnumLabel,
                    Title = q.Title,
                    Num_Row_Response = q.Num_Row_Response,
                    Num_Column_Response = q.Num_Column_Response,
                    Id_ControlGroup = q.Id_ControlGroup,
                    Is_Images = q.Is_Images,
                    Is_ResponseUser = db.SurveyResponse.Where(sr => sr.Id_Survey == id && sr.Id_Quesion == q.Id).FirstOrDefault() != null ? true : false,
                    Responses = q.Responces_Question.Select(r => new ResponseMaster
                    {
                        Id = r.Id,
                        Title = r.Title,
                        Value = r.Value,
                        Id_Response_Type = r.Id_Response_Type,
                        Id_TypeData = r.Id_TypeData,
                        Index_Row = r.Index_Row,
                        Index_Column = r.Index_Column,
                        Is_File = r.Is_Files,
                        Url_File = db.ResponseFile.Where(m => m.ShareSurvey.Id_Survey == shareSurvey.Id_Survey && m.Id_Question == q.Id && m.Id_Response == r.Id).FirstOrDefault().Url,
                        Colspan = r.Colspan,
                        ValueSelect = db.SurveyResponse.Where(sr => sr.Id_Survey == id && sr.Id_Quesion == q.Id && sr.ID_Response == r.Id).Select(sr => sr.Value).FirstOrDefault()
                    }).ToList()
                }).ToListAsync();
            ViewBag.sheetname = shareSurvey.ContactName;
            MySurveyMaster sm = new MySurveyMaster() { Id = id, Title = "Survey chevere", Questions = question };
            return View("Index",sm);
        }

        // GET: SurveyManager
        public async Task<ActionResult> Index(int id)
        {

            int id_surveytemplate = db.SurveyMasters.Where(m => m.Id == id).Select(m => m.Id_SurveyTemplate).FirstOrDefault();
            ViewBag.Id_Template = id_surveytemplate;

            var question = await db.Questions.Where(m => m.Id_SurveyTemplate == id_surveytemplate).
                OrderBy(m => m.OrderQuestion).
                Select(q => new QuestionMaster()
                {
                    Id = q.Id,
                    EnumLabel = q.EnumLabel,
                    Title = q.Title,
                    Num_Row_Response = q.Num_Row_Response,
                    Num_Column_Response = q.Num_Column_Response,
                    Id_ControlGroup = q.Id_ControlGroup,
                    Is_Images = q.Is_Images,
                    Is_ResponseUser = db.SurveyResponse.Where(sr => sr.Id_Survey == id && sr.Id_Quesion == q.Id).FirstOrDefault() != null ? true : false,
                    Responses = q.Responces_Question.Select(r => new ResponseMaster
                    {
                        Id = r.Id,
                        Title = r.Title,
                        Value = r.Value,
                        Id_Response_Type = r.Id_Response_Type,
                        Id_TypeData = r.Id_TypeData,
                        Index_Row = r.Index_Row,
                        Index_Column = r.Index_Column,
                        Colspan = r.Colspan,
                        ValueSelect = db.SurveyResponse.Where(sr => sr.Id_Survey == id && sr.Id_Quesion == q.Id && sr.ID_Response == r.Id).Select(sr => sr.Value).FirstOrDefault()
                    }).ToList()
                }).ToListAsync();

            MySurveyMaster sm = new MySurveyMaster() { Id = id, Title = "Survey chevere", Questions = question };
            return View(sm);
        }
        [Authorize(Roles = "Administrator")]
        public FileContentResult DowloadHeaders(int id)
        {
            string csv = "";
            var questions = db.Questions.Where(m => m.Id_SurveyTemplate == id).OrderBy(m => m.OrderQuestion);


            foreach (var question in questions)
            {
                switch (question.Id_ControlGroup)
                {

                    case 2:
                        csv += question.Id + ";";

                        break;
                    case 3:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.Index_Row).OrderBy(m => m.Index_Column))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += question.Id + ";";

                            }
                        }
                        break;

                    default:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.OrderResponse))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += question.Id + ";";

                            }
                        }
                        break;
                }
            }
            csv += "\n";
            //--------------------------
            int c = 0;
            foreach (var question in questions)
            {
                switch (question.Id_ControlGroup)
                {

                    case 2:
                        csv += question.Responces_Question.First().Id + ";";
                        break;
                    case 3:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.Index_Row).OrderBy(m => m.Index_Column))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += response.Id + ";";
                            }
                        }
                        break;
                    default:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.OrderResponse))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += response.Id + ";";
                            }
                        }
                        break;
                }
            }
            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "Report.csv");
        }


        [Authorize(Roles = "Administrator")]
        public FileContentResult DownloadCSV(int id)
        {
            string csv = "";
            var questions = db.Questions.Where(m => m.Id_SurveyTemplate == id).OrderBy(m => m.OrderQuestion);

            foreach (var question in questions)
            {
                switch (question.Id_ControlGroup)
                {

                    case 2:
                        csv += question.Id + "." + question.Title + ";";
                        break;
                    case 3:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.Index_Row).OrderBy(m => m.Index_Column))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += question.Id + "." + question.Title + ";";
                            }
                        }
                        break;

                    default:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.OrderResponse))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += question.Id + "." + question.Title + ";";
                            }
                        }
                        break;
                }
            }
            csv += "\n";
            //--------------------------
            foreach (var question in questions)
            {
                switch (question.Id_ControlGroup)
                {

                    case 2:
                        csv += question.Title + ";";
                        break;
                    case 3:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.Index_Row).OrderBy(m => m.Index_Column))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += response.Id + "." + response.Title + ";";
                            }
                        }
                        break;
                    default:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.OrderResponse))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += response.Id + "." + response.Title + ";";
                            }
                        }
                        break;
                }
            }
            csv += "\n";

            foreach (var question in questions)
            {
                switch (question.Id_ControlGroup)
                {

                    case 2:
                        csv += "p." + question.OrderQuestion + ";";
                        break;

                    case 3:
                        int cr = 1;
                        foreach (var response in question.Responces_Question.OrderBy(m => m.Index_Row).OrderBy(m => m.Index_Column))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += "p." + question.OrderQuestion + "." + response.OrderResponse + ";";
                            }
                        }
                        break;
                    default:
                        foreach (var response in question.Responces_Question.OrderBy(m => m.OrderResponse))
                        {
                            if (response.Id_Response_Type != 0)
                            {
                                csv += "p." + question.OrderQuestion + "." + response.OrderResponse + ";";
                            }
                        }
                        break;

                }

            }
            csv += "\n";
            foreach (var survey in db.SurveyMasters.Where(m => m.Id_SurveyTemplate == id).OrderBy(m => m.Id))
            {
                foreach (var question in questions)
                {
                    switch (question.Id_ControlGroup)
                    {

                        case 2:
                            csv += db.SurveyResponse.Where(sr => sr.Id_Survey == survey.Id && sr.Id_Quesion == question.Id).Select(sr => sr.Value).FirstOrDefault() + ";";
                            break;
                        case 3:
                            foreach (var response in question.Responces_Question.OrderBy(m => m.Index_Row).OrderBy(m => m.Index_Column))
                            {
                                if (response.Id_Response_Type != 0)
                                {
                                    csv += db.SurveyResponse.Where(sr => sr.Id_Survey == survey.Id && sr.Id_Quesion == question.Id && sr.ID_Response == response.Id).Select(sr => sr.Value).FirstOrDefault() + ";";
                                }
                            }
                            break;

                        default:

                            foreach (var response in question.Responces_Question.OrderBy(m => m.OrderResponse))
                            {
                                if (response.Id_Response_Type != 0)
                                {
                                    csv += db.SurveyResponse.Where(sr => sr.Id_Survey == survey.Id && sr.Id_Quesion == question.Id && sr.ID_Response == response.Id).Select(sr => sr.Value).FirstOrDefault() + ";";
                                }
                            }
                            break;
                    }
                }
                csv += "\n";
            }


            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "Report.csv");
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Dialog(int id, int id_question)
        {
            List<QuestionImage> interesPlaceImages = await db.QuestionImages.Where(m => m.Id_Survey == id && m.Id_Question == id_question).ToListAsync();
            string enum_label = await db.Questions.Where(m => m.Id == id_question).Select(m => m.EnumLabel).FirstOrDefaultAsync();
            ViewBag.Enum_Label = enum_label;
            return PartialView("Dialog", interesPlaceImages);
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> ConductSurvey(Guid Id)
        {
            var q = db.ConfigurationSurveys.OrderByDescending(m => m.EndDate).FirstOrDefault();

            if (!q.Is_Open && !User.Identity.IsAuthenticated)
            {
                return HttpNotFound();
            }

            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(Id);
            if (shareSurvey == null)
            {
                return HttpNotFound();
            }
            if (!User.Identity.IsAuthenticated && shareSurvey.Is_Finished)
            {
                return HttpNotFound();
            }
            var surveymaster = await db.SurveyMasters.FindAsync(shareSurvey.Id_Survey);
            if (surveymaster == null)
            {
                SurveyMaster sm = new SurveyMaster() { Latitude = 0, Altitude = 0, Longitude = 0, DateCreate = DateTime.Now.ToString(), DateFinish = DateTime.Now.ToString(), Id_Artefact = 0, Id_MobileArtefact = 0, Id_SurveyTemplate = shareSurvey.Id_SurveyTemplate, Is_active = 1, Id_Survey = 0 };
                db.SurveyMasters.Add(sm);
                await db.SaveChangesAsync();
                shareSurvey.Is_Accessed = true;
                shareSurvey.Id_Survey = sm.Id;
                db.Entry(shareSurvey).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
            ShareModel s = new ShareModel() { Name = shareSurvey.SurveyTemplate.Name, ContactName = shareSurvey.ContactName, Id_ShareQuestion = Id, FirstQuestion = db.Questions.Where(m => m.Id_SurveyTemplate == shareSurvey.Id_SurveyTemplate).OrderBy(m => m.OrderQuestion).Select(m => m.Id).First() };
            return View(s);
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> SurveyManagerListIndex()
        {
            List<String> shared_surveys =await db.ShareSurveys.Where(s => s.Id_SurveyTemplate == 1).Select(m=>m.ContactName).Distinct().ToListAsync();
            return View(shared_surveys);
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> SurveyManagerList(string id)
        {
            List<CVModel> cvmodels = new List<CVModel>();
            int[] id_questions = { 50, 1, 2 };
            int[] id_responses = { 1107, 1, 22, 31, 32 };
            if (string.IsNullOrEmpty(id))
            {
                var shared_surveys = await db.ShareSurveys.Select(m => new { m.ContactName, m.Id_Survey, m.Is_Finished, m.Id }).ToListAsync();
                foreach (var item in shared_surveys)
                {
                    CVModel cvm = new CVModel();
                    cvm.Id = item.Id;
                    cvm.Identification = item.ContactName;
                    var obj = await db.SurveyResponse.Where(m => m.Id_Survey == item.Id_Survey && id_questions.Contains(m.Id_Quesion) && id_responses.Contains(m.ID_Response)).Select(m => new { m.Id_Quesion, m.ID_Response, m.Value }).ToListAsync();

                    var nc = obj.Where(m => m.Id_Quesion == 50 && m.ID_Response == 1107).Select(m => m.Value).FirstOrDefault();
                    cvm.NiuCode = nc == null ? "" : nc;
                    var lu = obj.Where(m => m.Id_Quesion == 1 && m.ID_Response == 1).Select(m => m.Value).FirstOrDefault();
                    cvm.LasUpdate = lu == null ? "" : lu;
                    var mu = obj.Where(m => m.Id_Quesion == 2 && m.ID_Response == 22).Select(m => m.Value).FirstOrDefault();
                    string municipality = mu == null ? "" : mu;
                    if (!string.IsNullOrEmpty(municipality))
                    {
                        string[] muns = municipality.Split(':');
                        if (muns.Count() > 1)
                        {
                            cvm.Municipality = muns[1];
                        }
                        else
                        {
                            cvm.Municipality = "";
                        }
                    }

                    var kv = obj.Where(m => m.Id_Quesion == 2 && m.ID_Response == 31).Select(m => m.Value).FirstOrDefault();
                    cvm.Kv = kv == null ? "" : kv;
                    var cp = obj.Where(m => m.Id_Quesion == 2 && m.ID_Response == 32).Select(m => m.Value).FirstOrDefault();
                    cvm.Capacity = cp == null ? "" : cp;
                    cvm.StateCV = StateCV.inicio;
                    if (item.Is_Finished)
                    {
                        cvm.StateCV = StateCV.Ejecucion;
                        var sr_ids = db.SurveyResponse.Where(m => m.Id_Survey == item.Id_Survey && db.Responses.Where(n => n.Is_Files == true).Select(n => n.Id).Contains(m.ID_Response) && m.Value == "1").Select(m => m.ID_Response);
                        int csr = await sr_ids.CountAsync();
                        var crf = await db.ResponseFile.Where(m => m.Id_ShareQuestion == item.Id && sr_ids.Contains(m.Id_Response)).CountAsync();
                        if (csr == crf)
                        {
                            cvm.StateCV = StateCV.Fin;
                        }
                    }
                    cvmodels.Add(cvm);
                }

            }
            else
            {
                var shared_surveys = await db.ShareSurveys.Where(m => m.ContactName.Contains(id)).Select(m => new { m.ContactName, m.Id_Survey, m.Is_Finished, m.Id }).ToListAsync();
                foreach (var item in shared_surveys)
                {
                    CVModel cvm = new CVModel();
                    cvm.Id = item.Id;
                    cvm.Identification = item.ContactName;
                    var obj = await db.SurveyResponse.Where(m => m.Id_Survey == item.Id_Survey && id_questions.Contains(m.Id_Quesion) && id_responses.Contains(m.ID_Response)).Select(m => new { m.Id_Quesion, m.ID_Response, m.Value }).ToListAsync();

                    var nc = obj.Where(m => m.Id_Quesion == 50 && m.ID_Response == 1107).Select(m => m.Value).FirstOrDefault();
                    cvm.NiuCode = nc == null ? "" : nc;
                    var lu = obj.Where(m => m.Id_Quesion == 1 && m.ID_Response == 1).Select(m => m.Value).FirstOrDefault();
                    cvm.LasUpdate = lu == null ? "" : lu;
                    var mu = obj.Where(m => m.Id_Quesion == 2 && m.ID_Response == 22).Select(m => m.Value).FirstOrDefault();
                    string municipality = mu == null ? "" : mu;
                    if (!string.IsNullOrEmpty(municipality))
                    {
                        string[] muns = municipality.Split(':');
                        if (muns.Count() > 1)
                        {
                            cvm.Municipality = muns[1];
                        }
                        else
                        {
                            cvm.Municipality = "";
                        }
                    }

                    var kv = obj.Where(m => m.Id_Quesion == 2 && m.ID_Response == 31).Select(m => m.Value).FirstOrDefault();
                    cvm.Kv = kv == null ? "" : kv;
                    var cp = obj.Where(m => m.Id_Quesion == 2 && m.ID_Response == 32).Select(m => m.Value).FirstOrDefault();
                    cvm.Capacity = cp == null ? "" : cp;
                    cvm.StateCV = StateCV.inicio;
                    if (item.Is_Finished)
                    {
                        cvm.StateCV = StateCV.Ejecucion;
                        var sr_ids = db.SurveyResponse.Where(m => m.Id_Survey == item.Id_Survey && db.Responses.Where(n => n.Is_Files == true).Select(n => n.Id).Contains(m.ID_Response) && m.Value == "1").Select(m => m.ID_Response);
                        int csr = await sr_ids.CountAsync();
                        var crf = await db.ResponseFile.Where(m => m.Id_ShareQuestion == item.Id && sr_ids.Contains(m.Id_Response)).CountAsync();
                        if (csr == crf)
                        {
                            cvm.StateCV = StateCV.Fin;
                        }
                    }
                    cvmodels.Add(cvm);
                }

            }
            
            return View(cvmodels);
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> QuestionSurveyNext([ModelBinder(typeof(SurveyQuestionModelBinder))]MySurveyMasterRequest sm)
        {
            //
            //
            var q = db.ConfigurationSurveys.OrderByDescending(m => m.EndDate).FirstOrDefault();

            if (!q.Is_Open)
            {
                return HttpNotFound();
            }
            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(sm.Id_ShareQuestion);
            if (shareSurvey == null)
            {
                return HttpNotFound();
            }
            if (!shareSurvey.Is_Accessed || (shareSurvey.Is_Finished && !User.Identity.IsAuthenticated))
            {
                return null;
            }
            //This model have state islast
            MyNextResultModel myNextResultModel = await Next(sm, shareSurvey);
            int id_question = sm.Id_Question;
            if (myNextResultModel.is_validate)
            {
                id_question = myNextResultModel.id_questionNext;
                if (sm.Is_Last)
                {
                    shareSurvey.Is_Finished = true;
                    db.Entry(shareSurvey).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    string[] gratitudeMessage = {" ", 
                                                 "Formulario de hoja de vida ha finalizado!",
                                                 "Configuración guardada y terminada." };

                    return PartialView("~/Areas/SurveyApp/Views/SurveyManager/QuestionSurvey.cshtml", ChangeQuestion(sm, shareSurvey, id_question, myNextResultModel.is_validate, gratitudeMessage[shareSurvey.Id_SurveyTemplate])); //El identificador de la plantilla debe coincidir con el mensaje

                }
            }
            sm.Is_Last = myNextResultModel.is_last;
            return PartialView("~/Areas/SurveyApp/Views/SurveyManager/QuestionSurvey.cshtml", ChangeQuestion(sm, shareSurvey, id_question, myNextResultModel.is_validate,null));
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> QuestionSurveyLast([ModelBinder(typeof(SurveyQuestionModelBinder))]MySurveyMasterRequest sm)
        {
            var q = db.ConfigurationSurveys.OrderByDescending(m => m.EndDate).FirstOrDefault();
            if (!q.Is_Open)
            {
                return HttpNotFound();
            }
            ShareSurvey shareSurvey = await db.ShareSurveys.FindAsync(sm.Id_ShareQuestion);
            if (shareSurvey == null)
            {
                return HttpNotFound();
            }
            if (!shareSurvey.Is_Accessed || (shareSurvey.Is_Finished && !User.Identity.IsAuthenticated))
            {
                return null;
            }

            int id_question = sm.Id_QuestionLast;
            sm.Is_Last = false; //Si se regresa, no puede ser la última pregunta
            return PartialView("QuestionSurvey", ChangeQuestion(sm, shareSurvey, id_question, true, null));
        }



        private MySurveyMasterResponse ChangeQuestion(MySurveyMasterRequest sm, ShareSurvey shareSurvey, int id_question, bool is_validate, string gratMessage)
        {
            try
            {

                var question = db.Questions.Where(m => m.Id_SurveyTemplate == shareSurvey.Id_SurveyTemplate && m.Id == id_question).
                OrderBy(m => m.OrderQuestion).
                Select(q => new QuestionMaster()
                {
                    Id = q.Id,
                    EnumLabel = q.EnumLabel,
                    Title = q.Title,
                    Num_Row_Response = q.Num_Row_Response,
                    Num_Column_Response = q.Num_Column_Response,
                    Id_ControlGroup = q.Id_ControlGroup,
                    Is_Images = q.Is_Images,
                    Is_ResponseUser = db.SurveyResponse.Where(sr => sr.Id_Survey == shareSurvey.Id_Survey && sr.Id_Quesion == q.Id).FirstOrDefault() != null ? true : false,
                    Responses = q.Responces_Question.OrderBy(m => m.OrderResponse).Select(r => new ResponseMaster
                    {
                        Id = r.Id,
                        Title = r.Title,
                        Value = r.Value,
                        Id_Response_Type = r.Id_Response_Type,
                        Id_TypeData = r.Id_TypeData,
                        Index_Row = r.Index_Row,
                        Index_Column = r.Index_Column,
                        Colspan = r.Colspan,
                        Is_File = r.Is_Files,
                        Url_File = db.ResponseFile.Where(m => m.ShareSurvey.Id_Survey == shareSurvey.Id_Survey && m.Id_Question == q.Id && m.Id_Response == r.Id).FirstOrDefault().Url,
                        ValueSelect = db.SurveyResponse.Where(sr => sr.Id_Survey == shareSurvey.Id_Survey && sr.Id_Quesion == q.Id && sr.ID_Response == r.Id).Select(sr => sr.Value).FirstOrDefault(),
                        DataSourceItems = r.DataSource.DataSourceItems.ToList()

                    }).ToList()
                }).ToList();

                MySurveyMasterResponse msmr = new MySurveyMasterResponse();
                msmr.Id_ShareQuestion = sm.Id_ShareQuestion;

                var id_question_back = sm.Id_Question;//sm.Id_Question is cursor back somebody smaller than id_question
                List<SurveyResponse> sr_backlist = db.SurveyResponse.Where(m => m.Id_Survey == shareSurvey.Id_Survey && m.Id_Quesion == id_question).ToList(); //Se asume todas las respuestas de una pregunta tienen las mismas respuestas siguientes o anteriores

                int? sr_back    = db.SurveyResponse.Where(m => m.Id_Survey == shareSurvey.Id_Survey && m.Id_Quesion == id_question).Select(m => m.ID_QuestionLast).FirstOrDefault(); //Se asume todas las respuestas de una pregunta tienen las mismas respuestas siguientes o anteriores
                if (sr_back.HasValue && sr_back.Value != 0)//El caso 0 se da cuando no hay ninguna respuesta almacenada respondiendo ese ítem
                {
                    id_question_back = sr_back.Value;
                }


                msmr.Id_QuestionLast = id_question_back;
                msmr.Id_Question = id_question;
                msmr.Questions = question;
                msmr.Is_Last = sm.Is_Last;
                msmr.Is_Validate = is_validate;
                if (gratMessage != null)
                    msmr.gratMessage = gratMessage;
                return msmr;
            }
            catch (Exception)
            {
                return null;

            }

        }


        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<MyNextResultModel> Next(MySurveyMasterRequest sm, ShareSurvey shareSurvey)
        {
            int id_question = sm.Id_Question;//
            bool is_validate = true;
            if (sm.Responses != null && sm.Responses.Count > 0)
            {
                bool is_firstdb = true;

                foreach (var responsedb in db.Responses.Where(m => m.Id_Question == id_question))
                {
                    if (responsedb.Response_Types.Id == 0 || responsedb.Response_Types.Id == 2) continue;

                    if (!sm.Responses.Keys.Contains(responsedb.Id))
                    {
                        MyResponseForm mrf = new MyResponseForm();
                        mrf.AditionalResponse = "";
                        mrf.ValueAditionalResponse = "";
                        mrf.Value = "0";
                        sm.Responses.Add(responsedb.Id, mrf);
                    }
                }


                foreach (var item in sm.Responses) //Itera en todas las respuestas la pregunta siguiente, y se queda solamente con la mayor. Generalmente todas las respuestas tienen la misma pregunta siguiente
                {

                    var response = await db.Responses.FindAsync(item.Key);
                    if (!Validate(response, item.Value.Value))
                    {
                        is_validate = false;
                        break;
                    }
                    if (is_firstdb && response.Id_Question_Next > id_question) //Se asigna la pregunta siguiente, basado en que la primera tiene un ID mayor. Esto puede ser un poco innecesario, al tener todas las preguntas la misma respuesta siguiente
                    {
                        id_question = response.Id_Question_Next.Value;
                        is_firstdb = false;
                    }
                    else if (response.Id_Question_Next < id_question)
                    {
                        id_question = response.Id_Question_Next.Value;
                    }

                }

                if (is_validate)
                {
                    foreach (var item in sm.Responses)
                    {
                        SurveyResponse sr = new SurveyResponse();
                        sr.Id_Artefact = 0;
                        sr.Id_Quesion = sm.Id_Question;
                        sr.ID_QuestionNext = id_question;
                        sr.ID_QuestionLast = sm.Id_QuestionLast;
                        sr.Id_Survey = shareSurvey.Id_Survey;
                        sr.ID_Response = item.Key;
                        sr.Observation = "";
                        sr.AditionalResponse = item.Value.AditionalResponse;
                        sr.ValueAditionalResponse = item.Value.ValueAditionalResponse;
                        sr.Value = item.Value.Value;
                        int irt = db.Responses.Find(sr.ID_Response).Id_Response_Type;

                        if (irt == 2)//RadioButton
                        {
                            var qsr0 = db.SurveyResponse.Where(m => m.Id_Survey == sr.Id_Survey && m.Id_Quesion == sm.Id_Question).Select(m => m.Id);
                            int? id0 = qsr0.FirstOrDefault();
                            if (qsr0.Count() == 0)
                            {
                                sr.Id = 0;
                                db.SurveyResponse.Add(sr);
                            }
                            else
                            {
                                sr.Id = id0.Value;
                                db.Entry(sr).State = EntityState.Modified;
                            }
                        }
                        else
                        {
                            var qsr = db.SurveyResponse.Where(m => m.Id_Survey == sr.Id_Survey && m.Id_Quesion == sm.Id_Question && m.ID_Response == sr.ID_Response).Select(m => m.Id);
                            int? id = qsr.FirstOrDefault();
                            if (qsr.Count() == 0)
                            {
                                sr.Id = 0;
                                db.SurveyResponse.Add(sr); 
                            }
                            else
                            {
                                sr.Id = id.Value;
                                db.Entry(sr).State = EntityState.Modified; //Se guarda las respuestas del formulario
                            }
                        }
                    }
                    await db.SaveChangesAsync();
                    var iq1 = db.Questions.Where(m => m.Id == sm.Id_Question).Select(m => m.OrderQuestion).FirstOrDefault(); //Número de orden de la pregunta anterior
                    var iqn = db.Questions.Where(m => m.Id == id_question).Select(m => m.OrderQuestion).FirstOrDefault();//Número de orden de la pregunta siguiente
                    var questions = db.Questions.Where(m => m.OrderQuestion > iq1 && m.OrderQuestion < iqn).Select(m => m.Id);
                    var q = db.SurveyResponse.Where(m => questions.Contains(m.Id_Quesion)); 

                    db.SurveyResponse.RemoveRange(q);

                    await db.SaveChangesAsync();

                }

            }
            MyNextResultModel myNextResultModel = new MyNextResultModel();
            myNextResultModel.id_questionNext = id_question;
            myNextResultModel.is_validate = is_validate;
            myNextResultModel.is_last =
                id_question ==
                db.Questions.Where(m => m.Id_SurveyTemplate == shareSurvey.Id_SurveyTemplate).OrderByDescending(m => m.OrderQuestion).Select(m => m.Id).FirstOrDefault(); //Se determina si es el último
            return myNextResultModel;
        }



        private bool Validate(Response response, string value)
        {
            if (response.Is_Required && string.IsNullOrEmpty(value)) return false;
            if (response.MaxLenght.HasValue && response.MaxLenght.Value != -1 && value.Length >= response.MaxLenght) return false;
            if (response.MaxValue.HasValue && response.MaxValue.Value != -1)
            {
                try
                {
                    float val = float.Parse(value);
                    if (val > response.MaxValue.Value) return false;
                }
                catch (Exception)
                {

                    return false;
                }
            }
            if (response.MinValue.HasValue && response.MinValue.Value != -1)
            {
                try
                {
                    float val = float.Parse(value);
                    if (val < response.MinValue.Value) return false;
                }
                catch (Exception)
                {

                    return false;
                }
            }
            if (!string.IsNullOrEmpty(response.RexExpresion))
            {
                Regex Val = new Regex(response.RexExpresion);
                if (!Val.IsMatch(value))
                {
                    return false;
                }

            }

            return true;

        }
    }


}