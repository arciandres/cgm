﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using System.IO;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    
    public class ProceduresController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Procedures
        public async Task<ActionResult> Index()
        {
            return View(await db.Procedures.ToListAsync());
        }

        // GET: Procedures/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Procedure procedure = await db.Procedures.FindAsync(id);
            if (procedure == null)
            {
                return HttpNotFound();
            }
            return View(procedure);
        }

        // GET: Procedures/Create
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Procedures/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,Url")] Procedure procedure)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = (HttpPostedFileBase) Request.Files[0];
                if (file == null)
                {
                    return View(procedure);
                }
                string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + '_' + file.FileName;
                file.SaveAs(Server.MapPath("~/UploadedFiles/Procedure/"+adjunto));
                procedure.Url = "/UploadedFiles/Procedure/" + adjunto;
                db.Procedures.Add(procedure);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(procedure);
        }

        // GET: Procedures/Edit/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Procedure procedure = await db.Procedures.FindAsync(id);
            if (procedure == null)
            {
                return HttpNotFound();
            }
            return View(procedure);
        }

        // POST: Procedures/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,Url")] Procedure procedure)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = (HttpPostedFileBase)Request.Files[0];
                var proceduredb = await db.Procedures.Where(m => m.Id == procedure.Id).Select(m => new { m.Id, m.Url }).FirstAsync();
                procedure.Url = proceduredb.Url;
                if (file != null)
                {                    
                    System.IO.File.Delete(Server.MapPath("~" +proceduredb.Url));
                    string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + file.FileName;
                    file.SaveAs(Server.MapPath("~/UploadedFiles/Procedure/" + adjunto));
                    procedure.Url = "/UploadedFiles/Procedure/" + adjunto;
                }                
                db.Entry(procedure).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(procedure);
        }

        // GET: Procedures/Delete/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Procedure procedure = await db.Procedures.FindAsync(id);
            if (procedure == null)
            {
                return HttpNotFound();
            }
            return View(procedure);
        }

        // POST: Procedures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Procedure procedure = await db.Procedures.FindAsync(id);
            System.IO.File.Delete(Server.MapPath("~" + procedure.Url));
            db.Procedures.Remove(procedure);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
