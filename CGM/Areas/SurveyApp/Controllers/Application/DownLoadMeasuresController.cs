﻿using ElectricalConsuming.Models;
using ElectricalConsuming.Models.DataTableModels;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class DownLoadMeasuresController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        // GET: DownLoadMeasures
        public ActionResult Index()
        {            
            //var q = db.Measures.GroupBy(m => new { m.Boundary, m.datemeasure.Year, m.datemeasure.Month, m.datemeasure.Day, m.datemeasure.Hour }).
            //            Select(m => new Consuming()
            //            {
            //                Id_Boundary = m.Key.Boundary.Id,
            //                BoundaryName = m.Key.Boundary.Name,
            //                datemeasure = DbFunctions.CreateDateTime(m.Key.Year, m.Key.Month, m.Key.Day, m.Key.Hour, 0, 0).Value,
            //                kVarhD = m.Sum(s => s.kVarhD) * m.Key.Boundary.Factor,
            //                kWhD = m.Sum(s => s.kWhD) * m.Key.Boundary.Factor
            //            }).Where(m => !string.IsNullOrEmpty(m.Id_Boundary)).OrderBy(m => new { m.Id_Boundary, m.datemeasure });

            MemoryStream fs = new MemoryStream();
            TextWriter tx = new StreamWriter(fs);
            tx.WriteLine("Id;BoundaryName;datemeasure;kVarhD;kWhD;PenalizationEnergy");
            SqlConnection xcn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            xcn.Open();
            SqlCommand xst = xcn.CreateCommand();
            xst.CommandText = "SELECT ID,[Boundaries].Name, dyear,dmonth,dday,dhour,sa*[Boundaries].Factor sa,sr*[Boundaries].Factor sr,CASE WHEN (sr-(sa/2))>0 THEN (sr-(sa/2))*[Boundaries].Factor ELSE 0 END sp FROM [Boundaries]," +
                "(SELECT [Id_Boundary], YEAR([datemeasure]) dyear,MONTH([datemeasure]) dmonth,DAY([datemeasure]) dday,DATEPART(hh, [datemeasure]) dhour,SUM([kVarhD]) sa,SUM([kWhD]) sr"+
                " FROM[Measures]"+
                    "group by[Id_Boundary], YEAR([datemeasure]), MONTH([datemeasure]),DAY([datemeasure]),DATEPART(hh, [datemeasure]))T"+
                    " WHERE Boundaries.id = T.[Id_Boundary]";
            SqlDataReader xr = xst.ExecuteReader();
            
            //foreach (var item in q)
            while(xr.Read())
            {
                tx.Write(xr["ID"]);
                tx.Write(";");
                tx.Write(xr["Name"]);
                tx.Write(";");                
                tx.Write(xr["dyear"]);
                tx.Write("/");
                tx.Write(xr["dmonth"]);
                tx.Write("/");
                tx.Write(xr["dday"]);
                tx.Write(" ");
                tx.Write(xr["dhour"]);
                tx.Write(":00:00;");
                tx.Write(xr["sa"]);
                tx.Write(";");
                tx.Write(xr["sr"]);
                tx.Write(";");
                tx.Write(xr["sp"]);
                tx.WriteLine();
                //tx.Write(item.Id_Boundary);
                //tx.Write(";");
                //tx.Write(item.BoundaryName);
                //tx.Write(";");
                //tx.Write(item.datemeasure);
                //tx.Write(";");
                //tx.Write(item.kVarhD);
                //tx.Write(";");
                //tx.Write(item.kWhD);
                //tx.Write(";");
                //tx.Write(item.PenalizationEnergy);                
                //tx.WriteLine();

            }
            xr.Close();
            xcn.Close();
            tx.Flush();
            fs.Flush();

            return File(fs.ToArray(), "text/csv", "Report.csv");
        }
    }
}