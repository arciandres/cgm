﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class DataSourceItemsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: DataSourceItems
        public async Task<ActionResult> Index(int id)
        {
            DataSourceItemList dsil = new DataSourceItemList();
            dsil.DataSource = await db.DataSource.FindAsync(id);
            dsil.DatasourceItems = await db.DataSourceItem.Where(m => m.Id_DataSource == id).Include(m => m.DataSource).ToListAsync();
            
            return View(dsil);
        }

        // GET: DataSourceItems/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataSourceItem dataSourceItem = await db.DataSourceItem.FindAsync(id);
            if (dataSourceItem == null)
            {
                return HttpNotFound();
            }
            return View(dataSourceItem);
        }

        // GET: DataSourceItems/Create
        public async Task<ActionResult> Create(int id)
        {
            DataSource dataSource = await db.DataSource.FindAsync(id);
            DataSourceItem di = new DataSourceItem();
            di.DataSource = dataSource;
            di.Id_DataSource = id;
                     
            return View(di);
        }

        // POST: DataSourceItems/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,key,Value,Id_DataSource")] DataSourceItem dataSourceItem)
        {
            if (ModelState.IsValid)
            {
                db.DataSourceItem.Add(dataSourceItem);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new {id=dataSourceItem.Id_DataSource });
            }

            dataSourceItem.DataSource = await db.DataSource.FindAsync(dataSourceItem.Id_DataSource);
            return View(dataSourceItem);
        }

        // GET: DataSourceItems/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataSourceItem dataSourceItem = await db.DataSourceItem.FindAsync(id);
            if (dataSourceItem == null)
            {
                return HttpNotFound();
            }
            
            return View(dataSourceItem);
        }

        // POST: DataSourceItems/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,key,Value,Id_DataSource")] DataSourceItem dataSourceItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dataSourceItem).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new {id=dataSourceItem.Id_DataSource });
            }
            dataSourceItem.DataSource = await db.DataSource.FindAsync(dataSourceItem.Id_DataSource);
            return View(dataSourceItem);
        }

        // GET: DataSourceItems/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataSourceItem dataSourceItem = await db.DataSourceItem.FindAsync(id);
            if (dataSourceItem == null)
            {
                return HttpNotFound();
            }
            return View(dataSourceItem);
        }

        // POST: DataSourceItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DataSourceItem dataSourceItem = await db.DataSourceItem.FindAsync(id);
            db.DataSourceItem.Remove(dataSourceItem);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", new {id=dataSourceItem.Id_DataSource });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
