﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class Response_TypeController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Response_Type
        public async Task<ActionResult> Index()
        {
            return View(await db.Response_Types.ToListAsync());
        }

        // GET: Response_Type/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Response_Type response_Type = await db.Response_Types.FindAsync(id);
            if (response_Type == null)
            {
                return HttpNotFound();
            }
            return View(response_Type);
        }

        // GET: Response_Type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Response_Type/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] Response_Type response_Type)
        {
            if (ModelState.IsValid)
            {
                db.Response_Types.Add(response_Type);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(response_Type);
        }

        // GET: Response_Type/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Response_Type response_Type = await db.Response_Types.FindAsync(id);
            if (response_Type == null)
            {
                return HttpNotFound();
            }
            return View(response_Type);
        }

        // POST: Response_Type/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] Response_Type response_Type)
        {
            if (ModelState.IsValid)
            {
                db.Entry(response_Type).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(response_Type);
        }

        // GET: Response_Type/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Response_Type response_Type = await db.Response_Types.FindAsync(id);
            if (response_Type == null)
            {
                return HttpNotFound();
            }
            return View(response_Type);
        }

        // POST: Response_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Response_Type response_Type = await db.Response_Types.FindAsync(id);
            db.Response_Types.Remove(response_Type);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
