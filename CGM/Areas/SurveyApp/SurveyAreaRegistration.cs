﻿using System.Web.Mvc;

namespace CGM.Areas.SurveyApp
{
    public class SurveyAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SurveyApp";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Survey_default",
                "SurveyApp/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}