﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("InteresPlaceImage", Schema = "Survey")]
    public class InteresPlaceImage
    {
        public int Id { get; set; }
        public int Id_InteresPlace { get; set; }
        public string Path { get; set; }
        public string DateCreate { get; set; }        
        public int Id_mobileArtefact { get; set; }
    }
}