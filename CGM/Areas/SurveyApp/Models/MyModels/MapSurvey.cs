﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class MapSurvey
    {
        public int Id { get; set; }
        //public int Id_Artefact { get; set; }
        //public int Id_Survey { get; set; }
        public String Name_TemplateSurvey { get; set; }
        public String Description { get; set; }
        //public int Is_active { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Altitude { get; set; }
        public String DateCreate { get; set; }
        public String DateFinish { get; set; }
        //public int Id_MobileArtefact { get; set; }
    }
}