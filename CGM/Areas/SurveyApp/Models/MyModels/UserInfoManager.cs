﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class UserInfoManager
    {
        public String UserId { get; set; }
        public String UserName { get; set; }
        public List<String> Roles { get; set; }
    }

    public class UserRoleInfoManager
    {
        public String UserId { get; set; }
        public String UserName { get; set; }
        public List<RolInfoManager> Roles { get; set; }
    }

    public class RolInfoManager
    {
        public String RolId { get; set; }
        public String RolName { get; set; }
    }

}