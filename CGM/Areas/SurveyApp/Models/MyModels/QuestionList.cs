﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class QuestionList
    {
        public List<Question> Questions { get; set; }
        public SurveyTemplate SurveyTemplate { get; set; }
    }

    public class ResponseList
    {
        public List<Response> Responses { get; set; }        
        public Question Question { get; set; }

    }

    public class DataSourceItemList
    {
        public List<DataSourceItem> DatasourceItems { get; set; }
        public DataSource DataSource { get; set; }
    }
}