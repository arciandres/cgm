﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class MyNextResultModel
    {
        public int id_questionNext { get; set; }
        public bool is_validate { get; set; }
        public bool is_last { get; set; }
    }
}