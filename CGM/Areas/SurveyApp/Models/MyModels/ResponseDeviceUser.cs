﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class ResponseDeviceUser
    {
        public int Id_DeviceUser { get; set; }
        public List<String> User_Roles { get; set; }
    }
}