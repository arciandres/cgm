﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectricalConsuming.Models.FileModels
{
    public class FileRequest
    {
        public String Search { get; set; }
        public int Frecuency { get; set; }
        public String Rangedate { get; set; }
    }
}