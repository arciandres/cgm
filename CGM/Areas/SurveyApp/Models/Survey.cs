﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("Survey", Schema = "Survey")]
    public class Survey
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }        
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }


        public string JsonSurvey { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public float Altitude { get; set; }

        public int Id_SurveyTemplate { get; set; }
        public string Id_Interviewer { get; set; }

        [ForeignKey("Id_SurveyTemplate")]
        public virtual SurveyTemplate SurveyTemplate { get; set; }
        [ForeignKey("Id_Interviewer")]
        public virtual AspNetUsers Interviewer { get; set; }
    }
}