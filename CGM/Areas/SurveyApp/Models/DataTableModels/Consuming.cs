﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectricalConsuming.Models.DataTableModels
{
    public class Consuming
    {

        public String Id_Boundary { get; set; }
        public String BoundaryName { get; set; }
        public DateTime datemeasure { get; set; }
        public double? kVarhD { get; set; }//Reactiva
        public double? kWhD { get; set; }//Activa

        public double PenalizationEnergy
        {
            get
            {
                if (kVarhD.HasValue && kWhD.HasValue)
                {
                    double val = kVarhD.Value - (kWhD.Value / 2);

                    return val < 0 ? 0 : Math.Round(val, 2);
                }
                else
                {
                    return 0;
                }

            }

        }
    }
}