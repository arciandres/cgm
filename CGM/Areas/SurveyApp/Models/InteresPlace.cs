﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("InteresPlace", Schema = "Survey")]
    public class InteresPlace
    {
        public int Id { get; set; }
        public String PlaceName { get; set; }
        public String Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public String Datecreate { get; set; }
        public String Datefinish { get; set; }
        public bool is_send { get; set; }
        public int Id_MobileArtefact { get; set; }
    }
}