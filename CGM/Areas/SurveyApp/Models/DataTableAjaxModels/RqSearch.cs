﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CGM.Areas.SurveyApp.Models.DataTableAjaxModels
{
    public class RqSearch
    {
        public string Value { get; set; }

        public bool Regex { get; set; }
    }
}
