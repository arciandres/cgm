﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CGM.Areas.SurveyApp.Models.DataTableAjaxModels
{
    public class RqOrder
    {
        public int Column { get; set; }
        public string Dir { get; set; }
    }
}
