﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("Response_Type", Schema = "Survey")]
    public class Response_Type
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        [Index("IX_Name", 1, IsUnique = true)]
        [Display(Name="Nombre del tipo")]
        public String Name { get; set; }

        public virtual ICollection<Response> Responses { get; set; }
    }
}