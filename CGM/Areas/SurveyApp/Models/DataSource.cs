﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace CGM.Areas.SurveyApp.Models
{
    [DisplayName("Conjunto de datos")]
    [Table("DataSource", Schema = "Survey")]
    public class DataSource
    {
        [Display(Name = "Grupo")]
        public int Group { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Identificador")]
        public int Id { get; set; }
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        public virtual ICollection<DataSourceItem> DataSourceItems { get; set; }

    }
}