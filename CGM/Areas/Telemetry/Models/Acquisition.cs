﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models
{
    [Table("Acquisition", Schema = "Telemetry")]
    public class Acquisition
    {
        public int id { get; set; }
        [DisplayName("Tipo")]
        public int type { get; set; } //0: Load Profile, 1: Registers, 2: Waveform, 3: Events, puede ser 4: Energy Quality, etc
        [DisplayName("Nombre")]
        public string Name { get; set; }
        [DisplayName("Archivado")]
        public bool is_filed { get; set; }
        /// <summary>
        /// True: Cannot be deleted. False: Created by user, and can be deleted
        /// </summary>
        public bool isSystemAcq { get; set; }
        public virtual ICollection<Acquisition_Item> Acquisition_Items { get; set; }
    }
}