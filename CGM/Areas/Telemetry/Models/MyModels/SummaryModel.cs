﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models.MyModels
{
    public class SummaryModel
    {
        public string statusCatStr { get; set; }
        public int statusCat { get; set; } // Se distingue de status en Informs, pues es algunos se combinan
        public List<Boundary> boundaries { get; set; }

        public List<VMeasure> measures { get; set; }
    }
}