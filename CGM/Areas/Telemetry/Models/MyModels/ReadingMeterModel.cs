﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models.MyModels
{
    public class ReadingMeterModel
    {
        public int id_meter { get; set; }
        public DateTime date { get; set; }
        public int id_acquisition_item { get; set; }

    }
}