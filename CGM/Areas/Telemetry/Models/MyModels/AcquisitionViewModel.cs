﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models.MyModels
{
    public class AcquisitionViewModel
    {
        public Acquisition acquisition { get; set; }
        public List<Acquisition_Item> acquisition_Items { get; set; }
    }
}