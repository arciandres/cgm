﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models
{
    //View Table
    public class VReport
    {
        [DisplayName("Id")]
        public long id { get; set; }
        [DisplayName("Código")]
        public string code { get; set; }
        [DisplayName("Nombre")]
        public string name { get; set; }
        [DisplayName("Tipo")]
        public bool? is_backup { get; set; }
        [DisplayName("Hora programada")]
        public DateTime dateProgrammed { get; set; }
        [DisplayName("Día de operación")]
        public DateTime operation_date { get; set; }

        /// <summary>
        /// Fecha y hora en que se ejecutó
        /// </summary>
        [DisplayName("Hora ejecutado")]
        public DateTime? end_time { get; set; }
        [DisplayName("Prioridad")]
        public int priority { get; set; }
        [DisplayName("Tiempo de realización")]
        public TimeSpan? time_elapsed { get; set; }
        [DisplayName("IdJob")]
        public int jobid { get; set; }
        [DisplayName("Reintentos")]
        public string retries { get; set; }
        [DisplayName("Estado")]
        public string StateName { get; set; }
        [DisplayName("Creado")]
        public DateTime CreatedAt { get; set; }
        [DisplayName("Medidor")]
        public string serial { get; set; }
        [DisplayName("Medidor")]
        public string brand { get; set; }
        public string Data { get; set; }
        [DisplayName("Para ejecución")]
        public double? Score { get; set; }

    }
}