namespace CGM.Areas.Telemetry.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Model_Meter", Schema = "Telemetry")]
    public partial class Model_Meter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Model_Meter()
        {
            Meter = new HashSet<Meter>();
        }

        public int id { get; set; }

        [StringLength(30)]
        [Display(Name = "Marca")]
        [Required]
        public string brand { get; set; }

        [StringLength(30)]
        [Display(Name = "Modelo")]
        [Required]
        public string model { get; set; }
        [Display(Name = "Protocolo (defecto)")]
        public string defaultProtocol { get; set; }

        //public int protocol_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Meter> Meter { get; set; }
    }
}
