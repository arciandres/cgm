namespace CGM.Areas.Telemetry.Models
{
    using CGM.Models.CGMModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Quantity", Schema = "Telemetry")]
    public partial class Quantity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]


        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(100)]
        [DisplayName("Cantidad")]
        public string name { get; set; }

        [DisplayName("Magnitud")]
        public int? magnitude_id { get; set; }

        [StringLength(30)]
        public string name_short { get; set; }
        public virtual ICollection<Register> Registers { get; set; }
        [ForeignKey("magnitude_id")]
        public virtual Magnitude Magnitude { get; set; }

    }
}
