namespace CGM.Areas.Telemetry.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Magnitude", Schema = "Telemetry")]
    public partial class Magnitude
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(30)]
        [Display(Name = "Nombre")]
        public string name { get; set; }

        [StringLength(6)]
        public string symbol { get; set; }

        [StringLength(35)]
        public string unit { get; set; }

        [StringLength(6)]
        public string unit_symbol { get; set; }
        public virtual ICollection<Quantity> Quantities{ get; set; }
    }
}
