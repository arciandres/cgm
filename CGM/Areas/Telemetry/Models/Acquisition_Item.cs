﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models
{
    [Table("Acquisition_Item", Schema = "Telemetry")]
    public class Acquisition_Item
    {
        public int Id { get; set; }

        [DisplayName("Grupo")]
        public int group { get; set; } //Indica si corresponde a 0: Quantity, 1: Register, 2: Waveform, 3: Events

        //Etiqueta que enfoca la solicitud. Ej. en LoadProfile
        //0: Neutro
        //1: Fase R
        //1: Fase S
        //2: Fase T
        //100: A.0
        //101: A.1
        //200: D.0 ... usw
        [DisplayName("Línea")]
        public int tag_line_id { get; set; }

        //Esta tabla no está diseñada para ser grande. 
        //Las siguientes llaves foráneas son independientes una de la otra, y dependen del objeto padre Acquisition.
        // Si Acquisition.type == 0, se adquirirán Quantities, y se usará solamente quantity_id, y las demás serán nulas.
        // Se considera que no vale la pena hacer 4 tablas para relacionar solamente, y al no ser grande, se puede tolerar esos valores nulos.
        [DisplayName("Cantidad")]
        public int? quantity_id { get; set; }
        public int? register_id { get; set; }
        public int? waveform_id { get; set; }
        public int? meter_events_id { get; set; }
        [ForeignKey("quantity_id")]
        public virtual Quantity Quantity {get;set;}

        [ForeignKey("tag_line_id")]
        public virtual Tag_Line Tag_Line { get; set; }

        public virtual ICollection<Acquisition> Acquisitions { get; set; }
            //public virtual Register Wegister {get;set;}
        //public virtual Waveform Waveform {get;set;}
        //public virtual Meter EvetMeterEvent {get;set; }

    }
}