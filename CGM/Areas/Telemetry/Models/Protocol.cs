namespace CGM.Areas.Telemetry.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    [Table("Protocol", Schema = "Telemetry")]
    public partial class Protocol // has not been added
    {
        public int id { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string name { get; set; }

        [StringLength(30)]
        [Display(Name = "Modelo")]
        [Required]
        public string alias { get; set; }

       // public virtual ICollection<Model_Meter> Model_Meters { get; set; }
    }
}
