namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity.Spatial;
    using System.ComponentModel;
    using CGM.Areas.Telemetry.Models;

    /// <summary>
    /// Class to perform and follow the syncronization of the clock in the meters
    /// </summary>
    [Table("ClockSync", Schema = "Telemetry")]
    public partial class ClockSync
    {
        public int id { get; set; }
        [DisplayName("Medidor")]
        [ForeignKey("Meter")]
        public int id_meter { get; set; }

        // Se almacena la hora actual del medidor respecto a la hora actual en el CGM.
        // Posiblemente se modifique en el futuro
        [DisplayName("Hora actual")]
        public DateTime? time_current { get; set; }
        [DisplayName("Hora asignada")]
        public DateTime? time_assigned { get; set; }
        [DisplayName("Hora de instrucción")]
        public DateTime? sync_date { get; set; }
        /// <summary>
        /// Los medidores ELGAMA EPQS se sincronizan con un parámetro de incremento o decremento de segundos
        /// </summary>
        public int time_shift { get; set; }
        [ForeignKey("Substatus")]
        public int id_substatus { get; set; }
        [DisplayName("Mensaje")]
        public string message { get; set; }
        [DisplayName("Tiempo de proceso")]
        public TimeSpan process_time { get; set; }
        virtual public Meter Meter {get; set;}
        virtual public Substatus Substatus { get; set; }
    }
}
