﻿using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models
{
    //View Table
    public class VMeter
    {
        [DisplayName("Id")]
        public int id { get; set; }
        [DisplayName("Serial")]
        public string serial { get; set; }
        [DisplayName("Marca")]
        public string brand { get; set; }
        [DisplayName("Modelo")]
        public string model { get; set; }
        [DisplayName("Código (Frontera)")]
        public string boundaryCode { get; set; }
        [DisplayName("Nombre (Frontera)")]
        public string boundaryName { get; set; }
        [NotMapped]
        public List<VAnswer> vAnswers { get; set; }

        public static VMeter parseMeter(Meter meter)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                VMeter vmeter = new VMeter()
                {
                    id = meter.id,
                    serial = meter.serial,
                };
                if (meter.Boundary != null)
                {
                    vmeter.boundaryCode = meter.Boundary.code;
                    vmeter.boundaryName = meter.Boundary.name;
                }
                if (meter.Model_Meter != null)
                {
                    vmeter.brand = meter.Model_Meter.brand;
                    vmeter.model = meter.Model_Meter.model;
                }
                return vmeter;
            }
        }
    }
}