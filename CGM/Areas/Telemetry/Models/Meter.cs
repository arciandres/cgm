namespace CGM.Areas.Telemetry.Models
{
    using CGM.Areas.SurveyApp.Models;
    using CGM.Models.CGMModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    /// <summary>
    /// 
    /// </summary>
    [Table("Meter", Schema = "Telemetry")]
    public partial class Meter
    {
        public Meter()
        {
            this.Filtros= new HashSet<Filtro>();
        }

        public int id { get; set; }

        [Display(Name = "Serial")]
        [Required]
        public string serial { get; set; }

        [Display(Name = "Factor Multiplicativo")]
        public double? multiplicative_factor { get; set; }

        [Display(Name = "Tipo")]
        public bool? is_backup { get; set; }
        [Display(Name = "Información")]
        public string info { get; set; }

        [DisplayName("Prioridad")]
        public int? priority { get; set; }

        [Required]
        public int model_meter_id { get; set; }
        public int? boundary_id { get; set; }
        [ForeignKey("FormConfiguration")]
        public int? formConfiguration_id { get; set; }
        public bool is_filed { get; set; } 

        [ForeignKey("model_meter_id")]
        [Display(Name = "Modelo de medidor")]
        public virtual Model_Meter Model_Meter { get; set; }

        [ForeignKey("boundary_id")]
        public virtual Boundary Boundary { get; set; }


        //[InverseProperty("MeterMain")]
        //public List<Boundary> BoundaryMain { get; set; }
        //[InverseProperty("MeterBackup")]
        //public List<Boundary> BoundaryBackup { get; set; }
        public virtual ParamFormInstance FormConfiguration { get; set; }
        public virtual ICollection<Filtro> Filtros { get; set; }
        public virtual ICollection<ClockSync> ClockSyncs { get; set; }
        [NotMapped]
        public Dictionary<string, string> parameters { get; set; }

    }
}
