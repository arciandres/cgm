﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Helpers;
using CGM.Areas.Telemetry.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.Telemetry.Controllers
{
    public class Model_MeterController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Model_Meter
        public ActionResult Index()
        {
            ViewBag.ControllerName="Model_Meter";
            return View(db.Model_Meter.ToList());
        }

    // GET: Model_Meter/Details/5
    public ActionResult Details(int? id)
        {
            ViewBag.ControllerName="Model_Meter";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Model_Meter model_Meter = db.Model_Meter.Find(id);
            if (model_Meter == null)
            {
                return HttpNotFound();
            }
            return View(model_Meter);
        }

        // GET: Model_Meter/Create
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]

        public ActionResult Create()
        {
            ViewBag.ControllerName="Model_Meter";

            return View();
        }

        // POST: Model_Meter/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Create([Bind(Include = "brand,model")] Model_Meter model_Meter)
        {
            ViewBag.ControllerName="Model_Meter";
            if (ModelState.IsValid)
            {
                db.Model_Meter.Add(model_Meter);
                db.SaveChanges();
                DataAndDB.insertEvents(model_Meter, 1, model_Meter.id.ToString(), model_Meter.GetType().Name); //1 = Creación, en Event_Category
                return RedirectToAction("Index");
            }

            return View(model_Meter);
        }

        // GET: Model_Meter/Edit/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Edit(int? id)
        {
            ViewBag.ControllerName="Model_Meter";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Model_Meter model_Meter = db.Model_Meter.Find(id);

            if (model_Meter == null)
            {
                return HttpNotFound();
            }        
            
            return View(model_Meter);
        }

        // POST: Model_Meter/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]

        public ActionResult Edit([Bind(Include = "id,brand,model")] Model_Meter model_Meter)
        {
            db.Configuration.ProxyCreationEnabled = false;
            ViewBag.ControllerName = "Model_Meter";
            if (ModelState.IsValid)
            {
                Model_Meter previousObj = db.Model_Meter.AsNoTracking().First((m => m.id == model_Meter.id)); //Se hace una copia del elemento original antes de cambiarlo
                db.Entry(model_Meter).State = EntityState.Modified;
                db.SaveChanges();
                DataAndDB.insertEvents(new { elementoPrevio= previousObj, elementoModificado = model_Meter , cambios = OtherMethods.getObjectChangesFromDB(previousObj, model_Meter) }, 2, model_Meter.id.ToString(), previousObj.GetType().Name); //2 = Modificación, en Event_Category
                return RedirectToAction("Index");
            }
            return View(model_Meter);
        }

        // GET: Model_Meter/Delete/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]

        public ActionResult Delete(int? id)
        {
            ViewBag.ControllerName="Model_Meter";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Model_Meter model_Meter = db.Model_Meter.Find(id);
            if (model_Meter == null)
            {
                return HttpNotFound();
            }
            return View(model_Meter);
        }

        // POST: Model_Meter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]

        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="Model_Meter";
            Model_Meter model_Meter = db.Model_Meter.Find(id);
            db.Model_Meter.Remove(model_Meter);
            db.SaveChanges();
            DataAndDB.insertEvents(model_Meter, 3, model_Meter.id.ToString(), model_Meter.GetType().BaseType.Name); //3 = Eliminación, en DataSourceItems
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

