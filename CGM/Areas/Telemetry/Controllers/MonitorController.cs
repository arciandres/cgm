﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using Newtonsoft.Json;
using CGM.Areas.Telemetry.Models.MyModels;
using System.Web.Script.Serialization;
using Hangfire;
using Newtonsoft.Json.Linq;
using CGM.Areas.Telemetry.Models;
using CGM.Helpers.BackgroundManager;
using CGM.Models.MyModels;
using NLog;

namespace CGM.Areas.Telemetry.Controllers
{
    public class MonitorController : Controller
    {
        static private CGM_DB_Entities db = new CGM_DB_Entities();
        private static List<Boundary> boundariesDB; //Se hace un objeto estático, pues se hace esta petición varias veces a la base de datos.
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ActionResult Index()
        {
            var servers = ServerManager.GetServers();
            ViewBag.activeserver = servers.Count > 0;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                ViewBag.daysBack = db.Config_CM.Where(cf => cf.element.Trim().Equals("DiasAtras")).FirstOrDefault().value;
            }
            ViewBag.Result = TempData["result"];
            ViewBag.active = "active"; //Estos parámetros se añaden a la barra de navegación lateral, para que en esta página se encoja por defecto
            ViewBag.shrink = "shrink";
            // var jobs = db.Jobs.ToList();
            return View();

        }

        /// <summary>
        /// Renders the subpage in the Monitor layout, given by the 'page' variable
        /// </summary>
        /// <param name="page">
        /// case 1: return "Scheduled";
        /// case 2: return "Processing";
        /// case 3: return "Failed";
        /// case 5: return "Succeeded";
        /// case 6: return "Enqueued";
        /// case 7: return "Deleted";
        /// </param>
        /// <param name="update"></param>
        /// <returns></returns>
        // GET: Telemetry/Monitor
        public ActionResult Page(int? page, bool update)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                db.Configuration.ProxyCreationEnabled = false;

                //var report_Informs = db.Report_Inform.AsNoTracking().Include(ri => ri.Boundary)
                //                                                      .Include(ri => ri.Meter)
                //                                                      .Include(ri => ri.Meter.Model_Meter)
                //                                                      .Include(ri => ri.Report);

                //float? daysBack = db.Config_CM.Where(cf => cf.element.Trim().Equals("DiasAtras")).FirstOrDefault().value;
                // Se modificó el query principal en VReport
                // Sólo se entregan los registros de los días atrás entregados por SQL.

                string pageState = getPageState(page);
                

                List<VReport> vReports = db.VReport.AsNoTracking().Where(vr => vr.StateName.Equals(pageState)).ToList();

                // Added on 27/12/2019
                // No puede haber un elemento en el monitor sin medidor.
                // Si por alguna razón existe un valor nulo (Puede ser porque se cambió de medidor o se desasoció),
                // se debe eliminar el report inform.
                var vRepsNull = vReports.Where(v => v.serial == null).ToList();
                if (vRepsNull.Count > 0)
                {
                    var vRepsIds = vRepsNull.Select(v => v.id).ToArray();
                    var RisDelete = db.Report_Inform.Where(ri => vRepsIds.Contains(ri.id)).ToList();

                    db.Report_Inform.RemoveRange(RisDelete);
                    
                    // Add a notification to the user.
                    List<Notification> notifs = new List<Notification>();
                    foreach (var item in vRepsNull)
                    {
                        notifs.Add(new Notification() { date_created = DateTime.Now, message = "Por inconsistencias con el medidor, se eliminó la instrucción para el elemento con nombre: " + item.name + ". Puede deberse a eliminación de medidor, desasociación del mismo con la frontera, u otro cambio de parámetros. Se sugiere añadir la instrucción nuevamente." });
                    }
                    db.NotificationGroups.Add(new NotificationGroup() { is_active=true, Notifications=notifs, message="Eliminación automática de instrucción en monitor por inconsistencias en medidores."});

                    db.SaveChanges();

                    vReports = vReports.Except(vRepsNull).ToList();
                }
                                             
                if (page == 3) //3 == Página de error
                {
                    int jobStatesCount;
                    var jobid_failedCount = db.JobStates.Where(j => j.Name == "Failed").Select(j => j.JobId).GroupBy(j => j).Select(g => new { JobjId=g.Key, count=g.Count()}).ToList();

                    foreach (var item in vReports)
                    {
                        jobStatesCount = jobid_failedCount.Where(j => j.JobjId == item.jobid).FirstOrDefault().count;
                        item.retries = jobStatesCount.ToString();
                    }
                }
                
                //var vReportFull = db.VReport.AsNoTracking().ToList();

                return View("~/Areas/Telemetry/Views/Monitor/" + pageState + ".cshtml", vReports);
            }
        }

        public ActionResult Summary()
        {
            boundariesDB = db.Boundary.ToList();
            return PartialView("~/Areas/Telemetry/Views/Monitor/Summary.cshtml");

        }

        public ActionResult SummaryData(string opDate = "", int negocio = 1, int quantity = 1, bool includesAll = true)
        {
            // The order is very important!
            string[,] statusCatsAndColors = new string[,] {
                                { "Programadas"   , "#7ebde5" },
                                { "Procesando"    , "#418cf4" },
                                { "Error"         , "#ea5263" },
                                { "Reintentos"    , "#ffa551" },
                                { "Completadas"   , "#55cc5d" },
                                { "No programadas", "#b7b7b7" },};

            if (boundariesDB == null)
            {
                boundariesDB = db.Boundary.ToList();
            }
            
               // Día de operación
            DateTime opdate = new DateTime();
            if (opDate == "") opdate = DateTime.Now.Date.AddDays(-1); // Si no se envía parámetros, la fecha por defecto es el día de ayer.
            else opdate = DateTime.Parse(opDate);

            ViewBag.date = opdate;
            List<List<int>> boundariesIdsLists = CGM.Controllers.API.StatsController.summaryOfCalls(opdate, negocio, includesAll);

            List<SummaryModel> summaryModels = new List<SummaryModel>();
            List<Object> dataPie = new List<object>();
            int boundsTotal = 0;
            // Mapea el objeto de ints en el objeto para realizar las medidas.
            for (int i = 0; i < boundariesIdsLists.Count; i++)
            {
                List<int> boundList = boundariesIdsLists[i];
                boundsTotal += boundList.Count;
                
                // data for pie chart
                dataPie.Add(new { name = statusCatsAndColors[i,0], y = boundList.Count, color = statusCatsAndColors[i, 1] });

                //if (boundList.Count > 0) // Solamente las que tengan elementos
                List<Boundary> boundaries = new List<Boundary>();
                foreach (int id in boundList)
                    boundaries.Add(boundariesDB.Where(b => b.id == id).FirstOrDefault());

                List<VMeasure> measuresList = new List<VMeasure>();
                if (i == 5-1) //Exitosos. De los elementos con informes completados, se extrae las medidas para mostrarlas en tabla
                {
                    measuresList = db.VMeasure.AsNoTracking().Where(v => v.assignedReading.Value
                                            && boundList.Contains(v.id)
                                            && v.report_type_id == negocio
                                            && v.is_active
                                            && v.operation_date.CompareTo(opdate) == 0
                                            && v.quantity_id == quantity).ToList();
                }
                if (measuresList.Count > 0 )
                {
                    ViewBag.max = measuresList.Max(m => m.measure);
                    ViewBag.min = measuresList.Min(m => m.measure);

                    string filename = negocio == 1 ? "Comercialización" : "Generación";
                    filename += "_" + db.Quantity.Find(quantity).name_short;
                    filename += "_" + opdate.ToShortDateString().Replace("/",".");

                    ViewBag.fileName = filename;
                }

                SummaryModel sm = new SummaryModel() { statusCat = i+1, boundaries = boundaries , measures = measuresList, statusCatStr = statusCatsAndColors[i,0]};
                summaryModels.Add(sm);
            }

            ViewBag.data = JsonConvert.SerializeObject(dataPie);
            ViewBag.boundsTotal = boundsTotal;
            ViewBag.boundsProgrammed = boundsTotal - boundariesIdsLists.Last().Count; // No tiene en cuenta las que no fueron programadas.

            return PartialView("~/Areas/Telemetry/Views/Monitor/_SummaryData.cshtml", summaryModels);

        }


        public ActionResult RetryDetails(int id) //Función que retorna el modal de los errores que generaron las repeticiones.
        {
            Report_Inform report_Inform = db.Report_Inform.AsNoTracking().Include(r => r.Boundary).Where(r => r.id == id).FirstOrDefault();
            List<JobState> jobStates = db.JobStates.Where(j => j.JobId == report_Inform.job_id && j.Name == "Failed").OrderBy(j => j.CreatedAt).ToList();
            List<StateView> states = new List<StateView>();
            List<Substatus> substatus = db.Substatus.ToList();

            foreach (JobState js in jobStates)
            {
                JObject state = JObject.Parse(js.Data);
                states.Add(new StateView()
                {
                    idjob = report_Inform.job_id.Value,
                    message = (string)state["ExceptionMessage"],
                    Time = (DateTime)state["FailedAt"],
                    //messageSubstatus = substatus.Where(s => s.id == )
                });
            }

            ViewBag.name = report_Inform.Boundary.name;
            ViewBag.code = report_Inform.Boundary.code;

            return PartialView("~/Areas/Telemetry/Views/Monitor/_RetryDetails.cshtml", states);
        }
        private string getPageState(int? page)
        {
            switch (page)
            {
                case 1: return "Scheduled";
                case 2: return "Processing";
                case 3: return "Failed";
                case 5: return "Succeeded";
                case 6: return "Enqueued";
                case 7: return "Deleted";
                default:
                    return "Succeeded";
            }
        }

        public ActionResult Details(int id_ri)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                Report_Inform ri = db.Report_Inform.FirstOrDefault(r => r.id == id_ri);
                if (ri.message != null)
                {
                    if (ri.message.Trim() == "Registros ya adquiridos.") //Cuando se ejecutó no estaba activada la sobreescritura, y ya se tenía registros
                    {
                        ViewBag.AlreadyRead = ri.message.Trim();
                        ri = db.Report_Inform.AsNoTracking().Include(r => r.Boundary)
                                                       .Include(r => r.Meter)
                                                       .Include(r => r.Substatus)
                                                       .Include(r => r.Measure_Groups.Select(g => g.Measures))
                                                       .Include(r => r.Measure_Groups.Select(g => g.Acquisition_Item))
                                                       .Include(r => r.Report)
                                                       .Where(r => r.boundary_id == ri.boundary_id
                                                       && r.Report.operation_date == ri.Report.operation_date
                                                       && r.is_backup == ri.is_backup
                                                       && r.assignedReading.Value).FirstOrDefault();
                        ViewBag.datetime = ri.end_time;
                    }
                }
                else
                {
                    ri = db.Report_Inform.AsNoTracking().Include(r => r.Boundary)
                                                   .Include(r => r.Meter)
                                                   .Include(r => r.Substatus)
                                                   .Include(r => r.Measure_Groups.Select(g => g.Measures))
                                                   .Include(r => r.Measure_Groups.Select(g => g.Acquisition_Item))
                                                   .Include(r => r.Report)
                                                   .FirstOrDefault(r => r.id == id_ri);
                }

                SimpleBoundary boundary = Helpers.ViewModels.ReportInformToSimpleBoundary(ri, 1);
                return PartialView("~/Areas/Telemetry/Views/Monitor/Details.cshtml", boundary);

            }
        }
    }
}