﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using CGM.Areas.Telemetry.Models.MyModels;
using Hangfire;
using Hangfire.States;
using NLog;
using System.Threading.Tasks;
using CGM.Helpers;
using CGM.Helpers.BackgroundManager;
using CGM.Areas.Telemetry.Models;

namespace CGM.Areas.Telemetry.Controllers.API
{
    public class MonitorController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        [HttpGet]
        public IHttpActionResult stats() //Trae los numeros que se muestran en el menú izquierdo del Monitor
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                //float? daysBack = db.Config_CM.Where(cf => cf.element.Trim().Equals("DiasAtras")).FirstOrDefault().value;
                // Se modificó el query principal en VReport
                // Sólo se entregan los registros de los días atrás entregados por SQL.

                //DateTime date_min = DateTime.Now.Date.AddDays(-daysBack.Value);

                stats enqueuedqueuescount = getStat("Enqueued"  , db);
                stats succededcount       = getStat("Succeeded" , db);
                stats processingcount     = getStat("Processing", db);
                stats deletedcount        = getStat("Deleted"   , db);
                stats scheduledcount      = getStat("Scheduled" , db);
                stats failedcount         = getStat("Failed"    , db);

                return Ok(new { enqueuedqueuescount, succededcount, processingcount, scheduledcount, deletedcount, failedcount });
            }
            //{"servers:count":{"value":"1","intValue":1,"style":"default","highlighted":false,"title":null},"recurring:count":{"value":"0","intValue":0,"style":"default","highlighted":false,"title":null},"retries:count":{"value":"0","intValue":0,"style":"default","highlighted":false,"title":null},"enqueued:count-or-null":{"value":"0","intValue":0,"style":"default","highlighted":false,"title":null},"failed:count-or-null":null,"enqueued-queues:count":{"value":"0 / 0","intValue":0,"style":"default","highlighted":false,"title":null},"scheduled:count":{"value":"0","intValue":0,"style":"default","highlighted":false,"title":null},"processing:count":{"value":"0","intValue":0,"style":"default","highlighted":false,"title":null},"succeeded:count":{"value":"153","intValue":153,"style":"default","highlighted":false,"title":null},"failed:count":{"value":"0","intValue":0,"style":"default","highlighted":false,"title":null},"deleted:count":{"value":"1","intValue":1,"style":"default","highlighted":false,"title":null},"awaiting:count":{"value":"0","intValue":0,"style":"default","highlighted":false,"title":null}}

        }

        public stats getStat(string statString, CGM_DB_Entities db)
        {
            int enqNum;
            //Se decide hacer el filtro de las horas en todos porque de esta manera se ejcuta más rápido.
            try
            {
                //dest == 1 es para traer solamente los datos de las fronteras, y no de los medidores individuales, en caso de haberlos.
                //enqNum = db.Report_Inform.Where(ri => ri.Job.StateName.Trim().Equals(statString)
                //                                   && ri.Report.destination_id == 1).Count(); //&& ri.Job.CreatedAt.CompareTo(date_start) >= 0)
                enqNum = db.VReport.AsNoTracking().Where(vr => vr.StateName.Equals(statString)).Count();

                stats stat = new stats() { value = enqNum, style = "default", intValue = enqNum, highlighted = false };

                return stat;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                //                throw;
            }
            return null;
        }

        /// <summary>
        /// Cambia de estado un informe que se encuentre en el monitor desde un botón, como "Poner en la cola de ejecución", o "Eliminar seleccionado"
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="value">600: Poner en cola de ejecución; 700: Eliminado manualmente</param>
        /// <returns></returns>
        [HttpGet]
        public string ToState(string ids, int value) //Añade los trabajos seleccionados a la tabla correspondiente. 
        {
            return ToStateStatic(ids, value);
        }

        /// <summary>
        /// Exactly the same method as above
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStateStatic(string ids, int value) //Añade los trabajos seleccionados a la tabla correspondiente. 
        {
            string[] jobs = ids.Split(',');
            Array.Sort(jobs, StringComparer.InvariantCulture);
            var client = new BackgroundJobClient();
            List<bool> check = new List<bool>();
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                List<Report_Inform> ris = db.Report_Inform.Where(ri => jobs.Contains(ri.job_id.Value.ToString())).ToList();
                Report_Inform report_Inform;
                foreach (string job in jobs)
                {
                    if (value == 600) //Enqueue
                        check.Add(client.Requeue(job));
                    if (value == 700) //Delete
                        check.Add(client.ChangeState(job, new DeletedState { Reason = "Cancelado en tabla de Monitor." }));
                    if (value == 701) // Eliminado manualmente
                        check.Add(client.ChangeState(job, new DeletedState { Reason = "Eliminado automáticamente por instrucción de múltiples ejecuciones." }));

                    report_Inform = ris.Where(ri => ri.job_id == int.Parse(job)).FirstOrDefault();
                    report_Inform.substatus_id = value;
                    report_Inform.assignedReading = false;
                    db.Entry(report_Inform).State = EntityState.Modified;
                }
                db.SaveChanges();
                Task.Run(() => LogEventHelper.LogJob(jobs, value)); //700: En cola
            }
            return check.Distinct().First().ToString() + " " + check.Distinct().Last().ToString(); //Se envía verificación del proceso. Debe devolver "true true". Más que todo para depurar.
        }

        [HttpGet]
        public string StopServers()
        {
            return ServerManager.DisposeServers();
        }
        [HttpGet]
        public string RestartServer()
        {
            return ServerManager.RestartServer(1); //1 = Se envía desde el Monitor
        }

        /// <summary>
        /// Devuelve las
        /// </summary>
        /// <param name="idsString">identificadores de las cantidades separados por coma. Se modifica simplemente en la vista</param>
        /// <returns></returns>
        public async Task<List<Quantity>> GetQuantities(string idsString)
        {
            http://localhost:50684/api/Monitor/GetQuantities?idsString=1,3
            int[] ids = Array.ConvertAll(idsString.Split(','), int.Parse);

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<Quantity> quantities = await db.Quantity.Where(q => ids.Contains(q.id)).ToListAsync();

                return quantities;
            }
        }

    }
}
