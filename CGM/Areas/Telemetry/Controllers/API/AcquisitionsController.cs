﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;

namespace CGM.Areas.Telemetry.Controllers.API
{
    public class AcquisitionsController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/Acquisitions
        public IQueryable<Acquisition> GetAcquisition()
        {
            return db.Acquisition;
        }

        // GET: api/Acquisitions/5
        [ResponseType(typeof(Acquisition))]
        public async Task<IHttpActionResult> GetAcquisitionByType(int type)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Acquisition> acquisition = await db.Acquisition.Where(a => a.type == type && !a.is_filed).ToListAsync();
            if (acquisition == null)
            {
                return NotFound();
            }

            return Ok(acquisition.OrderBy(a => a.Name));
        }

        // PUT: api/Acquisitions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAcquisition(int id, Acquisition acquisition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != acquisition.id)
            {
                return BadRequest();
            }

            db.Entry(acquisition).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AcquisitionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Acquisitions
        [ResponseType(typeof(Acquisition))]
        public async Task<IHttpActionResult> PostAcquisition(Acquisition acquisition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Acquisition.Add(acquisition);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = acquisition.id }, acquisition);
        }

        // DELETE: api/Acquisitions/5
        [ResponseType(typeof(Acquisition))]
        public async Task<IHttpActionResult> DeleteAcquisition(int id)
        {
            Acquisition acquisition = await db.Acquisition.FindAsync(id);
            if (acquisition == null)
            {
                return NotFound();
            }

            db.Acquisition.Remove(acquisition);
            await db.SaveChangesAsync();

            return Ok(acquisition);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AcquisitionExists(int id)
        {
            return db.Acquisition.Count(e => e.id == id) > 0;
        }
    }
}