﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Areas.Telemetry.Models;
using CGM.Areas.Telemetry.Models.MyModels;
using CGM.Models.CGMModels;

namespace CGM.Areas.Telemetry.Controllers
{
    public class AcquisitionsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Telemetry/Acquisitions
        public ActionResult Index()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName = "Acquisitions";
            return View(db.Acquisition.ToList());
        }

        // GET: Telemetry/Acquisitions/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.ControllerName = "Acquisitions";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition acquisition = db.Acquisition.Find(id);
            if (acquisition == null)
            {
                return HttpNotFound();
            }
            return View(acquisition);
        }

        // GET: Telemetry/Acquisitions/Create
        public ActionResult Create(int? id)
        {
            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName= "Acquisitions";
            if (id == null)
                id = 0;

            Acquisition acquisition = new Acquisition() { type = id.Value };
            List<Acquisition_Item> acquisition_Items = db.Acquisition_Item.Include(ai => ai.Quantity) 
                                                                           //Debe Agregarse el resto de Includes, cuando se cre
                                                                          .Include(ai => ai.Tag_Line)
                                                                          .ToList();
            AcquisitionViewModel acquisitionViewModel = new AcquisitionViewModel() { acquisition = acquisition, acquisition_Items = acquisition_Items };

            return View(acquisitionViewModel);
        }

        // POST: Telemetry/Acquisitions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,type,Name")] Acquisition acquisition, int[] SelectedAI)
        {
            ViewBag.ControllerName= "Acquisitions";

            List<Acquisition_Item> acquisition_Items = db.Acquisition_Item.Where(ai => SelectedAI.Any(s => s == ai.Id)).ToList();
            acquisition.Acquisition_Items = acquisition_Items;
            if (ModelState.IsValid)
            {
                db.Acquisition.Add(acquisition);
                db.SaveChanges();
                TempData["result"] = new KeyValuePair<string, string>("true", "Grupo creado exitosamente!");
                return RedirectToAction("Index");
            }

            TempData["result"] = new KeyValuePair<string, string>("warning","Error creando grupo de adquisición. Datos ingresados no válidos." );
            return RedirectToAction("Create", new { acquisition.type});
        }

        // GET: Telemetry/Acquisitions/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Result = TempData["result"];
            db.Configuration.ProxyCreationEnabled = false; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas

            ViewBag.ControllerName="Acquisitions";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Acquisition_Item> acquisition_Items = db.Acquisition_Item.Include(ai => ai.Quantity)
                                                                          //Debe Agregarse el resto de Includes, cuando se creen
                                                                          .Include(ai => ai.Tag_Line)
                                                                          .ToList();

            Acquisition acquisition = db.Acquisition.Include(a => a.Acquisition_Items).FirstOrDefault(a => a.id == id);

            AcquisitionViewModel acquisitionViewModel = new AcquisitionViewModel() { acquisition = acquisition, acquisition_Items = acquisition_Items };

            if (acquisition == null)
            {
                return HttpNotFound();
            }        
            
            return View(acquisitionViewModel);
        }

        // POST: Telemetry/Acquisitions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,type,Name")] Acquisition acquisition, int[] SelectedAI)
        {
            acquisition.Acquisition_Items = null;
            if (!SelectedAI.Any())
            {
                TempData["result"] = new KeyValuePair<string, string>("warning", "No se seleccionó ninguna opción. Favor seleccionar.");
                return RedirectToAction("Edit", new { acquisition.id });
            }
            if (acquisition.Name == null)
            {
                TempData["result"] = new KeyValuePair<string, string>("warning", "No se ingresó nombre de grupo. Favor ingrese nombre.");
                return RedirectToAction("Edit", new { acquisition.id });
            }
            List<Acquisition_Item> acquisition_Items = db.Acquisition_Item.Where(ai => SelectedAI.Any(s => s == ai.Id)).ToList();
            acquisition.Acquisition_Items = acquisition_Items;

            ViewBag.ControllerName="Acquisitions";
            if (ModelState.IsValid)
            {
                db.Entry(acquisition).State = EntityState.Modified;
                db.SaveChanges();
                TempData["result"] = new KeyValuePair<string, string>("true","Grupo editado exitosamente." );
                return RedirectToAction("Index");
            }
            TempData["result"] = new KeyValuePair<string, string>("warning","Error editando grupo de adquisición. Datos ingresados no válidos." );
            return RedirectToAction("Edit", new { acquisition.id });
        }

        // GET: Telemetry/Acquisitions/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.ControllerName="Acquisitions";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition acquisition = db.Acquisition.Find(id);
            if (acquisition == null)
            {
                return HttpNotFound();
            }
            return View(acquisition);
        }

        // POST: Telemetry/Acquisitions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="Acquisitions";
            Acquisition acquisition = db.Acquisition.Find(id);
            acquisition.is_filed = true;
            db.Entry(acquisition).State = EntityState.Modified;
            db.SaveChanges();
            TempData["result"] = new KeyValuePair<string, string>("true","Grupo archivado con éxito." );
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

