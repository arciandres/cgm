﻿using CGM.Models.CGMModels;
using Hangfire;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace CGM.Areas.Telemetry.Controllers
{
    public class SettingsController : Controller
    {
        // GET: Telemetry/Settings
        public ActionResult Index()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.newconfig = TempData["newconfig"];

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
               ViewBag.DiasAtras =                 db.Config_CM.Where(config => config.element.Trim().Equals("DiasAtras")).FirstOrDefault().value ;
               ViewBag.ProcesadoresSimultaneos =   db.Config_CM.Where(config => config.element.Trim().Equals("ProcesadoresSimultaneos")).FirstOrDefault().value ;
               ViewBag.RetriesDefault =            db.Config_CM.Where(config => config.element.Trim().Equals("RetriesDefault")).FirstOrDefault().value ;
               ViewBag.Sobreescritura =            db.Config_CM.Where(config => config.element.Trim().Equals("Sobreescritura")).FirstOrDefault().boolean ;
            }

            return View();
        }
        [HttpPost]
        public ActionResult Index(int DiasAtras, int ProcesadoresSimultaneos, int RetriesDefault, int? Sobreescritura)
        {
            bool sobreescritura = Sobreescritura == null ? false : true;
            bool restartServer = false;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                bool[] restart = new bool[] { changeConfigValue("ProcesadoresSimultaneos", ProcesadoresSimultaneos, db), //Parámetros que generan reinicio del servidor Hangfire
                                                changeConfigValue("RetriesDefault", RetriesDefault, db)};

                //No generan reinicio del servidor
                bool[] norestart = new bool[] { changeConfigValue("DiasAtras", DiasAtras, db),
                                                changeConfigValue("Sobreescritura", Sobreescritura, db)};

                restartServer = restart.Contains(true) ? true : false;
                if (restart.Contains(true) || norestart.Contains(true))
                {
                    db.SaveChanges();
                }
            }
            TempData["newconfig"] = restartServer;
            //System.Web.HttpRuntime.UnloadAppDomain();

            TempData["result"] = new KeyValuePair<string,string>("true", "Configuración guardada con éxito. Los cambios tomarán algunos segundos en aplicarse.");
            return RedirectToAction("Index");
        }

        private bool changeConfigValue(string configElement, int? formConfig, CGM_DB_Entities db) //Solamente para parámetros que generan reinicio del servidor de Hangfire
        {
            if (configElement.Equals("Sobreescritura"))
            {
                bool sobreescritura = formConfig == null ? false : true;
                if (db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault().boolean.Value != sobreescritura)
                {
                    db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault().boolean = sobreescritura;
                    return true; //Irrelevante
                }
                return false;
            }
            else
            {
                bool restartServer = false; ////Solamente relevante para parámetros que generan reinicio del servidor de Hangfire
                int value = (int) db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault().value.Value;

                if (value != formConfig)
                {
                    restartServer = true; //No reinicia para DiasAtras
                    db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault().value = formConfig;
                }
                return restartServer;
            }
        }

        [HttpPost]
        public string NewConfigActions()
        {
            bool disposeHangfireServers = DisposeHangfireServers();
            HttpRuntime.UnloadAppDomain();
            return disposeHangfireServers.ToString();
        }

        internal static bool DisposeHangfireServers()
        {
            try
            {
                var type = Type.GetType("Hangfire.AppBuilderExtensions, Hangfire.Core", throwOnError: false);
                if (type == null) return false;

                var field = type.GetField("Servers", BindingFlags.Static | BindingFlags.NonPublic);
                if (field == null) return false;

                var value = field.GetValue(null) as ConcurrentBag<BackgroundJobServer>;
                if (value == null) return false;

                var servers = value.ToArray();

                foreach (var server in servers)
                {
                    // Dispose method is a blocking one. It's better to send stop
                    // signals first, to let them stop at once, instead of one by one.
                    server.SendStop();
                }

                foreach (var server in servers)
                {
                    server.Dispose();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}