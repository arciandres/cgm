﻿using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using CGM.Areas.Telemetry.Models.MyModels;
using System.Threading.Tasks;
using CGM.Helpers;
using Hangfire;
using System.Net;

namespace CGM.Areas.Telemetry.Controllers
{
    public class ReadingController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        // GET: Telemetry/Reading
        async public Task<ActionResult> Index()
        {
            ViewBag.Result = TempData["result"];
            //ReadingViewModel rvm = await ReadingViewModel.BuildReadingViewModel();
            List<Meter> meters = db.Meter.Where(m => !m.is_filed).ToList();
            return View(meters);
        }

        /// <summary>
        /// Formulario para realizar interrogación a medidores
        /// </summary>
        /// <param name="radioMeter">0:Individual; 1:Filtro</param>
        /// <param name="meterOrFilter">[0]MeterId; [1] FiltroId</param>
        /// <param name="checkAquisitions">[0]Perfil de Carga; [1]Registros; [2]Eventos; [3]Forma de Onda</param>
        /// <param name="selAquisitions">Id de Acquisition</param>
        /// <param name="datetimepicker">[1]DateStart;[2]DateEnd</param>
        /// <param name="chkRangoFecha">Hay rango?</param>
        /// <returns></returns>
        [HttpPost]
        async public Task<ActionResult> Index(int dest, int item, int acquisition_id, string[] datetimepicker, int[] chkRangoFecha)
        {

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Interroga a un medidor por medio de la web API
        /// </summary>
        /// <param name="ip">La ip del medidor a leer</param>
        /// <param name="fecha">La fecha de la solicitud</param>
        /// <returns>"ok" si todo salió bien</returns>
        async public Task<string> CallMethodinWebAPI(string ip, string fecha)
        {

            WebClient client = new WebClient();

            client.QueryString.Add("ip", ip);
            client.QueryString.Add("fecha", fecha);
            //var response = client.UploadValues("http://localhost/CGMAPI/api/Protocol/ReadMeter", "POST", client.QueryString);
            // return response.ToString();
            return "ok";
        }
        async public Task<string> ExampleLoadingFromExcel()
        {
            Array dataExcel = ExcelManagement.GetDataFromExcel("D:/GoogleDrive/Proyectos/CGMPro/Common/Archivos/ExampleData/TestMeter1.xlsx", 1, "B8:Y31");

            Measure measure = new Measure();
            DateTime fecha = new DateTime();
            List<Measure> measures = new List<Measure>();
            fecha = DateTime.Now.Date;
            for (int i = 1; i <= dataExcel.GetLength(0); i++) //El array arranca desde 1,1 y no 0,0
            {
                measure = new Measure()
                {
                    measure1 = Double.Parse(dataExcel.GetValue(1, i).ToString()),
                    measure_date = fecha.AddHours(i)
                };
                measures.Add(measure);
            }
            Measure_Group measure_Group = new Measure_Group()
            {
                acquisition_item_id = 1,
                Measures = measures
            };

            int report_id = DataAndDB.insertReports(new Report()
            {
                is_active = false,
                date_and_time_to_report = DateTime.Now,
                date_and_time_report = DateTime.Now,
                is_auto = false, //0 = Manual, 1 = Automático
                external_report_id = "",
                time_elapsed = null,
                status_id = 5
            });

            int meterId = 5;

            Report_Inform reportInform = new Report_Inform()
            {
                meter_id = meterId,
                boundary_id = db.Meter.Where(m => m.id ==  meterId).Select(m => m.boundary_id).FirstOrDefault(),
                substatus_id = 500,
                is_backup = db.Meter.Where(m => m.id == meterId).Select(m => m.is_backup).FirstOrDefault(),
                reports_id = report_id,
                Measure_Groups = new List<Measure_Group>() { measure_Group }
            };
            long ri_id = DataAndDB.InsertReportInforms(reportInform);
            return "Todo en orden " + ri_id;
        }
    }
}