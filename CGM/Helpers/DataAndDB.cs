﻿using CGM.Models;
//using CGM.Models.MyModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using CGM.Controllers.CGM;
using System.Threading.Tasks;
using CGM.Areas.Telemetry.Models;

namespace CGM.Helpers
{
    public class DataAndDB
    {

        private CGM_DB_Entities db = new CGM_DB_Entities();

        public static string pathCR = "";
        public static string pathResults = "";
        public static string MensajeView = "";
        public static List<Acquisition_Item> acquisition_Items = GetAcquisition_Items();
        /// <summary>
        /// Regresa un conjunto de informes con los grupos de medidas para adquirir, listo para consignar las medidas.
        /// </summary>
        /// <param name="radioMeter">Determina si es solamente un medidor o varios (Filtro)</param>
        /// <param name="meterOrFilter">[0]IdMeter; [1]IdFiltro. Depende de radioMeter para tener en cuenta alguno de los dos</param>
        /// <param name="checkAquisitions">0:Perfil de carga, 2: Registros, 3: Eventos, 4: Forma de onda</param>
        /// <param name="selAquisitions" ></param>
        /// <returns></returns>
        internal static List<Report_Inform> getReportInforms(int radioMeter, int[] meterOrFilter, int[] checkAquisitions, int[] selAquisitions)
        {
            CGM_DB_Entities db = new CGM_DB_Entities();
            List<Report_Inform> report_Informs = new List<Report_Inform>();
            List<Acquisition_Item> acquisition_Items = new List<Acquisition_Item>();

            if (checkAquisitions.Any(chk => chk == 0)) //Perfil de carga
            {
                int selAq = selAquisitions[0];
                acquisition_Items = db.Acquisition_Item.Where(aci => aci.Acquisitions.Any(ac => ac.id == selAq)).ToList();
            }

            List<Measure_Group> measure_Groups = getGroupMeasures(acquisition_Items);
            if (radioMeter == 0) //Un sólo medidor
            {
                report_Informs.Add(new Report_Inform()
                {
                    meter_id = meterOrFilter[0],
                    Measure_Groups = measure_Groups,
                    substatus_id = 100,//Pendiente
                });
                return report_Informs;
            }
            if (radioMeter == 1) //Varios medidores desde el filtro.
            {
                int meterOrFil = meterOrFilter[1];
                int[] idMeters = db.Meter.Where(m => m.Filtros.Any(f => f.Id == meterOrFil)).Select(m => m.id).ToArray();
                
                foreach (var item in idMeters)
                {
                    report_Informs.Add(new Report_Inform()
                    {
                        meter_id = item,
                        Measure_Groups = measure_Groups,
                        substatus_id = 100, //Pendiente
                    });
                }
                return report_Informs;
            }
            return report_Informs;
        }
        /// <summary>
        /// Regresa una lista de grupos de medidores a partir de los acquisitionItems.
        /// </summary>
        /// <param name="acquisition_Items"></param>
        /// <returns></returns>
        private static List<Measure_Group> getGroupMeasures(List<Acquisition_Item> acquisition_Items)
        {
            List<Measure_Group> measure_Groups = new List<Measure_Group>();
            foreach (var item in acquisition_Items)
            {
                measure_Groups.Add(new Measure_Group() { acquisition_item_id = item.Id });
            }
            return measure_Groups;
        }
        
        internal async static Task<KeyValuePair<string, string>> InsertReportElementSet(ReportElementSet repElemSet, int destination)
        {
            try
            {
                int report_id = insertReports(new Report()
                {
                    is_active = false,
                    date_and_time_to_report = DateTime.Now,
                    date_and_time_report = DateTime.Now,
                    is_auto = false, //0 = Manual, 1 = Automático
                    external_report_id = "",
                    time_elapsed = null,
                    status_id = 5,
                    operation_date = repElemSet.dateReport
                    
                });


                List<Measure> measures = new List<Measure>();

                foreach (ReportElement rp in repElemSet.reportElements)
                {
                    Report_Inform reportInform = new Report_Inform()
                    {
                        boundary_id = rp.boundary.id,
                        substatus_id = 500,
                        is_backup = rp.isBackup,
                        reports_id = report_id
                    };

                    long ri_id = InsertReportInforms(reportInform);

                    Measure_Group measure_Group = new Measure_Group()
                    {
                        acquisition_item_id = repElemSet.acquisition_item_id,
                        Measures = rp.measures,
                        report_inform_id = ri_id
                    };
                    long gm_id = InsertMeasure_Group(measure_Group);
                }
                return new KeyValuePair<string, string>("true", "Conjunto de medidas insertadas con éxito.");
            }
            catch (Exception ex)
            {
                return new KeyValuePair<string, string>("false", "Hubo un problema con el registro en base de datos. Inténtelo nuevamente. Si el problema persiste, contácte los administradores del sitio.");
            }
        }

        public static int insertReports(Report report)
        {
            string user_id = getCurrentUser();

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                report.asp_net_users_id = user_id;

                try
                {
                    db.Reports.Add(report);
                    db.SaveChanges();
                    return report.id;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
        /// <summary>
        /// Short script that returns the current user in the case of login, otherwise the Developer mail (Just to add anyone, but login has to be asured). 
        /// </summary>
        /// <returns></returns>
        public static string getCurrentUser()
        {
            string user_id = "";
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                if (System.Web.HttpContext.Current == null)
                {
                    // If no login is made, give the authentication to the first Developer.
                    user_id = db.AspNetRoles.Where(r => r.Name == "Desarrollador").FirstOrDefault().AspNetUsers.FirstOrDefault().Id;
                }
                else
                {
                    var user = db.AspNetUsers.Where(model => model.UserName == System.Web.HttpContext.Current.User.Identity.Name).FirstOrDefault();
                    user_id = user.Id;
                }
            }
            return user_id;
        }

        public static long InsertReportInforms(Report_Inform reportInform)
        {
            CGM_DB_Entities db = new CGM_DB_Entities();

            try
            {
                db.Report_Inform.Add(reportInform);
                db.SaveChanges();
                return reportInform.id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static long InsertMeasure_Group(Measure_Group measure_Group)
        {
            CGM_DB_Entities db = new CGM_DB_Entities();

            try
            {
                db.Measure_Group.Add(measure_Group);
                db.SaveChanges();
                return measure_Group.id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async static Task<string> InsertMeasures(List<Measure> ms)
        {
            if (ms == null) return "Array empty";
            CGM_DB_Entities db = new CGM_DB_Entities();
            try
            {
                db.Measure.AddRange(ms);
                await db.SaveChangesAsync();
                return "ok";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static string insertReportInforms(List<Report_Inform> m)
        {
            if (m == null) return "Array empty";
            CGM_DB_Entities db = new CGM_DB_Entities();
            List<Report_Inform> ReportInformlist = new List<Report_Inform>();
            //List<Measure> measuresupdate = new List<Measure>();
            foreach (var item in m)
            {
                try
                {
                    Report_Inform myinform = db.Report_Inform.Where(model => model.boundary_id == item.boundary_id && model.reports_id == item.reports_id).FirstOrDefault();
                    if (myinform == null)
                        ReportInformlist.Add(item);
                    else
                    {
                        //measuresupdate.Add(mymeasure);
                        myinform.message = item.message;
                        myinform.substatus_id = item.substatus_id;
                        //mymeasure.ProcessID = item.ProcessID;
                        db.Entry(myinform).State = EntityState.Modified;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            db.Report_Inform.AddRange(ReportInformlist);
            try
            {
                db.SaveChanges();
                //UpdateMeasure(measuresupdate);
            }
            catch (Exception)
            {
                return "Bad";
            }
            return "OK";
        }

        /// <summary>
        /// Basic template to insert an event. Takes an object as input, and all relevant information is parsed as JSON
        /// </summary>
        /// <param name="details">Object to be parsed as JSON. In case "Changes", the original object, and the changes should be added</param>
        /// <param name="category">
        ///1:Creación       ;
        ///2:Modificación   ;
        ///3:Eliminación    ;
        ///4:Reporte        ;
        ///5:Carga de datos ;
        ///6:Archivo        ;
        /// </param>
        /// <param name="obj_id">El identificador del objeto afectado en el evento</param>
        /// <param name="table">Tabla del objeto afectado en el evento</param>
        /// <returns></returns>
        public static string insertEvents (Object details, int category, string obj_id, string table)
        {
            //Example: DataAndDB.insertEvents(new { elementoPrevio = previousObj, elementoModificado = meter, cambios = OtherMethods.getObjectChangesFromDB(previousObj, meter) }, 2, meter.id.ToString(), meter.GetType().BaseType.Name); //2 = Modificación, en Event_Category

            CGM_DB_Entities db = new CGM_DB_Entities();
            Event m = new Event()
            {
                asp_net_users_id = DataAndDB.getCurrentUser(),
                event_category_id = category,
                details = OtherMethods.objectToJSON(details),
                table =  table,
                object_id = obj_id,
                event_date = DateTime.Now,
            };

            try
            {
                db.Event.Add(m);
                db.SaveChanges();
                return "OK";
            }
            catch (Exception ex) 
            {
                throw; // Registers the error in the conventional NLog handler
            }
        }

        public static string insertEditEvent(Object objPrev, Object objNew , int id)
        {
            //2 = Modificación, en Event_Category
            return insertEvents(new { elementoPrevio = objPrev, elementoModificado = objNew, cambios = OtherMethods.getObjectChangesFromDB(objPrev, objNew) }, 2, id.ToString(), objNew.GetType().BaseType.Name);

        }
               
        internal static string GenerateAndInsertReportInforms(Report report, List<Boundary> fronteras)
        {
            CGM_DB_Entities db = new CGM_DB_Entities();
            List<Report_Inform> InformList = new List<Report_Inform>();

            foreach (var boundary in fronteras)
            {
                InformList.Add(new Report_Inform()
                {
                    boundary_id = boundary.id,
                    reports_id = report.id
                });
            }
            return insertReportInforms(InformList);
        }

        public static void logError(Exception ex) {
            ErrorHandlerController.LoggerFile(ex, true); //1 = Enviar por correo a desarrollador
            ErrorHandlerController.LoggerDB(ex);
        }

        /// <summary>
        /// Initializes a list of acq_items 
        /// </summary>
        /// <returns></returns>
        public static List<Acquisition_Item> GetAcquisition_Items() {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                return db.Acquisition_Item.Include(a => a.Quantity).Include(a => a.Tag_Line).ToList();
            }
        }

        /// <summary>
        /// Simple method to return a stylized name for the acquisition item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetAcquisition_ItemsName(int id)
        {
            var acq_item = acquisition_Items.Where(a => a.Id == id).FirstOrDefault();
            return acq_item.Quantity.name + " - " + acq_item.Tag_Line.name;
        }



    }
}