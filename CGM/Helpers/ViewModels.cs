﻿using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Helpers
{
    public class ViewModels
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ri"></param>
        /// <param name="whatfor">
        /// 0: Normal (Horizontal)
        /// 1: Details (Verticar)
        /// </param>
        /// <returns></returns>
        public static SimpleBoundary ReportInformToSimpleBoundary(Report_Inform ri, int whatfor) //Método para convertir y dar un formato más sencillo a los datos que se van a la vista.
        {
            Meter meter = ri.Boundary.Meters.FirstOrDefault();
            SimpleBoundary sb = new SimpleBoundary()
            {
                code = ri.Boundary.code,
                name = ri.Boundary.name,
            };
            if (ri.is_backup == null)
            {

            }
            else
            {
                sb.Meter = meter.Model_Meter.brand + "- #" + meter.serial;
                if (meter.is_backup==true){ sb.Tipo = "Respaldo"; }
                else{ sb.Tipo = "Principal"; }
            }

            if (whatfor == 1) //Details (Verticar)
            {
                sb.measures = new List<SimpleMeasure>();
                foreach (var gm in ri.Measure_Groups)
                {
                    foreach (var item in gm.Measures)
                    {
                        sb.measures.Add(new SimpleMeasure() { measure = item.measure1, time = item.measure_date, quantity = gm.Acquisition_Item.Quantity.name, unit = gm.Acquisition_Item.Quantity.Magnitude.unit_symbol });
                    }
                }
            }
            if (whatfor == 0)
            {
                foreach (Measure_Group gm in ri.Measure_Groups)
                {
                    SimpleQuantity sq = new SimpleQuantity() { name = gm.Acquisition_Item.Quantity.name, shortname = gm.Acquisition_Item.Quantity.name_short };
                    sq.measures = measuresToSMeasures(gm.Measures);
                }
            }
            return sb;
        }

        public static List<SimpleBoundary> ReportInformToSimpleBoundary(List<Report_Inform> report_Informs, int whatfor) 
        {
            List<SimpleBoundary> simpleBoundaries = new List<SimpleBoundary>();

                foreach (Report_Inform ri in report_Informs)
                {
                    Meter meter = ri.Boundary.Meters.FirstOrDefault();
                    SimpleBoundary sb = new SimpleBoundary()
                    {
                        code = ri.Boundary.code,
                        name = ri.Boundary.name,
                        Meter = meter.Model_Meter.brand + "- #" + meter.serial,
                    };
                    if (meter.is_backup == true) { sb.Tipo = "Respaldo"; }
                    else { sb.Tipo = "Principal"; }

                    if (whatfor == 1) //Details (Vertical)
                    {
                        sb.measures = new List<SimpleMeasure>();
                        foreach (var gm in ri.Measure_Groups)
                        {
                            foreach (var item in gm.Measures)
                            {
                                sb.measures.Add(new SimpleMeasure() { measure = item.measure1, time = item.measure_date, quantity = gm.Acquisition_Item.Quantity.name});
                            }
                        }
                    }
                    if (whatfor == 0) //Horizontal
                    {
                        sb.Quantities = new List<SimpleQuantity>();
                        foreach (Measure_Group gm in ri.Measure_Groups)
                        {
                            SimpleQuantity sq = new SimpleQuantity() { name = gm.Acquisition_Item.Quantity.name, shortname = gm.Acquisition_Item.Quantity.name_short };
                            sq.measures = measuresToSMeasures(gm.Measures);
                            sb.Quantities.Add(sq);
                        }
                    }
                    
                    simpleBoundaries.Add(sb);
                }
            
            
            return simpleBoundaries;
        }

        private static List<SimpleMeasure> measuresToSMeasures(ICollection<Measure> measures)
        {
            List<SimpleMeasure> sms = new List<SimpleMeasure>();
            foreach (Measure m in measures)
            {sms.Add(new SimpleMeasure() {measure = m.measure1, time = m.measure_date });}
            return sms;
        }
    }
}