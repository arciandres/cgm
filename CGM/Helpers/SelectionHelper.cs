﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using CGM.Models.CGMModels;
using CGM.Areas.Telemetry.Models;
using System;

namespace CGM.Helpers
{
    public class SelectHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="meterId"></param>
        /// <param name="returnAliases">True for return alias name in answer options for radiobuttons. Added for building csv from configurations</param>
        /// <returns></returns>
        public static Dictionary<string, string> getMeterParameters(int meterId, bool returnAliases)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                Meter meter = db.Meter.Find(meterId);
                Dictionary<string, string> parameters = new Dictionary<string, string>();

                List<ParamAnswer> answers = db.ParamAnswers.Where(a => a.formInstance_id == meter.formConfiguration_id).ToList();

                if (answers.Count == 0)
                    return null;

                var answerOptions = db.ParamAnswerOptions.Include(a => a.Question).ToList();

                foreach (var answer in answers)
                {
                    // Get the answerOption. We don't get it from the object directly
                    // in case the answerOptions object is not included in the current answer(lazy load)
                    var ansOption = answerOptions.Where(a => a.id == answer.answerOption_id).FirstOrDefault();

                    if (ansOption.type_id == 4) // Radio Button
                    {
                        if (returnAliases)
                        {
                            parameters.Add(ansOption.Question.alias, ansOption.alias);
                        }
                        else
                        {
                            parameters.Add(ansOption.Question.alias, ansOption.name);
                        }
                    }
                    else
                    {
                        parameters.Add(ansOption.alias, answer.value);
                    }
                }
                return parameters;
            }
        }

        public static List<Tuple<int, Dictionary<string, string>>> getMeterParameters(int[] meterIds, bool returnAliases)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                List<Meter> meters = db.Meter.Where(m => !m.is_filed).Where(m => meterIds.Contains(m.id)).ToList();

                List<Tuple<int, Dictionary<string, string>>> parametersAll = new List<Tuple<int, Dictionary<string, string>>>();

                int[] formConfiguration_ids = meters.Select(m => m.formConfiguration_id.Value).ToArray();

                List<ParamAnswer> answersAll = db.ParamAnswers.Where(a => formConfiguration_ids.Contains(a.formInstance_id)).ToList();
                var answerOptions = db.ParamAnswerOptions.Include(a => a.Question).ToList();
                                             
                foreach (var meter in meters)
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();

                    var answers = answersAll.Where(a => a.formInstance_id == meter.formConfiguration_id).ToList();

                    foreach (var answer in answers)
                    {
                        // Get the answerOption. We don't get it from the object directly
                        // in case the answerOptions object is not included in the current answer(lazy load)
                        var ansOption = answerOptions.Where(a => a.id == answer.answerOption_id).FirstOrDefault();

                        if (ansOption.type_id == 4) // Radio Button
                        {
                            if (returnAliases)
                            {
                                parameters.Add(ansOption.Question.alias, ansOption.alias);
                            }
                            else
                            {
                                parameters.Add(ansOption.Question.alias, ansOption.name);
                            }
                        }
                        else
                        {
                            parameters.Add(ansOption.alias, answer.value);
                        }
                    }

                    parametersAll.Add(new Tuple<int, Dictionary<string, string>>(meter.id, parameters));

                }

                return parametersAll;

            }
        }

    }
}