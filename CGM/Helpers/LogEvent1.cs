﻿using CGM.Models.CGMModels;
using Microsoft.AspNet.Identity;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;

namespace CGM.Helpers
{
    /// <summary>
    /// This partial class handles the methods included in the NLog configuration.
    /// </summary>
    public partial class LogEventHelper 
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void MailEventDeveloper(string date, string level, string loggerCode, string user, string url, string machine, string message, string exception)
        {
            List<Config_CM> conf;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                conf = db.Config_CM.ToList();
            }

            string nl = Environment.NewLine;
            string logEvent = "Información del error" + nl + nl +
                              date + nl +
                              level + nl +
                              loggerCode + nl +
                              "USER: " + user + nl +
                              "URL: " + url + nl +
                              "DEVICE: " + machine + nl +
                              "MESSAGE: " + message + nl + 
                              "EXCEPTION: " + exception;

            try
            {
                MailManagement.EnviarCorreoAhora
                   (
                     conf.Where(c => c.element.Contains("CorreoCGM")).FirstOrDefault().text,
                     StringCipher.Decrypt(conf.Where(c => c.element == "PasswordCorreoCGM").FirstOrDefault().text, "nardidelucabothworkatsapienzauniversitadiroma"),
                     "Error en software CGM - Cliente",
                     logEvent,
                     new List<string>() { conf.Where(c => c.element.Contains("DeveloperMail")).FirstOrDefault().text },
                     null,
                     false
                   );
                Debug.WriteLine("Send to debug output.");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex,"Error al momento de enviar el correo dentro del método de Log. Se pudo haber perdido la conexión a internet.");
                //throw;
            }
        }
        public void MailEventCustomer(string date, string level, string logger, string user, string url, string machine, string message, string exception)
        {
            List<Config_CM> conf;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                conf = db.Config_CM.ToList();
            }

            string nl = Environment.NewLine;
            string logEvent = "Información del error" + nl + nl+ 
                              date + nl +
                              level + nl +
                              logger + nl +
                              "Usuario: " + user + nl +
                              "URL: " + url + nl +
                              "Equipo: " + machine + nl +
                              "Mensaje: " + message;

            MailManagement.EnviarCorreoAhora
            (
              conf.Where(c => c.element == "CorreoCGM").FirstOrDefault().text,
              StringCipher.Decrypt(conf.Where(c => c.element == "6").FirstOrDefault().text, "nardidelucabothworkatsapienzauniversitadiroma"),
              "Error en software CGM - Cliente",
              logEvent,
              new List<string>() { conf.Where(c => c.element == "CorreoCGM").FirstOrDefault().text },
              null,
              false
            );
            Debug.WriteLine("Send to debug output.");

        }
    }
}
