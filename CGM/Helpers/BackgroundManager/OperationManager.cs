﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using CGM.Areas.Telemetry.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Hangfire;
using Hangfire.States;
using CGM.Helpers.Attributes;
using System.Xml.Serialization;
using System.IO;
using NLog;
using CGM.Areas.Telemetry.Controllers.API;
using CGM.Controllers.CGM;

namespace CGM.Helpers.BackgroundManager
{
    public partial class OperationManager
    {
        //public static string urlbase = "http://localhost/CGMAPI/"; //Servidor local
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string checkDailyPrograms()
        {
            System.Threading.Thread.Sleep(5000);
            return "OK";
        }

        public static async Task<string> resultCheck(Report_Inform ri, HttpClient client)
        {
            //logger.Info("Inicio de lectura con identificador" + repinf);
            //throw new CGMResultException(re)
            //registerResultinText(result);
            Dictionary<int, int> statusDict = new Dictionary<int, int>();
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                client.BaseAddress = new Uri(db.Config_CM.Where(c => c.element.Trim().Equals("ipapi")).FirstOrDefault().text);
                statusDict = db.Substatus.ToList().ToDictionary(s => s.id, s => s.idStatus);
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            logger.Info("Se envía petición a servicio interno de conexión a medidores.");
            var response = client.GetAsync("api/Protocol/RunIndividualAcquisition" + "/?" +
                                                  "repinf_id=" + ri.id // Se agrega más argumentos con "&" 
                                                  );
            response.Wait();

            var result = response.Result;


            if (result.IsSuccessStatusCode)
            {
                logger.Debug("Conexión con servicio interno exitosa.");
                string r = await result.Content.ReadAsStringAsync();
                var check = r.Split(new char[] { ',', '"' });
                int statusResponse = int.Parse(check[1]);
                int caseStatus = 0;

                // En el caso de que el API devuelva uno de estos números, se lo asigna directamente, pero esto es un error, porque no debe pasar.
                // Pero si no se hace hay un error porque el número no se encuentra en el diccionario, y la aplicación colapsa, y esto tampoco es aceptable.
                if ((new int[] { 3, 4, 5 }).Contains(statusResponse)) // Se puede hacer bifurcación, pero se deja así para poder hacer debug, porque este caso no debería presentarse nunca
                {
                    caseStatus = statusResponse;
                }
                else
                {
                    caseStatus = statusDict[statusResponse];
                }

                #region Envío de correos generados con alarmas.
                using (CGM_DB_Entities db = new CGM_DB_Entities())
                {
                    // Se adquiere las alarmas disparadas y se verifica si deben ser enviadas por correo o no.

                    List<AlarmFire> alarmFires = db.AlarmFires.Where(a => a.reportInformId == ri.id && 
                                                                          a.emailSent == 1 ).ToList(); // 1 = Must be sent by email, but haven't

                    if (alarmFires.Count != 0)
                    {
                        await AlarmHelper.ValorExcedido(alarmFires);
                        alarmFires.ForEach(a => a.emailSent = 2);
                        await db.SaveChangesAsync();
                    }
                }

                #endregion

                switch (caseStatus)
                {

                    case 5: //Adquisición exitosa

                        // Double check the fact that only these assigned readings are marked as assigned. Sometimes we're getting two sets of readings that ruin everything
                        using (CGM_DB_Entities db = new CGM_DB_Entities())
                        {
                            var previousReadings = db.Report_Inform.Where(rp => rp.boundary_id == ri.boundary_id //Que sea de la misma frontera
                                                           && rp.id != ri.id // Que no sea el mismo reporte que se está actualizando en este momento (obviamente) 
                                                           && rp.Report.operation_date.Value.CompareTo(ri.Report.operation_date.Value) == 0 //Que corresponda al mismo día
                                                           && rp.assignedReading.Value
                                                           && rp.is_backup.Value == ri.is_backup.Value).ToList(); //Que las medidas hayan sido aceptadas. Idealmente debería ser solamente cero o uno, diferente al actual.
                            previousReadings.ForEach(pr => pr.assignedReading = false); //Medidas ya no tenidas en cuenta
                            db.SaveChanges();
                        }                   

                        return "Lectura realizada con éxito";

                    case 4: //Advertencia. Error que genera repetición
                        throw new CGMResultException("[" + ri.Report.operation_date.Value.ToShortDateString() + "] " + check[2]); //Se devuelve el mensaje de error, de modo que se haga reintentos.
                    case 3: //Error que no genera repetición, como una contraseña errada.
                        //Este tipo de error hace que la frontera no se vuelva a leer. Por esta razón no devuelve una excepción.
                        if (statusResponse == 301) // Contraseña inválida
                        {
                            await Helpers.AlarmHelper.AcquisitionError( ri , "Contraseña incorrecta");
                        }

                        if (ri.job_id != null)
                        {
                            BackgroundJobClient jobclient = new BackgroundJobClient();
                            FailedState failedState = new FailedState(new CGMResultException("Error (no reintentar): " + check[2]));
                            var toFailed = jobclient.ChangeState(ri.job_id.Value.ToString(), failedState, ProcessingState.StateName);
                        }
                        return "Error de no repetición de adquisición. " + check[2];
                    default:
                        throw new CGMResultException(check[2]); //No entró a ninguno de los casos determinados, y se devuelve lo que quiera que se recibió, pero con reintento.
                }
            }
            else
            {
                logger.Warn("Conexión con servicio interno no exitosa.");
                Exception ex = new Exception("CGM non handled Error " + result.RequestMessage.RequestUri, null);
                throw ex;
            }
        }

        private static void registerResultinText(HttpResponseMessage result)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(result.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, result);
                string text = textWriter.ToString();

                FileManagement.WriteTXT(text, System.Web.HttpContext.Current.Server.MapPath("~//Files//ResulHttp.txt"));
            }
        }

        /// <summary>
        /// Se registra el reporte y se incluyen los informes de modo que se visualizan en el monitor.
        /// Se restringe algunas adiciones en caso de que algunas fronteras ya se encuentren ingresadas.
        /// El registro puede ser exitoso o no, dependiendo de las fronteras que se esté agregando
        /// Devuelve un Tuple con tres objetos así:
        /// 1. <string> Tipo de mensaje que se da al usuario: true, warning o false (error)
        /// 2. <string> El cuerpo del mensaje
        /// 3. <Report> El objeto de Reporte cuando se agregó fronteras a la programación.
        /// </summary>
        /// <param name="program_id"></param>
        /// <param name="opDay"></param>
        /// <param name="auto"></param>
        /// <returns></returns>
        public static Tuple<string, string, Report> registerReport(int program_id, string opDay, bool auto)
        {
            string msg = ""; // Mensaje que se regresa al usuario

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {

                Program p = db.Program.FirstOrDefault(pr => pr.id == program_id);
                Report report = new Report()
                {
                    destination_id = p.destination_id,
                    acquisition_id = p.acquisition_id,
                    is_active = true,
                    is_auto = auto,
                    status_id = 1, //Pendiente
                    program_id = p.id
                };

                #region Include Report_Informs
                List<Report_Inform> report_Informs = new List<Report_Inform>();

                #region Sólo medidores (Not used, maybe in future)
                if (p.destination_id == 0) //Sólo medidores
                {
                    var meters = db.Filtro.Where(f => f.Id == p.IdFiltro).Select(f => f.Meters).FirstOrDefault();
                    //var acquisition_items = db.Acquisition.Where(ac => ac.id == p.acquisition_id).Select(ac => ac.Acquisition_Items).FirstOrDefault();
                    foreach (var meter in meters)
                    {
                        report_Informs.Add(new Report_Inform()
                        {
                            meter_id = meter.id,
                            substatus_id = 100,
                        });
                    }
                }
                #endregion

                if (p.destination_id == 1 || p.destination_id == 2) //Fronteras y cargas a XM
                {
                    List<Boundary> boundariesInFilter = db.Filtro.Where(f => f.Id == p.IdFiltro).Select(f => f.Boundaries).FirstOrDefault();
                    List<Boundary> boundsToAdd = new List<Boundary>();
                    List<Boundary> boundsFiltro1 = new List<Boundary>();
                    int[] boundIds = boundariesInFilter.Select(b => b.id).ToArray();

                    // Validaciones y casos particulares
                    // 1. Adquisiciones no múltiples. 
                    if (p.destination_id == 1 && !p.multiple) //Sólo aplica para adquisiciones, y registros que no sean múltiples (modo continuo)
                    {
                        report.operation_date = DateTime.Parse(opDay);
                        report.date_and_time_to_report = p.execution_time;

                        ///////////////// Filtro 1:
                        // No se permite agregar dos informes de la misma frontera para el mismo día, a menos que se haya marcado como completado y no aceptado.

                        List <Boundary> boundariesToExclude = db.Report_Inform.Where(r => boundIds.Contains(r.boundary_id.Value) // Que corresponda a la actual petición
                                                            &&  r.Report.operation_date.Value.CompareTo(report.operation_date.Value) == 0 //Los reportes de la misma fecha
                                                            && (r.Substatus.idStatus == 1 || r.Substatus.idStatus == 2 ||/*(r.Substatus.idStatus == 4 && r.Job.StateName.Trim()!= "Failed") added by david */r.Substatus.idStatus == 4/**/ || r.Substatus.idStatus == 6 // Que esté programado, en proceso, para reintento o en cola (Debe estar terminado)
                                                            || (r.Substatus.idStatus == 5 && r.assignedReading.Value)) // Que tenga datos aceptados
                                                             ).Select(ri => ri.Boundary).Distinct().ToList();
                        int c = 0;
                        if (boundariesToExclude.Count != 0)
                        {
                            boundsFiltro1 = boundariesInFilter.Except(boundariesToExclude).ToList();
                            msg += "Las siguientes fronteras no van a ser agregadas a la instrucción, pues ya se encuentran en una instrucción activa, o ya contienen datos aceptados para el día de operación ("+ opDay +")."+ Environment.NewLine
                                + " (No. de fronteras: " + boundariesToExclude.Count + ")" + Environment.NewLine;
                            foreach (var boundary in boundariesToExclude)
                                msg += ++c + ". Código " + boundary.code + " - Nombre: " + boundary.name + Environment.NewLine;
                            c = 0;
                            if (boundsFiltro1.Count != 0)
                            {
                                msg += Environment.NewLine + "Se agregarán las siguientes fronteras (No. de fronteras: " + boundsFiltro1.Count + ")" + Environment.NewLine;
                                foreach (var boundary in boundsFiltro1)
                                 msg += ++c + ". Código " + boundary.code + " - Nombre: " + boundary.name + Environment.NewLine; 
                            }
                        }
                        else
                        {
                            boundsFiltro1 = boundariesInFilter;
                        }


                        ////////// Filtro 2
                        // El modo múltiple tiene prioridad, y no se permite agregar una frontera si esta ya está incluida en una programación múltiple

                        // Se selecciona las fronteras que estén en programas múltiples activos
                        // Normalmente son pocos elementos.

                        List<List<Boundary>> boundsInMultipleTemp = db.Program.Where(pr => !pr.filed && pr.multiple && pr.acquisition_id == 1)
                                                                               .Select(pr => pr.Filtro.Boundaries).ToList(); //Returns a list, not a single object
                        List<Boundary> boundsInMultiple = new List<Boundary>();
                        foreach (var item in boundsInMultipleTemp)
                            boundsInMultiple.AddRange(item);
                        boundsInMultiple=boundsInMultiple.Distinct().ToList();

                        if (boundsInMultiple.Count != 0)
                        {
                            boundsToAdd = boundsFiltro1.Except(boundsInMultiple).ToList();
                            boundariesToExclude = boundsFiltro1.Intersect(boundsInMultiple).ToList();

                            c = 0;
                            msg += "Las siguientes fronteras no van a ser agregadas a la instrucción, pues ya se encuentran en un programa múltiple." + Environment.NewLine
                                + " (No. de fronteras: " + boundariesToExclude.Count + ")" + Environment.NewLine;
                            foreach (var boundary in boundsInMultiple)
                                msg += ++c + ". Código " + boundary.code + " - Nombre: " + boundary.name + Environment.NewLine; 
                            c = 0;
                            if (boundsToAdd.Count != 0)
                            {
                                msg += Environment.NewLine + "Se agregarán las siguientes fronteras (No. de fronteras: " + boundsToAdd.Count + ")" + Environment.NewLine;
                                foreach (var boundary in boundsToAdd)
                                msg += ++c + ". Código " + boundary.code + " - Nombre: " + boundary.name + Environment.NewLine; 
                            }
                        }
                        else
                        {
                            boundsToAdd = boundsFiltro1;
                        }

                        if (boundsToAdd.Count == 0)
                        {
                            logger.Warn(msg);
                            return new Tuple<string, string, Report>("false", "No se creó ninguna instrucción. Mensaje: " + msg, null);
                        }
                        if (msg != "") logger.Info(msg);
                    }

                    // 2. Adquisiciones múltiples. 
                    else if (p.destination_id == 1 && p.multiple) // Si es un programa múltiple
                    {
                        // Whenever this is called, the command has to be executed right now
                        report.date_and_time_to_report = DateTime.Now;
                        report.operation_date = DateTime.Now.Date.AddDays(-1); // Por defecto, las medidas que se actualizarán serán del día anterior

                        // Renew all the informs that are included in the list of the program.
                        // If any inform is still active, gets erased, and a new one is added.

                        var activeInforms = db.Report_Inform.Where(r => boundIds.Any(i => i == r.boundary_id) // Que estén incluidos en la lista del programa
                                                                        && (r.Substatus.idStatus == 1 || r.Substatus.idStatus == 2 && r.Substatus.idStatus == 4) // Que esté en algún estado activo (En cola, procesando, por procesar)
                                                                        && r.job_id != null // Que tenga asignado un número de trabajo
                                                                  ).ToList();

                        if (activeInforms.Count > 0)
                        {
                            // id de jobs separados por comas. Se deja así pues es la petición que llega desde el browser a la API, y pues ya está lista.
                            var jobsInforms = String.Join(",", activeInforms.Select(ri => ri.job_id).ToArray());
                            string res = MonitorController.ToStateStatic(jobsInforms, 701); // 701: Eliminado manualmente desde instrucción múltiple
                        }

                        // Se define nuevos informes. Son los mismos del filtro, completos.
                        boundsToAdd = boundariesInFilter;

                        var boundariesCompleted = db.Report_Inform.Where(r => r.assignedReading.Value // Si ya tiene medidas asignadas y activas
                                                                           && r.Report.operation_date.Value.CompareTo(report.operation_date.Value) == 0 // Si la fecha es la misma
                                                                           ).Select(r => r.Boundary)
                                                                           .ToList();
                        if (boundariesCompleted.Count > 0)
                        {
                            if (boundariesCompleted.Count == boundariesInFilter.Count)
                            {
                                msg += "La totalidad de las fronteras en el filtro no se agregaron a la instrucción, debido a que ya contienen datos aceptados en la base de datos para esa fecha. (" + report.operation_date.Value.ToShortDateString() +")";
                                logger.Warn(msg);
                                return new Tuple<string, string, Report>("false", "No se creó ninguna instrucción. Mensaje: " + msg, null);
                            }

                            msg += "Las siguientes fronteras no se agregarán, pues ya cuentan con valores aceptados en la base de datos: " + Environment.NewLine;
                            foreach (var item in boundariesCompleted)
                                msg += "Código " + item.code + " - Nombre: " + item.name + Environment.NewLine;

                            boundsToAdd.Except(boundariesCompleted);
                        }
                    }

                    // 3. Reportes múltiples (No existen reportes de una sola instrucción ==> eso es reporte manual, y tiene su propio asistente). 
                    else if (p.destination_id == 2 && p.multiple)
                    {
                        boundsToAdd = boundariesInFilter;

                        // Filter the informs that are included in the program list

                        // La fecha debe definirse a diario

                        DateTime fecha = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                                        p.frequency.Value.Hours,
                                                        p.frequency.Value.Minutes,
                                                        p.frequency.Value.Seconds);
                        DateTime opDate = fecha;

                        // Se tiene en cuenta la hora del reporte comparada con la hora actual, 
                        // de modo que no se agregue un reporte para el mismo día, o una hora ya pasada
                        if (DateTime.Now.CompareTo(fecha) <= 0)
                        {
                            report.operation_date = fecha.AddDays(-p.daysBack.Value).Date;
                            report.date_and_time_to_report = fecha;
                        }
                        else
                        {
                            report.operation_date = fecha.AddDays(-(p.daysBack.Value - 1));
                            report.date_and_time_to_report = fecha.AddDays(1);
                        }
                    }

                    //var acquisition_items = db.Acquisition.Where(ac => ac.id == p.acquisition_id).Select(ac => ac.Acquisition_Items).FirstOrDefault();
                    foreach (var boundary in boundsToAdd)
                    {
                        if (p.destination_id == 1 && boundary.source_id == 15) //15 = Telemetría
                        {
                            var metersInBoundary = db.Meter.Where(m => m.boundary_id == boundary.id).ToList();

                            if (metersInBoundary.Count == 0) // Si no tiene medidr asignado, se ignora. (Todas las fronteras que se interrogan deberían tener un medidor asignado)
                                continue; 
                            
                            foreach (var meter in metersInBoundary)
                            {
                                if (meter == null) // Se agrega solamente los medidores activos
                                { continue; }
                                else
                                {
                                    Report_Inform ri = new Report_Inform()
                                    {
                                        boundary_id = boundary.id,
                                        substatus_id = 100, // 100 = Programado para ejecución
                                        meter_id = meter.id,
                                        is_backup = meter.is_backup
                                    };
                                    report_Informs.Add(ri);
                                }
                            }
                        }
                        else if (p.destination_id == 1 && boundary.source_id != 15) // != 15 Diferente a cualquier fuente que no sea telemetría
                        {
                            Report_Inform ri = new Report_Inform()
                            {
                                boundary_id = boundary.id,
                                substatus_id = 100, // 100 = Programado para ejecución
                            };
                            report_Informs.Add(ri);
                        }
                        if (p.destination_id == 2) //Reporte al mercado
                        {
                            Report_Inform ri = new Report_Inform()
                            {
                                boundary_id = boundary.id,
                                substatus_id = 100, // 100 = Programado para ejecución
                            };
                            report.market_id = db.Filtro.Where(f => f.Id == p.IdFiltro).Select(f => f.market_id).FirstOrDefault();
                            report_Informs.Add(ri);
                        }
                    }
                }

                #endregion
                if (report_Informs.Count == 0)
                {
                    msg = "No se agregó ninguna instrucción. Es posible que no haya ningún medidor asociado a la(s) frontera(s) seleccionadas.";
                    logger.Info(msg);
                    return new Tuple<string, string, Report>("false", msg, null);
                }

                report.Report_Informs = report_Informs;
                DataAndDB.insertReports(report);
                //if (p.multiple)
                //{
                //    scheduleInforms(report);
                //}
                if (msg != "")  //Si existió una operación indeseada y se incluyó un mensaje de error
                {
                    logger.Info(msg);
                    return new Tuple<string, string, Report>("warning", msg, report);
                }
                else
                {
                    return new Tuple<string, string, Report>("true", "Reporte creado con éxito.", report);
                }
            }
        }

        public static KeyValuePair<string, string> scheduleInforms(Report report)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                Program program = db.Program.Find(report.program_id);

                DateTime date_to_report = report.date_and_time_to_report.Value;

                try
                {
                    foreach (var repinf in report.Report_Informs)
                    {
                        string jobId = "";
                        Report_Inform report_Inform = db.Report_Inform.FirstOrDefault(ri => ri.id == repinf.id);
                        switch (report_Inform.Boundary.id_priority)
                        {
                            case 1: jobId = BackgroundJob.Schedule(() => 
                                    RunPrograms.RunProgramDefault(repinf.id), date_to_report); break;

                            case 2: jobId = BackgroundJob.Schedule(() => 
                                    RunPrograms.RunProgram2(repinf.id), date_to_report); break;

                            case 3: jobId = BackgroundJob.Schedule(() => 
                                    RunPrograms.RunProgram3(repinf.id), date_to_report); break;

                            case 4: jobId = BackgroundJob.Schedule(() => 
                                    RunPrograms.RunProgram4(repinf.id), date_to_report); break;

                            default:
                                break;
                        }
                        report_Inform.job_id = int.Parse(jobId);
                        //RecurringJob.AddOrUpdate(,,,)
                        //new BackgroundJobClient().
                    }
                    db.SaveChanges();
                    return new KeyValuePair<string, string>("true", "Programa creado con éxito.");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static bool checkRewrite(long repinf)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                //Se adquiere el parámetro de verificación desde la base de datos.
                bool checkRewrite = db.Config_CM.Where(config => config.element.Trim().Equals("Sobreescritura")).Select(config => config.boolean.Value).FirstOrDefault();
                if (checkRewrite) return true;
                else
                {
                    Report_Inform ri = db.Report_Inform.Find(repinf);

                    bool areTherePreviousReadings = db.Report_Inform.Any(r => r.boundary_id == ri.boundary_id //Que sea de la misma frontera
                                                     && r.Report.operation_date.Value.CompareTo(ri.Report.operation_date.Value) == 0 //Que corresponda al mismo día
                                                     && r.Substatus.idStatus == 5 //Que haya sido exitoso
                                                     && r.is_backup == ri.is_backup //Que sea principal o de respaldo igual al original.
                                                     && r.assignedReading.Value); //Que las medidas hayan sido aceptadas.

                    //!Existe comportamiento particular para dejar en claro. Siempre se tendría en cuanta las últimas medidas adquiridas. Si después no se trajo una cantidad que antes sí, esta ya no se tendría en cuenta.
                    //!Es decir. Para un día en particular, si ayer adquirí voltaje, pero hoy ya no lo adquiero, oficialmente no adquirí voltaje, y tendría que remplazar manualmente la adquisición por aquella que sí tiene voltaje.
                    //
                    //!Es bastante improbable que ese caso suceda, tendría que cambiarse la adquisición. Con la vista de visualización de medidas se tiene que considerar este caso particular, de modo que se pueda incluir todas las medidas, sin perder registros.


                    if (areTherePreviousReadings)
                    {
                        ri.message = "Registros ya adquiridos.";
                        ri.assignedReading = false;
                        ri.end_time = DateTime.Now;
                        RunPrograms.performReadingReponse = ri.message;
                        try
                        {
                            db.Entry(ri).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        return false;
                    }
                    else return true;
                }
            }
        }

        /// <summary>
        /// --- FILL DAYS ---
        /// Este método retorna los identificadores de reporte de cierta frontera que no estén llenos aún, 
        /// el número de días atrás que esté programado en la configuración.
        /// </summary>
        ///// <returns></returns>
        public static List<long> getrepInfs(long repinf, int fillDays)
        {
            List<long> repinfs = new List<long>();


            // Se interroga a la base de datos por fecha de operación, frontera y por P/R. 
            // Si el report_inform existe, se evalúa si ya tiene medidas, o aún no. Si no existe se lo crea.
            // Si ya tiene medidas, no se añaden a la lista para ser interrogadas nuevamente
            // pero si no las tiene, se añade para intentar traer medidas

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                Report_Inform ri = db.Report_Inform.Find(repinf);
                DateTime currentOperationDay = ri.Report.operation_date.Value;
                for (int i = 1; i <= fillDays; i++)
                {
                    DateTime dateEval = currentOperationDay.AddDays(-i);
                    List<Report_Inform> ris_eval = db.Report_Inform.Where(r => DbFunctions.TruncateTime(r.Report.operation_date) == dateEval
                                                                        && r.boundary_id == ri.boundary_id
                                                                        && r.is_backup == ri.is_backup
                                                                        && !(r.Substatus.idStatus == 7 || r.Substatus.idStatus == 3) // Eliminated and Errors are not considered
                                                                        
                                                                        ).ToList();


                    if (ris_eval.Count == 0) // Si el informe no existe, debe ser añadido a la lista
                    {
                        long ri_new = insertSingleRepInf(ri, dateEval);
                        repinfs.Add(ri_new);
                        continue;
                    }

                    // If the day has already an accepted set of measurements, do nothing. Pass.
                    if (ris_eval.Any(r => r.assignedReading.Value))
                    {
                        continue;
                    }

                    else // if not, we add it. There should be only one
                    {
                        // We work under the suposition that only ONE report_inform should be on pending o scheduled, or anywhere but Eliminated or Error.
                        // If the inform is on error, it should not be brought back, because the errors are situations that must be handled by the user by hand
                        repinfs.AddRange(ris_eval.Select(r => r.id).ToList());
                    }                    
                }
            }
            return repinfs;
        }


        /// <summary>
        /// This method registers a single report_inform as a copy of inserted as parameters, but changed on the operation date.
        /// The associated Program is the same. The report has to be added, if no matching Report exists.
        /// It is designed to be used when the exactQuery parameter is deactivated, and we have to retrieve the measurements of days before.
        /// </summary>
        /// <returns></returns>
        public static long insertSingleRepInf(Report_Inform ri, DateTime opDay)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                Report_Inform ri_new;
                Report rep = ri.Report;
                Report report = db.Reports.Where(r => DbFunctions.TruncateTime(r.operation_date) == opDay
                                                    && r.program_id == rep.program_id // Se asocia al mismo programa. De este modo no se mezcla con los programas de otros días.
                                                    && r.destination_id == rep.destination_id
                                                    && r.acquisition_id == rep.acquisition_id
                                                    ).FirstOrDefault();
                if (report == null) //En el API (ProtocolController/defaultReading) se describe cómo el informe se puede sobrescribir dependiendo de los resultados, pero esto no depende ni del programa ni del reporte (fecha de ejecución).
                {
                    report = new Report()
                    {
                        status_id = 1, //Programado
                        is_active = true,
                        date_and_time_to_report = DateTime.Now,
                        asp_net_users_id = rep.asp_net_users_id,
                        is_auto = rep.is_auto,
                        destination_id = rep.destination_id,
                        operation_date = opDay,
                        program_id = rep.program_id,
                        acquisition_id = rep.acquisition_id
                    };
                    db.Reports.Add(report);
                    db.SaveChanges();
                }

                ri_new = new Report_Inform()
                {
                    reports_id = report.id,
                    substatus_id = 100, // 100: Programado
                    boundary_id = ri.boundary_id,
                    is_backup = ri.is_backup,
                    meter_id = ri.meter_id,
                    assignedReading = false //No se asigna la medida.
                };
                
                db.Report_Inform.Add(ri_new);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    throw ex;
                }

                return ri_new.id;
            }
        }


        //Recurring jobs &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        public static string LoadRecurringPrograms()
        {
            //RecurringJob.AddOrUpdate(() => Console.Write("Hard!"), Cron.Daily(), null, "4" );
            //RecurringJob.AddOrUpdate(() => checkDailyPrograms(), "* * * * *",null ,"1");
            return "OK";
        }


    }
}