﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Helpers.BackgroundManager
{
    public class BackgroundManager
    {

        /// <summary>
        /// Este método se ejecuta todos los días a media noche, para incluir nuevamente las fronteras que tienen programa múltiple en la cola.
        /// Es totalmente para manejo del desarrollador, y la visualización de sus resultados no está disponible para el usuario.
        /// Esta función no aplica para reportes al mercado automáticos. Sólo para adquisiciones de medidores.
        /// </summary>
        /// <returns></returns>
        public string multipleProgramsAcquisitionsHandler()
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                List<Program> programs = db.Program.Where(p => p.multiple // Que sea múltiple
                                                           && !p.filed  // Que no esté archivado
                                                           && p.destination_id == 1 // Que sea de adquicisiones de medidas (no de reportes al mercado)
                                                           && !p.Filtro.isFiled).ToList(); // Que el filtro no esté archivado


                //if (programs.Count != 0)
                //{

                //    foreach (Program program in programs)
                //    {
                //        RecurringJob.AddOrUpdate(program.id.ToString(), () => OperationManager.registerReport(program.id, null, true),
                //                                                         OtherMethods.TimespanToCron(program.frequency.Value) //Conversión del timespan a Cron
                //                                                         , null, "4"); //Prioridad alta por defecto.
                //    }

                //}
                //else
                //{
                //    return "No se ha encontrado programas múltiples que dan ser activados.";
                //}
            }

            return "";
        }

        public string BackgroundProcessesManager()
        {
            return "";
        }

    }

}