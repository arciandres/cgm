﻿using CGM.Models.CGMModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Hangfire;
using CGM.Helpers.Attributes;
using CGM.Controllers.CGM;
using NLog;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;

namespace CGM.Helpers.BackgroundManager
{
    //[LogEverything]
    //Los reintentos están definidos en Startup.cs
    [ProlongExpirationTime]

    public class RunPrograms
    {
        public static string performReadingReponse;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        async public static Task<string> GeneralExecution (long repinf) //Se envía el id de ReportInform
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                Report_Inform ri = db.Report_Inform.Include(r => r.Report).Where(r => r.id == repinf).FirstOrDefault();
                logger.Info("Se comienza el proceso de ejecución de la lectura de frontera " + ri.Boundary.code + "(" + ri.Report.operation_date.Value.ToShortDateString() + ")");

                if (!OperationManager.checkRewrite(repinf))
                {
                    logger.Info("Se aborta la ejecución, pues la frontera o medidor ya tiene datos en esa configuración. " + repinf);
                    return performReadingReponse; //Checks the "Sobreescritura" field in config.
                }

                using (HttpClient client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(5);
                    try
                    {
                        string ans = await OperationManager.resultCheck(ri, client);
                        //Completar logs!
                        //NLog.LogEventInfo info = new LogEventInfo(LogLevel.Info, "Name", cus.Name);
                        //info.Properties.Add("Name", cus.Name);
                        //log.Log(info);
                        logger.Info(ans + ". Frontera: " + ri.Boundary.code + ". " + ri.Report.operation_date.Value.ToShortDateString());

                        return "[" + ri.Report.operation_date.Value.ToShortDateString() + "] " + ans;
                    }
                    catch (CGMResultException ex) { throw ex; } //Se registra el log en el catch afuera de esta función.
                    catch (Exception ex) //Se registra el log en el catch afuera de esta función.
                    {
                        logger.Warn(ex,"Se presentó un error. Rastrear la excepción en mensajes previos.");
                        //ErrorHandlerController.LoggerFile(ex, true);
                        //ErrorHandlerController.LoggerDB(ex);

                        if (ex.Message == "One or more errors occurred.") //Cuando la API está cerrada o apagada
                        {

                            //ri.substatus_id = 409; //Se realizó el intento de lectura, pero no fue exitoso, y se debe volver a hacer.
                            //db.Entry(ri).State = EntityState.Modified;
                            //db.SaveChanges();
                            try
                            {
                                if (ex.InnerException.Message == "An error occurred while sending the request.")
                                {
                                    if (ex.InnerException.InnerException.Message == "A connection with the server could not be established" ||
                                        ex.InnerException.InnerException.Message == "The operation timed out")
                                    {
                                        //ex.Data.Add("UserMessage", "Problemas internos de conexión con medidores. Contáctese con los administradores.");
                                        //throw ex;
                                        throw new CGMResultException("[" + ri.Report.operation_date.Value.ToShortDateString() + "] " +
                                                                    "Problemas internos de conexión con medidores. Contáctese con los administradores.");
                                    }
                                }
                            }
                            catch (Exception ex2)
                            {
                                throw ex2;
                            }
                            throw (ex);
                        }
                        
                        throw ex; // Si no entra a ninguno de los casos, se lanza la excepción pura, para que se incluya toda la información en el registro del error.
                        
                    }
                }
            }
        }

        [Queue("default")] 
        async public static Task<string> RunProgramDefault(long repinf) //Se envía el id de ReportInform
        {
            LogEventInfo info = new LogEventInfo(LogLevel.Info, "Boundary", "Ejecución de programa con prioridad regular.");
            info.Properties.Add("inform", repinf);
            logger.Log(info);

            bool exactQuery = false;
            float fillDays = 0;
            Report_Inform ri;
            DateTime currentReportDate;
            List<long> repInfs = new List<long>(); // Guarda los report_informs de los días anteriores
            repInfs.Add(repinf); // El propio informe original es el primero que se añade

            #region FILL DAYS
            //Este valor determina si se va a interrogar días atrás. 
            //Si es falso, se ejecuta solamente el informe del día presente.
            //Si es verdadero, se evalúa los informes de los días anteriores (fillDays días.)

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                ri = db.Report_Inform.Find(repinf);
                exactQuery = db.Config_CM.Where(c => c.element.Equals("exactQuery")).FirstOrDefault().boolean.Value;
                currentReportDate = ri.Report.operation_date.Value;
                if (!exactQuery)
                {   
                    fillDays = (int)db.Config_CM.Where(c => c.element.Equals("fillDays")).FirstOrDefault().value.Value;
                    List<long> previousRepInfs = OperationManager.getrepInfs(repinf, (int)fillDays); //Get the informs of previous days
                    repInfs.AddRange(previousRepInfs);
                    logger.Info(String.Format("Se realizan las lecturas desde el día {0} hasta {1}, bajo configuración establecida. Si ya se tiene medidas, no se sobreescribirán.",
                            currentReportDate.AddDays(-fillDays).ToShortDateString(), 
                            currentReportDate.ToShortDateString()));
                }
                else
                {
                    // Solo se interroga el día presente
                    logger.Info("Precisión en la solicitud desactivada. Se interroga solamente el día presente. (" + 
                                                   currentReportDate.ToShortDateString() + ")");
                }
            }
            #endregion
            try
            {
                string returningAnswer = ""; // Cummulative answer to return to the interface

                // Con el siguiente trozo de código se pretende que se levante la excepción solamente
                // si el primer elemento de la lista, que es el informe original, presenta excepción.
                // Si el primer elemento es exitoso, el job se calificará como exitoso y no se repetirá.
                // Se debe hacer este ciclo con try-catch, pues si uno de los primeros elementos levanta una
                // excepción, el resto no se interrogarían. El comportamiento deseado es que se intente traer
                // las medidas de todos, incluso si las fechas intermedias levantan errores.
                // Este caso se puede dar y hay que considerarlo.

                int raiseException = 0; // Cambia dependiendo del tipo de excepción
                for (int i = 0; i < repInfs.Count; i++)
                {
                    try
                    {
                        string ans = await GeneralExecution(repInfs[i]);
                        returningAnswer += ans + Environment.NewLine;
                        logger.Info(returningAnswer);

                    }
                    catch (CGMResultException ex)
                    {
                        returningAnswer += ex.Message + Environment.NewLine;
                        if (i == 0)
                            raiseException = 1;
                        logger.Warn(ex,returningAnswer);
                    }
                    catch (Exception ex)
                    {
                        returningAnswer += ex.Message + Environment.NewLine;
                        if (i == 0)
                            raiseException = 2;
                        logger.Error(ex,returningAnswer);
                    }
                }
                // Si todas las adquisiciones fueron exitosas, se devolverá un string "Exitoso". 
                // De lo contrario se disparará una excepción.
                // Las adquisiciones adicionales se podrían ver en los logs, pero como tal ya se registrarían en las medidas.

                if (raiseException == 1)
                    throw new CGMResultException(returningAnswer);
                if (raiseException == 2)
                    throw new Exception(returningAnswer);

                if (repInfs.Count > 1)
                    return "Ejecución exitosa de las adquisiciones. " + repInfs.Count +" días adquiridos.";
                return "Ejecución exitosa de la adquisición actual.";

            }
            catch (CGMResultException ex)
            {
                LogEventHelper.ConditionalLogger(ex, "Error en ejecución del proceso de interrogación.");
                throw ex;
            }
            catch (Exception ex)
            {
                LogEventHelper.ConditionalLogger(ex, "Error en la ejecución de la llamada.");
                throw ex;
            }
        }

        [Queue("2")]
        async public static Task<string> RunProgram2(long repinf) //Se envía el id de ReportInform
        {
            try { return await GeneralExecution(repinf); }
            catch (CGMResultException ex)
            {

                logger.Error(ex, "Error en ejecución del proceso de interrogación.");
                throw ex;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error en la ejecución de la llamada.");
                throw ex;
            }
        }
        [Queue("3")]
        async public static Task<string> RunProgram3(long repinf) //Se envía el id de ReportInform
        {
            try { return await GeneralExecution(repinf); }
            catch (CGMResultException ex)
            {
                logger.Error(ex, "Error en ejecución del proceso de interrogación.");
                throw ex;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error en la ejecución de la llamada.");
                throw ex;
            }
        }
        [Queue("4")]
        async public static Task<string> RunProgram4(long repinf) //Se envía el id de ReportInform
        {
            try { return await GeneralExecution(repinf); }
            catch (CGMResultException ex)
            {
                logger.Error(ex, "Error en ejecución del proceso de interrogación.");
                throw ex;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error en la ejecución de la llamada.");
                throw ex;
            }
        }

    }
}