﻿using CGM.Models.CGMModels;
using Microsoft.AspNet.Identity;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CGM.Helpers
{
    /// <summary>
    /// /// This partial class handles specific logging cases to be considered. 
    /// </summary>
    public partial class LogEventHelper 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobs">List of strings of jobs</param>
        /// <param name="operation">1: A Programación; 2: En proceso; 3: Error; 4: Advertencia ; 5: Exitoso ;7 : En cola; 7: Eliminado</param>
        /// <returns></returns>
        public static void LogJob(string[] jobs, int operation)
        {
            string nl = Environment.NewLine;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                var triplets = db.Report_Inform.Where(ri => jobs.Contains(ri.job_id.ToString())).Select(ri => new { ri.Boundary.name, ri.Boundary.code, ri.is_backup });
                string infoString = triplets.Count() + " Elemento(s) agreagado(s) a lista: ";
                switch (operation)
                {
                    case 1: infoString += "Programados: " + nl; break;
                    case 3: infoString += "Con errores: " + nl; break;
                    case 6: infoString += "En cola de ejecución: " + nl; break;
                    case 7: infoString += "Eliminados: " + nl; break;
                    default:
                        break;
                }

                foreach (var item in triplets)
                {
                    infoString += item.code +" (" + (item.is_backup.Value ? "R" : "P") + ")"+ " - " + item.name +";" +  nl;
                }
                logger.Info(infoString);
            }
        }

        /// <summary>
        /// It depends on the parameter on DB to allow or not the mails when there's an error.
        /// It can be annoying to receive them (the mails) all the time, so it can be disabled.
        /// </summary>
        /// <param name="ex"> The Exception object to store</param>
        /// <param name="msg">The custom message to undestand the context</param>
        public static void ConditionalLogger(Exception ex, string msg)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                //If error, send an email. Otherwise, just Info (db and file)
                bool sendMailOnErrors = db.Config_CM.AsNoTracking().Where(c => c.element.Trim().Equals("sendMailOnErrors")).FirstOrDefault().boolean.Value;
                if (sendMailOnErrors)
                    logger.Error(ex, msg);
                else
                    logger.Info(ex, msg);
            }
        }
    }
}
