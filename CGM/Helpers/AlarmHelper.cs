﻿using CGM.Models.CGMModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CGM.Helpers
{
    public class AlarmHelper
    {
        /// <summary>
        /// Caso 1: Valor Excedido
        /// </summary>
        /// <param name="alarmFires">Listado de alarmas que se dispararon en el proceso de adquisición</param>
        /// <returns></returns>
        public static async Task<string> ValorExcedido(List<AlarmFire> alarmFires)
        {
            // Filters the AlarmFires by only thod who have active mails.
            // Format the ids in the format to send the query in the url --> ?id=309&id=308
            int[] idsAlarms = alarmFires.Where(a => a.Alarm.send_mail).Select(a => a.id).ToArray();
            string idsQuery = "?";

            foreach (var item in idsAlarms)
                idsQuery += "ids=" + item.ToString() + "&";

            // Para enviar el correo, se hace una petición GET al controlador Mail (cerado especialmente para este fin)
            string mailResult = await MailManagement.SendGETMail("Mail/DetailsToMailMultiple" + idsQuery);            

            return mailResult;
        }
        /// <summary>
        /// Caso 2: Alarmas que se disparan sin parámetros de configuración, como contraseña incorrecta
        /// Cuando durante la adquisición se presenta un error de contraseña incorrecta: 301
        /// </summary>
        /// <param name="alarmFires"></param>
        /// <param name="alarmCase"> El nombre del caso de alarma, que puede ser "Contraseña incorrecta". Debe ser exacto el que se muestra en la plataforma</param>
        /// <returns></returns>
        public static async Task<string> AcquisitionError(Report_Inform report_inform, string alarmCase)
        {
            string mailResult ="";
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                // Se supone se debería crear solamente un elemento de alarma de este tipo. (De hecho debería ser algo que se active o desactive no más)
                Alarm alarm = db.Alarms.Where(a => a.AlarmCase.name == alarmCase && a.isActive).FirstOrDefault();

                if (alarm != null) // If the alarm exist, these have no extra parameters, then we just fire it!
                {
                    // Only is the boundary is part of the filter should be included.
                    // There's the problem with the fast filters, so this should be considered.
                    // Evaluate either it is useful to have the filters on the alarms. Or just apply to all of them

                    AlarmFire alarmFire = fireAlarm(db, alarm, report_inform, "");
                                        
                    if (alarm.send_mail) // If the mails are active, send the mail
                    {
                        mailResult = await MailManagement.SendGETMail("Mail/GeneralAlarmReportInform?id=" + alarmFire.id);
                        alarmFire.emailSent = 2; // 2 = Email sent
                        await db.SaveChangesAsync();
                    }
                }
            }
            return mailResult;
        }

        public static async Task<string> GeneralAlarm(string alarmCase, Dictionary<string, string> dictionary)
        {
            string mailResult ="";
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {

                // Se supone se debería crear solamente un elemento de alarma de este tipo. (De hecho debería ser algo que se active o desactive no más)
                Alarm alarm = db.Alarms.Where(a => a.AlarmCase.name == alarmCase && a.isActive).FirstOrDefault();

                if (alarm != null) // If the alarm exist, these have no extra parameters, then we just fire it!
                {
                    // Only is the boundary is part of the filter should be included.
                    // There's the problem with the fast filters, so this should be considered.
                    // Evaluate either it is useful to have the filters on the alarms. Or just apply to all of them

                    string dict_json = JsonConvert.SerializeObject(dictionary);

                    AlarmFire alarmFire = fireAlarm(db, alarm, null, dict_json);

                    if (alarm.send_mail) // If the mails are active, send the mail
                    {
                        mailResult = await MailManagement.SendGETMail("Mail/GeneralAlarm?id=" + alarmFire.id);
                        alarmFire.emailSent = 2; // 2 = Email sent
                        await db.SaveChangesAsync();
                    }
                }
            }
            return "";

        }


        // ========================================================
        // ------------------ INSERT TO DATABASE ------------------
        // ========================================================

        public static AlarmFire fireAlarm(CGM_DB_Entities db, Alarm alarm, Report_Inform report_inform, string details)
        {

            AlarmFire alarmFire = new AlarmFire()
            {
                Alarm = alarm,
                firedOn = DateTime.Now,
                isActive = true, // Esto se debe desactivar en la plataforma, de modo que se notifica la alarma.
                reportInformId = report_inform?.id,
                details = details,
                emailSent = alarm.send_mail ? 1 : 0 // If must be sent by email, 1, to set it to 2 after sending it. If mails deactivated, 0.
            };
            db.AlarmFires.Add(alarmFire);
            db.SaveChanges();

            NotificationManager.addAlarmNotification(alarmFire);
            return alarmFire;
        }


    }
}