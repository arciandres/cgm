﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CGM.Helpers
{
    public class MailManagement
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void EnviarCorreoAhora(string from, string password, string Subject, string MessageBody, List<string> mails, List<string> attachments, bool isHTML) //attachments is a list of address of the files in PC
        {
            string rta = "";
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(from, password);
            SmtpServer.EnableSsl = true;
            mail.IsBodyHtml = isHTML;
            mail.From = new System.Net.Mail.MailAddress(from);
            mail.Subject = Subject;
            mail.Body = MessageBody;
            mail.To.Add(String.Join(",", mails.ToArray<string>()));

            if (attachments != null)
            {
                foreach (string at in attachments)
                {
                    if (System.IO.File.Exists(at))
                    {
                        Attachment att = new Attachment(at);
                        mail.Attachments.Add(att);
                    }
                }  
            }

            try
            {
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                logger.Info(ex, "Error al enviar mensaje por correo. Verificar direcciones y contraseñas.");
                throw ex;
            }
            mail.Dispose();
        }

        /// <summary>
        /// Method created to send emails with use of partial views. Makes a HTTP request to Mail controller, in order to access Controller Context
        /// </summary>
        /// <param name="URL_relative">Relative URL of the function. Example: "Mail/DetailsToMailMultiple" + idsQuery</param>
        /// <returns></returns>
        internal static async Task<string> SendGETMail(string URL_relative)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(USER.URIBASE);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(URL_relative);
                response.Wait();
                HttpResponseMessage result = response.Result;
                var r = await result.Content.ReadAsStringAsync(); // This can be added to acknowledge the correct operation
                return r;
            }
        }
    }
}
