﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using GM_GD.XMReportService;
using GM_GD.Models.SGMModels;
using CGM.Models.CGMModels;

namespace GM_GD.Models.CMORModel
{
    public class Envio_a_XM
    {
        string catchException = "";
        CGM_DB_Entities db = new CGM_DB_Entities();
        public void ReportToXM(Reports reports) //reports debe incluir a su elemento padre en Dates_To_Reports
        {
            Stopwatch ProcessTime = new Stopwatch();
            getXMConfigData();

            //if (reports.Dates_To_Reports.report_type_id == 1) //Se incluye las fronteras de AP si el reporte es de Comercialización
            //    IncludeAPBoundaryActivelist(reports);

            DateTime OpDateStart = reports.Dates_To_Reports.operation_date.AddHours(1); //Se arranca a la 1 de la mañana
            DateTime OpDateEnd = OpDateStart.Date.AddDays(1);

            List<Group_Measure> ActiveBoundaryMeasuresGroupsList = db.Group_Measure.Include(g => g.Boundary).Include(g => g.Measure).Where(
                                                                   g => g.time >= OpDateStart &&
                                                                        g.time <= OpDateEnd &&
                                                                        g.Boundary.report_type_id == reports.Dates_To_Reports.report_type_id &&
                                                                        g.Boundary.is_active == true).ToList();


            //List<Measure> ActiveBoundaryMeasuresList = db.Measure.Include(m => m.Boundary).Where(
            //                                           m => m.measure_date >= OpDateStart &&
            //                                                m.measure_date <= OpDateEnd &&
            //                                                m.Boundary.report_type_id== reports.Dates_To_Reports.report_type_id &&
            //                                                m.Boundary.is_active== true).ToList();

            List<ReportElement> reportElements = DataAndDB.getReportElements(ActiveBoundaryMeasuresGroupsList);

            ProcessTime.Start();
            ProcessRequestResult result = SendReportToXM(reports, reportElements);
            ReportReadingProcessResult ProcessResult = null;
            string resultrta = "";
            if (result == null)
                resultrta = catchException;
            else
            {
                resultrta = ProcessResult.Results.Any(r => r.ErrorMessage != null) ? "Error" : "Success";
                ProcessResult = GetProcessResult(result);
            }
            ProcessTime.Stop();


            var report = db.Reports.Find(reports.id);
            report.is_active = false;
            report.report_id = result.ProcessId.ToString();
            report.result = resultrta;
            report.time_elapsed = ProcessTime.Elapsed;
            report.date_and_time_report = DateTime.Now;
            db.SaveChanges();

            if (result != null)
                SaveReportInforms(reports, result, ProcessResult, db.Boundary.ToList());

            string path1 = "D:\\Archivos de lectura\\" + "CR" + String.Format("{0}", reports.Dates_To_Reports.report_type_id == 0 ? "25" : "26") + reports.Dates_To_Reports.operation_date.Month.ToString("D2") + reports.Dates_To_Reports.operation_date.Day.ToString("D2") + "_" + report.report_id + ".txt";
            string path2 = "D:\\Archivos de lectura\\" + "Results-CR" + String.Format("{0}", reports.Dates_To_Reports.report_type_id == 0 ? "25" : "26") + reports.Dates_To_Reports.operation_date.Month.ToString("D2") + reports.Dates_To_Reports.operation_date.Day.ToString("D2") + "_" + report.report_id + ".txt";

            GenerateCRFile(path1, db.Reports.Include(model => model.Dates_To_Reports).Include(model => model.Report_Inform).Where(r => r.id == report.id).First(), reportElements);
            GenerateResultsFile(path2, db.Reports.Include(model => model.Dates_To_Reports).Include(model => model.Report_Inform).Where(r => r.id == report.id).First(), reportElements);
            //var kop = db.ReportInform.Include(tdy=>tdy.);
            string TextConclusion = ReportConclusion(reports);
            var tempdavid = db.Report_Type.Where(r => r.id == reports.Dates_To_Reports.report_type_id).Select(r => r.report_type1).First().Trim() + " " + reports.Dates_To_Reports.operation_date.ToShortDateString() + " ";
            SendMailNow(
                 MailAddress, //Dirección de Correo CMOR
                 MailPassword, //Password de correo CMOR
                 db.Report_Type.Where(r => r.id == reports.Dates_To_Reports.report_type_id).Select(r => r.report_type1).First().Trim() + " " +reports.Dates_To_Reports.operation_date.ToShortDateString() + " ",
                 TextConclusion + Environment.NewLine,
                 db.Mail_Item.Where(m => m.active == true).Select(m => m.mail_address).ToList(),
                 new List<string> { pathCR, pathResults },
                 true  //Se especifica que es un correo en formato HTML 
                 );
        }
        public ProcessRequestResult SendReportToXM(Reports reports, List<ReportElement> reportElements)
        {
            List<ReadingReportItem> listResult = new List<ReadingReportItem>();  // Se crea la lista inicial para incluir todas las Fronteras
            ReadingReportServiceClient client = new ReadingReportServiceClient("BasicHttpBinding_IReadingReportService", ServiceURL); //Inicialización del cliente , se ingresa la URL del servicio
            ReadingReportItem XMReport = new ReadingReportItem();
            int C = 0;


            foreach (ReportElement reportElement in reportElements)
            {
                try
                {
                    listResult.Add(new ReadingReportItem()
                    {
                        is_backup = reportElement.isBackup,
                        ReadingCount = 24,
                        ReadingInterval = 60,
                        StartDate = reports.Dates_To_Reports.operation_date, //Fecha de operación
                        BorderCode = reportElement.boundary.id.Trim(), //Inserta código de frontera
                        Readings = Array.ConvertAll(reportElement.measures.ToArray(), x => x.measure)
                    });
                    C++;
                }
                catch(Exception ex)
                {
                    throw;
                }
            }
            try
            {
                ProcessRequestResult result = client.ReportReadings(listResult.ToArray(), userData);
                // Si el resultado contiene un mensaje de error ocurrió algun problema y no fue posible procesar las lecturas.
                if (!string.IsNullOrEmpty(result.ErrorMessage))
                {
                    //MessageBox.Show(string.Format("Error enviando lecturas : {0}", result.ErrorMessage));
                }

                client.Close();
                return result;
            }
            catch (Exception ex)
            {
#if Debug_On
                Console.WriteLine(ex);
#endif      
                catchException = ex.ToString();
                client.Close();
                return null;
            }
        }

//        public ProcessRequestResult SendReportToXMOld(Reports reports, List<Measure> MeasureList)
//        {
//            List<ReadingReportItem> listResult = new List<ReadingReportItem>();  // Se crea la lista inicial para incluir todas las Fronteras
//            ReadingReportServiceClient client = new ReadingReportServiceClient("BasicHttpBinding_IReadingReportService", ServiceURL); //Inicialización del cliente , se ingresa la URL del servicio
//            var lista = MeasureList.GroupBy(m => m.Boundary).ToList();
//            ReadingReportItem XMReport = new ReadingReportItem();
//            int C = 0;
//            foreach (var boundary in MeasureList.GroupBy(m => m.Boundary))
//            {
//                try
//                {
//                    listResult.Add(new ReadingReportItem()
//                    {
//                        is_backup = boundary.Key.is_backup.Value,
//                        ReadingCount = 24,
//                        ReadingInterval = 60,
//                        StartDate = reports.Dates_To_Reports.operation_date, //Fecha de operación
//                        BorderCode = boundary.Key.code.Trim(), //Inserta código de frontera
//                        Readings = Array.ConvertAll(boundary.ToArray(), x => (double)x.measure1)
//                    });
//                    C++;
//                }
//                catch (Exception ex)
//                {
//                    throw;
//                }
//            }
//            try
//            {
//                ProcessRequestResult result = client.ReportReadings(listResult.ToArray(), userData);
//                // Si el resultado contiene un mensaje de error ocurrió algun problema y no fue posible procesar las lecturas.
//                if (!string.IsNullOrEmpty(result.ErrorMessage))
//                {
//                    //MessageBox.Show(string.Format("Error enviando lecturas : {0}", result.ErrorMessage));
//                }

//                client.Close();
//                return result;
//            }
//            catch (Exception ex)
//            {
//#if Debug_On
//                Console.WriteLine(ex);
//#endif      
//                catchException = ex.ToString();
//                client.Close();
//                return null;
//            }
//        }

        private ReportReadingProcessResult GetProcessResult(ProcessRequestResult result)
        {
            ReadingReportServiceClient client = new ReadingReportServiceClient("BasicHttpBinding_IReadingReportService", ServiceURL);
            ReportReadingProcessResult ProcessResult = new ReportReadingProcessResult();
            bool ban = false;
            while (ban == false)
            {
                try //a veces llega nulo el resultado de proceso, y se produce error
                {
                    do //Se pide la respuesta hasta que llegue, pues no es inmediata
                    {
                        ProcessResult = client.GetProcessResult(result.ProcessId, userData);
                    } while (ProcessResult.Results.Count() == 0);
                    ban = true;
                }
                catch (Exception ex)
                {
                    ban = false;
                }
            }
            client.Close();
            return ProcessResult;
        }
        private void SaveReportInforms(Reports report, ProcessRequestResult result, ReportReadingProcessResult processResult, List<Boundary> boundaryList)
        {
            List<Report_Inform> ReportInformList = new List<Report_Inform>();

            if (processResult != null)
            {
                foreach (var item in processResult.Results)
                {
                    ReportInformList.Add(new Report_Inform()
                    {
                        boundary_id = boundaryList.Where(b => b.id.Trim() == item.Code.Trim()).Select(b => b.id).First(),
                        reports_id = report.id,
                        success = item.ResultFlag.ToString(),
                        message = item.ErrorMessage != null ? item.ErrorMessage.ToString() : ""
                    });
                }
            }

            insertReportInforms(ReportInformList);
        }

        public static string insertReportInforms(List<Report_Inform> m) 
        {
            if (m == null) return "Array empty";
            CGM_DB_Entities db = new CGM_DB_Entities();
            List<Report_Inform> ReportInformlist = new List<Report_Inform>();
            //List<Measure> measuresupdate = new List<Measure>();
            foreach (var item in m)
            {
                try
                {
                    Report_Inform myinform = db.Report_Inform.Where(model => model.boundary_id == item.boundary_id && model.reports_id == item.reports_id).FirstOrDefault();
                    if (myinform == null)
                        ReportInformlist.Add(item);
                    else
                    {
                        //measuresupdate.Add(mymeasure);
                        myinform.message = item.message;
                        myinform.success = item.success;
                        //mymeasure.ProcessID = item.ProcessID;
                        db.Entry(myinform).State = EntityState.Modified;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            db.Report_Inform.AddRange(ReportInformlist);
            try
            {
                db.SaveChanges();
                //UpdateMeasure(measuresupdate);
            }
            catch (Exception)
            {
                return "Bad";
            }
            return "Ok";
        }
        public void getXMConfigData()
        {
            List<Config_CM> configList = db.Config_CM.ToList();
            userData = new UserData
            { // Estructura con los datos del usuario
                Password = configList.Where(c => c.element.Trim() == "WebServicePassword").Select(c => c.text).First().Trim(),
                UserName = configList.Where(c => c.element.Trim() == "WebServiceUser").Select(c => c.text).First().Trim(),
            };
            ServiceURL = configList.Where(c => c.element.Trim() == "WebServiceURL").Select(c => c.text).First().Trim();
            MailAddress = configList.Where(c => c.element.Trim() == "CorreoCMOR").Select(c => c.text).First().Trim();
            MailPassword = configList.Where(c => c.element.Trim() == "PasswordCorreoCMOR").Select(c => c.text).First().Trim();
        }


        public UserData userData { get; set; }
        public string ServiceURL { get; set; }
        public static string pathCR { get; set; }
        public static string pathResults { get; set; }
        public static string MailAddress { get; set; }
        public static string MailPassword { get; set; }

        public static void GenerateCRFile(string path, Reports reports, List<ReportElement> reportElements)
        {
            //path = System.Web.HttpContext.Current.Server.MapPath("~//Files//Reports//CRFiles//") + "CR" + String.Format("{0}", cmorObject.Dates_To_Reports.report_type_id == 0 ? "25" : "26") + cmorObject.Dates_To_Reports.operation_date.Month.ToString("D2") + cmorObject.Dates_To_Reports.operation_date.Day.ToString("D2") + "_" + cmorObject.reports.report_id + ".txt";

            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            pathCR = path;

            string text = "";
            foreach (ReportElement reportElement in reportElements)
            {
                text += reportElement.boundary + "\tP\t0\t"; //Inserta el código de frontera
                foreach (vMeasure measure in reportElement.measures)
                {
                    text += String.Format("{0}\t", Math.Round(measure.measure, 2)).Replace(',', '.'); //Horas de la 1 am hasta 12 am del día siguiente
                }
                text += Environment.NewLine;
            }
            WriteTXT(text, path);
        }

        private static void WriteTXT(string text, string path)
        {
            using (StreamWriter sw = File.CreateText(path))
            {
                sw.Write(text);
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
    

        public static void GenerateResultsFile(string path, Reports reports, List<ReportElement> reportElements)
        {
            string sp = Environment.NewLine;
            //string path = System.Web.HttpContext.Current.Server.MapPath("~//Files//Reports//ProcessResults//") + "Resultado-" + "CR" + String.Format("{0}", cmorObject.Dates_To_Reports.report_type_id == 0 ? "25" : "26") + cmorObject.Dates_To_Reports.operation_date.Month.ToString("D2") + cmorObject.Dates_To_Reports.operation_date.Day.ToString("D2") + "_" + cmorObject.reports.report_id + ".txt";
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            string text = "";
            pathResults = path;

            text += "Resultado de Reporte" + sp + sp +
                        "\t\tDatos de Envío" + sp + sp +
                        "Unidad de Negocio: " + (reports.Dates_To_Reports.report_type_id == 0 ? "Generación" : "Comercialización") + sp +
                        "Fecha de Envío: " + reports.date_and_time_report.Value.ToLongDateString() + sp +
                        "Hora de Envío " + reports.date_and_time_report.Value.ToLongTimeString() + sp +
                        "Proceso Aceptado: " + reports.result.ToString() + sp +
                        "ID de Proceso: " + reports.report_id + sp +
                        "Número de Fronteras reportadas: " + reportElements.Count() + sp +
                        "Datos enviados:" + sp + sp;

            foreach (var element in reportElements)
            {
                text += element.boundary + "\tP\t0\t"; //Inserta el código de frontera
                foreach (var measure in element.measures)
                {
                    text += String.Format("{0}\t", Math.Round(measure.measure, 2)).Replace(',', '.'); //Horas de la 1 am hasta 12 am del día siguiente
                }
                text += sp;
            }
            text += sp + sp;

            text += "\t\tResultado de Proceso" + sp + sp +
                    "Fecha de Operación: " + reports.Dates_To_Reports.operation_date.ToLongDateString() + sp +
                    "Resultado: " + reports.result + sp +
                    "Datos:" + sp +
                    sp;

            text += "No." + "\tFrontera".PadRight(30) + "Código SIC" + "\tResultado" + "\tMensaje de Error" + sp;
            int c = 1;
            foreach (var inform in reports.Report_Inform)
            {
                text += c++ + "\t" +
                        reportElements.Where(f => f.boundary == inform.Boundary).Select(f => f.boundary.name).First().PadRight(30) + "\t" + //Inserta el nombre de frontera
                        reportElements.Where(f => f.boundary == inform.Boundary).Select(f => f.boundary.id).First() + "\t" + //Inserta el código de frontera
                        inform.success + "\t\t" +
                        inform.message; ;
                text += sp;
            }

            WriteTXT(text, path);
        }
        public static void SendMailNow(string from, string password, string Subject, string MessageBody, List<string> mails, List<string> attachments, bool isHTML) //attachments is a list of address of the files in PC
        {
            string rta = "";
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(from, password);
            SmtpServer.EnableSsl = true;
            mail.IsBodyHtml = isHTML;
            mail.From = new System.Net.Mail.MailAddress(from);
            mail.Subject = Subject;
            mail.Body = MessageBody;
            mail.To.Add(String.Join(",", mails.ToArray<string>()));

            foreach (string at in attachments)
            {
                if (System.IO.File.Exists(at))
                {
                    System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(at);
                    mail.Attachments.Add(att);
                }
            }
            try
            {
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw;
            }
            mail.Dispose();
        }
        public static string ReportConclusion(Reports report)
        {
            CGM_DB_Entities db = new CGM_DB_Entities();
            List<Report_Inform> ReportInformlist = db.Report_Inform.Include(m => m.Reports).Where(i => i.Reports.id == report.id).ToList();

            string sp = Environment.NewLine;
            string lightredcolor = "#f86e6e";
            string whitecolor = "#ffffff";
            int c = 1;
            string redstyle = "style =\"color: red\"";
            string greenstyle = "style =\"color: green\"";
            string h1red_open = "<h1 " + redstyle + ">";
            string h2green_open = "<h2 " + greenstyle + ">";
            string h4red_open = "<h4 " + redstyle + ">";
            string h1_close = "</h1>";
            string h4_close = "</h4>";
            string h2_close = "</h2>";
            string par_open = "<p>";
            string par_close = "</p>";

            var informGroup = ReportInformlist.GroupBy(r => new { r.success, r.message }).GroupBy(i => i.Key.success);
            string textc = report.result;

            switch (report.Dates_To_Reports.destination)
            {
                case 0:
                    textc += "<h2>" + "Carga a base de datos de CMOR " + "</h2>" + sp;
                    break;
                case 1:
                    textc += "<h2>" + "Envío de reporte a X.M. E.S.P. " + "</h2>" + sp;
                    break;
                case 2:
                    textc += "<h2>" + "Actualización de medidas desde equipo remoto " + "</h2>" + sp;
                    break;
                default:
                    break;
            }

            textc += "<h2>" + "Fronteras Cargadas: " + ReportInformlist.Count + "</h2>" + sp;
            foreach (var igroup in informGroup)
            {
                if (igroup.Key.Trim() == "Error" || igroup.Key.Trim() == "False")
                {
                    c = 1;
                    textc += h1red_open + "ERROR:" + h1_close + sp;

                    foreach (var igroupinfo in igroup)
                    {
                        textc += h4red_open + c++ + ". " + igroupinfo.Key.message + h4_close + sp;
                        textc += par_open + "Fronteras con este error: " + igroupinfo.Count() + par_close + sp;
                        textc += BoundaryTableHTML(igroupinfo.ToList(), lightredcolor, whitecolor);
                    }
                }
                if (igroup.Key.Trim() == "Warning")
                {
                    c = 1;
                    textc += h1red_open + "Advertencia:" + h1_close + sp;

                    foreach (var igroupinfo in igroup)
                    {
                        textc += h4red_open + c++ + ". " + igroupinfo.Key.message + h4_close + sp;
                        textc += par_open + "Fronteras con esta advertencia: " + igroupinfo.Count() + par_close + sp;
                        textc += BoundaryTableHTML(igroupinfo.ToList(), lightredcolor, whitecolor);
                    }
                }

                if (igroup.Key.Trim() == "Success" || igroup.Key.Trim() == "True")
                {
                    c = 1;
                    textc += h2green_open + "Fronteras cargadas exitosamente: "+ igroup.Where(i => i.Key.success.Trim() == "Success").First().Count() + h2_close + sp;
                }
            }

            textc += sp + sp + sp + sp + sp + sp + sp;
            textc += "<img style=\"width:400px; height: auto; text-align:left\" src=\"http://oi65.tinypic.com/kds46g.jpg\" />" + sp;

            return textc;
        }

        public static string BoundaryTableHTML(List<Report_Inform> informs, string colorHeader, string fontColor)
        {
            string sp = Environment.NewLine;
            string HTMLstring = sp;
            string htmlHeaderRowStart = "<tr style = \"background-color:" + colorHeader + "; color:" + fontColor + ";\">";
            string columnStart = "<td style = \"border: 1px solid #aaaaaa; text-align:left; padding: 8px\">";
            string headerColumnStart = "<th style =\"border:1px solid #aaaaaa; text-align: left ;padding:10px\">";
            string headerColumnEnd = "</th>";
            string columnEnd = "</td>";
            string rowStart = "<tr>";
            string rowEnd = "</tr>";

            HTMLstring += "<table style=\"border-collapse:collapse;  width:100%\" > " + sp +
            "<thead>" +
            htmlHeaderRowStart +
            headerColumnStart + "No. " + headerColumnEnd +
            headerColumnStart + "Nombre " + headerColumnEnd +
            headerColumnStart + "Código SIC" + headerColumnEnd +
            rowEnd +
            "</thead>" +
            "<tbody>";
            int conta = 1;
            foreach (var item in informs)
            {
                HTMLstring += rowStart +
                columnStart + conta++ + columnEnd +
                columnStart + item.Boundary.name + columnEnd +
                columnStart + item.Boundary.id + columnEnd +
                rowEnd;
            };
            HTMLstring += "</tbody>" +
           "</table>" + sp + sp;
            return HTMLstring;
        }
    }

}