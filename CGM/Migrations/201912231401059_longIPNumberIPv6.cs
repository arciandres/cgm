namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class longIPNumberIPv6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LogUser", "IP", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LogUser", "IP", c => c.String(maxLength: 20));
        }
    }
}
