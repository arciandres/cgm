namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreFiledsInProgram : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Program", "retries", c => c.Int());
            AddColumn("dbo.Program", "multiple", c => c.Boolean(nullable: false));
            AddColumn("dbo.Program", "frequency", c => c.Time(nullable: false, precision: 7));
            AlterColumn("dbo.Program", "ExecutionTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Program", "ExecutionTime", c => c.Time(nullable: false, precision: 7));
            DropColumn("dbo.Program", "frequency");
            DropColumn("dbo.Program", "multiple");
            DropColumn("dbo.Program", "retries");
        }
    }
}
