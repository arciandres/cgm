namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupMeasureNameChange : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Measure_Group", newName: "Measure_Group");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Measure_Group", newName: "Measure_Group");
        }
    }
}
