namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameCMOR_Logs : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.CMOR_Logs", newName: "Event");
           // DropForeignKey("dbo.Measure", "Report_Inform_id", "dbo.Report_Inform");
            //DropIndex("dbo.Measure", new[] { "Report_Inform_id" });
           // DropPrimaryKey("dbo.Report_Inform");
            CreateTable(
                "dbo.Event_Category",
                c => new
                    {
                        id = c.Int(nullable: true, identity: true),
                        categoryName = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Event", "event_id", c => c.Int(nullable: true));
            //AlterColumn("dbo.Measure", "report_inform_id", c => c.Long());
            //AlterColumn("dbo.Report_Inform", "id", c => c.Long(nullable: false, identity: true));
            //AddPrimaryKey("dbo.Report_Inform", "id");
            DropColumn("dbo.Event", "operation");
            CreateIndex("dbo.Event", "event_id");
            //CreateIndex("dbo.Measure", "report_inform_id");
            AddForeignKey("dbo.Event", "event_id", "dbo.Event_Category", "id", cascadeDelete: true);
            //AddForeignKey("dbo.Measure", "report_inform_id", "dbo.Report_Inform", "id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Event", "operation", c => c.String(maxLength: 50));
            DropForeignKey("dbo.Measure", "report_inform_id", "dbo.Report_Inform");
            DropForeignKey("dbo.Event", "event_id", "dbo.Event_Category");
            DropIndex("dbo.Measure", new[] { "report_inform_id" });
            DropIndex("dbo.Event", new[] { "event_id" });
            DropPrimaryKey("dbo.Report_Inform");
            AlterColumn("dbo.Report_Inform", "id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Measure", "report_inform_id", c => c.Int());
            DropColumn("dbo.Event", "event_id");
            DropTable("dbo.Event_Category");
            AddPrimaryKey("dbo.Report_Inform", "id");
            CreateIndex("dbo.Measure", "Report_Inform_id");
            AddForeignKey("dbo.Measure", "Report_Inform_id", "dbo.Report_Inform", "id");
            RenameTable(name: "dbo.Event", newName: "CMOR_Logs");
        }
    }
}
