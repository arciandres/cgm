namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addStatusToReport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Report", "status_id", c => c.Int(nullable: false));
            CreateIndex("dbo.Report", "status_id");
            AddForeignKey("dbo.Report", "status_id", "dbo.Status", "id", cascadeDelete: true);
            DropColumn("dbo.Report", "result");
            AddColumn("dbo.Report_Inform", "time_elapsed", c => c.Time(precision: 7));
            AddColumn("dbo.Program", "acquisition_id", c => c.Int(nullable: false));
            CreateIndex("dbo.Program", "acquisition_id");
            AddForeignKey("dbo.Program", "acquisition_id", "Telemetry.Acquisition", "id", cascadeDelete: true);
            AddColumn("dbo.Report", "program_id", c => c.Int());
            CreateIndex("dbo.Report", "program_id");
            AddForeignKey("dbo.Report", "program_id", "dbo.Program", "id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Report", "program_id", "dbo.Program");
            DropIndex("dbo.Report", new[] { "program_id" });
            DropColumn("dbo.Report", "program_id");
            DropForeignKey("dbo.Program", "acquisition_id", "Telemetry.Acquisition");
            DropIndex("dbo.Program", new[] { "acquisition_id" });
            DropColumn("dbo.Program", "acquisition_id");
            DropColumn("dbo.Report_Inform", "time_elapsed");
            AddColumn("dbo.Report", "result", c => c.String(maxLength: 10, fixedLength: true, unicode: false));
            DropForeignKey("dbo.Report", "status_id", "dbo.Status");
            DropIndex("dbo.Report", new[] { "status_id" });
            DropColumn("dbo.Report", "status_id");
        }
    }
}
