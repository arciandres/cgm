namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeigMarkeyKey : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Report", "market_id");
            AddForeignKey("dbo.Report", "market_id", "dbo.Report_Type", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report", "market_id", "dbo.Report_Type");
            DropIndex("dbo.Report", new[] { "market_id" });
        }
    }
}
