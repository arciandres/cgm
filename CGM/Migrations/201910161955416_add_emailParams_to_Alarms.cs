namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_emailParams_to_Alarms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Alarm", "mail_params", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Alarm", "mail_params");
        }
    }
}
