namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createMeterAndModelMeter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Meter",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        serial = c.String(maxLength: 20, fixedLength: true),
                        multiplicative_factor = c.Double(),
                        info = c.String(),
                        model_meter_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Model_Meter", t => t.model_meter_id, cascadeDelete: true)
                .Index(t => t.model_meter_id);
            
            CreateTable(
                "dbo.Model_Meter",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        brand = c.String(maxLength: 10),
                        model = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.id);


            CreateIndex("dbo.Boundary", "meter_id");
            CreateIndex("dbo.Boundary", "meter_id2");
            AddForeignKey("dbo.Boundary", "meter_id2", "dbo.Meter", "id");
            AddForeignKey("dbo.Boundary", "meter_id", "dbo.Meter", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Boundary", "meter_id", "dbo.Meter");
            DropForeignKey("dbo.Boundary", "meter_id2", "dbo.Meter");
            DropIndex("dbo.Boundary", new[] { "meter_id2" });
            DropIndex("dbo.Boundary", new[] { "meter_id" });
        }
    }
}
