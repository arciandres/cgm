// <auto-generated />
namespace CGM.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addDefaultProtocolToModelMeter : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addDefaultProtocolToModelMeter));
        
        string IMigrationMetadata.Id
        {
            get { return "201906290926133_addDefaultProtocolToModelMeter"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
