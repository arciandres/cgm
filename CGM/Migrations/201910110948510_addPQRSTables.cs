namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPQRSTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PQRS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id = c.String(maxLength: 128),
                        date_created = c.DateTime(nullable: false),
                        date_closed = c.DateTime(nullable: false),
                        solved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.user_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.PQRS_Message",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        PQRS_id = c.Int(nullable: false),
                        message = c.String(),
                        user_id = c.String(maxLength: 128),
                        date_created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.PQRS", t => t.PQRS_id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.user_id)
                .Index(t => t.PQRS_id)
                .Index(t => t.user_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PQRS", "user_id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PQRS_Message", "user_id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PQRS_Message", "PQRS_id", "dbo.PQRS");
            DropIndex("dbo.PQRS_Message", new[] { "user_id" });
            DropIndex("dbo.PQRS_Message", new[] { "PQRS_id" });
            DropIndex("dbo.PQRS", new[] { "user_id" });
            DropTable("dbo.PQRS_Message");
            DropTable("dbo.PQRS");
        }
    }
}
