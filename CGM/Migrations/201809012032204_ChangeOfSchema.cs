namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeOfSchema : DbMigration
    {
        public override void Up()
        {
            MoveTable(name: "dbo.Quantity", newSchema: "Telemetry");
            MoveTable(name: "dbo.Meter", newSchema: "Telemetry");
            MoveTable(name: "dbo.Model_Meter", newSchema: "Telemetry");
            MoveTable(name: "dbo.SurveyMaster", newSchema: "Survey");
            MoveTable(name: "dbo.ConfigurationSurvey", newSchema: "Survey");
            MoveTable(name: "dbo.ControlGroup", newSchema: "Survey");
            MoveTable(name: "dbo.Question", newSchema: "Survey");
            MoveTable(name: "dbo.Response", newSchema: "Survey");
            MoveTable(name: "dbo.TypeData", newSchema: "Survey");
            MoveTable(name: "dbo.DataSource", newSchema: "Survey");
            MoveTable(name: "dbo.DataSourceItem", newSchema: "Survey");
            MoveTable(name: "dbo.Response_Type", newSchema: "Survey");
            MoveTable(name: "dbo.SurveyTemplate", newSchema: "Survey");
            MoveTable(name: "dbo.ShareSurvey", newSchema: "Survey");
            MoveTable(name: "dbo.DeviceUser", newSchema: "Survey");
            MoveTable(name: "dbo.InteresPlaceImage", newSchema: "Survey");
            MoveTable(name: "dbo.InteresPlace", newSchema: "Survey");
            MoveTable(name: "dbo.Magnitude", newSchema: "Telemetry");
            MoveTable(name: "dbo.Norm", newSchema: "Survey");
            MoveTable(name: "dbo.Procedure", newSchema: "Survey");
            MoveTable(name: "dbo.QuestionImage", newSchema: "Survey");
            MoveTable(name: "dbo.ResponseFile", newSchema: "Survey");
            MoveTable(name: "dbo.SurveyResponse", newSchema: "Survey");
            MoveTable(name: "dbo.Survey", newSchema: "Survey");
        }
        
        public override void Down()
        {
            MoveTable(name: "Survey.Survey", newSchema: "dbo");
            MoveTable(name: "Survey.SurveyResponse", newSchema: "dbo");
            MoveTable(name: "Survey.ResponseFile", newSchema: "dbo");
            MoveTable(name: "Survey.QuestionImage", newSchema: "dbo");
            MoveTable(name: "Survey.Procedure", newSchema: "dbo");
            MoveTable(name: "Survey.Norm", newSchema: "dbo");
            MoveTable(name: "Telemetry.Magnitude", newSchema: "dbo");
            MoveTable(name: "Survey.InteresPlace", newSchema: "dbo");
            MoveTable(name: "Survey.InteresPlaceImage", newSchema: "dbo");
            MoveTable(name: "Survey.DeviceUser", newSchema: "dbo");
            MoveTable(name: "Survey.ShareSurvey", newSchema: "dbo");
            MoveTable(name: "Survey.SurveyTemplate", newSchema: "dbo");
            MoveTable(name: "Survey.Response_Type", newSchema: "dbo");
            MoveTable(name: "Survey.DataSourceItem", newSchema: "dbo");
            MoveTable(name: "Survey.DataSource", newSchema: "dbo");
            MoveTable(name: "Survey.TypeData", newSchema: "dbo");
            MoveTable(name: "Survey.Response", newSchema: "dbo");
            MoveTable(name: "Survey.Question", newSchema: "dbo");
            MoveTable(name: "Survey.ControlGroup", newSchema: "dbo");
            MoveTable(name: "Survey.ConfigurationSurvey", newSchema: "dbo");
            MoveTable(name: "Survey.SurveyMaster", newSchema: "dbo");
            MoveTable(name: "Telemetry.Model_Meter", newSchema: "dbo");
            MoveTable(name: "Telemetry.Meter", newSchema: "dbo");
            MoveTable(name: "Telemetry.Quantity", newSchema: "dbo");
        }
    }
}
