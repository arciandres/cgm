namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class filedRemovedAcq : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Telemetry.Acquisition", "is_filed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("Telemetry.Acquisition", "is_filed", c => c.Boolean());
        }
    }
}
