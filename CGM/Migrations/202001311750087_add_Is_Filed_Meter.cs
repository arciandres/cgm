namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_Is_Filed_Meter : DbMigration
    {
        public override void Up()
        {
            AddColumn("Telemetry.Meter", "is_filed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Telemetry.Meter", "is_filed");
        }
    }
}
