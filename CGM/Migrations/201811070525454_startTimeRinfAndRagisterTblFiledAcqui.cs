namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class startTimeRinfAndRagisterTblFiledAcqui : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Telemetry.Register",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        quantity_id = c.Int(),
                        model_id = c.Int(),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("Telemetry.Model_Meter", t => t.model_id)
                .ForeignKey("Telemetry.Quantity", t => t.quantity_id)
                .Index(t => t.quantity_id)
                .Index(t => t.model_id);
            
            AddColumn("Telemetry.Acquisition", "is_filed", c => c.Boolean());
            AddColumn("dbo.Report_Inform", "start_time", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropForeignKey("Telemetry.Register", "quantity_id", "Telemetry.Quantity");
            DropForeignKey("Telemetry.Register", "model_id", "Telemetry.Model_Meter");
            DropIndex("Telemetry.Register", new[] { "model_id" });
            DropIndex("Telemetry.Register", new[] { "quantity_id" });
            DropColumn("dbo.Report_Inform", "start_time");
            DropColumn("Telemetry.Acquisition", "is_filed");
            DropTable("Telemetry.Register");
        }
    }
}
