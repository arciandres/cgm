namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncludeAttemptsAndSource : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Report_Inform", "boundary_source_id", c => c.Int());
            AlterColumn("dbo.Program", "max_attempts", c => c.Int());
            AlterColumn("dbo.Boundary_Source", "source", c => c.String(maxLength: 20, fixedLength: true, unicode: false));
            AlterColumn("dbo.Report_Inform", "attempts", c => c.Int());
            CreateIndex("dbo.Report_Inform", "boundary_source_id");
            AddForeignKey("dbo.Report_Inform", "boundary_source_id", "dbo.Boundary_Source", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report_Inform", "boundary_source_id", "dbo.Boundary_Source");
            DropIndex("dbo.Report_Inform", new[] { "boundary_source_id" });
            AlterColumn("dbo.Report_Inform", "attempts", c => c.Int(nullable: false));
            AlterColumn("dbo.Boundary_Source", "source", c => c.String(maxLength: 20, fixedLength: true, unicode: false));
            AlterColumn("dbo.Program", "max_attempts", c => c.Int());
            DropColumn("dbo.Report_Inform", "boundary_source_id");
        }
    }
}
