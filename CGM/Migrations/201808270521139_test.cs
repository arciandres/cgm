namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Event", name: "event_category", newName: "event_category_id");
            RenameIndex(table: "dbo.Event", name: "IX_event_category", newName: "IX_event_category_id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Event", name: "IX_event_category_id", newName: "IX_event_category");
            RenameColumn(table: "dbo.Event", name: "event_category_id", newName: "event_category");
        }
    }
}
