namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddJobsIdToInform : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Report_Inform", "job_id", c => c.Int());
            CreateIndex("dbo.Report_Inform", "job_id");
            AddForeignKey("dbo.Report_Inform", "job_id", "Hangfire.Job", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report_Inform", "job_id", "Hangfire.Job");
            DropIndex("dbo.Report_Inform", new[] { "job_id" });
            DropColumn("dbo.Report_Inform", "job_id");
        }
    }
}
