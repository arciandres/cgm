namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class daysBackdateEndReports : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Program", "daysBack", c => c.Int());
            AddColumn("dbo.Report", "date_and_time_to_report_end", c => c.DateTime());
            DropColumn("dbo.Program", "retries");
            DropColumn("dbo.Program", "max_attempts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Program", "max_attempts", c => c.Int());
            AddColumn("dbo.Program", "retries", c => c.Int());
            DropColumn("dbo.Report", "date_and_time_to_report_end");
            DropColumn("dbo.Program", "daysBack");
        }
    }
}
