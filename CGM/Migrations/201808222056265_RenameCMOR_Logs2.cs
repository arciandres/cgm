namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameCMOR_Logs2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Event", name: "event_id", newName: "event_category");
            RenameIndex(table: "dbo.Event", name: "IX_event_id", newName: "IX_event_category");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Event", name: "IX_event_category", newName: "IX_event_id");
            RenameColumn(table: "dbo.Event", name: "event_category", newName: "event_id");
        }
    }
}
