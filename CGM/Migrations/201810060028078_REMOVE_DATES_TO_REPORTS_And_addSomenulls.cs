namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class REMOVE_DATES_TO_REPORTS_And_addSomenulls : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dates_To_Reports", "report_type_id", "dbo.Report_Type");
            DropForeignKey("dbo.Report", "dates_to_report_id", "dbo.Dates_To_Reports");
            DropIndex("dbo.Report", new[] { "dates_to_report_id" });
            DropIndex("dbo.Dates_To_Reports", new[] { "report_type_id" });
            AddColumn("dbo.Report", "is_auto", c => c.Int(nullable: false));
            AddColumn("dbo.Report", "destination", c => c.Int(nullable: false));
            AddColumn("dbo.Report", "operation_date", c => c.DateTime(storeType: "date"));
            AddColumn("dbo.Report", "market_id", c => c.Int());
            AddColumn("dbo.Report", "acquisition_id", c => c.Int());
            AlterColumn("dbo.Report", "date_and_time_to_report", c => c.DateTime());
            AlterColumn("dbo.Program", "execution_time", c => c.DateTime());
            AlterColumn("dbo.Program", "frequency", c => c.Time(precision: 7));
            CreateIndex("dbo.Report", "acquisition_id");
            AddForeignKey("dbo.Report", "acquisition_id", "Telemetry.Acquisition", "id");
            DropColumn("dbo.Report", "dates_to_report_id");
            DropColumn("dbo.Report", "report_class");
            DropTable("dbo.Dates_To_Reports");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Dates_To_Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        report_type_id = c.Int(nullable: false),
                        operation_date = c.DateTime(nullable: false, storeType: "date"),
                        destination = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Report", "report_class", c => c.Int(nullable: false));
            AddColumn("dbo.Report", "dates_to_report_id", c => c.Int(nullable: false));
            DropForeignKey("dbo.Report", "acquisition_id", "Telemetry.Acquisition");
            DropIndex("dbo.Report", new[] { "acquisition_id" });
            AlterColumn("dbo.Program", "frequency", c => c.Time(nullable: false, precision: 7));
            AlterColumn("dbo.Program", "execution_time", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Report", "date_and_time_to_report", c => c.DateTime(nullable: false));
            DropColumn("dbo.Report", "acquisition_id");
            DropColumn("dbo.Report", "market_id");
            DropColumn("dbo.Report", "operation_date");
            DropColumn("dbo.Report", "destination");
            DropColumn("dbo.Report", "is_auto");
            CreateIndex("dbo.Dates_To_Reports", "report_type_id");
            CreateIndex("dbo.Report", "dates_to_report_id");
            AddForeignKey("dbo.Report", "dates_to_report_id", "dbo.Dates_To_Reports", "Id");
            AddForeignKey("dbo.Dates_To_Reports", "report_type_id", "dbo.Report_Type", "id");
        }
    }
}
