namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMailSentFlgToAlarFire : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AlarmFire", "emailSent", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AlarmFire", "emailSent");
        }
    }
}
