namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNameToAlarm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Alarm", "name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Alarm", "name");
        }
    }
}
