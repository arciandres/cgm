namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class intToEmailSentAlarmFire : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AlarmFire", "emailSent", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AlarmFire", "emailSent", c => c.Boolean(nullable: false));
        }
    }
}
