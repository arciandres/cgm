namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFechasToCurvaTips : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CurvaTip", "fecha1", c => c.DateTime(nullable: false));
            AddColumn("dbo.CurvaTip", "fecha2", c => c.DateTime());
            DropColumn("dbo.CurvaTip", "month");
            DropColumn("dbo.CurvaTip", "year");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CurvaTip", "year", c => c.Int(nullable: false));
            AddColumn("dbo.CurvaTip", "month", c => c.Int(nullable: false));
            DropColumn("dbo.CurvaTip", "fecha2");
            DropColumn("dbo.CurvaTip", "fecha1");
        }
    }
}
