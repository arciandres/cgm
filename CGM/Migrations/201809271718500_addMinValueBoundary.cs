namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMinValueBoundary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Boundary", "min_measure_value", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Boundary", "min_measure_value");
        }
    }
}
