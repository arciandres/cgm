namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class makeIdEditableSubStatus : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Report_Inform", "substatus_id", "dbo.Substatus");
            DropPrimaryKey("dbo.Substatus");
            AlterColumn("dbo.Substatus", "id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Substatus", "id");
            AddForeignKey("dbo.Report_Inform", "substatus_id", "dbo.Substatus", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report_Inform", "substatus_id", "dbo.Substatus");
            DropPrimaryKey("dbo.Substatus");
            AlterColumn("dbo.Substatus", "id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Substatus", "id");
            AddForeignKey("dbo.Report_Inform", "substatus_id", "dbo.Substatus", "id");
        }
    }
}
