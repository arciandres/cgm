namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ALotOfChangesIncludeFilterChangeOfKeyBoundaryMeter : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Reports", newName: "Report");
            DropForeignKey("dbo.Report_Inform", "quantity_id", "Telemetry.Quantity");
            DropForeignKey("dbo.Boundary", "meter_id2", "Telemetry.Meter");
            DropForeignKey("dbo.Boundary", "meter_id", "Telemetry.Meter");
            DropIndex("dbo.Boundary", new[] { "meter_id" });
            DropIndex("dbo.Boundary", new[] { "meter_id2" });
            DropIndex("dbo.Report_Inform", new[] { "quantity_id" });
            RenameColumn(table: "dbo.Report_Inform", name: "success", newName: "status");
            CreateTable(
                "Telemetry.Filtro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30, unicode: false),
                        FilterType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Telemetry.Program",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30),
                        Destination = c.Int(nullable: false),
                        ExecutionTime = c.Time(nullable: false, precision: 7),
                        IdFiltro = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("Telemetry.Filtro", t => t.IdFiltro, cascadeDelete: true)
                .Index(t => t.IdFiltro);
            
            CreateTable(
                "dbo.FilterMeter",
                c => new
                    {
                        FiltroId = c.Int(nullable: false),
                        MeterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FiltroId, t.MeterId })
                .ForeignKey("Telemetry.Filtro", t => t.FiltroId, cascadeDelete: true)
                .ForeignKey("Telemetry.Meter", t => t.MeterId, cascadeDelete: true)
                .Index(t => t.FiltroId)
                .Index(t => t.MeterId);
            
            AddColumn("dbo.Measure", "QuantityId", c => c.Int());
            AddColumn("dbo.Report_Inform", "EventId", c => c.Int(nullable: false));
            AddColumn("Telemetry.Meter", "is_backup", c => c.Boolean());
            AddColumn("Telemetry.Meter", "BoundaryId", c => c.String(maxLength: 8, fixedLength: true));
            CreateIndex("dbo.Measure", "QuantityId");
            CreateIndex("dbo.Report_Inform", "EventId");
            CreateIndex("Telemetry.Meter", "BoundaryId");
            AddForeignKey("dbo.Measure", "QuantityId", "Telemetry.Quantity", "id");
            AddForeignKey("dbo.Report_Inform", "EventId", "dbo.Event", "id", cascadeDelete: true);
            AddForeignKey("Telemetry.Meter", "BoundaryId", "dbo.Boundary", "id");
            DropColumn("dbo.Boundary", "multiplicative_factor");
            DropColumn("dbo.Boundary", "meter_id"); //Se botaron estas columnas manualmente
            DropColumn("dbo.Boundary", "meter_id2");
            DropColumn("dbo.Report_Inform", "quantity_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Report_Inform", "quantity_id", c => c.Int());
            AddColumn("dbo.Boundary", "meter_id2", c => c.Int());
            AddColumn("dbo.Boundary", "meter_id", c => c.Int());
            AddColumn("dbo.Boundary", "multiplicative_factor", c => c.Double());
            DropForeignKey("Telemetry.Program", "IdFiltro", "Telemetry.Filtro");
            DropForeignKey("dbo.FilterMeter", "MeterId", "Telemetry.Meter");
            DropForeignKey("dbo.FilterMeter", "FiltroId", "Telemetry.Filtro");
            DropForeignKey("Telemetry.Meter", "BoundaryId", "dbo.Boundary");
            DropForeignKey("dbo.Report_Inform", "EventId", "dbo.Event");
            DropForeignKey("dbo.Measure", "QuantityId", "Telemetry.Quantity");
            DropIndex("dbo.FilterMeter", new[] { "MeterId" });
            DropIndex("dbo.FilterMeter", new[] { "FiltroId" });
            DropIndex("Telemetry.Program", new[] { "IdFiltro" });
            DropIndex("Telemetry.Meter", new[] { "BoundaryId" });
            DropIndex("dbo.Report_Inform", new[] { "EventId" });
            DropIndex("dbo.Measure", new[] { "QuantityId" });
            DropColumn("Telemetry.Meter", "BoundaryId");
            DropColumn("Telemetry.Meter", "is_backup");
            DropColumn("dbo.Report_Inform", "EventId");
            DropColumn("dbo.Measure", "QuantityId");
            DropTable("dbo.FilterMeter");
            DropTable("Telemetry.Program");
            DropTable("Telemetry.Filtro");
            RenameColumn(table: "dbo.Report_Inform", name: "status", newName: "success");
            CreateIndex("dbo.Report_Inform", "quantity_id");
            CreateIndex("dbo.Boundary", "meter_id2");
            CreateIndex("dbo.Boundary", "meter_id");
            AddForeignKey("dbo.Boundary", "meter_id", "Telemetry.Meter", "id");
            AddForeignKey("dbo.Boundary", "meter_id2", "Telemetry.Meter", "id");
            AddForeignKey("dbo.Report_Inform", "quantity_id", "Telemetry.Quantity", "id");
            RenameTable(name: "dbo.Report", newName: "Reports");
        }
    }
}
