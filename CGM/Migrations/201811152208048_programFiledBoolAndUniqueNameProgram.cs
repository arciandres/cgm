namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class programFiledBoolAndUniqueNameProgram : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Program", new[] { "name" });
            AlterColumn("dbo.Program", "filed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Program", "filed", c => c.Boolean());
            CreateIndex("dbo.Program", "name", unique: true);
        }
    }
}
