namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUnusedTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CMOR_Settings", "boundary_id", "dbo.Boundary_Source");
            DropForeignKey("dbo.C_h_CMOR", "boundary_id", "dbo.Boundary");
            DropIndex("dbo.CMOR_Settings", new[] { "boundary_id" });
            DropIndex("dbo.C_h_CMOR", new[] { "boundary_id" });
            DropTable("dbo.CMOR_Settings");
            DropTable("dbo.C_h_CMOR");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.C_h_CMOR",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        boundary_id = c.Int(nullable: false),
                        operation_date = c.DateTime(nullable: false, storeType: "date"),
                        hour_0 = c.Double(nullable: false),
                        hourt_24 = c.Double(nullable: false),
                        is_right = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.CMOR_Settings",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fmailv = c.Int(nullable: false),
                        boundary_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateIndex("dbo.C_h_CMOR", "boundary_id");
            CreateIndex("dbo.CMOR_Settings", "boundary_id");
            AddForeignKey("dbo.C_h_CMOR", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.CMOR_Settings", "boundary_id", "dbo.Boundary_Source", "Id");
        }
    }
}
