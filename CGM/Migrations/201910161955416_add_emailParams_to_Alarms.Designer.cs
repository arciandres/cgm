// <auto-generated />
namespace CGM.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class add_emailParams_to_Alarms : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add_emailParams_to_Alarms));
        
        string IMigrationMetadata.Id
        {
            get { return "201910161955416_add_emailParams_to_Alarms"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
