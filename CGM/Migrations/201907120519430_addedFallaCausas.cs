namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedFallaCausas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FallaCausa",
                c => new
                    {
                        id = c.Int(nullable: false),
                        causa = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Falla",
                c => new
                    {
                        id = c.Int(nullable: false),
                        causa_id = c.Int(nullable: false),
                        boundary_id = c.Int(nullable: false),
                        date_init = c.DateTime(nullable: false),
                        date_end = c.DateTime(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Falla");
            DropTable("dbo.FallaCausa");
        }
    }
}
