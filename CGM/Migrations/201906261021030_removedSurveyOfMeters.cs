namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedSurveyOfMeters : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Telemetry.Meter", "ShareSurveyId", "Survey.ShareSurvey");
            DropIndex("Telemetry.Meter", new[] { "ShareSurveyId" });
            DropColumn("Telemetry.Meter", "ShareSurveyId");
        }
        
        public override void Down()
        {
           AddColumn("Telemetry.Meter", "ShareSurveyId", c => c.Guid(nullable: false));
            CreateIndex("Telemetry.Meter", "ShareSurveyId");
            AddForeignKey("Telemetry.Meter", "ShareSurveyId", "Survey.ShareSurvey", "Id", cascadeDelete: true);
        }
    }
}
