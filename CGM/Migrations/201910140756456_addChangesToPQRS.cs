namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addChangesToPQRS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PQRS", "title", c => c.String());
            AddColumn("dbo.PQRS_Message", "files", c => c.String());
            AlterColumn("dbo.PQRS", "date_closed", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PQRS", "date_closed", c => c.DateTime(nullable: false));
            DropColumn("dbo.PQRS_Message", "files");
            DropColumn("dbo.PQRS", "title");
        }
    }
}
