namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddJobIdToPrograms : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Falla");
            AddColumn("dbo.Program", "job_id", c => c.Int());
            AlterColumn("dbo.Falla", "id", c => c.Int(nullable: false));
            AlterColumn("dbo.Falla", "date_end", c => c.DateTime());
            AddPrimaryKey("dbo.Falla", "id");
            DropColumn("dbo.Program", "continuous");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Program", "continuous", c => c.Boolean());
            DropPrimaryKey("dbo.Falla");
            AlterColumn("dbo.Falla", "date_end", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Falla", "id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Program", "job_id");
            AddPrimaryKey("dbo.Falla", "id");
        }
    }
}
