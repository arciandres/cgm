namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addGroupsMeasure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Measure", "acquisition_item_id", "Telemetry.Acquisition_Item");
            DropForeignKey("dbo.Measure", "report_inform_id", "dbo.Report_Inform");
            DropIndex("dbo.Measure", new[] { "acquisition_item_id" });
            DropIndex("dbo.Measure", new[] { "report_inform_id" });
            CreateTable(
                "dbo.Measure_Group",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        acquisition_item_id = c.Int(),
                        report_inform_id = c.Long(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("Telemetry.Acquisition_Item", t => t.acquisition_item_id)
                .ForeignKey("dbo.Report_Inform", t => t.report_inform_id)
                .Index(t => t.acquisition_item_id)
                .Index(t => t.report_inform_id);
            
            AddColumn("dbo.Report_Inform", "meter_id", c => c.Int());
            AddColumn("dbo.Measure", "group_measure_id", c => c.Long());
            CreateIndex("dbo.Report_Inform", "meter_id");
            CreateIndex("dbo.Measure", "group_measure_id");
            AddForeignKey("dbo.Measure", "group_measure_id", "dbo.Measure_Group", "id");
            AddForeignKey("dbo.Report_Inform", "meter_id", "Telemetry.Meter", "id");
            DropColumn("dbo.Measure", "acquisition_item_id");
            DropColumn("dbo.Measure", "report_inform_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Measure", "report_inform_id", c => c.Long());
            AddColumn("dbo.Measure", "acquisition_item_id", c => c.Int());
            DropForeignKey("dbo.Report_Inform", "meter_id", "Telemetry.Meter");
            DropForeignKey("dbo.Measure_Group", "report_inform_id", "dbo.Report_Inform");
            DropForeignKey("dbo.Measure", "group_measure_id", "dbo.Measure_Group");
            DropForeignKey("dbo.Measure_Group", "acquisition_item_id", "Telemetry.Acquisition_Item");
            DropIndex("dbo.Measure", new[] { "group_measure_id" });
            DropIndex("dbo.Measure_Group", new[] { "report_inform_id" });
            DropIndex("dbo.Measure_Group", new[] { "acquisition_item_id" });
            DropIndex("dbo.Report_Inform", new[] { "meter_id" });
            DropColumn("dbo.Measure", "group_measure_id");
            DropColumn("dbo.Report_Inform", "meter_id");
            DropTable("dbo.Measure_Group");
            CreateIndex("dbo.Measure", "report_inform_id");
            CreateIndex("dbo.Measure", "acquisition_item_id");
            AddForeignKey("dbo.Measure", "report_inform_id", "dbo.Report_Inform", "id");
            AddForeignKey("dbo.Measure", "acquisition_item_id", "Telemetry.Acquisition_Item", "Id");
        }
    }
}
