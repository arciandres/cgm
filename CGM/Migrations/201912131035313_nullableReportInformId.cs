namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullableReportInformId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AlarmFire", "reportInformId", "dbo.Report_Inform");
            DropIndex("dbo.AlarmFire", new[] { "reportInformId" });
            AlterColumn("dbo.AlarmFire", "reportInformId", c => c.Long());
            CreateIndex("dbo.AlarmFire", "reportInformId");
            AddForeignKey("dbo.AlarmFire", "reportInformId", "dbo.Report_Inform", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AlarmFire", "reportInformId", "dbo.Report_Inform");
            DropIndex("dbo.AlarmFire", new[] { "reportInformId" });
            AlterColumn("dbo.AlarmFire", "reportInformId", c => c.Long(nullable: false));
            CreateIndex("dbo.AlarmFire", "reportInformId");
            AddForeignKey("dbo.AlarmFire", "reportInformId", "dbo.Report_Inform", "id", cascadeDelete: true);
        }
    }
}
