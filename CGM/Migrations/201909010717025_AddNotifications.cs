namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNotifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notification",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        message = c.String(),
                        alarmFire_id = c.Int(),
                        date_created = c.DateTime(nullable: false),
                        notification_group_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AlarmFire", t => t.alarmFire_id)
                .ForeignKey("dbo.NotificationGroup", t => t.notification_group_id, cascadeDelete: true)
                .Index(t => t.alarmFire_id)
                .Index(t => t.notification_group_id);
            
            CreateTable(
                "dbo.NotificationGroup",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        message = c.String(),
                        is_active = c.Boolean(nullable: false),
                        alarm_id = c.Int(),
                        date_read = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Alarm", t => t.alarm_id)
                .Index(t => t.alarm_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notification", "notification_group_id", "dbo.NotificationGroup");
            DropForeignKey("dbo.NotificationGroup", "alarm_id", "dbo.Alarm");
            DropForeignKey("dbo.Notification", "alarmFire_id", "dbo.AlarmFire");
            DropIndex("dbo.NotificationGroup", new[] { "alarm_id" });
            DropIndex("dbo.Notification", new[] { "notification_group_id" });
            DropIndex("dbo.Notification", new[] { "alarmFire_id" });
            DropTable("dbo.NotificationGroup");
            DropTable("dbo.Notification");
        }
    }
}
