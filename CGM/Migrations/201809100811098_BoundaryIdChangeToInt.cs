namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoundaryIdChangeToInt : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Measure", "Boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.Boundary_To_Report", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.C_h_CMOR", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.FilterBoundary", "BoundaryId", "dbo.Boundary");
            DropForeignKey("Telemetry.Meter", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.Measure_AP", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.Report_Inform", "boundary_id", "dbo.Boundary");
            DropIndex("dbo.Boundary_To_Report", new[] { "boundary_id" });
            DropIndex("dbo.C_h_CMOR", new[] { "boundary_id" });
            DropIndex("Telemetry.Meter", new[] { "boundary_id" });
            DropIndex("dbo.Measure", new[] { "Boundary_id" });
            DropIndex("dbo.Report_Inform", new[] { "boundary_id" });
            DropIndex("dbo.Measure_AP", new[] { "boundary_id" });
            DropIndex("dbo.FilterBoundary", new[] { "BoundaryId" });
            DropPrimaryKey("dbo.Boundary");
            DropPrimaryKey("dbo.FilterBoundary");
            AlterColumn("dbo.Boundary", "id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Boundary_To_Report", "boundary_id", c => c.Int(nullable: false));
            AlterColumn("dbo.C_h_CMOR", "boundary_id", c => c.Int(nullable: false));
            AlterColumn("Telemetry.Meter", "boundary_id", c => c.Int());
            AlterColumn("dbo.Report_Inform", "boundary_id", c => c.Int(nullable: false));
            AlterColumn("dbo.Measure_AP", "boundary_id", c => c.Int());
            AlterColumn("dbo.FilterBoundary", "BoundaryId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Boundary", "id");
            AddPrimaryKey("dbo.FilterBoundary", new[] { "FiltroId", "BoundaryId" });
            CreateIndex("dbo.Boundary_To_Report", "boundary_id");
            CreateIndex("dbo.C_h_CMOR", "boundary_id");
            CreateIndex("Telemetry.Meter", "boundary_id");
            CreateIndex("dbo.Measure_AP", "boundary_id");
            CreateIndex("dbo.Report_Inform", "boundary_id");
            CreateIndex("dbo.FilterBoundary", "BoundaryId");
            AddForeignKey("dbo.Boundary_To_Report", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.C_h_CMOR", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.FilterBoundary", "BoundaryId", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("Telemetry.Meter", "boundary_id", "dbo.Boundary", "id");
            AddForeignKey("dbo.Measure_AP", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.Report_Inform", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            DropColumn("dbo.Measure", "Boundary_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Measure", "Boundary_id", c => c.String(maxLength: 8, fixedLength: true));
            DropForeignKey("dbo.Report_Inform", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.Measure_AP", "boundary_id", "dbo.Boundary");
            DropForeignKey("Telemetry.Meter", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.FilterBoundary", "BoundaryId", "dbo.Boundary");
            DropForeignKey("dbo.C_h_CMOR", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.Boundary_To_Report", "boundary_id", "dbo.Boundary");
            DropIndex("dbo.FilterBoundary", new[] { "BoundaryId" });
            DropIndex("dbo.Report_Inform", new[] { "boundary_id" });
            DropIndex("dbo.Measure_AP", new[] { "boundary_id" });
            DropIndex("Telemetry.Meter", new[] { "boundary_id" });
            DropIndex("dbo.C_h_CMOR", new[] { "boundary_id" });
            DropIndex("dbo.Boundary_To_Report", new[] { "boundary_id" });
            DropPrimaryKey("dbo.FilterBoundary");
            DropPrimaryKey("dbo.Boundary");
            AlterColumn("dbo.FilterBoundary", "BoundaryId", c => c.String(nullable: false, maxLength: 8, fixedLength: true));
            AlterColumn("dbo.Measure_AP", "boundary_id", c => c.String(maxLength: 8, fixedLength: true));
            AlterColumn("dbo.Report_Inform", "boundary_id", c => c.String(nullable: false, maxLength: 8, fixedLength: true));
            AlterColumn("Telemetry.Meter", "boundary_id", c => c.String(maxLength: 8, fixedLength: true));
            AlterColumn("dbo.C_h_CMOR", "boundary_id", c => c.String(nullable: false, maxLength: 8, fixedLength: true));
            AlterColumn("dbo.Boundary_To_Report", "boundary_id", c => c.String(nullable: false, maxLength: 8, fixedLength: true));
            AlterColumn("dbo.Boundary", "id", c => c.String(nullable: false, maxLength: 8, fixedLength: true));
            AddPrimaryKey("dbo.FilterBoundary", new[] { "FiltroId", "BoundaryId" });
            AddPrimaryKey("dbo.Boundary", "id");
            CreateIndex("dbo.FilterBoundary", "BoundaryId");
            CreateIndex("dbo.Measure_AP", "boundary_id");
            CreateIndex("dbo.Report_Inform", "boundary_id");
            CreateIndex("dbo.Measure", "Boundary_id");
            CreateIndex("Telemetry.Meter", "boundary_id");
            CreateIndex("dbo.C_h_CMOR", "boundary_id");
            CreateIndex("dbo.Boundary_To_Report", "boundary_id");
            AddForeignKey("dbo.Report_Inform", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.Measure_AP", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("Telemetry.Meter", "boundary_id", "dbo.Boundary", "id");
            AddForeignKey("dbo.FilterBoundary", "BoundaryId", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.C_h_CMOR", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.Boundary_To_Report", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
            AddForeignKey("dbo.Measure", "Boundary_id", "dbo.Boundary", "id");
        }
    }
}
