namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncludeBoundaryInFilter : DbMigration
    {
        public override void Up()
        {
            MoveTable(name: "Telemetry.Filtro", newSchema: "dbo");
            MoveTable(name: "Telemetry.Program", newSchema: "dbo");
            CreateTable(
                "dbo.FilterBoundary",
                c => new
                    {
                        FiltroId = c.Int(nullable: false),
                        BoundaryId = c.String(nullable: false, maxLength: 8, fixedLength: true),
                    })
                .PrimaryKey(t => new { t.FiltroId, t.BoundaryId })
                .ForeignKey("dbo.Filtro", t => t.FiltroId, cascadeDelete: true)
                .ForeignKey("dbo.Boundary", t => t.BoundaryId, cascadeDelete: true)
                .Index(t => t.FiltroId)
                .Index(t => t.BoundaryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FilterBoundary", "BoundaryId", "dbo.Boundary");
            DropForeignKey("dbo.FilterBoundary", "FiltroId", "dbo.Filtro");
            DropIndex("dbo.FilterBoundary", new[] { "BoundaryId" });
            DropIndex("dbo.FilterBoundary", new[] { "FiltroId" });
            DropTable("dbo.FilterBoundary");
            MoveTable(name: "dbo.Program", newSchema: "Telemetry");
            MoveTable(name: "dbo.Filtro", newSchema: "Telemetry");
        }
    }
}
