namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAlarmClasses2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Alarm", "alarmCase_id", "dbo.AlarmCase");
            DropPrimaryKey("dbo.AlarmCase");
            AddColumn("dbo.AlarmCase", "name", c => c.String());
            AlterColumn("dbo.AlarmCase", "id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.AlarmCase", "id");
            AddForeignKey("dbo.Alarm", "alarmCase_id", "dbo.AlarmCase", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Alarm", "alarmCase_id", "dbo.AlarmCase");
            DropPrimaryKey("dbo.AlarmCase");
            AlterColumn("dbo.AlarmCase", "id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.AlarmCase", "name");
            AddPrimaryKey("dbo.AlarmCase", "id");
            AddForeignKey("dbo.Alarm", "alarmCase_id", "dbo.AlarmCase", "id", cascadeDelete: true);
        }
    }
}
