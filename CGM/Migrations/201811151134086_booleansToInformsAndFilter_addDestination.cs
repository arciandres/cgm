namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class booleansToInformsAndFilter_addDestination : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Destination",
                c => new
                    {
                        id = c.Int(nullable: false),
                        name = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);
            
            RenameColumn("dbo.Filtro",  "filter_type",  "destination_id");
            AddColumn("dbo.Filtro",     "defaultFilter", c => c.Boolean(nullable: false));
            RenameColumn("dbo.Program", "destination", "destination_id");
            RenameColumn("dbo.Report",  "destination", "destination_id");
            AddColumn("dbo.Report_Inform", "assignedReading", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Filtro", "destination_id");
            CreateIndex("dbo.Program", "destination_id");
            CreateIndex("dbo.Report", "destination_id");
            AddForeignKey("dbo.Filtro", "destination_id", "dbo.Destination", "id", cascadeDelete: true);
            AddForeignKey("dbo.Program", "destination_id", "dbo.Destination", "id", cascadeDelete: true);
            AddForeignKey("dbo.Report", "destination_id", "dbo.Destination", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            AddColumn("dbo.Report", "destination", c => c.Int(nullable: false));
            AddColumn("dbo.Program", "destination", c => c.Int(nullable: false));
            AddColumn("dbo.Filtro", "filter_type", c => c.Int(nullable: false));
            DropForeignKey("dbo.Report", "destination_id", "dbo.Destination");
            DropForeignKey("dbo.Program", "destination_id", "dbo.Destination");
            DropForeignKey("dbo.Filtro", "destination_id", "dbo.Destination");
            DropIndex("dbo.Report", new[] { "destination_id" });
            DropIndex("dbo.Program", new[] { "destination_id" });
            DropIndex("dbo.Filtro", new[] { "destination_id" });
            DropColumn("dbo.Report_Inform", "assignedReading");
            RenameColumn("dbo.Report", "destination_id", "destination");
            RenameColumn("dbo.Program", "destination_id", "destination");
            DropColumn("dbo.Filtro", "defaultFilter");
            RenameColumn("dbo.Filtro", "destination_id", "filter_type");
            DropTable("dbo.Destination");
        }
    }
}
