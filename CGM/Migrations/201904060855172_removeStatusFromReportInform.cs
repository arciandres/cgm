namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeStatusFromReportInform : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Report_Inform", "status_id", "dbo.Status");
            DropForeignKey("dbo.Substatus", "idStatus", "dbo.Status");
            DropForeignKey("dbo.Report", "status_id", "dbo.Status");
            DropIndex("dbo.Report_Inform", new[] { "status_id" });
            DropPrimaryKey("dbo.Status");
            AlterColumn("dbo.Status", "id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Status", "id");
            AddForeignKey("dbo.Substatus", "idStatus", "dbo.Status", "id", cascadeDelete: true);
            AddForeignKey("dbo.Report", "status_id", "dbo.Status", "id", cascadeDelete: true);
            DropColumn("dbo.Report_Inform", "status_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Report_Inform", "status_id", c => c.Int(nullable: false));
            DropForeignKey("dbo.Report", "status_id", "dbo.Status");
            DropForeignKey("dbo.Substatus", "idStatus", "dbo.Status");
            DropPrimaryKey("dbo.Status");
            AlterColumn("dbo.Status", "id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Status", "id");
            CreateIndex("dbo.Report_Inform", "status_id");
            AddForeignKey("dbo.Report", "status_id", "dbo.Status", "id", cascadeDelete: true);
            AddForeignKey("dbo.Substatus", "idStatus", "dbo.Status", "id", cascadeDelete: true);
            AddForeignKey("dbo.Report_Inform", "status_id", "dbo.Status", "id", cascadeDelete: true);
        }
    }
}
