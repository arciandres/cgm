namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsUniqueFilterProgramName : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Filtro", "Name", unique: true);
            CreateIndex("dbo.Program", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Program", new[] { "Name" });
            DropIndex("dbo.Filtro", new[] { "Name" });
        }
    }
}
