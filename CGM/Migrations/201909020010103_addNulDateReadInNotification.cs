namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNulDateReadInNotification : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.NotificationGroup", "date_read", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.NotificationGroup", "date_read", c => c.DateTime(nullable: false));
        }
    }
}
