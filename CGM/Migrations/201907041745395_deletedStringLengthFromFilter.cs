namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deletedStringLengthFromFilter : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Filtro", new[] { "Name" });
            AlterColumn("dbo.Filtro", "Name", c => c.String(nullable: false, maxLength: 8000, unicode: false));
            CreateIndex("dbo.Filtro", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Filtro", new[] { "Name" });
            AlterColumn("dbo.Filtro", "Name", c => c.String(nullable: false, maxLength: 30, unicode: false));
            CreateIndex("dbo.Filtro", "Name", unique: true);
        }
    }
}
