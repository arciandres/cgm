namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAlarmClasses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alarm",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        alarmCase_id = c.Int(nullable: false),
                        priority_id = c.Int(nullable: false),
                        parameters = c.String(),
                        isActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AlarmCase", t => t.alarmCase_id, cascadeDelete: true)
                .ForeignKey("dbo.Priority", t => t.priority_id, cascadeDelete: false)
                .Index(t => t.alarmCase_id)
                .Index(t => t.priority_id);
            
            CreateTable(
                "dbo.AlarmCase",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        destination_id = c.Int(nullable: false),
                        priority_id = c.Int(nullable: false),
                        description = c.String(),
                        parameters = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Destination", t => t.destination_id, cascadeDelete: true)
                .ForeignKey("dbo.Priority", t => t.priority_id, cascadeDelete: true)
                .Index(t => t.destination_id)
                .Index(t => t.priority_id);
            
            CreateTable(
                "dbo.AlarmFire",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        alarmId = c.Int(nullable: false),
                        reportInformId = c.Int(nullable: false),
                        firedOn = c.DateTime(nullable: false),
                        details = c.String(),
                        isActive = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Alarm", t => t.alarmId, cascadeDelete: true)
                .Index(t => t.alarmId);
            
            CreateTable(
                "dbo.AlarmFiltro",
                c => new
                    {
                        Alarm_id = c.Int(nullable: false),
                        Filtro_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Alarm_id, t.Filtro_Id })
                .ForeignKey("dbo.Alarm", t => t.Alarm_id, cascadeDelete: true)
                .ForeignKey("dbo.Filtro", t => t.Filtro_Id, cascadeDelete: false)
                .Index(t => t.Alarm_id)
                .Index(t => t.Filtro_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Alarm", "priority_id", "dbo.Priority");
            DropForeignKey("dbo.AlarmFiltro", "Filtro_Id", "dbo.Filtro");
            DropForeignKey("dbo.AlarmFiltro", "Alarm_id", "dbo.Alarm");
            DropForeignKey("dbo.AlarmFire", "alarmId", "dbo.Alarm");
            DropForeignKey("dbo.Alarm", "alarmCase_id", "dbo.AlarmCase");
            DropForeignKey("dbo.AlarmCase", "priority_id", "dbo.Priority");
            DropForeignKey("dbo.AlarmCase", "destination_id", "dbo.Destination");
            DropIndex("dbo.AlarmFiltro", new[] { "Filtro_Id" });
            DropIndex("dbo.AlarmFiltro", new[] { "Alarm_id" });
            DropIndex("dbo.AlarmFire", new[] { "alarmId" });
            DropIndex("dbo.AlarmCase", new[] { "priority_id" });
            DropIndex("dbo.AlarmCase", new[] { "destination_id" });
            DropIndex("dbo.Alarm", new[] { "priority_id" });
            DropIndex("dbo.Alarm", new[] { "alarmCase_id" });
            DropTable("dbo.AlarmFiltro");
            DropTable("dbo.AlarmFire");
            DropTable("dbo.AlarmCase");
            DropTable("dbo.Alarm");
        }
    }
}
