namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedFormConfigurationToMeter : DbMigration
    {
        public override void Up()
        {
            AddColumn("Telemetry.Meter", "formConfiguration_id", c => c.Int());
            CreateIndex("Telemetry.Meter", "formConfiguration_id");
            AddForeignKey("Telemetry.Meter", "formConfiguration_id", "dbo.ParamFormInstance", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("Telemetry.Meter", "formConfiguration_id", "dbo.ParamFormInstance");
            DropIndex("Telemetry.Meter", new[] { "formConfiguration_id" });
            DropColumn("Telemetry.Meter", "formConfiguration_id");
        }
    }
}
