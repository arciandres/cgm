﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
    /// <summary>
    /// Se asigna a un objeto una vez se crea o se interactúa con él, para describir el estado en el que se encuentra
    /// </summary>
    public class Status
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }
        /// <summary>
        /// Nombre del estado.
        /// 1 o null:Pendiente; 2:En proceso; 3:Error; 4:Advertencia; 5:Exitoso
        /// </summary>
        public string name { get; set; }
        public virtual ICollection<Substatus> Substatuslist { get; set; }
    }
}