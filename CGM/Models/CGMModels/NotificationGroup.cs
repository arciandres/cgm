namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Simple sistema de notificaciones para mostrar al usuario. 
    /// Agrupa notificaciones del mismo tipo para una mejor lectura.
    /// </summary>
    [DisplayName("Grupo de notificaciones")]
    [Table("NotificationGroup")]
    public partial class NotificationGroup
    {
        public int id { get; set; }
        /// <summary>
        /// Not every notification has a message, since many of them are taken from templates in AlarmCases.
        /// Although for speed and store purposes, it's better to keep every message.
        /// </summary>
        [DisplayName("Mensaje")]
        public string message { get; set; }
        /// <summary>
        /// It's true when created and stored, and false when shown to the user.
        /// </summary>
        [DisplayName("Activa")]
        public bool is_active { get; set; }
        ///// <summary>
        ///// Indicative color for the respective message. 
        ///// 0: Secondary (gray); 1: Info (Blue); 2: Success (Green); 3: Danger (Red)
        ///// </summary>
        //int type_id { get; set; }
        /// <summary>
        /// It gets associated with an alarm case, that has a template to be displayed on screen. 
        /// </summary>
        [ForeignKey("Alarm")]
        public int? alarm_id { get; set; }
        // Date when the user opens the window and sees the notification.
        public DateTime? date_read { get; set; }
        public virtual List<Notification> Notifications { get; set; }
        public virtual Alarm Alarm { get; set; }
    }
}
