namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Boundary_Source
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Boundary_Source()
        {
            Boundary = new HashSet<Boundary>();
        }
        public int Id { get; set; }

        [StringLength(20)]
        [DisplayName("Fuente de datos")]
        public string source { get; set; }

        [StringLength(50)]
        public string mail_to_send { get; set; }

        public int fmail_value_1 { get; set; }

        public int fmail_value_2 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? date_alert { get; set; }

        public bool? active_alert { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Boundary> Boundary { get; set; }
    }
}
