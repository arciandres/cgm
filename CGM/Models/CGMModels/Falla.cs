namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Se consigna la raz�n por la cual una frontera se declara como inactiva. No necesariamente tiene una falla, pero
    /// por convenci�n y manejo com�n del t�rmino, se le llama as�.
    /// </summary>
    [Table("Falla")]
    public partial class Falla
    {
        /// <summary>
        /// Identificaodr de la falla.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        /// <summary>
        /// Raz�n por la cual se declara la frontera como inactiva
        /// </summary>
        public int causa_id { get; set; }
        /// <summary>
        /// Identificador de la frontera
        /// </summary>
        public int boundary_id { get; set; }
        /// <summary>
        /// Fecha cuando la frontera se declara inactiva
        /// </summary>
        public DateTime date_init { get; set; }
        /// <summary>
        /// Fecha cuando la frontera vuelve a estado activo. Nulo cuando no se ha reactivado la frontera.
        /// </summary>
        public DateTime? date_end { get; set; }

    }
}
