namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Measure")]
    public partial class Measure
    {
        public long id { get; set; }
        [DisplayName("Fecha y hora")]
        public DateTime measure_date { get; set; }

        [Column("measure")]
        [DisplayName("Valor")]
        public double measure1 { get; set; }
        [ForeignKey("Measure_Group")]
        public long? group_measure_id { get; set; }
        public virtual Measure_Group Measure_Group { get; set; }
    }
}
