namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Hilo de mensajes en un PQRS
    /// Preguntas - Quejas - Reclamos - Sugerencias
    /// </summary>
    [Table("PQRS_Message")]
    public partial class PQRS_Message
    {
        public int id { get; set; }
        /// <summary>
        /// Identificador de PQRS correspondiente
        /// </summary>
        [DisplayName("PQRS")]
        [ForeignKey("PQRS")]
        public int PQRS_id { get; set; }

        /// <summary>
        /// Contenido del mensaje
        /// </summary>
        [DisplayName("Mensaje")]
        public string message { get; set; }

        /// <summary>
        /// Usuario que cre� el mensaje
        /// </summary>
        [DisplayName("Usuario")]
        [ForeignKey("User")]
        public string user_id { get; set; }
        
        /// <summary>
        /// Archivos adjuntos al mensaje. La ruta de estos se guardan en formato JSON.
        /// </summary>
        [DisplayName("Archivos")]
        public string files { get; set; }

        /// <summary>
        /// Fecha de creaci�n para tener referencia de la l�nea de tiempo
        /// </summary>
        [DisplayName("Fecha de creaci�n")]
        public DateTime date_created { get; set; }

        public virtual AspNetUsers User{ get; set; }
        public virtual PQRS PQRS { get; set; }
    }
}
