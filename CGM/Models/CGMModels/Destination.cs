namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Hace de las veces de 'm�dulo' en la aplicaci�n.
    /// 'Frontera' se refiere a la carga de medidas a la plataforma
    /// 'Reporte al mercado' se refiere a los reportes a XM
    /// </summary>
    public partial class Destination
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(30)]
        public string name { get; set; }
    }
}
