﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
    public class VParam
    {
        public int id { get; set; }
        public int idAnswerOption { get; set; }
        public int idQuestion { get; set; }
        public int type_id { get; set; }
        public string value { get; set; }
        public string alias { get; set; }
        public string name { get; set; }
    }
}