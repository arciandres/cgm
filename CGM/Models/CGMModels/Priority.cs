namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity.Spatial;
    using System.ComponentModel;

    [Table("Priority", Schema = "dbo")]
    public partial class Priority
    {
        public int id { get; set; }
        [DisplayName("Valor")]
        public int value { get; set; }
        [DisplayName("Nivel")]
        public string name { get; set; }

    }
}
