namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Define los posibles casos que podr�an disparar una alarma, como valores excedidos, o contrase�as inv�lidas.
    /// </summary>
    [Table("AlarmCase")]
    [DisplayName("Caso")]
    public partial class AlarmCase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }
        [DisplayName("M�dulo")]
        [ForeignKey("Destination")]
        public int destination_id { get; set; }

        [ForeignKey("Priority")]
        public int priority_id { get; set; }

        [DisplayName("Descripci�n")]
        public string description { get; set; }
        [DisplayName("Nombre")]
        public string name { get; set; }
        [DisplayName("Permite m�ltiples")]
        public bool allows_multiple { get; set; }
        /// <summary>
        /// Par�meters of the current alarm
        /// </summary>
        [DisplayName("Par�metros")]
        public string parameters { get; set; } 
        // It was thought to be in a ParamForm, but it's so particular that better went for JSON, that will be parsed in a custom class object
        
        public virtual Priority Priority{ get; set; }
        public virtual Destination Destination{ get; set; }
        public virtual ICollection<Alarm> Alarms{ get; set; }
    }
}
