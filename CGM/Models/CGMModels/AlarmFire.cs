namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using CGM.Models.MyModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AlarmFire")]
    public partial class AlarmFire
    {
        public int id { get; set; }
        /// <summary>
        /// Alarma que se dispar�
        /// </summary>
        [ForeignKey("Alarm")]
        public int alarmId { get; set; }
        /// <summary>
        /// Identificador del evento que gener� la alarma
        /// </summary>
        [ForeignKey("Report_Inform")]
        public long? reportInformId { get; set; }
        /// <summary>
        /// Fecha y hora en que se dispar�
        /// </summary>
        [DisplayName("Fecha y hora")]
        public DateTime firedOn { get; set; }
        /// <summary>
        /// Detalles de lo ocurrido
        /// </summary>
        [DisplayName("Detalles")]
        public string details { get; set; }
        /// <summary>
        /// Notificaci�n activa. Se desactiva cuando el usuario la ha revisado.
        /// </summary>
        [DisplayName("Activa")]
        public bool isActive { get; set; }
        /// <summary>
        /// Bandera que dice si la alarma fue enviada por correo.
        /// 0: Alarm has emails deactivated
        /// 1: Mail not sent yet
        /// 2: Mail sent.
        /// </summary>
        [DisplayName("Activa")]
        public int emailSent { get; set; }
        public virtual Alarm Alarm { get; set; }
        public virtual Report_Inform Report_Inform { get; set; }
        [NotMapped]
        public CriticSummary CriticSummary { get; set; }


    }
}
