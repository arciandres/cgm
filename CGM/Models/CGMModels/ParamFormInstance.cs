namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Each set of answers (Ex. meter configuration), will be associated with the instance
    /// </summary>
    [Table("ParamFormInstance")]
    public partial class ParamFormInstance
    {
        public int id { get; set; }
        public int paramForm_id { get; set; }
        [ForeignKey("paramForm_id")]
        public virtual ParamForm Form { get; set; }
        public virtual ICollection<ParamAnswer> Answers { get; set; }
    }
}
