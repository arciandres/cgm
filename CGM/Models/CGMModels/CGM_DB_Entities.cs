namespace CGM.Models.CGMModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using CGM.Areas.SurveyApp.Models;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using CGM.Areas.Telemetry.Models;

    public partial class CGM_DB_Entities : DbContext
    {
        public CGM_DB_Entities()
            : base("name=CGM_DB_Entities")
        {
        }

        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Boundary> Boundary { get; set; }
        public virtual DbSet<Boundary_Source> Boundary_Source { get; set; }
        public virtual DbSet<Boundary_To_Report> Boundary_To_Report { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<Config_CM> Config_CM { get; set; }
        public virtual DbSet<ExceptionLog> ExceptionLog { get; set; }
        public virtual DbSet<LogUser> Log { get; set; }
        public virtual DbSet<Magnitude> Magnitude { get; set; }
        public virtual DbSet<Mail_Item> Mail_Item { get; set; }
        public virtual DbSet<Measure> Measure { get; set; }
        public virtual DbSet<Measure_AP> Measure_AP { get; set; }
        public virtual DbSet<Meter> Meter { get; set; }
        public virtual DbSet<Model_Meter> Model_Meter { get; set; }
        public virtual DbSet<Quantity> Quantity { get; set; }
        public virtual DbSet<Report_Inform> Report_Inform { get; set; }
        public virtual DbSet<Report_Type> Report_Type { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<Buffer_Measure> Buffer_Measure { get; set; }

        public virtual DbSet<SurveyTemplate> SurveyTemplates { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<QuestionImage> QuestionImages { get; set; }
        public virtual DbSet<Response> Responses { get; set; }
        public virtual DbSet<Response_Type> Response_Types { get; set; }
        public virtual DbSet<Survey> Surveys { get; set; }
        public virtual DbSet<SurveyMaster> SurveyMasters { get; set; }
        public virtual DbSet<SurveyResponse> SurveyResponse { get; set; }
        public virtual DbSet<DeviceUser> DevicesUser { get; set; }
        public virtual DbSet<ControlGroup> ControlGroup { get; set; }
        public virtual DbSet<TypeData> TypeData { get; set; }
        public virtual DbSet<DataSource> DataSource { get; set; }
        public virtual DbSet<DataSourceItem> DataSourceItem { get; set; }
        public virtual DbSet<ShareSurvey> ShareSurveys { get; set; }
        public virtual DbSet<ConfigurationSurvey> ConfigurationSurveys { get; set; }
        public virtual DbSet<ResponseFile> ResponseFile { get; set; }
        public virtual DbSet<Procedure> Procedures { get; set; }
        public virtual DbSet<Norm> Norms { get; set; }
        public virtual DbSet<InteresPlace> InteresPlaces { get; set; }
        public virtual DbSet<InteresPlaceImage> InteresPlaceImages { get; set; }
        public virtual DbSet<Event_Category> Event_Category { get; set; }
        public virtual DbSet<Substatus> Substatus { get; set; }
        public virtual DbSet<ClockSync> ClockSync{ get; set; }

        public virtual DbSet<CurvaTip> CurvaTip { get; set; }
        public virtual DbSet<CurvaTipMeasure> CurvaTipMeasure { get; set; }
        public virtual DbSet<CurvaTipMeasureGroup> CurvaTipMeasureGroup { get; set; }


        public virtual DbSet<Filtro> Filtro { get; set; }
        public virtual DbSet<Program> Program { get; set; }
        public virtual DbSet<Acquisition> Acquisition{ get; set; }
        public virtual DbSet<Acquisition_Item> Acquisition_Item{ get; set; }
        public virtual DbSet<Tag_Line> Tag_Line{ get; set; }
        public virtual DbSet<Measure_Group> Measure_Group{ get; set; }
        public virtual DbSet<Dependency> Dependencies{ get; set; }
        public virtual DbSet<Status> Status{ get; set; }
        public virtual DbSet<Job> Jobs{ get; set; }
        public virtual DbSet<JobParameter> JobParameters{ get; set; }
        public virtual DbSet<Register> Registers{ get; set; }
        public virtual DbSet<Priority> Priority{ get; set; }
        public virtual DbSet<Destination> Destination { get; set; }
        public virtual DbSet<VReport> VReport { get; set; }
        public virtual DbSet<VMeasure> VMeasure { get; set; }
        public virtual DbSet<VMeter> VMeter { get; set; }
        public virtual DbSet<VCurvaTipMeasure> VCurvaTipMeasures { get; set; }


        public virtual DbSet<ParamForm> ParamForm { get; set; }
        public virtual DbSet<ParamFormInstance> ParamFormInstances { get; set; }
        public virtual DbSet<ParamType> ParamTypes { get; set; }
        public virtual DbSet<ParamGroup> ParamGroups { get; set; }
        public virtual DbSet<ParamQuestion> ParamQuestions { get; set; }
        public virtual DbSet<ParamAnswerOption> ParamAnswerOptions { get; set; }
        public virtual DbSet<ParamAnswer> ParamAnswers { get; set; }
        public virtual DbSet<VParam> VParams { get; set; }
        public virtual DbSet<Alarm> Alarms { get; set; }
        public virtual DbSet<AlarmCase> AlarmCases { get; set; }
        public virtual DbSet<AlarmFire> AlarmFires { get; set; }
        public virtual DbSet<Falla> Fallas { get; set; }
        public virtual DbSet<FallaCausa> FallaCausas { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<NotificationGroup> NotificationGroups { get; set; }
        public virtual DbSet<PQRS> PQRS { get; set; }
        public virtual DbSet<PQRS_Message> PQRS_Messages { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.Events)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.asp_net_users_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.Reports)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.asp_net_users_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Boundary>()
                .Property(e => e.id)
               ;

            modelBuilder.Entity<Boundary>()
                .Property(e => e.commentary)
                .IsFixedLength();

            modelBuilder.Entity<Boundary>()
                .HasMany(e => e.Boundary_To_Report)
                .WithRequired(e => e.Boundary)
                .HasForeignKey(e => e.boundary_id);

            modelBuilder.Entity<Boundary>()
                .HasMany(e => e.Measure_AP)
                .WithOptional(e => e.Boundary)
                .HasForeignKey(e => e.boundary_id)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Boundary>()
                .HasMany(e => e.Report_Inform)
                .WithRequired(e => e.Boundary)
                .HasForeignKey(e => e.boundary_id);

            modelBuilder.Entity<Boundary_Source>()
                .Property(e => e.source)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Boundary_Source>()
                .Property(e => e.mail_to_send)
                .IsFixedLength();

            modelBuilder.Entity<Boundary_Source>()
                .HasMany(e => e.Boundary)
                .WithRequired(e => e.Boundary_Source)
                .HasForeignKey(e => e.source_id);

            modelBuilder.Entity<Boundary_To_Report>()
                .Property(e => e.boundary_id)
                ;

            modelBuilder.Entity<Config_CM>()
                .Property(e => e.element)
                .IsFixedLength();

            modelBuilder.Entity<Config_CM>()
                .Property(e => e.text)
                .IsFixedLength();

            modelBuilder.Entity<Magnitude>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Magnitude>()
                .Property(e => e.unit)
                .IsUnicode(false);

            modelBuilder.Entity<Mail_Item>()
                .Property(e => e.mail_address)
                .IsFixedLength();

            modelBuilder.Entity<Measure_AP>()
                .Property(e => e.boundary_id)
                ;

            modelBuilder.Entity<Meter>()
                .Property(e => e.serial)
                .IsFixedLength();

            modelBuilder.Entity<Model_Meter>()
                .HasMany(e => e.Meter)
                .WithRequired(e => e.Model_Meter)
                .HasForeignKey(e => e.model_meter_id);

            modelBuilder.Entity<Quantity>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Quantity>()
                .Property(e => e.name_short)
                .IsUnicode(false);

            modelBuilder.Entity<Report_Inform>()
                .Property(e => e.message)
                .IsFixedLength();

            modelBuilder.Entity<Report_Inform>()
                .Property(e => e.boundary_id);

            modelBuilder.Entity<Report_Type>()
                .Property(e => e.report_type1)
                .IsFixedLength();

            modelBuilder.Entity<Report_Type>()
                .Property(e => e.path)
                .IsFixedLength();

            modelBuilder.Entity<Report_Type>()
                .HasMany(e => e.Boundary)
                .WithRequired(e => e.Report_Type)
                .HasForeignKey(e => e.report_type_id);

            modelBuilder.Entity<Report>()
                .Property(e => e.external_report_id)
                .IsUnicode(false);

            modelBuilder.Entity<Report>()
                .HasMany(e => e.Report_Informs)
                .WithOptional(e => e.Report)
                .HasForeignKey(e => e.reports_id);

            modelBuilder.Entity<Buffer_Measure>()
                .Property(e => e.boundary_id)
                .IsFixedLength();

            //modelBuilder.Entity<Boundary>()
            //    .HasOptional(e => e.MeterBackup)
            //    .WithMany().HasForeignKey(e => e.meter_id2);

            //modelBuilder.Entity<Boundary>()
            //    .HasOptional(e => e.MeterMain)
            //    .WithMany().HasForeignKey(e => e.meter_id);

            modelBuilder.Entity<Response>()
             .HasRequired(e => e.Question)
             .WithMany().HasForeignKey(q => q.Id_Question).WillCascadeOnDelete(true);


            modelBuilder.Entity<Filtro>()
               .HasMany<Meter>(m => m.Meters)
               .WithMany(f => f.Filtros)
               .Map(cs =>
               {
                   cs.MapLeftKey("FiltroId");
                   cs.MapRightKey("MeterId");
                   cs.ToTable("FilterMeter");
               });

            modelBuilder.Entity<Filtro>()
               .HasMany<Boundary>(m => m.Boundaries)
               .WithMany(f => f.Filtros)
               .Map(cs =>
               {
                   cs.MapLeftKey("FiltroId");
                   cs.MapRightKey("BoundaryId");
                   cs.ToTable("FilterBoundary");
               });

            modelBuilder.Entity<Acquisition>()
               .HasMany<Acquisition_Item>(m => m.Acquisition_Items)
               .WithMany(f => f.Acquisitions)
               .Map(cs =>
               {
                   cs.MapLeftKey("acquisition_id");
                   cs.MapRightKey("acquisition_item_id");
                   cs.ToTable("AcquisitionBindTable");
               });

        }

        public System.Data.Entity.DbSet<CGM.Models.CGMModels.JobState> JobStates { get; set; }
    }
}
