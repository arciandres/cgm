namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LogUser")]
    public partial class LogUser
    {
        public int id { get; set; }

        public Guid id_user { get; set; }

        [Required]
        [StringLength(50)]
        public string user_rol { get; set; }

        [Required]
        [StringLength(50)]
        public string usr_name { get; set; }

        [StringLength(50)]
        public string IP { get; set; }

        [StringLength(50)]
        public string lat { get; set; }

        [StringLength(50)]
        public string lon { get; set; }

        public DateTime f_ini { get; set; }

        public DateTime? f_end { get; set; }
    }
}
