namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Job", Schema = "Hangfire")]
    public partial class Job
    {
        public int Id { get; set; }
        public int StateId { get; set; }

        public string StateName { get; set; }
        public string InvocationData { get; set; }
        public string Arguments { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? ExpireAt { get; set; }
        public virtual ICollection<JobParameter> JobParameters { get; set; }
        public virtual ICollection<JobState> JobStates { get; set; }


    }
}
