namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Individuals who are defined as recipients of the events when an alarm is fired.
    /// </summary>
    [Table("AlarmRecipients")]
    public partial class AlarmRecipients
    {
        public int id { get; set; }
        [DisplayName("Nombre")]
        public string name { get; set; }
        [DisplayName("Correo")]
        public string email { get; set; }
        [DisplayName("Prioridad")]
        public int priority_id { get; set; }
        [DisplayName("Activo")]
        public bool isActive { get; set; }
        public virtual Priority Priority { get; set; }

    }
}
