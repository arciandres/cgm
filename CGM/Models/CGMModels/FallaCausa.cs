namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Enumera las causas por las cuales una frontera se puede declarar en falla.
    /// </summary>
    [Table("FallaCausa")]
    public partial class FallaCausa
    {
        /// <summary>
        /// Identificador de la falla
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Descripción de la causa.
        /// </summary>
        public string causa { get; set; }
    }
}