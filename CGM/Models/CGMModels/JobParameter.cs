namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobParameter", Schema = "Hangfire")]
    public partial class JobParameter
    {
        public int Id { get; set; }
        public int JobId { get; set; }

        public string Name { get; set; }
        public string Value { get; set; }
        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }
    }
}
