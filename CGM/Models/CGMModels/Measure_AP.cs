namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Measure_AP
    {
        public int id { get; set; }

        public DateTime? measure_date { get; set; }

        public double? measure { get; set; }

        [StringLength(20)]
        public string unit { get; set; }

        public int? boundary_id { get; set; }

        public virtual Boundary Boundary { get; set; }
    }
}
