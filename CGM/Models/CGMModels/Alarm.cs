namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Configuraci�n individual de cada alarma. 
    /// </summary>
    [DisplayName("Alarma")]
    [Table("Alarm")]
    public partial class Alarm
    {
        public Alarm()
        {
            this.Users= new HashSet<AspNetUsers>();
        }
        public int id { get; set; }
        [DisplayName("Nombre")]
        public string name { get; set; }
        [DisplayName("Caso")]
        [ForeignKey("AlarmCase")]
        public int alarmCase_id { get; set; }
        [ForeignKey("Priority")]
        public int priority_id { get; set; }


        public bool send_mail { get; set; }


        /// <summary>
        /// Extra par�meters to be set by default
        /// </summary>
        [DisplayName("Par�metros")]
        public string parameters { get; set; }
        ///// <summary>
        ///// Par�metros de correo:
        ///// Actualmente define 4 par�metros:
        ///// (bool) send_mail: Env�a correos o no
        ///// (int[]) usernames: Users to send the mail to.
        ///// (string[]) other_mails: Other mails with no user registration to send the messages to.
        ///// (int) acum_alarms: alarmas acumuladas antes de enviar un correo.
        ///// </summary>
        //[DisplayName("Par�metros")]
        //public string mail_params { get; set; }
        [DisplayName("Activa")]
        public bool isActive { get; set; }
        public bool isFiled { get; set; }
        public virtual AlarmCase AlarmCase { get; set; }
        public virtual Priority Priority{ get; set; }
        public virtual ICollection<Filtro> Filtros { get; set; }
        public virtual ICollection<AlarmFire> AlarmFires { get; set; }
        public virtual ICollection<AspNetUsers> Users { get; set; }

    }
}
