﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
    public class VMeasure
    {
        /// <summary>
        /// Identificador de la frontera
        /// </summary>
        [DisplayName("id")]
        public int id { get; set; }
        [DisplayName("Negocio")]
        public int report_type_id { get; set; }
        [DisplayName("Hora")]
        public DateTime hour { get; set; }
        [DisplayName("Medida")]
        public double measure { get; set; }
        [DisplayName("Código")]
        public string code { get; set; }
        [DisplayName("Nombre")]
        public string name { get; set; }
        [DisplayName("Válidas")]
        public bool? assignedReading { get; set; }
        [DisplayName("Respaldo")]
        public bool? is_backup { get; set; }
        [DisplayName("Activa")]
        public bool is_active { get; set; }
        [DisplayName("Unidad")]
        public string unit { get; set; }
        [DisplayName("Medición")]
        public int quantity_id { get; set; }
        [DisplayName("Linea")]
        public int tag_line_id { get; set; }
        [DisplayName("Fecha de operación")]
        public DateTime operation_date { get; set; }
    }
}