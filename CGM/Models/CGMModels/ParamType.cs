namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Determina el tipo de parámetro para configurar las opciones del formulario. Ejemplo: Radio, texto, password, etc.
    /// </summary>
    [Table("ParamType")]
    public partial class ParamType
    {
        public int id { get; set; }
        /// <summary>
        /// 0:AnswerOption ; 1:Question
        /// </summary>
        public int level { get; set; }
        /// <summary>
        /// Nombre del tipo. (Ej. Text, radio, form, etc.)
        /// </summary>
        public string type { get; set; }
    }
}
