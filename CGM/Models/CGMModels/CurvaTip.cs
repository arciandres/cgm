namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CurvaTip")]
    public partial class CurvaTip //Curva t�pica
    {

        public int id { get; set; }

        [DisplayName("Nombre")]
        /// <summary>
        /// Optional parameter, just to make a distinction in case needed. It is formed automatically
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Fecha inicial del rango de una curva t�pica, o de alumbrado p�blico
        /// </summary>
        public DateTime fecha1 { get; set; }
        /// <summary>
        /// Fecha final del rango una curva t�pica, o de alumbrado p�blico
        /// </summary>
        public DateTime? fecha2 { get; set; }

        // Este par�metro se reubica, y no est� en el grupo de medidas como en Measure, pues es �nico para toda la curva t�pica.
        /// <summary>
        /// Tells the type of measurement to be taken
        /// </summary>
        [ForeignKey("Acquisition_Item")]
        public int? acquisition_item_id { get; set; }
        [DisplayName("Enfoque")]
        /// <summary>
        /// Scope is an internal json string that gets mapped to the CurvaTip parameters, such as days pick, days of the week, and tag.
        /// </summary>
        public string scope { get; set; }
        [DisplayName("Fecha de carga")]
        /// <summary>
        /// Time stamp
        /// </summary>
        public DateTime dateCreated { get; set; }
        /// <summary>
        /// Defines which Curva tipica would be used to compare measurements in the alarm.
        /// </summary>
        public bool isDefault { get; set; }
        
        public virtual Acquisition_Item Acquisition_Item { get; set; }
        public virtual ICollection<CurvaTipMeasureGroup> Groups { get; set; }
    }
}
