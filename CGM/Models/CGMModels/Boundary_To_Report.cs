namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Boundary_To_Report
    {
        public int id { get; set; }
        public int boundary_id { get; set; }

        public int reports_id { get; set; }

        public virtual Boundary Boundary { get; set; }
    }
}
