namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Buffer_Measure
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [Key]
        [Column(Order = 1)]
        public double measure { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime time { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(10)]
        public string boundary_id { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool is_backup { get; set; }
    }
}
