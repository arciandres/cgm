﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class ParamConditional
    {
        /// <summary>
        /// 0:AnswerOption; 1:Question
        /// </summary>
        public int Class { get; set; }
        /// <summary>
        /// Changes depending on the class; 0:Form; 1:Radio (Defined in database). This is added in case in the future different types might have different effects
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// Identificador del objeto, ya sea answerOption o Question
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name added only for reference while coding
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 0: equalsTo; 1: notEqualsTo ; 2: higherThan; 3:lowerThan; 
        /// </summary>
        public int operatorId { get; set; }
        /// <summary>
        /// Value of string to compare
        /// </summary>
        public string value { get; set; }


    }
    /// <summary>
    /// Defines the conditional clauses in an object
    /// </summary>
    public class ParamConditionalsFull
    {
        /// <summary>
        /// 0:AnswerOption; 1:Question
        /// </summary>
        public int Class { get; set; }
        /// <summary>
        /// 0:Form; 1:Radio (Defined in database). This is added in case in the future different types might have different effects
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// Identificador del objeto, ya sea answerOption o Question
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Name added only for reference while coding
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// List of conditional clauses that causes that the object gets blocked.
        /// </summary>
        public List<ParamConditional> conditionals { get; set; }

        public static List<ParamConditionalsFull> GetParamConditionalFullSet(){
            List<ParamConditionalsFull> conditionsSet = new List<ParamConditionalsFull>();

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                var answerOptionsWithConditionals = db.ParamAnswerOptions.AsNoTracking().Where(ao => ao.conditionals != null).ToList();
                foreach (var item in answerOptionsWithConditionals)
                {
                    ParamConditionalsFull conditionalsFullItem = new ParamConditionalsFull()
                    {
                        Class = (int)ParamLevel.AnswerOption,
                        id = item.id,
                        name = item.name,
                        type = item.type_id,
                        conditionals = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ParamConditional>>(item.conditionals)
                    };
                    conditionsSet.Add(conditionalsFullItem);
                }
                var questionsWithConditionals = db.ParamQuestions.AsNoTracking().Where(q => q.conditionals != null).ToList();
                // It's the same code as above, with diff set (-_-).
                foreach (var item in db.ParamQuestions.Where(q => q.conditionals != null).ToList())
                {
                    ParamConditionalsFull conditionalsFullItem = new ParamConditionalsFull()
                    {
                        Class = (int)ParamLevel.AnswerOption,
                        id = item.id,
                        name = item.name,
                        type = item.type_id,
                        conditionals = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ParamConditional>>(item.conditionals)
                    };
                    conditionsSet.Add(conditionalsFullItem);
                }

                return conditionsSet;
            }
        }
    }
    public enum ParamLevel
    {
        AnswerOption = 0,
        Question = 1,
        Group = 2
    }
    public enum OperatorConditional
    {
        equalsTo = 0,
        notEqualsTo = 1,
        higherThan = 2,
        lowerThan = 3
    }



}