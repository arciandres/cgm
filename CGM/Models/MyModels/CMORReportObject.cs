﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using CGM.Models.CGMModels;

namespace CGM.Models.MyModels
{
    public class CMORReportObject //Este es un objeto global. Se utiliza para almacenar variables necesarias en los procesos de manera transversal.
    {
        // A read-write instance property:
        [NotMapped]
        public int id { get; set; }
        public List<Boundary> BoundaryList { get; set; }
        public List<Report_Type> report_types { get; set; }
        public List<ReportElement> ReportList { get; set; }
        public List<Report_Inform> ReportInformlist { get; set; }
        public Report reports { get; set; }
        //public List<CheckedBoundary> CheckedBoundariesList { get; set; }
        public static CMORReportObject CmorObject = new CMORReportObject();
    }
}