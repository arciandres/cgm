﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using CGM.Models.CGMModels;


namespace CGM.Models.MyModels
{
    public class ReportElement   //Se incluyen todos los campos, aunque a veces pueda ser redundante, pero en muchas ocasiones es muy útil, junto a vMeasure
    {
        [NotMapped]
        public int id { get; set; }
        // A read-write instance property:
        public Boundary boundary { get; set; }
        public List<Measure> measures { get; set; }
        public bool isBackup { get; set; }
        public int acquisition_item_id { get; set; }

    }
}