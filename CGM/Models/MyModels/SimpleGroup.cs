﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class SimpleGroup
    {
        public DateTime date { get; set; }
        public List<SimpleMeasure> measures { get; set; }
    }
}