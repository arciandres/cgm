﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class AnalysisData
    {
        public bool multi_boundary { get; set; }
        public int grouping_level { get; set; }
        public List<SimpleBoundary> boundaries { get; set; }

    }

    public class AnalysisDataSingle
    {
        public int multi_boundary { get; set; }
        SimpleBoundary boundary { get; set; }
        List<SimpleMeasure> measuresPerDay { get; set; }

    }
}