﻿using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class ReportDest
    {
        public Meter meter { get; set; }
        public Boundary boundary{ get; set; }
        public int destination { get; set; }
        public List<Report> reports { get; set; }
    }
}