﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Areas.Telemetry.Models;

namespace CGM.Models.MyModels
{
    public class MeterSurvey
    {
        public Meter Meter { get; set; }
        public ShareModel ShareModel { get; set; }
    }
    //public static ModelMeterView getModelMeterView(int id)
    //{
    //    CGM_DB_Entities db = new CGM_DB_Entities();
    //    Meter mm = db.Meter.Include(m => m.Model_Meter).Include(m => m.b;
    //    ModelMeterView modelMeterView = new ModelMeterView()
    //    {
    //        model_Meter = db.Model_Meter.Find(id),
    //        meters = mm.Meter
    //    }
    //    return modelMeterView;
    //}
}