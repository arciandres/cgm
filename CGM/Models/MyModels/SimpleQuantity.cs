﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class SimpleQuantity
    {
        public string name { get; set; }
        public string shortname { get; set; }
        public string unit { get; set; }
        public List<SimpleGroup> groups { get; set; }
        public List<SimpleMeasure> measures { get; set; }
        // Used to appplications where another set of measures is required, like in the charts
        public List<SimpleMeasure> measures2 { get; set; }
        public List<SimpleMeasure> measures3 { get; set; }
    }
}