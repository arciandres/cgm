﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class MyBoundary
    {
        public int id { get; set; }
        public int report_type_id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int source_id { get; set; }
        public string real_name { get; set; }
        public bool has_backup { get; set; }
        public string commentary { get; set; }
        public bool is_active { get; set; }
        public Nullable<double> max_active { get; set; }
        public bool is_filed { get; set; }
        public Nullable<double> min_active { get; set; }
        public Nullable<float> latitude { get; set; }
        public Nullable<float> longitude { get; set; }
        public Nullable<double> max_reactive { get; set; }
        public Nullable<double> min_reactive { get; set; }
        public Nullable<int> id_priority { get; set; }
        public double max_measure_active { get; set; }
        public double min_measure_active { get; set; }
    }
}