﻿using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class FiltroViewModel
    {
        public Filtro filtro{ get; set; }
        public List<Meter> meters { get; set; }
        public List<Boundary> boundaries { get; set; }
    }
}