﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CGM.Models.MyModels
{
    /// <summary>
    /// Esta clase no es una vista
    /// </summary>
    public class VBoundary
    {
        public int id { get; set; }
        public int report_type_id { get; set; }
        public string code { get; set; }
        public bool is_backup { get; set; }
        public DateTime operation_date { get; set; }
        public List<VMeasure> VMeasures { get; set; }


        public static List<VBoundary> ToVBoundaries(List<VMeasure> vMeasures)
        {
            List<VBoundary> vBoundaries = new List<VBoundary>();
            var grupos = vMeasures.GroupBy(v => new Tuple<string,int,bool,DateTime,int>(v.code, v.id, v.is_backup.Value, v.operation_date, v.report_type_id));
            foreach (var g in grupos)
            {
                vBoundaries.Add(new VBoundary { code = g.Key.Item1, is_backup = g.Key.Item3, operation_date = g.Key.Item4, id = g.Key.Item2, report_type_id = g.Key.Item5 , VMeasures = g.ToList() });
            }
            return vBoundaries;
        }
       
    }


}