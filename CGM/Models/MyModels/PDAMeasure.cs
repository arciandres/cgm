﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace CGM.Models.MyModels
{
    /// <summary>
    /// Esta clase no es una vista en la base de datos. Se crea para adecuar las medidas del PDA a la base de datos una vez se carguen. 
    /// Estas medidas se deben encriptar.
    /// </summary>
    public class PDAMeasure // No se usa vMeasure, porque esta no contiene el parámetro "serial"
    {
        public DateTime operation_date { get; set; }
        public double measure { get; set; }

        //public static List<VBoundary> ToVBoundaries(List<VMeasure> vMeasures)
        //{
        //    List<VBoundary> vBoundaries = new List<VBoundary>();
        //    var grupos = vMeasures.GroupBy(v => new Tuple<string,int,bool,DateTime,int>(v.code, v.id, v.is_backup.Value, v.operation_date.Value, v.report_type_id));
        //    foreach (var g in grupos)
        //    {
        //        vBoundaries.Add(new VBoundary { code = g.Key.Item1, is_backup = g.Key.Item3, operation_date = g.Key.Item4, id = g.Key.Item2, report_type_id = g.Key.Item5 , VMeasures = g.ToList() });
        //    }
        //    return vBoundaries;
        //}
       
    }


}