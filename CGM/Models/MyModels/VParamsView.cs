﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Web;

namespace CGM.Models.MyModels
{
    public class VAnswer // It is not on the database
    {
        public string answerOptionName { get; set; }
        public string value { get; set; }
        /// <summary>
        /// Tells the form field type (4: Radio, etc...)
        /// </summary>
        public int type_id { get; set; } 

        /// <summary>
        /// Gets an object of ParamAnswers and converts it into a vAnswers object, suitable for visualization and reference.
        /// </summary>
        /// <param name="answers">List of ParamAnswer object</param>
        /// <returns></returns>
        public static List<VAnswer> parseVAnswers(List<ParamAnswer> answers)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                var answerOptions = db.ParamAnswerOptions.Include(a => a.Question).ToList();
            
                List<VAnswer> vAnswers = new List<VAnswer>();
                foreach (var answer in answers)
                {
                    VAnswer va = new VAnswer();
                    // Get the answerOption. We don't get it from the object directly
                    // in case the answerOptions object is not included in the current answer(lazy load)
                    var ansOption = answerOptions.Where(a => a.id == answer.answerOption_id).FirstOrDefault();

                    va.type_id = ansOption.type_id;
                    if (ansOption.type_id == 4) // Radio Button
                    {
                        va.answerOptionName = ansOption.Question.name;
                        va.value = ansOption.name;
                    }
                    else
                    {
                        va.answerOptionName = ansOption.name;
                        va.value = answer.value;
                    }
                    vAnswers.Add(va);
                }
                return vAnswers;
            }
        }
    }

}