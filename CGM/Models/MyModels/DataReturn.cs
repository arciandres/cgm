﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class DataReturn
    {
        public MyBoundary boundary { get; set; }
        public double sumatoria_activa { get; set; }
        public double sumatoria_reactiva { get; set; }
        public List<MyMeasure> measure_activa { get; set; }
        public List<MyMeasure> measure_reactive { get; set; }
    }
}