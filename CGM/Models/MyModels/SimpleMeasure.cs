﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using CGM.Areas.Telemetry.Models;

namespace CGM.Models.MyModels
{
    public class SimpleMeasure
    {
        public DateTime time { get; set; }
        public double measure { get; set; }
        public string quantity { get; set; }
        public string unit { get; set; }

        public static List<SimpleMeasure> vMeasuresToSMeasures(ICollection<VMeasure> measures)
        {
            List<SimpleMeasure> sms = new List<SimpleMeasure>();
            foreach (VMeasure m in measures)
            { sms.Add(new SimpleMeasure() { measure = Math.Round(m.measure,2), time = m.hour}); }
            return sms;
        }
    }
}