﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class MyMeasure
    {
        public int id { get; set; }
        public string measure_date { get; set; }
        public Nullable<double> measure { get; set; }     
    }
}