﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using CGM.Areas.Telemetry.Models;
using CGM.Helpers;

namespace CGM.Models.MyModels
{
    /// <summary>
    /// Esta clase no es una vista en la base de datos. Se crea para adecuar las medidas del PDA a la base de datos una vez se carguen. 
    /// Estas medidas se deben encriptar.
    /// </summary>
    public class PDAMeter
    {
        public int? id { get; set; }
        /// <summary>
        /// Identificador de la cantidad del medidor
        /// </summary>
        public int? quantity_id { get; set; }
        public string serial { get; set; }
        public List<PDAMeasure> measures { get; set; }
        public DateTime operation_date { get; set; }
        public static KeyValuePair<string, string> LoadPDAToDatabase(PDAMeter meter_data)
        {
            try
            {
                using (CGM_DB_Entities db = new CGM_DB_Entities())
                {
                    Meter meter = db.Meter.Include(m => m.Boundary).Where(m => m.serial.Trim().Equals(meter_data.serial.Trim())).FirstOrDefault();

                    if (meter == null)
                    {
                        return new KeyValuePair<string, string>("error", "El serial del dispositivo no corresponde a ninguna frontera.");
                    }

                    List<Measure> measures = new List<Measure>();

                    foreach (var measure in meter_data.measures)
                    {
                        measures.Add(new Measure() { measure1 = measure.measure, measure_date = measure.operation_date });
                    }
                    Measure_Group measure_Group = new Measure_Group()
                    {
                        Measures = measures,
                        acquisition_item_id = db.Acquisition_Item.Where(acq => acq.quantity_id == meter_data.quantity_id && acq.tag_line_id == 4).Select(acq => acq.Id).FirstOrDefault(), // tag_line_id = 4 es el total de las líneas                
                    };

                    Report_Inform report_Inform = new Report_Inform()
                    {
                        assignedReading = true,
                        Measure_Groups = new List<Measure_Group>() { measure_Group },
                        boundary_id = meter.boundary_id,
                        is_backup = meter.is_backup,
                        substatus_id = 502,
                        boundary_source_id = db.Boundary_Source.Where(b => b.source.Trim().Equals("PDA")).FirstOrDefault().Id,
                        end_time = DateTime.Now,
                        meter_id = meter.id
                    };

                    Report report = new Report()
                    {
                        operation_date = meter_data.measures[0].operation_date,
                        destination_id = 1,
                        is_auto = false,
                        Report_Informs = new List<Report_Inform>() { report_Inform },
                        status_id = 5,
                        is_active = false
                    };

                    DataAndDB.insertReports(report);

                    return new KeyValuePair<string, string>("true", "Registro de medidas realizado exitósamente");
                }
            }
            catch (Exception ex)
            {
                return new KeyValuePair<string, string>("error", "Hubo un error en el registro de las medidas. Inténtelo nuevamente, y si el error persiste, contáctese con los administradores.");
                throw;
            }
        }
    }
}