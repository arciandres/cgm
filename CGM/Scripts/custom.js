﻿//import { debug } from "util" ;

//import { error } from "util";

$(function chkAll() {
    $("input[name$='chkcol']").click(function () {
        var col = $(this).prop("value")
        $('[id ^= chkCol' + col + ']').prop("checked", $(this).prop("checked"))
        console.log(col)
    })
})

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function getListParameters(p) {
    var response
    if (p == 0 || p == 2) { var url = ROOT + "api/Meters/GetMeters"; }
    if (p == 1) { var url = ROOT + "api/Boundaries/GetBoundaries"; }

    $.ajax({
        'async': false,
        type: "GET",
        url: url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { pick: p },
        success: function (data) {
            response = data
        }
    })
    return response;
}


function successCoords(pos) {
    clat = "" + pos.coords.latitude;
    clon = "" + pos.coords.longitude;
    $.get("https://ipapi.co/json/", function (response) {
        //alert(response.ip);
        cip = response.ip;
    })
};

function errorCoords(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
    $.get("https://ipinfo.io/json", function (response) {

        //alert(cip);
        cip = response.ip;
        if (clat == "" || clong == "") {
            coords = response.loc.split(',')
            clat = coords[0]
            clong = coords[1]
        }
    })
};


function saveLog(clat, clong, cip) {
    $.ajax({
        type: 'GET',
        url: ROOT + 'LogUser/SaveLog',
        data: {
            ip: cip,
            lat: clat,
            lon: clong
        },
        success: function (response) {
            console.log("LogExitoso")
        },
        error: function (error) {
            console.log("Log con Error")
        }
    });
}

//%%%%%%%%%%%%%%

//Automatically fade alerts
$(".fadeauto").fadeTo(5000, 1000).slideUp(1000, function () {
    $(this).slideUp(500);
});


//##########################

// Colorbar

function getGradient(min, max, rgbs) {

    // Genera un vector de este tipo (Usado para la barra de color)

    //gradient = [[
    //    inrange(0),
    //    [184, 255, 181]
    //],
    //[
    //    inrange(0.28),
    //    [0, 128, 0]
    //],
    //[
    //    inrange(0.7),
    //    [0, 0, 255]
    //],
    //[
    //    inrange(1),
    //    [249, 174, 182]
    //    ]]


    // rgbs = 
    var range = max - min

    function inrange(x) {
        return x * range + min
    }
    var gradient = []
    rgbslen = rgbs.length - 1

    $.each(rgbs, function (index, value) {
        gradient = gradient.concat([
            [
                inrange(index * 1 / rgbslen),
                value
            ]]) // Rango Tipo 0,25,50,70,100
        //console.log(gradient)
    })

    return gradient;
};

function getScale(min1, max1) {
    // Calculates a linear function that wraps the original values, with max and min as bounds.
    min2 = (min1 + 0.01) * (1.01)
    max2 = max1 * (0.99)
    m = (max2 - min2) / (max1 - min1);
    b = max2 - m * max1
    return function (x) {
        return x * m + b;
    }
}


function getColorbar(min, max) {
    var range = (max - min);
    rgbs =
        [
            [249, 255, 239],
            [255, 226, 153],
            [255, 145, 99],
        ]
    gradient = getGradient(min, max, rgbs);
    scale = getScale(min, max)
    return function (num) { //value of the measure
        num1 = scale(num)
        if (num1 == 0)
            var colorRange = []
        $.each(gradient, function (index, value) { // Get the closest values in the colorbar
            //console.log("Num: " + num + "\t value[0]: " + value[0])
            if (num1 <= value[0]) {
                colorRange = [index - 1, index]
                //return false;
            }
        });

        //Get the two closest colors
        var firstcolor = gradient[colorRange[0]][1];
        var secondcolor = gradient[colorRange[1]][1];

        //Calculate ratio between the two closest colors
        var firstcolor_x = gradient[colorRange[0]][0];
        var secondcolor_x = gradient[colorRange[1]][0] - firstcolor_x;
        var slider_x = (num1) - firstcolor_x;
        var ratio = slider_x / secondcolor_x

        //Get the color with pickHex(thx, less.js's mix function!)
        var result = pickHex(secondcolor, firstcolor, ratio);

        return result
    }
}

function pickHex(color1, color2, weight) {
    var p = weight;
    var w = p * 2 - 1;
    var w1 = (w / 1 + 1) / 2;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
    Math.round(color1[1] * w1 + color2[1] * w2),
    Math.round(color1[2] * w1 + color2[2] * w2)];
    return rgb;
}

//------------------------------------------- Tables -------------------------

function renderReportTable(min, max, urlLanguage, title) {
    var colorBar = getColorbar(min, max)

    columnStartData = 4;  // 4 is the column where the numeric values begin

    table = $('#tblBoundaries').DataTable(
        {
            "language": { "url": urlLanguage },
            "columnDefs": [{
                "targets": '_all',
                "createdCell": function (td, cellData, rowData, row, col) {
                    //console.log(col)
                    if (col >= columnStartData + 1) { // 4 is the column where the numeric values begin // + 1 to skip the zero column
                        //console.log(cellData)
                        //debugger;
                        numText = $.parseHTML(cellData)[0].innerText
                        num = parseFloat(numText)
                        rgb = colorBar(num)
                        $(td).css('background-color', `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`)
                    }
                }
            },
            ],
            "autoWidth": false,
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Visibilidad'
                },
                {
                    text: 'Formato',
                    action: function (e, dt, node, config) {
                        // Header of columns to hide before exporting
                        colHeaders = ['#', 'Frontera', 'H0'];

                        for (var i = 0; i < table.columns()[0].length; i++) {
                            column = table.column(i)
                            // aria-label: "Código: Activar para ordenar la columna de manera ascendente"
                            colHeader = column.header().getAttribute('aria-label').split(':')[0]
                            if (colHeaders.includes(colHeader)) {
                                column.visible(!column.visible()); // Toggle visibility of column
                            }
                        }

                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible',
                        charset: 'UTF-8',
                        bom: true,
                        format: {
                            body: function (data, row, column, node) {
                                // Strip $ from salary column to make it numeric
                                //debugger;
                                dataElements = $.parseHTML(data)
                                if (dataElements.length > 1) { 
                                    // Returns only the first element, which is a text object
                                    return dataElements[0].innerText
                                }
                                else {
                                    return data
                                }
                            }
                        }
                    },
                    title: title
                }

            ]
        })
    table.columns.adjust().draw()

    // Hide H0 column by default
    for (var i = 0; i < table.columns()[0].length; i++) {
        column = table.column(i);
        if (column.header().getAttribute('aria-label').split(':')[0] == 'H0') {
            column.visible(false);
        }
    }

    //column.visible(!column.visible());

}
// =================== Notifications =====================

function getNotifFeed(flag, groups) {
    function addToFeed(message1, message2) {
        return '<li>' +
            '<a style="width:350px; white-space: normal" rel="nofollow" href="' + ROOT+'Notifications/Index' + '" class="dropdown-item">' +
            '<div class="notification d-flex justify-content-between">' +
            '<div class="notification-content" style="overflow-wrap: break-word !important;"><i class="fa fa-upload"></i>' + message1 + '</div>' +
            '<div class="notification-time"><small>' + message2 + '</small></div>' +
            '</div>' +
            '</a>' +
            '</li>'; 
    }


    var feed = "";

    if (flag == "false") {
        feed = addToFeed("No hay nuevas notificaciones", "")
    }
    else if (flag == "true") {
        groups.forEach(function (item, value) {
            feed += addToFeed(item.message, item.Notifications.length)
        })
    }
    feed += '<li><a rel="nofollow" href="'+ ROOT+'Notifications/Index' +'" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-bell"></i>Ir a Notificaciones</strong></a></li>'

    return feed;
}

function GetNotifications(init) {
    $.ajax({
        type: "GET",
        url: ROOT + "Notifications/GetNotificationGroups",
        data: {
            activeOnly: 1, // Get only the active notifications
            init : init
        },
        dataType: "json",
        success: function (data) {
            if (init == 1 && data.flag == "false") { // Si apenas se abre la página, y no hay novedades, se pone que no hay nuevas notificaciones
                getNotifFeed(data.flag, null)
            }
            if (data.flag == "true") {
                notifFeed = JSON.parse(data.result)
                if (notifFeed.length > 0) {
                    $("#notifFeed").empty();
                    $("#notifFeed").append(getNotifFeed(data.flag, notifFeed));

                    var notifCount = 0;
                    notifFeed.forEach(function (item, value) { notifCount += item.Notifications.length })
                    $("#notifBell").find("span").remove();
                    $("#notifBell").append('<span id="notifCount" class="badge badge-warning">' + notifCount + '</span>')
                }
            }
            else {
                //console.log(data);
            }
        },
        error: function (data) {
            //console.log("Error in ajax")
           // console.log(data);
        }
    });
}
$(function () {
    GetNotifications(1)
    var myTimer = setInterval(function () { GetNotifications(0) }, 10000);
})
$("#notifBell").click(function () {
    $("#notifCount").remove()
})


//< script >
//    $("#notifBell").append('<span id="notifCount" class="badge badge-warning">' + @Model.Count + '</span>')
//</script >

// Script to change asp.net dates to moment js in spanish
// The date should be place in the following manner
//<span data-utcdate="@item.firedOn.ToString(" s", System.Globalization.CultureInfo.InvariantCulture)"></span>

moment.locale('es')
$(function () {
    $('[data-utcdate]').each(function () {
        var d = moment($(this).attr('data-utcdate')).fromNow();
        $(this).text(d);
    });
});


// ================== Miscelaneous =======================

/**
 * Converts a C# Model to javascript. 
 * @param {string} json_viewbag - Model in JSON parsed from C#, like this: "@JsonConvert.SerializeObject(Model)"
 */
function parseViewBagJSONData(json_viewbag) {
    var replacements = [
        [/&quot;/g, '"'],

        [/&#193;/g, 'Á'],
        [/&#201;/g, 'É'],
        [/&#205;/g, 'Í'],
        [/&#211;/g, 'Ó'],
        [/&#218;/g, 'Í'],
         
        [/&#225;/g, 'á'],
        [/&#233;/g, 'é'],
        [/&#237;/g, 'í'],
        [/&#243;/g, 'ó'],
        [/&#250;/g, 'ú'],
                    
    ]

    replacements.forEach(function (item, index) {
        json_viewbag = json_viewbag.replace(item[0], item[1])
    })
    
    return JSON.parse(json_viewbag)
}

// To use in moment().calendar(). Otherwise, drops the date like "Yesterday"
// moment().startOf('month').calendar(null, formatDaysMoment),

var formatDaysMoment = {
    sameDay: 'YYYY-MM-DD',
    nextDay: 'YYYY-MM-DD',
    nextWeek: 'YYYY-MM-DD',
    lastDay: 'YYYY-MM-DD',
    lastWeek: 'YYYY-MM-DD',
    sameElse: 'YYYY-MM-DD'
}