﻿Highcharts.setOptions({
    global: {
        useUTC: false
    }
});

let seriesSets = [];

Chart_Columns = (data, numFront, domName) => {
    var maxRate = {}, minRate = {}

    // Single boundary. 
    if (numFront <= 1) {
        seriesSets = preprocessSingleBoundary(data, 1)
    }
    else {
        seriesSets = preprocessMultipleBoundaries(data, 1)
    }

    Highcharts.chart(domName, {
        chart: {
            type: 'column'
        },
        title: {
            text: "Consumo diario"
        },
        xAxis: {
            type: 'datetime'
        },
        legend: {
            enabled: true
        },

        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            valueDecimals: 2,
            split: true
        },

        series: seriesSets
    });
}

Chart_Diamond = (data, numFront) => {

    let nombres = new Array()
    let minimos = new Array()
    let maximos = new Array()

    for (i = 0; i < data.length; i++) {
        nombres.push(data[i].boundary.name)
        maximos.push(data[i].boundary.max_active)
        minimos.push(data[i].boundary.min_active)
    }

    if (numFront == 1) {
        nombres.push(data[0].boundary.name + ' - REACTIVA')
        maximos.push(data[0].boundary.max_reactive)
        minimos.push(data[0].boundary.min_reactive)
    }

    Highcharts.chart('grafica3', {

        chart: {
            polar: true,
            type: 'line'
        },

        title: {
            text: 'Minimos vs Maximos (' + $("#fechaIni").val() + ' - ' + $("#fechaFin").val() + ')',
            x: -80
        },

        pane: {
            size: '90%'
        },

        xAxis: {
            categories: nombres,
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },

        series: [{
            name: 'Valor Minimo',
            data: minimos,
            pointPlacement: 'on'
        }, {
            name: 'Valor Maximo',
            data: maximos,
            pointPlacement: 'on'
        }]

    });

}

Chart_Stock = (data, numFront, domName) => {

    var maxRate = {}, minRate = {}

    // Single boundary. 
    if (numFront <= 1) {
        seriesSets = preprocessSingleBoundary(data, 0)
    }
    else { // Multiboundary
        seriesSets = preprocessMultipleBoundaries(data, 0)
    }

    Highcharts.stockChart(domName, {
        rangeSelector: {
            selected: 1
        },

        legend: {
            enabled: true
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            valueDecimals: 2,
            split: true
        },

        series: seriesSets
    });
}

Chart_BasicLine = (data, numFront, domName, title, subtitle) => {

    var maxRate = {}, minRate = {}

    // Single boundary. 
    if (numFront <= 1) {
        seriesSets = preprocessSingleBoundary(data, 0, 1)
    }
    else { // Multiboundary
        seriesSets = preprocessMultipleBoundaries(data, 0)
    }

    Highcharts.chart(domName, {
        title: {
            text: title
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            type: 'datetime'
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            valueDecimals: 2,
            split: true,
            valueSuffix: " "

        },
        
        legend: {
            enabled: false
        },
        //tooltip: {
        //    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
        //    xDateFormat: 'datetime',
        //    valueDecimals: 2,
        //    split: true,
        //    valueSuffix: " ",
        //},
        
        series: seriesSets
    });
}

Chart_Histogram = (data, numFront, domName) => {

    if (numFront <= 1) {
        // Si no se ha procesado los datos antes, hacerlo. De lo contrario, usar los ya procesados
        if (seriesSets.length == 0)
            seriesSets = preprocessSingleBoundary(data, 0)

        seriesSetsHist = []
        seriesSets.forEach(function (seriesSet, i) {
            seriesSetsHist.push({    
                name: seriesSet.name + " - Hist.",
                type: 'histogram',
                xAxis: 1,
                yAxis: 1,
                baseSeries: 's'+i,
                zIndex: -1
            })
            seriesSetsHist.push({
                name: seriesSet.name,
                type: 'scatter',
                data: seriesSet.data,
                id: 's'+i,
                marker: {
                    radius: 1.5
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">Medida</span>: <b>{point.y}</b><br/>' + 
                                 '<span>Fecha: {point.x:%Y-%m-%d %H:%M}</span>' ,
                    xDateFormat: 'datetime'
                },
            })

        })
    }
    else {
        // Si no se ha procesado los datos antes, hacerlo. De lo contrario, usar los ya procesados
        if (seriesSets.length == 0)
            seriesSets = preprocessMultipleBoundary(data, 0)

        seriesSetsHist = []
        seriesSets.forEach(function (seriesSet, i) {
            seriesSetsHist.push({
                name: seriesSet.name + " - Hist.",
                type: 'histogram',
                xAxis: 1,
                yAxis: 1,
                baseSeries: 's' + i,
                zIndex: -1
            })
            seriesSetsHist.push({
                name: seriesSet.name,
                type: 'scatter',
                data: seriesSet.data,
                id: 's' + i,
                marker: {
                    radius: 1.5
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">Medida</span>: <b>{point.y}</b><br/>' +
                        '<span>Fecha: {point.x:%Y-%m-%d %H:%M}</span>',
                    xDateFormat: 'datetime'
                },
            })

        })
    }

    Highcharts.chart(domName, {
        title: {
            text: 'Histograma de medidas'
        },
        plotOptions: {
            histogram: {
                binsNumber:40
            }
        },
        xAxis: [{
            type: 'datetime',
            title: { text: 'Data' },
            alignTicks: false
        }, {
            title: { text: 'Histograma' },
            alignTicks: false,
            opposite: true
        }],

        yAxis: [{
            title: { text: 'Data' }
        }, {
            title: { text: 'Histogram' },
            opposite: true
        }],

        series: seriesSetsHist
    });
}

Chart_Pie = (data, numFront, domName) => {
    var seriesSets = [];
    let accum = 0;

    // Single Boundary
    // The piechart is formed by  the different measures of the boundary
    if (numFront <= 1) {
        boundary = data[0]
        boundary.Quantities.forEach(function (quantity) {
            let accum = 0;
            let seriesData = [];

            quantity.measures.forEach(function (measure) {
                accum += measure.measure
            }),

            seriesData = {
                name: quantity.name,
                y: accum,
                suffix: quantity.unit
            }

            seriesSets.push(seriesData)
        })
    }
    else { // Multiple boundaries
        data.forEach(function (boundary) {
            let accum = 0;
            let seriesData = [];

            boundary.measures.forEach(function (measure) {
                accum += measure.measure
            }),

                seriesData = {
                    name: boundary.code + " - " + boundary.name,
                    y: accum,
                }

            seriesSets.push(seriesData)
        })
    }

    Highcharts.chart(domName, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Consumo total - Circular'
        },
        subtitle: {
            text: '(' + $("#fechaIni").val() + ' - ' + $("#fechaFin").val() + ')'
        },
        tooltip: {
            pointFormat: '<b>{point.y:.1f} {point.options.suffix}</b> <br/> <h2>{point.percentage:.1f} %</h2>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    //format: '<b>{point.name}</b>: {point.percentage:.1f} % '
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Medida',
            colorByPoint: true,
            data: seriesSets
        }]
    });
}

Chart_Pie_Summary = (data, domName) => {
    Highcharts.chart(domName, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            animation: true
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: 'Fronteras: <b>{point.y}</b><br/>' +
                'Porcentaje: <b>{point.percentage:.1f}%</b><br/>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f}%)',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                animation: false,
                size: '90%'

            }
        },
        
        series: [{
            name: 'Fronteras',
            colorByPoint: true,
            data: data
        }]
    });
}



// =======================================================================================
// ------------------------------------ Methods ------------------------------------------
// =======================================================================================


function preprocessSingleBoundary(data, measureInd, dateFormat=0) {
    var seriesSets = [];

    boundary = data[0]

    boundary.Quantities.forEach(function (quantity) {
        let seriesData = []
        let measures

        switch (measureInd) {
            case 0: measures = quantity.measures; break;
            case 1: measures = quantity.measures2; break;
            case 2: measures = quantity.measures3; break;
            default: measures = quantity.measures; break;
        }
        measures.forEach(function (measure) {

            var timeInt; // Integer of time stamp. Ex: parseInt(measure.time.substr(6))

            if (dateFormat == 0) // date arrives as int string, in javascript. Ex. "/Date(1574143200000)/"
                timeInt = parseInt(measure.time.substr(6))

            else if (dateFormat == 1) // date arrives as date string, maybe from C#. Ex. // date arrives as string, in javascript. Ex. "/Date(1574143200000)/"
                timeInt = Date.parse(measure.time)

            seriesData.push([
                timeInt,
                measure.measure
            ])
        })

        seriesSets.push({
            name: quantity.name,
            data: seriesData,
            tooltip: {
                valueSuffix: " " + quantity.unit,
            },
            showInNavigator: true
        })
    })
    return seriesSets;
}

function preprocessMultipleBoundaries(data, measureInd) {
    var seriesSets = [];

    // data is a List<SimpleBoundary>

    data.forEach(function (boundary) {
        let seriesData = []
        let measures

        switch (measureInd) {
            case 0: measures = boundary.measures; break;
            case 1: measures = boundary.measures2; break;
            case 2: measures = boundary.measures3; break;
            default: measures = boundary.measures; break;
        }
        measures.forEach(function (measure) {
            seriesData.push([
                parseInt(measure.time.substr(6)),
                measure.measure
            ])
        })

        seriesSets.push({
            name: boundary.name + " (" + boundary.code + ")",
            data: seriesData,
            //tooltip: {
            //    valueSuffix: " " + quantity.unit
            //},
            showInNavigator: true
        })
    })
    return seriesSets;
}

function intlRound(numero, decimales = 2, usarComa = false) {
    var opciones = {
        maximumFractionDigits: decimales,
        useGrouping: false
    };
    usarComa = usarComa ? "es" : "en";
    let ret = new Intl.NumberFormat(usarComa, opciones).format(numero);
    return parseFloat(ret)
}

Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miércoles',
            'Jueves', 'Viernes', 'Sábado'
        ]
    }
});