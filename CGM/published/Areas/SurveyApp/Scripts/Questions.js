﻿$(function () {
    enabled_TLY();
    $("#Id_ControlGroup").change(function () {
        enabled_TLY();
    });
});

function enabled_TLY() {
    var Id_ControlGroup = $("#Id_ControlGroup").val();
    if (Id_ControlGroup == 3) {
        $("#box_TLY").show();
    }
    else {
        $("#box_TLY").hide();
    }
}